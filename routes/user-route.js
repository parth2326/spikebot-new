const ip = require('ip');

const { check } = require('express-validator');
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const userController = require('./../controller/userController');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

module.exports = function (homeAutomationModule, appContext, shared) {

  let appUtil = homeAutomationModule.appUtil;
  let app = appContext.app;

  app.post('/auth/login', [
    check('user_name').not().isEmpty().withMessage('User Name is Required and Minimum 5 characters Required.'),
    check('user_password').not().isEmpty().withMessage('User Password is Required.'),
    check('phone_id').not().isEmpty().withMessage('Phone Id is Required.'),
    check('phone_type').not().isEmpty().isIn(['IOS', 'android', 'ios', 'alexa', 'google', 'Alexa', 'Google']).withMessage('Phone type is Required and should be ios, android, alexa, or google.'),
    check('fcm_token').not().isEmpty().withMessage('Fcm Token is Required.'),
  ], formValidationMiddleware, function (req, res) {
    userController.login(req.body, function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  app.post('/auth/logout', [
    check('auth_key').not().isEmpty().withMessage('Auth Key is Required.')
  ], formValidationMiddleware, function (req, res) {
    userController.logout(req.body, function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  app.post('/signupdetails', [
    check('user_name').not().isEmpty().isLength({ min: 5 }).withMessage('User Name is Required and Minimum 5 characters Required.'),
    check('user_password').not().isEmpty().isLength({ min: 6 }).withMessage('User Password is Requried and Minimim 6 characters Required.'),
    check('user_email').not().isEmpty().matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).withMessage('Email Id is not valid or Required.'),
    check('user_phone').not().isEmpty().isLength({ min: 10 }).matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/).withMessage('User Phone is Required of Please enter valid phone number.'),
    check('phone_type').not().isEmpty().isIn(['IOS', 'android', 'ios']).withMessage('Phone Type is Required or should be ios or android.'),
    check('phone_id').not().isEmpty().withMessage('Phone Id is Required.'),
    check('first_name').not().isEmpty().withMessage('First Name is Required.'),
    check('last_name').not().isEmpty().withMessage('Last Name is Required.'),
    check('device_push_token').not().isEmpty().withMessage('Device Push Token is Required.')
  ], formValidationMiddleware, function (req, res) {
    userController.signup(req.body, function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  // Change Password
  app.post('/user/change-password', authMiddleware, [
    check('password').not().isEmpty().withMessage('Password is Required.'),
    check('old_password').not().isEmpty().withMessage('Old Password is Required.')
  ], formValidationMiddleware, function (req, res) {
    userController.changeUserPassword(req.body, function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  // Get User Profile Data
  app.get('/getuserProfileInfo', function (req, res) {
    userController.getuserProfileInfo(function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  // Save User Profile Data
  app.post('/saveUserProfileDetails',  authMiddleware,function (req, res) {
    userController.saveUserProfileDetails(req.body, function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });

  app.post('/user/notification-settings', authMiddleware, function (req, res) {
    logger.info('Into getUserNotificationList');
    userController.getUserNotificationList(req.body, function (err, response) {
      if (err) return res.json(err);
      logger.info('Out getUserNotificationList');
      return res.json(response);
    });
  });

  app.post('/user/notification-settings/update', authMiddleware, function (req, res) {
    logger.info('Into saveUserNotificationList');
    userController.saveUserNotificationList(req.body, function (err, response) {
        if (err) return res.json(err);
        logger.info('Out saveUserNotificationList');
        return res.json(response);
    });
  });

};