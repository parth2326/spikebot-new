
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');
const { check } = require('express-validator');
const {
    appContext,
    homeAutomationModule,
} = require('./../util/general-util');

const cameraController = require('./../controller/cameraController');
const app = appContext.app;

// module.exports = function(homeAutomationModule, appContext, shared) {

//     var appUtil = homeAutomationModule.appUtil,
//         cameraController = homeAutomationModule.cameraController,
//         app = appContext.app;

// Add Camera 

app.post('/camera/fixcamera', function (req, res) {
    cameraController.fixCamera(function(err, response) {
        if (err) return res.json(err);
        return res.json(response);
    })
});

app.post('/camera/add', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('camera_name').not().isEmpty().withMessage('Camera Name is Required.'),
    check('camera_ip').not().isEmpty().withMessage('Camera IP is Required.'),
    check('video_path').not().isEmpty().withMessage('Video Path is Required.'),
    check('user_name').not().isEmpty().withMessage('User Name is Required.'),
    check('password').not().isEmpty().withMessage('Password is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In addCamera", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.addCamera(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out addCamera");
        return res.json(response);
    });
});

// Get Edit Camera Info
app.post('/camera/edit', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In editCamera ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    let id = req.body.camera_id;
    cameraController.editCamera(id, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out editCamera");
        return res.json(response);
    });
});

// Update Camera Info
app.post('/camera/update', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.'),
    check('camera_name').not().isEmpty().withMessage('Camera Name is Required.'),
    check('camera_ip').not().isEmpty().withMessage('Camera IP is Required.'),
    check('camera_videopath').not().isEmpty().withMessage('Video Path is Required.'),
    check('user_name').not().isEmpty().withMessage('User Name is Required.'),
    check('password').not().isEmpty().withMessage('Password is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In updateCamera ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.updateCamera(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out updateCamera");
        return res.json(response);
    });
});

// Delete Camera Info
app.post('/camera/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In deleteCamera ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.deleteCamera(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out deleteCamera");
        return res.json(response);
    });
});


// Get Camera Details for View          
app.get('/camera/caminfo/:camera_id', function (req, res) {
    //logger.info("In caminfo ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    let id = req.params.camera_id;
    cameraController.getCameraInfo(id, function (error, result) {
        if (error) {
            return res.json(error);
        } else {
            if (result.code === constants.responseCode.SUCCESS.code) {
                if (result.data && result.data.length > 0) {
                    let cameraViewResult = result.data;
                    let camUrl = cameraViewResult[0].camera_ip + cameraViewResult[0].camera_videopath;
                    //logger.info(camUrl);

                    let authToken = Buffer.alloc(cameraViewResult[0].userName + ':' + cameraViewResult[0].password).toString('base64');
                    res.cookie('camurl', camUrl, {
                        path: '/proxycam/' + id
                    });
                    if (cameraViewResult[0].userName)
                        res.cookie('authtoken', authToken, {
                            path: '/proxycam/' + id
                        });
                    return res.redirect(302, '/proxycam/' + id);
                }
            } else {
                return res.json(result);
            }
        }
    });
});


// Get Camera Details for View          
app.get('/camera/caminfo/:camera_id', authMiddleware, function (req, res) {
    //logger.info("In caminfo ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    let id = req.params.camera_id;
    return res.redirect(302, '/proxycam/' + id);
    /* cameraController.getCameraInfo(id, function (error, result) {
      if (error) {
        return res.json(error);
      } else {
        if (result.code === constants.responseCode.SUCCESS.code) {
          if (result.data && result.data.length > 0) {
            var cameraViewResult = result.data;
            var camUrl = cameraViewResult[0].camera_ip + cameraViewResult[0].camera_videopath;
            //logger.info(camUrl);

            var authToken = Buffer.alloc(cameraViewResult[0].userName + ':' + cameraViewResult[0].password).toString('base64');
            res.cookie('camurl', camUrl, {
              path: '/proxycam/' + id
            });
            if (cameraViewResult[0].userName)
              res.cookie('authtoken', authToken, {
                path: '/proxycam/' + id
              });
            return res.redirect(302, '/proxycam/' + id);
          }
        } else {
          return res.json(result);
        }
      }
    }); */
});

// Get Camera Recording List
// app.post('/camera/getCameraRecording', authMiddleware,[
app.post('/camera/recording', authMiddleware, [
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraRecording ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraRecording(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraRecording");
        return res.json(response);
    });
});

//Get camera listing by Date
// app.post('/camera/getCameraRecordingByDate',authMiddleware,[
app.post('/camera/recording-by-date', authMiddleware, [
    check('camera_details').not().isEmpty().withMessage('Camera Details is Required.'),
    check('camera_start_date').not().isEmpty().withMessage('Start Date is Required.'),
    check('camera_end_date').not().isEmpty().withMessage('End Date is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraRecordingByDate ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraRecordingByDate(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraRecordingByDate");
        return res.json(response);
    });
});


app.post('/camera/git-by-timestamp', authMiddleware, [
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.'),
    check('timestamp').not().isEmpty().withMessage('Timestamp is Required.'),
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraRecordingByDate ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getNotificationGifByTimestamp(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraRecordingByDate");
        return res.json(response);
    });
});

//Get camera listing by Date
// app.post('/camera/getCameraRecordingByTimestamp', authMiddleware, [
app.post('/camera/recording-by-timestamp', authMiddleware, [
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.'),
    check('timestamp').not().isEmpty().withMessage('Timestamp is Required.'),
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraRecordingByDate ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraRecordingByTimestamp(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraRecordingByDate");
        return res.json(response);
    });
});

// app.post('/camera/getCameraVideo', authMiddleware,[
app.post('/camera/video', authMiddleware, [
    check('timestamp').not().isEmpty().withMessage('Timestamp is Required.'),
    check('camera_id').not().isEmpty().withMessage('Camera Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("getCameraVideo ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraVideo(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getAllCameraToken");
        return res.json(response);
    });
});


//Get Camera Token
// app.get('/camera/getCameraToken/:camera_id', authMiddleware, function (req, res) {
app.get('/camera/token/:camera_id', authMiddleware, function (req, res) {
    //logger.info("getCameraToken ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    let data = {
        camera_id: req.params.camera_id
    };
    cameraController.getCameraToken(data, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraToken");
        return res.json(response);
    });
});

//Get All Camera Token
// app.post('/camera/getAllCameraToken', authMiddleware,
app.post('/camera/token/all', authMiddleware, function (req, res) {
    //logger.info("getAllCameraToken ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getAllCameraToken(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getAllCameraToken");
        return res.json(response);
    });
});

//Validate Camera Key to add cameras
// app.post('/camera/validatecamerakey', authMiddleware, 
app.post('/camera/validate-key', authMiddleware, [
    check('key').not().isEmpty().withMessage('Key is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In validatecamerakey ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.validatecamerakey(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out validatecamerakey");
        return res.json(response);
    });
});

//Add Camera Notification Alert
// app.post('/camera/addCameraNotification', authMiddleware, [
app.post('/camera/notification/add', authMiddleware, [
    check('start_time').not().isEmpty().withMessage('Start Time is Required.'),
    check('end_time').not().isEmpty().withMessage('End Time is Required.'),
    check('alert_interval').not().isEmpty().withMessage('Alert Interval is Required.'),
    check('cameraList').not().isEmpty().withMessage('Camera List is Required.'),
], formValidationMiddleware, function (req, res) {
    //logger.info("addCameraNotification ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.addCameraNotification(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

//Update Camera Notification Alert
// app.post('/camera/updateCameraNotification', authMiddleware,[
app.post('/camera/notification/update', authMiddleware, [
    check('camera_notification_id').not().isEmpty().withMessage('Notification Id is Required.'),
    check('start_time').not().isEmpty().withMessage('Start Time is Required.'),
    check('end_time').not().isEmpty().withMessage('End Time is Required.'),
    check('alert_interval').not().isEmpty().withMessage('Alert Interval is Required.'),
    check('cameraList').not().isEmpty().withMessage('Camera List is Required.'),
], formValidationMiddleware, function (req, res) {
    //logger.info("updateCameraNotification ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.updateCameraNotification(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

//Delete Camera Notification Alert
// app.post('/camera/deleteCameraNotification', authMiddleware,[
app.post('/camera/notification/delete', authMiddleware, [
    check('camera_notification_id').not().isEmpty().withMessage('Notification Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("deleteCameraNotification ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.deleteCameraNotification(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Get Camera Notification Alert Status
// app.post('/camera/changeCameraAlertStatus', authMiddleware,[
app.post('/camera/notification/status', authMiddleware, [
    check('camera_notification_id').not().isEmpty().withMessage('Notification Id is Required.'),
    check('is_active').not().isEmpty().withMessage('Notification Status is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In changeCameraAlertStatus ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.changeCameraAlertStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out changeCameraAlertStatus");
        return res.json(response);
    });
});

// Get Camera Notification Alert Status
app.post('/camera/recording-file/update', authMiddleware, function (req, res) {
    // app.post('/camera/updateRecordingFile', authMiddleware, function (req, res) {
    //logger.info("In updateRecordingFile ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.updateRecordingFile(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out updateRecordingFile");
        return res.json(response);
    });
});


// Get Camera Notification Alert List       
// app.get('/getCameraNotificationAlertList', function (req, res) {
//   //logger.info("In getCameraNotificationAlertList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
//   cameraController.getCameraNotificationAlertList(req.body, function (err, response) {
//     if (err) return res.json(err);
//     //logger.info("Out getNotificationInfo");
//     return res.json(response);
//   });
// });

// app.post('/camera/getCameraNotificationAlertList', authMiddleware, [
app.post('/camera/notification/all', authMiddleware, [
    check('user_id').not().isEmpty().withMessage('User Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraNotificationAlertList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraNotificationAlertList(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getNotificationInfo");
        return res.json(response);
    });
});

app.post('/camera/list', authMiddleware, [
    // check('user_id').not().isEmpty().withMessage('User Id is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In getCameraNotificationAlertList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraList(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getNotificationInfo");
        return res.json(response);
    });
});


// Camera Log list
// app.post('/camera/getCameraLogs', authMiddleware, function (req, res) {
// app.post('/camera/logs', authMiddleware, function (req, res) {
//         //logger.info("In getCameraLogs URL: ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
//     cameraController.getCameraLogs(req.body, function (err, response) {
//         if (err) return res.json(err);
//         //logger.info("Out getCameraLogs");
//         return res.json(response);
//     });
// });


//Update Unread Camera Logs
// app.post('/camera/updateUnReadCameraLogs', authMiddleware, function (req, res) {
// app.post('/camera/unread-logs/update', authMiddleware, function (req, res) {
//     //logger.info("In updateUnReadCameraLogs URL: ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
//     cameraController.updateUnReadCameraLogs(req.body, function (err, response) {
//         if (err) return res.json(err);
//         //logger.info("Out updateUnReadCameraLogs");
//         return res.json(response);
//     });
// });

//get Camera Details for Python
// app.get('/camera/getPythonCameraDetails', authMiddleware, function (req, res) {
// app.get('/camera/python-camera-details', authMiddleware, function (req, res) {
//     //   //logger.info("In getPythonCameraDetails URL: ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
//     cameraController.getPythonCameraDetails(req.body, function (err, response) {
//         if (err) return res.json(err);
//         //   //logger.info("Out getPythonCameraDetails");
//         return res.json(response);
//     });
// });

app.post('/camera/list-by-jetson', authMiddleware, function (req, res) {
    //logger.info("In getCameraListByJetson URL: ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    cameraController.getCameraListByJetson(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getCameraListByJetson");
        return res.json(response);
    });
});





/**
 * do
 */
// app.post('/camera/report-false-image', authMiddleware, function (req, res) {
//     //logger.info("In reportFalseImage URL: ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
//     cameraController.reportFalseImage(req.body, function (err, response) {
//         if (err) return res.json(err);
//         //logger.info("Out reportFalseImage");
//         return res.json(response);
//     });
// });


// };