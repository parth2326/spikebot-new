// const deviceRoutes = require('express').Router();
const scheduleController = require('../controller/spikeScheduleController');
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

const { check } = require('express-validator');
const {
	appContext,
} = require('./../util/general-util');


appContext.app.post('/schedule/add',authMiddleware, [
        check('schedule_days').optional().matches('^[0-6](,[0-6])*$').withMessage('Schedule days should be between 0 to 6'),
        check('on_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('On Time should contain atleast 4 characters and needs to be on 24hr format.'),
        check('off_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('Off Time should contain atleast 4 characters and needs to be on 24hr format.'),
        check('schedule_type').not().isEmpty().isIn(['schedule', 'timer']).withMessage(' Schedule Type Should be schedule or timer.'),
        check('schedule_device_type').optional().isIn(['room', 'mood']).withMessage('Schedule Device Type should be room or mood.')
    ], formValidationMiddleware, function (req, res) {
    scheduleController.addSchedule(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/schedule/list',authMiddleware, [
        check('user_id').not().isEmpty().withMessage('User Id is Required.')
    ], formValidationMiddleware,function (req, res) {
    scheduleController.listSchedule(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/schedule/edit',authMiddleware, [
        check('schedule_id').not().isEmpty().withMessage('Schedule Id is Required.'),
        check('is_active').optional().isIn(['y', 'n']).withMessage('Is Active should be y or n.'),
        check('schedule_days').optional().matches('^[0-6](,[0-6])*$').withMessage('Schedule days should be between 0 to 6'),
        check('on_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('On Time should contain atleast 4 characters and needs to be on 24hr format.'),
        check('off_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('Off Time should contain atleast 4 characters and needs to be on 24hr format.'),
    ], formValidationMiddleware, function (req, res) {
    scheduleController.editSchedule(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


appContext.app.post('/schedule/status',authMiddleware, [
    check('schedule_id').not().isEmpty().withMessage('Schedule Id is Required.'),
    check('is_active').optional().isIn(['y', 'n']).withMessage('Is Active should be y or n.'),
  ], formValidationMiddleware, function (req, res) {
scheduleController.changeScheduleStatus(req.body, function (err, response) {
    if (err) return res.json(err);
    return res.json(response);
});
});


appContext.app.post('/schedule/delete', authMiddleware, [
        check('schedule_id').not().isEmpty().withMessage('Schedule Id is Required.')
    ], formValidationMiddleware,function (req, res) {
    scheduleController.deleteSchedule(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


// module.exports = deviceRoutes;