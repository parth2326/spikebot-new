const { appContext } = require('./../util/general-util');
const iftttController = require('./../controller/iftttController');
const authMiddleware = require('./../util/middlewares/auth-middleware');

appContext.app.get('/ifttt/getAllActiveDevices', function (req, res) {
    iftttController.getAllActiveDevices(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});