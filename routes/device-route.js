// const deviceRoutes = require('express').Router();
const deviceController = require('../controller/deviceController');
const deviceStatusController = require('../controller/deviceStatusController');
const { authMiddleware } = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');
const {
    appContext,
} = require('./../util/general-util');
const { check } = require('express-validator');

// Configure device by device type (Zigbee devices configuration protocol)
appContext.app.get('/device/configure/:deviceType', [
    check('deviceType').not().isEmpty().withMessage('Device Type is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.configureRequest(req.params.deviceType, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Manually Configure Device
appContext.app.post('/device/manual-configure', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('module_type').not().isEmpty().withMessage('Module Type is Required.'),
    check('moduleId').not().isEmpty().withMessage('Module Id is Required.'),
    check('deviceId').not().isEmpty().withMessage('Device Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.configureRequestCallback(req.body);
    return res.json({
        code: 200,
        message: "done"
    });
});


// Add New Device API / Add From unassigned as well
appContext.app.post('/device/add', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    // check('device_name').not().isEmpty().withMessage('Device name is required.'),
    check('module_type').not().isEmpty().withMessage('Module Type is required.'),
    check('user_id').not().isEmpty().withMessage('User Id is required'),
    check('off_time').optional().isLength({ min: 4 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('Off Time should contain atleast 4 characters and needs to be on 24hr format.'),
    check('on_time').optional().isLength({ min: 4 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('Off Time should contain atleast 4 characters and needs to be on 24hr format.'),
    check('range').optional().isIn(["1", "2", "3", "4", "5", "6", "7", "8", "9"]).withMessage('Range must be between 1 to 9'),
], formValidationMiddleware, function (req, res) {
    deviceController.addDevice(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Add Custome Button Remote
appContext.app.post('/device/get-remote-command-name', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('device_id').not().isEmpty().withMessage('Device Id is Required!!!'),
    // check('remote_type').not().isIn(['tv', 'dth', 'tv_dth']).withMessage('Remote type is required and must be tv, dth, tv_dth')
], formValidationMiddleware, function (req, res) {
    deviceController.getRemoteCommandName(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
})

appContext.app.post('/device/add-custome-remote-button', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('device_id').not().isEmpty().withMessage('Device Id is Required!!!'),
    check('remote_type').not().isEmpty().withMessage('Remote type is required and must be tv, dth, tv_dth'),
    check('button_no').not().isEmpty().withMessage('Botton No is Required!!!'),
    check('button_name').not().isEmpty().withMessage('Botton Name is Required!!!'),
    check('button_code').not().isEmpty().withMessage('Botton Code is Required!!!'),
    check('button_ir_code').not().isEmpty().withMessage('Botton Ir Code is Required!!!')
], formValidationMiddleware, function (req, res) {
    deviceController.addCustomRemoteButton(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
})

appContext.app.post('/device/delete-custome-button', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('device_id').not().isEmpty().withMessage('Device Id is Required!!!'),
    check('button_no').not().isEmpty().withMessage('Button No is Required!!!')
], formValidationMiddleware, function (req, res) {
    deviceController.deleteCustomeButton(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    })
})

// Edit Device API
appContext.app.post('/device/edit', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('device_id').not().isEmpty().withMessage('Device Is is required.'),
    check('device_name').optional().isLength({ min: 3 }).withMessage('Minimum 3 characters required for Device Name.'),
    check('onetime_code').optional().isLength({ min: 4, max: 4 }).withMessage('Length must be 4 charcters for Onetime code.'),
    check('pass_code').optional().isLength({ min: 4, max: 4 }).withMessage('Length must be 4 charcters long for Pass code.'),
    check('enable_lock_unlock_from_app').optional().isIn(["1", "0", 1, 0]).withMessage('Enable Lock Unlock From App should be 0 or 1.'),
    check('on_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('On Time should contain atleast 4 characters and needs to be on 24hr format.'),
    check('off_time').optional().isLength({ min: 4 }).matches('([01]?[0-9]|2[0-3]):[0-5][0-9]').withMessage('Off Time should contain atleast 4 characters and needs to be on 24hr format.'),
    check('range').optional().isIn(["1", "2", "3", "4", "5", "6", "7", "8", "9"]).withMessage('Range must be between 1 to 9'),
], formValidationMiddleware, function (req, res) {
    deviceController.editDevice(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Delete Device API
appContext.app.post('/device/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('device_id').not().isEmpty().withMessage('Device Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.deleteDevice(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


appContext.app.post('/device/list-panel-device-name', authMiddleware, function (req, res) {
    deviceController.listPanelDeviceName(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response)
    })
})


// Getting device list api / Home Screen API
appContext.app.post('/device/list', authMiddleware, [
    check('room_type').optional().isIn(['room', 'mood']).withMessage('Room Type should be room or mood.'),
    check('only_rooms_of_beacon_scanner').optional().isIn([true, false, '1', '0', 1, 0]).withMessage('Only Rooms Of Beacon Scanner should be true or false.'),
    check('only_on_off_device').optional().isIn([true, false, '1', '0', 1, 0]).withMessage('Only On Off Device should be 1 or 0.')
], formValidationMiddleware, function (req, res) {
    deviceController.listDevice(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting device list by status API
appContext.app.post('/device/list/:status', authMiddleware, [
    check('status').optional().isIn(['y', 'n']).withMessage('Status should be y on n.')
], formValidationMiddleware, function (req, res) {
    deviceController.listByStatus(req.params, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting Icon List
appContext.app.post('/device/icon/list', authMiddleware, function (req, res) {
    deviceController.listIcons(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// change status of device
appContext.app.post('/device/status', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.'),
    check('device_status').not().isEmpty().withMessage('Device Status is Required.'),
    check('log').optional().isIn([true, false]).withMessage('Log should be on or off.')
], formValidationMiddleware, function (req, res) {
    deviceStatusController.changeDeviceStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting all unassigned devices
appContext.app.post('/device/unassigned', authMiddleware, function (req, res) {
    deviceController.unAssignedDevices(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting all info of specific device
appContext.app.post('/device/info', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.'),
], formValidationMiddleware, function (req, res) {
    deviceController.getDeviceInfo(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Find device by device type api
appContext.app.post('/device/find', authMiddleware, function (req, res) {
    deviceController.findDevices(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting heavy load realtime data in mobile via socket
appContext.app.post('/device/heavy-load/ping', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.getHeavyLoadValues(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting Heavy load usage (graph in mobile)
appContext.app.post('/device/heavy-load/usage', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.'),
    check('filter_type').optional().isIn(['month', 'year']).withMessage(' should be month or year'),
], formValidationMiddleware, function (req, res) {
    deviceController.getHeavyLoadUsage(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting Heavy load usage (graph in mobile)
appContext.app.post('/device/temprature/usage', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.'),
    check('filter_type').optional().isIn(['day', 'month', 'year']).withMessage('Filter Type should be day, month or year'),
], formValidationMiddleware, function (req, res) {
    deviceController.getTempratureUsage(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Delete Module / Delete From Unassigned as well
appContext.app.post('/device/module/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('module_id').not().isEmpty().not().withMessage('Module Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.deleteModule(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting custom panel details
appContext.app.post('/device/custom-panel-details', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
], formValidationMiddleware, function (req, res) {
    deviceController.getCustomPanelDetails(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Getting status of yale lock
appContext.app.post('/device/yale-lock/status', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.getYaleLockStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// setting pincode of yale lock
appContext.app.post('/device/yale-lock/pincode', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.'),
    check('code_type').not().isEmpty().withMessage('Code Type is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.setYaleLockPincode(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Deleting pincode of yale lock
appContext.app.post('/device/yale-lock/pincode/delete', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is Required.')
], formValidationMiddleware, function (req, res) {
    deviceController.deleteYaleLockPincode(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


// Getting list of beacons detected in specific scanner
appContext.app.post('/device/beacon-scanner/scan', authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is required.')
], formValidationMiddleware, function (req, res) {
    deviceController.getBeaconsScannedByScanner(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


// Getting location | which beacon is in which room
appContext.app.post('/device/beacon/locations', authMiddleware, function (req, res) {
    deviceController.getBeaconLocations(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

// Storing audio file
// appContext.app.post('/device/beacon/storeAudioFile', function(req, res) {
//     deviceController.sendAudioToWalkiTalki(req.body, function(err, response) {
//         if (err) return res.json(err);
//         return res.json(response);
//     });
// });

