// const deviceRoutes = require('express').Router();
const alertController = require('../controller/alertController');
const logController = require('../controller/logController');
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

const { check } = require('express-validator');
const {
	appContext,
} = require('./../util/general-util');


appContext.app.post('/logs/find',authMiddleware, function (req, res) {
    logController.find(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/logs/find2',authMiddleware, function (req, res) {
    logController.find2(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/logs/categories',authMiddleware, function (req, res) {
    logController.listLogCategories(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/logs/seen',authMiddleware, [
        check('user_id').not().isEmpty().withMessage('User Id is Required.')
    ], formValidationMiddleware,function (req, res) {
    logController.markSeen(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


// module.exports = deviceRoutes;