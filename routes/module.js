module.exports = function(appContext, shared) {

    let cloudURL = process.env.CLOUD_URL + ':' + process.env.CLOUD_PORT;
    let vpnURL = process.env.VPN_URL;

    let homeAutomationModule = {
        moduleName: "SpikeBot",
        cloudURL: cloudURL,
        vpnURL: vpnURL
    };

    
    let migrations = require('./../models/migration');

    let viewerDetails = appContext.viewerDetails,
        homeControllerDeviceId = viewerDetails.deviceID,
        homeControllerIP = viewerDetails.deviceIP,
        // clientSocketConn = shared.util.clientSocket,
        constants = appContext.constants;
    homeAutomationModule.constants = constants;

    //Socket communication
    // var clientSocketObj;
   
    // Util File    
    let appUtil = require("../util/app-util.js");
    appUtil.setParams(homeAutomationModule, appContext, shared);
    homeAutomationModule.appUtil = appUtil;

    let deviceUtil = require("../util/device-util.js");
    deviceUtil.setParams(homeAutomationModule, appContext, shared);
    homeAutomationModule.deviceUtil = deviceUtil;

    let mailUtil = require("../util/mail-util.js");
    homeAutomationModule.mailUtil = mailUtil;

    let systemUtil = require("../util/system-util.js");
    homeAutomationModule.systemUtil = systemUtil;

    let unixUtil = require("../util/unix-util.js");
    homeAutomationModule.unixUtil = unixUtil;

    // Rest Files
    let rest = require("../rest/rest.js");
    rest.setParams(homeAutomationModule, appContext, shared);
    homeAutomationModule.rest = rest;

    // Initialization Of General Util Parth Patel
    let generalUtil = require('./../util/general-util');
    generalUtil.initiate(homeAutomationModule, appContext, shared, null);

    require('./../util/schedule-manager');
    const syncManager = require('./../util/sync-manager');

    setTimeout(function() {
        logger.info('SYNC MANAGER - [SYNC CALLED]');
        syncManager.updateAllTables();
    }, 10000);


    // require("../util/event-manager");
    require("../util/event-listener");
    // Controller Files
    let cameraController = require("../controller/cameraController.js");
    homeAutomationModule.cameraController = cameraController;

    let systemController = require("../controller/systemController.js");
    homeAutomationModule.systemController = systemController;

    let userController = require("../controller/userController.js")
    homeAutomationModule.userController = userController;

    let IRController = require("../controller/IRController.js");
    homeAutomationModule.IRController = IRController;

    let adminController = require("../controller/adminController.js");
    homeAutomationModule.adminController = adminController;

    // Router Files
    require("./device-route");
    require("./room-route");
    require("./alert-route");
    require("./spike-schedule-route");
    // require("./spike-rule-route");
    require("./log-route");
    require("./jetson-route");
    require("./sync-route");
    require("./../util/cloud-socket");
    require("./camera-route.js");
    require("./ifttt-route");
    
    let systemRoute = require("./system-route.js")(homeAutomationModule, appContext, shared);
    let userRoute = require("./user-route.js")(homeAutomationModule, appContext, shared);
    let adminRoute = require("./admin-route");
    let IRRoute = require("./IR-route.js")(homeAutomationModule, appContext, shared);
    let deviceStatusProviderServerSocket = require("../socket/deviceStatusProviderServerSocket.js")(homeAutomationModule, appContext, shared);

    // Scanner Files
    let switchModuleStatusChecker = require("../scanner/switchModuleStatusChecker.js")(homeAutomationModule, appContext, shared);
    let cameraScanner = require("../scanner/cameraScanner.js");
    let backupScanner = require("../scanner/backupScanner.js")(homeAutomationModule, appContext, shared);
    let recentNotificationCleaner = require('../scanner/notificationCleaner.js');
    let mqttScanner = require('../scanner/mqttScanner');

    // General Listener
    require("../listener/generalEventListener");

    const beaconListener = require('./../listener/beaconListener');
    beaconListener.init();

};