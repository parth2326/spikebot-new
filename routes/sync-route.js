// const deviceRoutes = require('express').Router();
const syncController = require('../controller/syncController');
const {authMiddleware} = require('../util/middlewares/auth-middleware');

const { check } = require('express-validator');
const {
	appContext,
} = require('./../util/general-util');


/**
 * Sync User From Cloud To Local
 */
appContext.app.post('/sync/user', function (req, res) {
    syncController.syncUser(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

/**
 * Sync Home Controller from Cloud To Local
 */
appContext.app.post('/sync/home-controller', function (req, res) {
    syncController.syncHomeController(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/sync/table', function (req, res) {
    syncController.syncTable(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});
