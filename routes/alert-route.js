// const deviceRoutes = require('express').Router();
const alertController = require('../controller/alertController');
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

const { check } = require('express-validator');
const {
	appContext,
} = require('./../util/general-util');

appContext.app.post('/alert/add', authMiddleware,[
    check('device_id').not().isEmpty().withMessage('Device Id is required.'),
    check('days').optional().matches('^[0-6](,[0-6])*$').withMessage('Schedule days should be between 0 to 6'),
    check('start_time').optional().isLength({ min: 5, max:5 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('Start Time should contain atleast 5 characters and needs to be on 24hr format.'),
    check('end_time').optional().isLength({ min: 5, max:5 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('End Time should contain atleast 5 characters and needs to be on 24hr format.'),
    check('alert_type').optional().isIn(['temperature', 'humidity', 'door_open_close', 'water_detected','door_lock_unlock']).withMessage('Alert Type should be temprature, humidity, door_open_close, water_detected, door_lock_unlock.')
], formValidationMiddleware, function (req, res) {
    alertController.addAlert(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out addAlert");
        return res.json(response);
    });
});

appContext.app.post('/alert/list',authMiddleware, [
    check('device_id').not().isEmpty().withMessage('Device Id is required.'),
    check('alert_type').optional().isIn(['temperature', 'humidity', 'door_open_close', 'water_detected','door_lock_unlock']).withMessage('Alert Type should be temprature, humidity, door_open_close, door_lock_unlock.')
], formValidationMiddleware, function (req, res) {
    alertController.listAlert(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out listAlert");
        return res.json(response);
    });
});

appContext.app.post('/alert/edit',authMiddleware, [
    check('alert_id').not().isEmpty().withMessage('Alert Id is required.'),
    check('days').optional().matches('^[0-6](,[0-6])*$').withMessage('Schedule days should be between 0 to 6'),
    check('start_time').optional().isLength({ min: 4, max:5 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('Start Time should contain atleast 5 characters and needs to be on 24hr format.'),
    check('end_time').optional().isLength({ min: 4, max:5 }).matches(/([01]?[0-9]|2[0-3]):[0-5][0-9]/).withMessage('End Time should contain atleast 5 characters and needs to be on 24hr format.'),
    check('is_active').optional().isIn(['y', 'n']).withMessage('Is Active should be y or n.')
], formValidationMiddleware, function (req, res) {
    alertController.editAlert(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out editAlert");
        return res.json(response);
    });
});

appContext.app.post('/alert/delete',authMiddleware,[
    // check('alert_id').not().isEmpty().withMessage('Alert Id is required.')
], formValidationMiddleware,function (req, res) {
    alertController.deleteAlert(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out deleteAlert");
        return res.json(response);
    });
});



// module.exports = deviceRoutes;