// const deviceRoutes = require('express').Router();
const roomController = require('../controller/roomController');

const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');
const {
    appContext,
} = require('./../util/general-util');

const { check } = require('express-validator');

appContext.app.post('/rooms/list', authMiddleware, function (req, res) {
    roomController.listRoom(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/rooms/add',authMiddleware,[
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('room_name').not().isEmpty().withMessage('Room Name is required.')
], formValidationMiddleware, function (req, res) {
    roomController.addRoom(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out addRoom");
        return res.json(response);
    });
});

appContext.app.post('/rooms/get',authMiddleware,[
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.getRoom(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoom");
        return res.json(response);
    });
})

appContext.app.post('/rooms/get-room-status',authMiddleware,[
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.getRoomStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoom");
        return res.json(response);
    });
})

appContext.app.post('/rooms/get-total-unread-count',authMiddleware,[
    check('user_id').not().isEmpty().withMessage('User Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.getTotalUnreadCount(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoom");
        return res.json(response);
    });
})

appContext.app.post('/rooms/get-all-room-details',authMiddleware,[
    check('user_id').not().isEmpty().withMessage('User Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.getAllRoomDetails(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoom");
        return res.json(response);
    });
})

appContext.app.post('/rooms/get-room-details',authMiddleware,[
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.getRoomDetails(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoomDetails");
        return res.json(response);
    });
})

appContext.app.post('/rooms/edit',authMiddleware,[
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware, function(req, res){
    roomController.editRoom(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out editRoom");
        return res.json(response);
    });
})

appContext.app.post('/rooms/delete',authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware,function(req, res){
    roomController.deleteRoom(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out deleteRoom");
        return res.json(response);
    });
})

appContext.app.post('/room/status',authMiddleware, [
    check('room_id').not().isEmpty().withMessage('Room Id is required.'),
    check('room_status').optional().isIn([0, 1]).withMessage('Room Status should be 0 or 1'),
], formValidationMiddleware,function(req, res){
    roomController.changeRoomStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out roomStatus");
        return res.json(response);
    });
})

appContext.app.post('/panel/status',authMiddleware, [
    check('panel_id').not().isEmpty().withMessage('Panel Id is required.'),
    check('panel_status').optional().isIn([0, 1]).withMessage('Panel Status should be 0 ro 1.')
], formValidationMiddleware, function(req, res){
    roomController.changePanelStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out panelStatus");
        return res.json(response);
    });
});

appContext.app.post('/panel/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('panel_id').not().isEmpty().withMessage('Panel Id is required.')
], formValidationMiddleware,function(req, res){
    roomController.deletePanel(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out deletePanel");
        return res.json(response);
    });
});

appContext.app.post('/panel/update/devices', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('panel_id').not().isEmpty().withMessage('Panel Id is required.'),
    check('devices').not().isEmpty().withMessage('Devices is required.')
], formValidationMiddleware,function(req, res){
    roomController.updatePanelDevices(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out Panel Update Devices");
        return res.json(response);
    });
});

appContext.app.post('/panel/add-existing', authMiddleware,[
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('room_id').not().isEmpty().withMessage('Room Id is required.'),
    check('devices').not().isEmpty().withMessage('Devices is required.'),
    check('panel_name').not().isEmpty().withMessage('Panel Name is Required.')
], formValidationMiddleware,function(req, res){
    roomController.addExistingPanel(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out Add Existing Panel");
        return res.json(response);
    });
});


appContext.app.post('/mood/status',authMiddleware, [
    check('mood_id').not().isEmpty().withMessage('Mood Id is Required.'),
    check('mood_status').optional().isIn([0, 1,'0','1']).withMessage('Mood Status should be 0 or 1')
], formValidationMiddleware, function (req, res) {
    roomController.changeMoodStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out change Mood Status");
        return res.json(response);
    });
});

appContext.app.post('/mood/get/status',authMiddleware, [
    check('mood_id').not().isEmpty().withMessage('Mood Id is Required.')
], formValidationMiddleware, function (req, res) {
    roomController.getMoodStatus(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out change Mood Status");
        return res.json(response);
    });
});

appContext.app.post('/mood/list',authMiddleware, [
    check('user_id').not().isEmpty().withMessage('User Id is Required.')
    ], formValidationMiddleware,function (req, res) {
    roomController.listMood(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out listMood");
        return res.json(response);
    });
});

appContext.app.post('/mood/list/alexa',authMiddleware, [
    check('user_id').not().isEmpty().withMessage('User Id is Required.')
    ], formValidationMiddleware,function (req, res) {
    roomController.listMoodAlexa(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out listMood");
        return res.json(response);
    });
});


appContext.app.post('/mood/names/list',authMiddleware, function (req, res) {
    roomController.listMoodNames(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out mood name list");
        return res.json(response);
    });
});

appContext.app.post('/mood/add', authMiddleware, [
        check('mood_name_id').not().isEmpty().withMessage('Mood Name id is Required.'),
        check('panel_device_ids').not().isEmpty().withMessage('Panel Device Ids Required.')
    ], authMiddleware, function (req, res) {
    roomController.addMood(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out add Mood");
        return res.json(response);
    });
});

appContext.app.post('/mood/edit', authMiddleware, [
    // check('mood_id').not().isEmpty().withMessage('Mood Id is Required.')
    ], formValidationMiddleware, function (req, res) {
    roomController.editMood(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out edit Mood");
        return res.json(response);
    });
});


appContext.app.post('/mood/delete', authMiddleware, [
        check('mood_id').not().isEmpty().withMessage('Mood Id is Required.')
    ], formValidationMiddleware, function (req, res) {
    roomController.deleteMood(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out deleteMood");
        return res.json(response);
    });
});


appContext.app.post('/mood/smart-remote',authMiddleware, function (req, res) {
    roomController.updateMoodSmartRemoteNumber(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out mood/smart-remote");
        return res.json(response);
    });
});


appContext.app.post('/room/user/assign', authMiddleware,[
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('assigned_user_id').not().isEmpty().withMessage('User Id is required.'),
    check('room_id').not().isEmpty().withMessage('Room Id is required.')
], formValidationMiddleware,function (req, res) {
    roomController.assignUsers(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out assign user");
        return res.json(response);
    });
});

