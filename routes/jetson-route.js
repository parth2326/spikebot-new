// const deviceRoutes = require('express').Router();
const jetsonController = require('../controller/jetsonController');
const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

const { check } = require('express-validator');
const {
	appContext,
} = require('./../util/general-util');

appContext.app.post('/jetson/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('jetson_id').not().isEmpty().withMessage('Jetson Id is required.')
], formValidationMiddleware, function (req, res) {
    jetsonController.delete(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/jetson/update',authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('jetson_id').not().isEmpty().withMessage('Jetson Id is required.')
], formValidationMiddleware, function (req, res){
    jetsonController.update(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/jetson/add', authMiddleware,[
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('jetson_ip').not().isEmpty().withMessage('Jetson IP is required.'),
    check('jetson_name').not().isEmpty().withMessage('Jetson Name is required.')
], formValidationMiddleware, function (req, res) {
    jetsonController.add(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});

appContext.app.post('/jetson/list', authMiddleware,function (req, res) {
    jetsonController.list(req.body, function (err, response) {
        if (err) return res.json(err);
        return res.json(response);
    });
});
