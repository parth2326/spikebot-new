module.exports = function (homeAutomationModule, appContext, shared) {

    let IRController = homeAutomationModule.IRController,
        app = appContext.app;

    // Add New IR Blaster Module
    // app.post('/AddIRBlaster', function (req, res) {
    //     //logger.info("In AddIRBlaster ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.AddIRBlaster(req.body, function (err, result) {
    //         if (err) return res.json(err);
    //         //logger.info("Out AddIRBlaster");
    //         return res.json(response);
    //     });
    // });

    // Update IR Blaster Details
    // app.post('/UpdateIRBlaster', function (req, res) {
    //     //logger.info("In UpdateIRBlaster ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.UpdateIRBlaster(req.body, function (err, result) {
    //         if (err) return res.json(err);
    //         //logger.info("Out UpdateIRBlaster");
    //         return res.json(response);
    //     });
    // });

    // Delete IR Blaster Module
    // app.post('/deleteIRBlaster', function (req, res) {
    //     //logger.info("In deleteIRBlaster ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.deleteIRBlaster(req.body, function (err, result) {
    //         if (err) return res.json(err);
    //         //logger.info("Out deleteIRBlaster");
    //         return res.json(response);
    //     });
    // });

    //Get IR Blaster Devices List with all remotes
    // app.get('/getIRBlasterList', function (req, res) {
    //     //logger.info("In getIRBlasterList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.getIRBlasterList(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         return res.json(response);
    //     });
    // });

    //Send On off command
    app.post('/sendOnOffCommand', function (req, res) {
        //logger.info("In sendOnOffCommand ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
        IRController.sendOnOffCommand(req.body, function (err, response) {
            if (err) return res.json(err);
            //logger.info("Out SendOnOff Command");
            return res.json(response);
        });
    });

    //Send Remote Command
    app.post('/sendRemoteCommand', function (req, res) {
        IRController.sendRemoteCommand(req.body, function (err, response) {
            if (err) return res.json(err);
            // //logger.info("Send Remote Command");
            return res.json(response);
        });
    });

    //Add New Remote
    // app.post('/AddRemote', function (req, res) {
    //     //logger.info("In AddRemote ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.AddRemote(req.body, function (err, result) {
    //         if (err) return res.json(err);
    //         //logger.info("Out AddRemote");
    //         return res.json(response);
    //     });
    // });

    //Update Remote Daetails
    // app.post('/updateRemoteDetails', function (req, res) {
    //     //logger.info("updateRemoteDetails ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.updateRemoteDetails(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         return res.json(response);
    //     });
    // });

    //Delete Remote
    // app.post('/deleteRemote', function (req, res) {
    //     //logger.info("deleteRemote ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.deleteRemote(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out deleteRemote");
    //         return res.json(response);
    //     });
    // });

    //Individual Remote Info 
    // app.get('/getRemoteInfo/:remote_id', function (req, res) {
    //     //logger.info("getRemoteInfo ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     var data = {
    //         remote_id: req.params.remote_id
    //     };
    //     IRController.getRemoteInfo(data, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out getRemoteInfo");
    //         return res.json(response);
    //     });
    // });

    // Get IR Blaster Devices List with Room list and all device type list
    app.get('/getIRDeviceTypeList', function (req, res) {
        //logger.info("In getIRDeviceTypeList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
        IRController.getIRDeviceTypeList(req.body, function (err, response) {
            if (err) return res.json(err);
            return res.json(response);
        });
    });

    //for listing all the brands of device type
    app.get('/getIRDeviceTypeBrands/:device_id', function (req, res) {
        //logger.info("In getIRDeviceTypeBrands ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
        let data = {
            device_id: req.params.device_id
        };
        IRController.getIRDeviceTypeBrands(data, function (err, response) {
            if (err) return res.json(err);
            //logger.info("Out getIRDeviceTypeBrands");
            return res.json(response);
        });
    });

    //for listing all the remotes of the device brands
    app.get('/getDeviceBrandRemoteList/:device_brand_id', function (req, res) {
        //logger.info("In getDeviceBrandRemoteList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);

        //logger.info("req.query.with_ir_code", req.query.with_ir_code);
        let withIRCodes = req.query.with_ir_code == "no" ? false : true;

        let data = {
            device_brand_id: req.params.device_brand_id,
            with_ir_code: withIRCodes
        };

        IRController.getDeviceBrandRemoteList(data, function (err, response) {
            if (err) return res.json(err);
            //logger.info("Out device_brand_id");
            return res.json(response);
        });
    });

    //for getting remote ir_code json format
    app.get('/getRemoteIRJson/:remote_id', function (req, res) {
        //logger.info("In getRemoteIRJson ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
        let data = {
            remote_id: req.params.remote_id
        };
        IRController.getRemoteIRJson(data, function (err, response) {
            if (err) return res.json(err);
            //logger.info("Out getRemoteIRJson");
            return res.json(response);
        });
    });

    //Add Remote to Master DB in Cloud
    // app.post('/AddRemoteToCloud', function (req, res) {
    //     //logger.info("In AddRemoteToCloud ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.AddRemoteToCloud(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out AddRemoteToCloud");
    //         return res.json(response);
    //     });
    // });

    //Update Exisitng Remote in Master DB
    // app.post('/UpdateExistingRemote', function (req, res) {
    //     //logger.info("In UpdateExistingRemote ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.UpdateExistingRemote(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out UpdateExistingRemote", response);
    //         return res.json(response);
    //     });
    // });

    //Delete Master Remote from master DB
    // app.post('/deleteMasterRemote', function (req, res) {
    //     //logger.info("deleteMasterRemote ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.deleteMasterRemote(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out deleteMasterRemote");
    //         return res.json(response);
    //     });
    // });

    //for ir receiver app
    // app.get("/irreceiver", function (req, res) {
    //     //logger.info(' ir receiver web page');
    //     res.sendFile("/home/pi/node/homeController/static/iot/index.html");
    // });


    // app.get('/ir', function (req, res) {
    //     //logger.info('Forwaring Remote Page');
    //     res.render('ir.ejs');
    // });

    // Configure IR Sensor Request 
    // app.get('/configureIRBlasterRequest', function (req, res) {
    //     IRController.configureIRBlasterRequest(function (err, result) {
    //         if (err) return res.json(err);
    //         //logger.info("Out configureIRBlasterRequest");
    //         return res.json(response);
    //     });
    // });

    //  Get IR Blaster Devices List with Room list and all device type list
    // app.get('/getIRDeviceTypeList', function (req, res) {
    //     //logger.info("In getIRDeviceTypeList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.getIRDeviceTypeList(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         return res.json(response);
    //     });
    // });


    // //for listing all the brands
    // app.get('/getIRDeviceDetails/:deviceId', function (req, res) {
    //     //logger.info("In getIRDeviceDetails BrandList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     var data = {
    //         deviceId: req.params.deviceId
    //     };
    //     IRController.getIRDeviceDetails(data, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out BrandList");
    //         return res.json(response);
    //     });
    // });

    // //for listing all the on off list
    // app.get('/getIRDeviceDetails/:deviceId/:brandId', function (req, res) {
    //     //logger.info("In getIRDeviceDetails OnOffList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     var data = {
    //         deviceId: req.params.deviceId,
    //         brandId: req.params.brandId
    //     };
    //     IRController.getIRDeviceDetails(data, function (err, response) {
    //         if (err) return res.json(err);
    //         //logger.info("Out OnOffList");
    //         return res.json(response);
    //     });
    // });

    //  Get RoomList
    // app.get('/getRoomList', function (req, res) {
    //     //logger.info("In getRoomList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    //     IRController.getRoomList(req.body, function (err, response) {
    //         if (err) return res.json(err);
    //         return res.json(response);
    //     });
    // });

};