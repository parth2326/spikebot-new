
const dbbackup = require('../util/db-backup');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

module.exports = function (homeAutomationModule, appContext, shared) {

  let appUtil = homeAutomationModule.appUtil;
  let systemController = homeAutomationModule.systemController;
  let app = appContext.app;

app.post('/system/fixuser', function(req, res) {
  systemController.fixUser(function(err, response) {
    if (err) return res.json(err);
    return res.json(response)
  })  
})

  // Delete previous records
  app.get('/system/delete-previous-records', function(req, res) {
    systemController.deletePreviousRecords(function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });


  // Get Network Address
  app.get('/getDHCPinfo', function (req, res) {
    systemController.getDHCPInfo({
      data: req.body,
      cb: function (error, response) {
        if (error) {
          return res.json(error);
        } else {
          return res.json(response);
        }
      }
    });
  });

  app.get('/backup-db', function(req,res){
    const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;
    dbbackup.uploadDbBackup(home_controller_device_id);
    return res.json({
      message:"ok"
    });
  });

  app.get('/system/update',async (req,res) => {
      await systemController.updateCode(req.body);
      return res.json({
        message:"updating"
      });
  });

  // Get Network Details
  app.get('/getNetworkInfo', function (req, res) {
    systemController.getNetworkInfo({
      data: req.body,
      cb: function (error, response) {
        if (error) {
          return res.json(error);
        } else {
          return res.json(response);
        }
      }
    });
  });


  // Save Static IP Address
  // app.post('/savestaticIP', function (req, res) {
  //   //logger.info("savestaticIP ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
  //   systemController.savestaticIP(req.body, function (error, response) {
  //     if (error) {        
  //       return res.json(error);
  //     } else {        
  //       return res.json(response);
  //     }
  //   });
  // });

  // Get Wifi List
  app.get('/getWifiList', function (req, res) {
    systemController.getWifiList({
      data: req.body,
      cb: function (error, response) {
        if (error) {
          return res.json(error);
        } else {
          return res.json(response);
        }
      }
    });
  });



  // Connect and Save Wifi Details
  app.post('/saveWifiDetails', function (req, res) {
    //logger.info("saveWifiDetails ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    systemController.saveWifiDetails({
      data: req.body,
      cb: function (error, response) {
        if (error) {
          return res.json(error);
        } else {
          return res.json(response);
        }
      }
    });
  });

  app.get('/system/mac-address', function (req, res) {
    systemController.getLocalMacAddress(function (err, response) {
      if (err) return res.json(err);
      return res.json(response);
    });
  });  
};