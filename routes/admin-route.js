const {authMiddleware} = require('../util/middlewares/auth-middleware');
const formValidationMiddleware = require('../util/middlewares/form-validation-middleware');

const { check } = require('express-validator');
const {
    appContext,
    homeAutomationModule,
} = require('./../util/general-util');

const adminController = require('./../controller/adminController');
const app = appContext.app;


// app.get('/admin', function (req, res) {
//     //logger.info('Open Login Page First!');
//     res.render('adminLogin');
// });

// app.get('/adminLogin', function (req, res) {
//     res.render('adminLogin');
// });

// app.get('/home', function (req, res) {
//     res.render('home');
// });

// app.post('/adminlogin', function (req, res) {
//     var user_name = req.body.user_name,
//         user_password = req.body.user_password;

//     if (user_name != 'vatsal' && user_password != 'Hello$123') {
//         res.render('adminLogin');
//     } else {
//         res.render('home');
//     }
// });

// Get Devices list
// app.get('/getRoomCameraList', function (req, res) {
app.get('/admin/room-camera-list', function (req, res) {
    //logger.info("In getRoomCameraList ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    adminController.getRoomCameraList(function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getRoomCameraList");
        return res.json(response);
    });
});

// Add Child User
// app.post('/AddChildUser',authMiddleware,[
app.post('/admin/child-user/add', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('user_name').not().isEmpty().withMessage('User Name is Required.'),
    check('user_password').not().isEmpty().withMessage('User Password is Required.'),
    check('display_name').not().isEmpty().withMessage('Display Name is Required.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In AddChildUser ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    adminController.AddChildUser(req.body, function (err, result) {
        if (err) return res.json(err);
        //logger.info("Out AddChildUser");
        return res.json(result);
    });
});


// Delete Child User
// app.post('/DeleteChildUser',authMiddleware,[
app.post('/admin/child-user/delete', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In DeleteChildUser ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    adminController.DeleteChildUser(req.body, function (err, result) {
        if (err) return res.json(err);
        return res.json(response);
    });
});


// Update Child User
// app.post('/UpdateChildUser',authMiddleware,[
app.post('/admin/child-user/update', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.'),
    check('user_password').optional().isLength({ min: 6 }).withMessage('User Password should be of minimum 6 characters.'),
], formValidationMiddleware, function (req, res) {
    //logger.info("In UpdateChildUser ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    adminController.UpdateChildUser(req.body, function (err, result) {
        if (err) return res.json(err);
        //logger.info("Out UpdateChildUser");
        return res.json(response);
    });
});


// List Child Users
// app.get('/getChildUsers',authMiddleware,[
app.get('/admin/child-user/list', authMiddleware, [
    check('authenticatedUser.admin').equals('1').withMessage('You dont have priviledge to perform this operation.')
], formValidationMiddleware, function (req, res) {
    //logger.info("In getChildUsers ", req.headers['x-forwarded-for'], " Agent: ", req.headers['user-agent']);
    adminController.getChildUsers(function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out getChildUsers");
        return res.json(response);
    });
});

//

// app.post('/admin/developer/clear',function(req,res){

//     var tables = req.body.tables;
//     tables.forEach(table => {
//         shared.db.sqliteDB.run('delete from '+table);
//     });

//     return res.json({'status':'success','message':'done'});
// });

// app.post('/admin/developer/view', async function(req,res){

//     var tables = req.body.tables;
//     var response = {};
//     for(var i=0;i<tables.length;i++){
//         try{
//             response[tables[i]] = await dbManager.all('select * from '+tables[i]);
//         }catch(ex){
//             //logger.info('view developer exception:',ex);
//         }
//     }
//     return res.json(response);
// });

// app.post('/admin/developer/insert', async function(req,res){

//     var tables = req.body.table_name;
//     var response = {};

//     for(var i=0;i<req.body.data.length;i++){

//         try{
//             await dbManager.executeInsert(req.body.table_name,req.body.data[i]);
//             // response[tables[i]] = await dbManager.all('select * from '+tables[i]);
//         }catch(ex){
//             //logger.info('insert developer exception:',ex);
//         }

//     }
//     return res.json({
//         status:"success",
//         message:"done"
//     });
// });

app.post('/admin/developer/clear-except-login',function(req,res){
    let tablesList = sqliteDB.prepare("SELECT name FROM sqlite_master WHERE type='table'");
    tablesList.all(function (error, result) {

        if (error) {
            logger.error("GET Room  List Error : ", error);
            reject(error);
        } else {

            result.forEach(table => {
                console.log(table);
                if(['mst_mood_list','mst_icons','spike_log_categories','spike_migrations'].includes(table.name)){

                }else{
                    shared.db.sqliteDB.run('delete from '+table.name);
                }
            });

            return res.json({'status':'success','message':'done','tables':result});

        }
    });

})
                            
app.post('/admin/developer/clear-all', [
    check('qV1vtodCh3').not().isEmpty().withMessage('Parameter 1 is Required.'),
    check('zy7Hwiy9ea').not().isEmpty().withMessage('Parameter 2 is Required.'),
    check('SSd05DATjQ').not().isEmpty().withMessage('Parameter 3 is Required.')
], formValidationMiddleware, function(req,res){
    if (req.body.qV1vtodCh3 === "eNdkHKBfZ527k8Tw6PjL" && req.body.zy7Hwiy9ea === "3a2AVtJ2Gag3jtiuF0Ef" && req.body.SSd05DATjQ === "3UNbpBKhioUY9a4rdokO") {
        let tablesList = sqliteDB.prepare("SELECT name FROM sqlite_master WHERE type='table'");
        logger.info('Table List : ', tablesList)
        tablesList.all(function (error, result) {
    
            if (error) {
                logger.error("GET Room  List Error : ", error);
                reject(error);
            } else {
                result.forEach(table => {
                        shared.db.sqliteDB.run('delete from '+table.name);
                });
                return res.json({'status':'success','message':'done','tables':result});
            }
        });
    } else {
        return res.json({'status': 'error', 'message': 'Something went wrong! Please try again!'});
    }
}) 

// Forget Password Routes

app.post('/admin/forget-password', [
        check('user_phone').not().isEmpty().isLength({ min: 10 }).matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/).withMessage('User Phone is Required of Please enter valid phone number.')
    ], formValidationMiddleware,function (req, res) {
    //logger.info("In forgorPassword ", req.body);
    adminController.forgotPassword(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out forgotPassword");
        return res.json(response);
    });
});

app.post('/admin/verify-otp', [
        check('user_phone').not().isEmpty().isLength({ min: 10 }).matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/).withMessage('User Phone is Required of Please enter valid phone number.'),
        check('otp').not().isEmpty().isLength({min: 6, max: 6}).withMessage('Otp is Required and Length should be 6.')
    ], formValidationMiddleware, function (req, res) {
    //logger.info("In Verify Otp ", req.body);
    adminController.verifyOtp(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out Verify Otp");
        return res.json(response);
    });
});

app.post('/admin/reset-password', [
        check('user_phone').not().isEmpty().isLength({ min: 10 }).matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/).withMessage('User Phone is Required of Please enter valid phone number.'),
        check('otp').not().isEmpty().isLength({min: 6, max: 6}).withMessage('Otp is Required and Length should be 6'),
        check('user_password').not().isEmpty().isLength({ min: 6 }).withMessage('User Password is Required and Minimum 6 characters Required.'),
        check('phone_id').not().isEmpty().withMessage('Phone Id Required.'),
        check('phone_type').not().isEmpty().withMessage('Phone Type Required.'),
        check('fcm_token').not().isEmpty().withMessage('FCM Token Required.')
    ], formValidationMiddleware, function (req, res) {
    //logger.info("In Change Password ", req.body);
    adminController.resetPassword(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out Change Password!!");
        return res.json(response);
    });
});

app.post('/admin/resend-otp', [
        check('user_phone').not().isEmpty().isLength({ min: 10 }).matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/).withMessage('User Phone is Required of Please enter valid phone number.')
    ], formValidationMiddleware,function (req, res) {
    //logger.info("In Retry ", req.body);
    adminController.resendOtp(req.body, function (err, response) {
        if (err) return res.json(err);
        //logger.info("Out Change Password!!");
        return res.json(response);
    });
});