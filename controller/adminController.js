// Shivam Added the general Util
const {
    homeAutomationModule,
    appContext,
    shared
} = require('./../util/general-util');
const userModel = require('./../models/user');
const roomModel = require('./../models/room');
const logModel = require('./../models/logs');
const loginModel = require('./../models/login');
const dbManager = require('./../util/db-manager');
const moodModel = require('./../models/mood');
const syncManager = require('./../util/sync-manager');
const {inactiveAuthKey,inactiveAllUserKeys} = require('./../util/middlewares/auth-middleware');
const eventsEmitter = require("./../util/event-manager");

// module.exports = function (homeAutomationModule, appContext, shared, clientSocketObj) {
let appUtil = homeAutomationModule.appUtil,
    constants = homeAutomationModule.constants,
    // moment = shared.util.moment,
    // socketio = shared.util.socket_io,

    viewerDetails = appContext.viewerDetails,
    homeControllerDeviceId = viewerDetails.deviceID;

let forgotPasswordOtps = {};

/**
 * Get All room list and camera list
 */
exports.getRoomCameraList = async function (cb) {
    try {
        let data = {
            roomList: await roomModel.listByType('room', ['room_id', 'room_name', 'room_users', 'created_by']),
            cameradeviceList: await dbManager.all(constants.GET_ADMIN_CAMERA_LIST, [homeControllerDeviceId]),
        };
    
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
        return cb(null, response);
    } catch(error) {
        logger.error('Admin Controller | Get Room Camera List Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};


/**
 * Add New Child User
 * @param user_name (required)
 * @param user_password (required)
 * @param display_name (required)
 * @param roomList
 * @param cameraList
 * @param admin_user_id (admin user id)
 * @param phone_id
 * @param phone_type
 */
exports.AddChildUser = async function (params, cb) {

    //logger.info("Admin Controller | Add Child User API ", params);

    let admin_user_id = params.admin_user_id,
        user_name = params.user_name,
        encryptedPassword = appUtil.encryptPassword(params.user_password),
        user_password = encryptedPassword,
        first_name = params.display_name,
        roomList = params.roomList,
        cameraList = params.cameraList,
        admin = params.admin,
        phone_id = params.phone_id,
        phone_type = params.phone_type;

    try {

        // Check if user exists with same user name 
        if (await userModel.checkIfUserNameExists(user_name)) {

            response = appUtil.createErrorResponse(constants.responseCode.SAME_CHILD_USER_EXIST);
            return cb(null, response);

        } else {

            // var Client = require('node-rest-client').Client;
            // var restClient = new Client();

            // var args = {
            //     data: {
            //         user_name
            //     },
            //     headers: {
            //         "Content-Type": "application/json"
            //     }
            // };

            // restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/auth/user-name/validate", args, async function (data, response) {

            //     console.log('USERNAME CHECK IN ONLINE DB', data);

            //     if (data.code == 200) {
            //         return true;
            //     }

            //     return false;
            // });

            const isUserNameExists = await userModel.checkIfUserNameIsAvailable(user_name.trim());
            if (!isUserNameExists) {
                return cb(null, appUtil.createErrorResponse({ code: 400, message: "User Name already exists. Please use another user name." }));
            }

            let admin = await userModel.getAdminUser();
            if (!admin) {
                // If admin details are not found
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);

            } else {

                // Create New Child User Process 

                let user_id = appUtil.generateRandomId('USER');

                let user = await userModel.addChildUser({
                    "user_name": user_name,
                    "user_password": encryptedPassword,
                    "user_id": user_id,
                    "first_name": first_name,
                    "topic_arn": ''
                });

                // Assign Room To The User
                await roomModel.updatePrivilege({
                    room_list: roomList,
                    user_id: user_id
                });

                // Assign Camera To The User
                cameraList.forEach((camera) => {
                    dbManager.executeNonQuery(constants.ADD_CAMERA_CHILD_USER, [appUtil.generateRandomId('CAM-PRIVILEDGE'), user_id, camera, admin.user_id, appUtil.currentDateTime()]);
                });

                // Add To Log 
                logModel.add({
                    "log_type": "child_user_add",
                    "description": 'Child User | ' + user_name
                }, params.authenticatedUser);

                // No need for this now since we have seperated counter for logs for each user
                // logModel.markAllLogsSeenForUser(user_id);

                // Call Manual Sync of user table
                syncManager.updateTable('mst_user');

                response = appUtil.createSuccessResponse(constants.responseCode.CHILD_USER_ADDED);
                return cb(null, response);

            }

        }

    } catch (error) {
        logger.error('Admin Controller | Add Child User Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};


/**
 * Delete Child User 
 * @param child_user_id (User Id of the Child)
 * @param user_id
 * @param phone_id
 * @param phone_type
 */
exports.DeleteChildUser = async function (params, cb) {

    try {

        let user = await userModel.get(params.child_user_id);
        if (!user) {
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE({code: 419, message: "User Not Found."})))
        }

        // Update In Existing Rooms
        await roomModel.updatePrivilege({
            room_list: [],
            user_id: params.child_user_id
        });

        // Deactivate Child User
        await dbManager.executeNonQuery(constants.DEACTIVATE_CHILD_USER, [params.child_user_id]);
        // Delete All Logs Of Child
        await dbManager.executeNonQuery('DELETE from spike_logs where user_id=?', [params.child_user_id]);
        // Delete All Schedules Of Child
        await dbManager.executeNonQuery('DELETE from spike_schedules where created_by=?', [params.child_user_id]);
        // Delete All Alerts Of Child
        await dbManager.executeNonQuery('DELETE FROM spike_alerts where user_id=?', [params.child_user_id]);
        // Delete All Moods Of Child
        await moodModel.deleteAllMoodsByUserId(params.child_user_id);
        // await dbManager.executeNonQuery('DELETE FROM spike_rooms where room_type="mood" AND created_by=?', [params.child_user_id]);
        // Delete Camera Privs
        await dbManager.executeNonQuery(constants.DELETE_CHILD_CAMERA, [params.child_user_id]);

        await loginModel.inactiveAllUserKeys(params.child_user_id);
        inactiveAllUserKeys(params.child_user_id);
        syncManager.updateTable('spike_login');

        logModel.add({
            log_type: "child_user_delete",
            description: 'Child User | ' + user.user_name
        }, {
            user_id: params.user_id,
            phone_id: params.phone_id,
            phone_type: params.phone_type
        });

        eventsEmitter.emit('emitSocket', {
            topic: 'deleteChildUser',
            data: {
                "user_id": params.child_user_id,
                "message": "This User Profile does not exist anymore!!"    
            }
        });

        // socketio.emit("deleteChildUser", {
        //     "user_id": params.child_user_id,
        //     "message": "This User Profile does not exist anymore!!"
        // });

        // Call Manual Sync of user table
        syncManager.updateTable('mst_user');

        let response = appUtil.createSuccessResponse(constants.responseCode.CHILD_USER_DELETED);
        return cb(null, response);

    } catch (error) {
        logger.error('Admin Controller | Delete Child User Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, errorResponse);
    }
};



/**
 * Update Child Details
 * @param child_user_id
 * @param user_id
 * @param phone_id
 * @param phone_type
 * @param user_password (optional)
 * @param roomList (optional)
 * @param cameraList (optional)
 */
exports.UpdateChildUser = async function (params, cb) {

    //logger.info("Admin Controller | Update Child User API ", params);

    try {


        let user = await userModel.get(params.child_user_id);
        if (!user) {
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE({code: 419, message : "User Not Found."})))
        }

        // Update Room Details
        if (params.roomList) {
            await roomModel.updatePrivilege({
                room_list: params.roomList,
                user_id: params.child_user_id
            });
        }

        // Flush Camera Privilege and Then Add Again
        if (params.cameraList) {
            await dbManager.executeNonQuery(constants.DELETE_CHILD_CAMERA, [params.child_user_id]);
            params.cameraList.forEach((camera) => {
                dbManager.executeNonQuery(constants.ADD_CAMERA_CHILD_USER, [appUtil.generateRandomId('CAM-PRIVILEDGE'), params.child_user_id, camera, params.user_id, appUtil.currentDateTime()]);
            });
        }

        // Update Password And set Is sync 0 To Update to Cloud Server
        if (params.user_password && params.user_password.trim().length > 0) {
            await userModel.update(params.child_user_id, { user_password: appUtil.encryptPassword(params.user_password), is_sync: 0 });
            await loginModel.inactiveAllUserKeys(params.child_user_id);
            inactiveAllUserKeys(params.child_user_id);
            await syncManager.updateTable('spike_login');
        }

        logModel.add({
            log_type: "child_user_update",
            description: 'Child User | ' + user.user_name
        }, {
            user_id: params.user_id,
            phone_id: params.phone_id,
            phone_type: params.phone_type
        });

        eventsEmitter.emit('emitSocket', {
            topic: 'updateChildUser',
            data: {
                "user_id": params.child_user_id,
                "message": "User Profile has been Updated.!!"
            }
        });

        // socketio.emit("updateChildUser", {
        //     "user_id": params.child_user_id,
        //     "message": "User Profile has been Updated.!!"
        // });

        // Call Manual Sync of user table
        syncManager.updateTable('mst_user');

        let response = appUtil.createSuccessResponse({
            code: 200,
            message: "Child User Updated Successfully."
        });


        return cb(null, response);


    } catch (error) {

        logger.error('Admin Controller | Update Child User Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);

    }

};


/**
 * Get All The Child User with their rooms and camera priviledges
 */
exports.getChildUsers = async function (cb) {
    logger.info('IN ADMIN CONTROLLER');
    try {
        let userList = await userModel.getChildUsers();

        for (let user of userList) {
            user.roomList = await roomModel.listByUser(user.user_id, 'room');
            user.cameraList = await dbManager.all(constants.GET_CHILD_USER_CAMERAS, [user.user_id]);
        }
    
        let data = {
            child_list: userList
        };
    
        let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
        return cb(null, response);
    } catch(error) {
        logger.error('Admin Controller | Get Child User Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }

};


/**
 * 
 * @param {*} user_phone
 * @param {*} otp 
 */
exports.sendOtp = async (params) => {

    try {
        let Client = require('node-rest-client').Client;
        let restClient = new Client();
    
        params.home_controller_device_id = homeControllerDeviceId;
    
        let args = {
            data: params,
            headers: {
                "Content-Type": "application/json"
            }
        };
    
        return new Promise((resolve, reject) => {
            restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/auth/forget-password/send-otp", args, async function (data, response) {
                if (data.code == 200) {
                    resolve(true);
                }
                reject(false);
            });
        });    
        
    } catch(error) {

        logger.error('Admin Controller | Send OTP Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);

    }

}

/**
 * Forgot Password
 * @param user_phone (required)
 */
exports.forgotPassword = async function (params, cb) {
    try {
        //logger.info('Params : ', params.user_phone);
        let user = await userModel.getAdminUserUsingPhoneNumber(params.user_phone);

        if (!user) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Uer not Found")));
        } else {
            let otp = Math.floor(100000 + Math.random() * 900000);
            
            //logger.info('Otp', otp);
            //logger.info('User Phone : ', user.user_phone);

            let paramsForMessaga = {
                Message: 'Your Spikebot OTP is ' + otp,
                PhoneNumber: user.user_phone
            };

            //logger.info('forgotPassword controller');
            forgotPasswordOtps[user.user_phone] = {
                otp: otp,
                created_at: new Date(),
                count: 0,
                retryCount: 0
            };

            // uncomment when working
            // var otpSent = await exports.sendOtp({
            //     user_phone: user.user_phone,
            //     otp: otp
            // });

            let otpSent=true;

            if (otpSent == true) {
                let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, params);
                return cb(null, response);
            } else {
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            } 
        }
    } catch (error) {
        logger.error('Admin Controller | Forgot Password Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }
}

/**
 * Verify Otp
 * @param user_phone (required)
 * @param otp (required)
 */
exports.verifyOtp = async function (params, cb) {
    try {
        //logger.info('ForgotPasswordOtp', forgotPasswordOtps);
        if (forgotPasswordOtps[params.user_phone] && forgotPasswordOtps[params.user_phone].otp == params.otp) {

            if (forgotPasswordOtps[params.user_phone].count > 2) {
                delete forgotPasswordOtps[params.user_phone];
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(420, "Maximum attamp reached. Please try again."));
                return cb(errorResponse, null);
            }
            forgotPasswordOtps[params.user_phone].count++;
            let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
            return cb(null, response);
        } else {
            let errorResponse = appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(421, "Invalid OTP."));
            return cb(errorResponse, null);
        }
    } catch (error) {
        logger.error('Admin Controller | Verify Otp Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }
}

/**
 * Forgot Password
 * @param user_phone (required)
 * @param otp (Required)
 * @param user_password (Required)
 * @param fcm_token (required)
 * @param phone_id (required)
 * @param phone_type (required)
 */
exports.resetPassword = async function (params, cb) {
    try {

        if (forgotPasswordOtps[params.user_phone] && forgotPasswordOtps[params.user_phone].otp == params.otp) {
            
            let userDetails = await userModel.getAdminUserUsingPhoneNumber(params.user_phone);
            await userModel.update(userDetails.user_id, { user_password: appUtil.encryptPassword(params.user_password) });
            await loginModel.inactiveAllUserKeys(userDetails.user_id);
            inactiveAllUserKeys(userDetails.user_id);
            delete forgotPasswordOtps[params.user_phone];

            params.user_name = userDetails.user_name;
            const userController = require('./userController');
            return userController.login(params,cb);

        } else {
            let errorResponse = appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(421, "Phone number and otp not valid."));
            return cb(errorResponse, null);
        }
    } catch (error) {
        logger.error('Admin Controller | Change Password API Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }
}

/**
 * Forgot Password
 * @param user_phone (required)
 */
exports.resendOtp = async function (params, cb) {

    try {

        let paramsForMessaga = {
            Message: 'Your OTP is ' + forgotPasswordOtps[params.user_phone].otp,
            PhoneNumber: params.user_phone
        };

        if (forgotPasswordOtps[params.user_phone]) {

            if (forgotPasswordOtps[params.user_phone].retryCount > 2) {
                //logger.info('Retry Count : ', forgotPasswordOtps[params.user_phone].retryCount);
                delete forgotPasswordOtps[params.user_phone];
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(420, "Maximum attamp reached. Please try again."));
                return cb(errorResponse, null);
            }

            
            let otp = await exports.sendOtp({
                user_phone: params.user_phone,
                otp: forgotPasswordOtps[params.user_phone].otp
            });

            forgotPasswordOtps[params.user_phone].retryCount++;

            if (otp == true) {
                let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, params);
                return cb(null, response);
            } else {
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        }

    } catch (error) {
        logger.error('Admin Controller | Retry API Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }
}
