const appUtil = require('./../util/app-util');
const constants = require('./../util/constants');
const moment = require('moment');

// Models
const roomModel = require('../models/room');
const userModel = require('../models/user');
const logModel = require('../models/logs');
const scheduleModel = require('../models/schedule');


/**
 * Spike Schedule Controller : Add Schedule to the list
 * @param devices
 * @param schedule_days (comma seperated string)
 * @param on_time (24 hr format or timestamp)
 * @param off_time (24 hr format or timestamp)
 * @param on_time_difference (24 hr format only needed for timer)
 * @param off_time_difference (24 hr format only needed for timer)
 * @param schedule_type (schedule | timer)
 * @param schedule_name  Name of Schedule
 * @param schedule_device_type (room , mood)
 * @param user_id
 */
exports.addSchedule = async(params, cb) => {

    logger.info('Spike Schedule Controller | Add Schedule API', params);

    // Add to match old api
    if (params.schedule_type == "timer") {
        params.schedule_days = [];
    } else {
        params.schedule_days = params.schedule_days.split(",");
    }

    if(await scheduleModel.checkIfScheduleWithNameExists(params.schedule_name)){
        return cb(null, appUtil.createErrorResponse({
            code: 420,
            message: 'Schedule/Timer with same name already exists'
        })); 
    }

    params.created_by = params.authenticatedUser.user_id;

    try {

        let newSchedule = await scheduleModel.add(params);

        // Add To Logs
        logModel.add({
            log_object_id: newSchedule.schedule_id,
            log_type: "schedule_add"
        }, params.authenticatedUser);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Spike Schedule Controller | Add Schedule Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

/**
 * Delete Schedule
 * @param schedule_id (required)
 */
exports.deleteSchedule = async(params, cb) => {

    try {

        const scheduleData = await scheduleModel.get(params.schedule_id);
        if(!scheduleData){
            return cb(null, {
                code:400,
                message:'Invalid Schedule Id'
            });
        }

        if (params.authenticatedUser.admin==0 && scheduleData.created_by != params.authenticatedUser.user_id) {
            return cb(null, appUtil.createErrorResponse({
                code: 420,
                message: 'You don\'t have access to perform this'
            }))
        }

        await scheduleModel.delete(params.schedule_id);
        // Add To Logs
        logModel.add({
            log_type: "schedule_delete",
            deleted_data: JSON.stringify(scheduleData)
        }, params.authenticatedUser);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));    
    } catch (error) {
        logger.error('Spike Schedule Controller | Delete Schedule Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}




/**
 * Get List of Schedule
 * @param room_id (if you want to check schedules on room and mood)
 * @param user_id (Required, if you want to check schedules on room and mood)
 * @param schedule_device_type
 */
exports.listSchedule = async(params, cb) => {

    try {

        const user = await userModel.get(params.authenticatedUser.user_id);

        // Delete User Id from params to get all schedules of admin as well as child if the user is admin user
        if (user.admin == 1) {
            delete params.user_id;
        }

        // Delete User Id filter if room id is present
        if (params.room_id) delete params.user_id;

        let scheduleList = await scheduleModel.find(params);

        for (let i = 0; i < scheduleList.length; i++) {
            scheduleList[i].devices = await scheduleModel.getDeviceIdsByScheduleId(scheduleList[i].schedule_id);
            if (scheduleList[i].room_id) scheduleList[i].room = await roomModel.get(scheduleList[i].room_id);
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, scheduleList));

    } catch (error) {
        logger.error('Spike Schedule Controller | List Schedule Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}


/**
 * Edit Schedule 
 * @param schedule_id (required)
 * @param devices (optional)
 * @param schedule_name (optional)
 * @param is_active (optional) (y,n)
 * @param schedule_days (optional) - comma seperated string
 * @param on_time (optional) 18:00 24 hr format
 * @param off_time (optional) 11:00  24 hr format
 * @param on_time_difference (optional)
 * @param off_time_difference (optional) 
 * @param schedule_device_type
 */
exports.editSchedule = async(params, cb) => {

    //logger.info('Schedule Controller | Update Schedule API', params);

    try {

        let schedule = await scheduleModel.get(params.schedule_id);

        if (!schedule) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(102, "Schedule Not Found.")));
        }

        // check if schedule name isnt clashing with other schedules
        if(params.schedule_name && schedule.schedule_name!=params.schedule_name){
            if(await scheduleModel.checkIfScheduleWithNameExists(params.schedule_name)){
                return cb(appUtil.createErrorResponse({
                    code: 420,
                    message: 'Schedule/Timer with same name already exists.'
                }));
            }
        }

        if (params.authenticatedUser.admin==0 && schedule.created_by != params.authenticatedUser.user_id) {
            return cb(appUtil.createErrorResponse({
                code: 420,
                message: 'You don\'t have permission to perform this task.'
            }))
        }

        if (params.schedule_type == 'timer' && params.on_time && params.on_time.length < 18) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(102, "Invalid Time Format")));
        }

        if (params.schedule_type == 'timer' && params.off_time && params.off_time.length < 18) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(102, "Invalid Time Format")));
        }

        let updateData = {};
        if (params.schedule_days) updateData.schedule_days = params.schedule_days;
        if (params.is_active) updateData.is_active = params.is_active;
        
        if (params.on_time){
            updateData.on_time = params.on_time;
            updateData.on_time_difference = params.on_time_difference;
        }else{
            updateData.on_time = ""
             updateData.on_time_difference = null;
        } 
        
        if (params.off_time){
            updateData.off_time = params.off_time;
            updateData.off_time_difference = params.off_time_difference;
        }else{
            updateData.off_time = ""
            updateData.off_time_difference = null;
        }

        // if (params.on_time_difference && params.on_time_difference!="00:00"){
        //     updateData.on_time_difference = params.on_time_difference;
        // }else{
        //     updateData.on_time_difference = null;
        // } 

        // if (params.off_time_difference && params.on_time_difference!="00:00"){
        //     updateData.off_time_difference = params.off_time_difference;
        // }else{
            
        // }

        if (params.schedule_name) updateData.schedule_name = params.schedule_name;
        if (params.schedule_device_type) updateData.schedule_device_type = params.schedule_device_type;
        if (params.schedule_type) updateData.schedule_type = params.schedule_type;
        if (params.devices) updateData.devices = params.devices;

        // Update the Schedule in Database
        await scheduleModel.update(params.schedule_id, updateData);

        logModel.add({
            log_object_id: params.schedule_id,
            log_type: "schedule_update"
        }, params.authenticatedUser);

        if (params.is_active == 'y') {

            logModel.add({
                log_object_id: params.schedule_id,
                log_type: "schedule_enable"
            }, params.authenticatedUser);

        } else if (params.is_active == 'n') {

            logModel.add({
                log_object_id: params.schedule_id,
                log_type: "schedule_disable"
            }, params.authenticatedUser);

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Spike Schedule Controller | Edit Schedule Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}


/**
 * Edit Schedule 
 * @param schedule_id (required)
 * @param is_active (optional) (y,n)
 */
exports.changeScheduleStatus = async(params, cb) => {

    //logger.info('Schedule Controller | Update Schedule API', params);

    try {

        let schedule = await scheduleModel.get(params.schedule_id);

        if (!schedule) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(102, "Schedule Not Found.")));
        }

        if (params.authenticatedUser.admin==0 && schedule.created_by != params.authenticatedUser.user_id) {
            return cb(appUtil.createErrorResponse({
                code: 420,
                message: 'You don\'t have permission to perform this task.'
            }))
        }

        let updateData= {};
        if (schedule.schedule_type == "timer" && params.is_active == 'y' && schedule.on_time_difference != null && schedule.on_time_difference.length > 0) {
            const on_time_difference  = schedule.on_time_difference;
            if(on_time_difference){
                let new_on_time = moment().add(on_time_difference.split(":")[0], 'hours').format('YYYY-MM-DD HH:mm:ss');
                new_on_time = moment().add(on_time_difference.split(":")[1], 'minutes').format('YYYY-MM-DD HH:mm:ss');
                updateData.on_time = new_on_time;
            }
        }

        if (schedule.schedule_type == "timer" && params.is_active == 'y' && schedule.off_time_difference != null && schedule.off_time_difference.length > 0) {
            const off_time_difference  = schedule.off_time_difference;
            if(off_time_difference){
                let new_off_time = moment().add(off_time_difference.split(":")[0], 'hours').format('YYYY-MM-DD HH:mm:ss');
                new_off_time = moment().add(off_time_difference.split(":")[1], 'minutes').format('YYYY-MM-DD HH:mm:ss');
                updateData.off_time = new_off_time;
            }
        }

       
        if (params.is_active) updateData.is_active = params.is_active;
        
        // Update the Schedule in Database
        await scheduleModel.update(params.schedule_id, {...updateData});

        if (params.is_active == 'y') {

            logModel.add({
                log_object_id: params.schedule_id,
                log_type: "schedule_enable"
            }, params.authenticatedUser);

        } else if (params.is_active == 'n') {

            logModel.add({
                log_object_id: params.schedule_id,
                log_type: "schedule_disable"
            }, params.authenticatedUser);

        }


        logger.info('SCHEDULE DATA',updateData);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, updateData));

    } catch (error) {
        logger.error('Spike Schedule Controller | Change Schedule Status Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}