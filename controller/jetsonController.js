const appUtil = require('./../util/app-util');
const constants = require('./../util/constants');

// Models
const jetsonModel = require('../models/jetson');
const cameraModel = require('../models/camera');
const cameraController = require('./../util/general-util').homeAutomationModule.cameraController;

/**
 * Add Jetson (only Administrator)
 * @param jetson_name (required)
 * @param jetson_ip (required)
 */
exports.add = async (params, cb) => {

    try {

        let jetsonData = await jetsonModel.getByIp(params.jetson_ip.trim());

        if (jetsonData) {
            logger.info("Jetson is already added.");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Jetson is already added.")));
        }

        jetsonData = {
            jetson_ip: params.jetson_ip.trim(),
            jetson_name: params.jetson_name
        }

        jetsonData = await jetsonModel.add(jetsonData);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(200, "Smart Cam Device is added successfully.")));

    } catch (error) {
        logger.error("Jetson Controller | Add Jetson API Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete From Unassigned List (Only Administrator)
 * @param jetson_id (required)
 * @param jetson_name
 * @param jetson_ip
 */
exports.update = async (params, cb) => {

    try {

        let updateData = {};
        if (params.jetson_name) updateData.jetson_name = params.jetson_name;
        if (params.jetson_ip) updateData.jetson_ip = params.jetson_ip;

        await jetsonModel.update(params.jetson_id, updateData);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Jetson Controller | Update Jetson API Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}



/**
 * Delete From Unassigned List (Only Administrator)
 * @param jetson_id 
 */
exports.delete = async (params, cb) => {

    try {

        cameraController = require('./../util/general-util').homeAutomationModule.cameraController;

        await jetsonModel.delete(params.jetson_id);

        const cameraList = await cameraModel.getJetsonCamera(params.jetson_id);

        for (let camera of cameraList) {
            params.camera_id = camera.camera_id;
            cameraController.deleteCamera(params, function () {

            });
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Jetson Controller | Delete Jetson API Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * List Jetson 
 */
exports.list = async (params, cb) => {
    try {   
        let deviceList = await jetsonModel.list();
        logger.info('JETSON LIST : ', deviceList);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, deviceList));
    } catch (error) {
        logger.error("Jetson Controller | List Jetson API Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}