// Shivam Added the general Util
const network = require('network')
const dbManager = require('./../util/db-manager');
const appUtil = require('./../util/app-util');
const syncManager = require('./../util/sync-manager');
const constants = require('./../util/constants');
const userModel = require('./../models/user');
const {inactiveAuthKey,inactiveAllUserKeys} = require('./../util/middlewares/auth-middleware');
const homeControllerModel = require('./../models/home_controller');
const fireBaseUtil = require('./../util/firebase-util');
const loginModel = require('./../models/login');
const {
    homeAutomationModule,
    appContext
} = require('./../util/general-util');
const generalMetaModel = require('./../models/general_meta');
const logger = require('../util/logger');
const viewerDetails = appContext.viewerDetails,
    homeControllerDeviceId = viewerDetails.deviceID,
    vpnURL = homeAutomationModule.vpnURL;

exports.checkGatewayInLocal = function (cb) {
    res = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
    return cb(null, res);
};


/**
 * Sign Up API - Register PI to Server
 * - Can be done only once 
 * @param user_name 
 * @param user_password
 * @param user_email
 * @param user_phone
 * @param phone_type
 * @param phone_id
 * @param first_name
 * @param last_name
 * @param device_push_token
 */
exports.signup = async function (data, cb) {

    try {

        // Check if the PI is already signed up
        const checkAdminExists = await dbManager.all(constants.CHECK_ADMIN_EXISTS, [1]);
        if (checkAdminExists.length) return cb(null, appUtil.createErrorResponse(constants.responseCode.ADMIN_ALREADY_EXISTS));

        // Check if user name exists on the cloud
        const isUserNameExists = await userModel.checkIfUserNameIsAvailable(data.user_name.trim());
        if (!isUserNameExists) {
            return cb(null, appUtil.createErrorResponse({ code: 400, message: "User Name already exists. Please use another user name." }));
        }

        // Check if phone no is already occupied by the user    
        const isPhoneNoExists = await userModel.checkIfUserPhoneIsAvailable(data.user_phone.trim());
        if (!isPhoneNoExists) {
            return cb(null, appUtil.createErrorResponse({ code: 400, message: "Account with Phone No already exists. Please use another phone no." }));
        }        

        // check if profile vpn port is setup in /camera/vpn_port.json file
        if(constants.profile_vpn_port==null || constants.profile_vpn_port.toString().length == 0){
            return cb(null, appUtil.createErrorResponse({ code: 400, message: "Please setup open vpn connection in your pi." }));
        }

        let useremail = data.user_email,
            encryptedPassword = appUtil.encryptPassword(data.user_password),
            userpassword = encryptedPassword,
            firstname = data.first_name.trim(),
            lastname = data.last_name.trim(),
            username = data.user_name.trim(),
            userphone = data.user_phone,
            device_type = data.phone_type,
            device_id = data.phone_id,
            device_arn = device_type == 'android' ? constants.android_platform_arn : (data.env == 'live' ? constants.ios_platform_arn_live : constants.ios_platform_arn_sandbox),
            device_push_token = data.device_push_token,
            homeControllerName = 'Work',
            vpn_port = constants.profile_vpn_port,
            currentDateTime = appUtil.currentDateTime(),
            user_id = appUtil.generateRandomId(),
            homeControllerId = appUtil.generateRandomId(),
            admin = 1;


        const signupUser = await dbManager.executeNonQuery(constants.SIGNUP_USER, [user_id, user_id, useremail, userpassword, firstname, lastname, username, '', userphone, vpn_port, homeControllerDeviceId, homeControllerName, admin, currentDateTime, '', currentDateTime]);
        constants.userId = user_id;
        constants.userEmail = useremail;

        network.get_active_interface(async function (error, ip) {

            if (error) {
                logger.error("Error while fetching DHCP info", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            }

            // adding it to the database for future reference and identification of the pi

            const homecontrollerData = await dbManager.executeNonQuery(constants.INSERT_HOME_CONTROLLER_DATA, [ip.ip_address, ip.netmask, ip.gateway_ip, homeControllerId, homeControllerDeviceId, useremail, currentDateTime]);

            //logger.info("Succesfully! Gateway details inserted!");
            syncManager.updateTable('mst_home_controller_list');
            syncManager.updateTable('mst_user');

            data.fcm_token = data.device_push_token;
            return exports.login(data, cb);


            // data = {
            //     first_name: firstname,
            //     last_name: lastname,
            //     ip: 'http://home.deepfoods.net:' + vpn_port,
            //     user_id: user_id,
            //     user_password: userpassword,
            //     admin: admin,
            //     admin_user_id: user_id,
            //     local_ip_address: ip.ip_address,
            //     mac_address: homeControllerDeviceId
            // };


            // response = appUtil.createErrorResponse(constants.responseCode.SIGNUP_SUCCESS, data);
            // return cb(null, response);

        });

    } catch (error) {
        logger.error("User Controller | Signup User Error" + error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};


/**
 * Login User
 * @param user_name (user name)
 * @param user_password (user password)
 * @param phone_id (phone id of device)
 * @param phone_type (phone type of device)
 * @param fcm_token (firebase push notification token)
 */
exports.login = async function (params, cb) {

    try {

        const loginData = await userModel.login(params);
        if (!loginData) {
            return cb(appUtil.createErrorResponse({
                code: 400,
                message: "Invalid Credentials"
            }), null);
        }

        // Subscribe to firebase token
        fireBaseUtil.subscribeToTopic(params.fcm_token, loginData.user_id);
        await syncManager.updateTable('spike_login');

        // Now Format the response and send the data
        const homeController = await homeControllerModel.get();
        let data = {
            ip: process.env.VPN_URL+':' + loginData.user.vpn_port,
            user_id: loginData.user_id,
            auth_key: loginData.auth_key,
            fcm_token: loginData.fcm_token,
            first_name: loginData.user.first_name,
            last_name: loginData.user.last_name,
            user_password: loginData.user.user_password,
            local_ip_address: homeController.home_controller_ip,
            admin: loginData.user.admin,
            admin_user_id: loginData.admin_user_id,
            mac_address: homeControllerDeviceId
        };


        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data));

    } catch (error) {
        logger.error('User Cntroller | login Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};

/**
 * Logout User
 * @param auth_key (required)
 */
exports.logout = async (params, cb) => {

    //logger.info('Logout API ', params);

    try {

        // Unsubscribe to firebase topic
        const loginKey = await loginModel.getByAuthKey(params.auth_key);
        if (loginKey) {
            fireBaseUtil.unsubscribeToTopic(loginKey.fcm_token, loginKey.user_id);
        }

        // Inactivate the auth key
        await loginModel.inactiveAuthKey(params.auth_key);
        inactiveAuthKey(params.auth_key);
        syncManager.updateTable('spike_login');

        return cb(null, appUtil.createSuccessResponse({
            code: 200,
            message: "User logged out successfully."
        }));

    } catch (error) {
        logger.error('User Controller | Logout Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }

}

// Generate TokenId
// Not being used at the moment: keep it as it is. may be we will use it.
exports.saveLoginDetails = async function (data, cb) {

    try {
        let token_id = data.token_id,
            device_id = data.device_id,
            device_type = data.device_type,
            user_id = data.user_id,
            created_by = data.user_email,
            created_date = appUtil.currentDateTime();

        const checkDeviceIdType = await dbManager.all(constants.CHECK_DEVICE_ID_ADN_TYPE, [device_id, device_type,]);

        if (checkDeviceIdType.length > 0) {
            const updatelogindetails = await dbManager.executeNonQuery(constants.UPDATE_LOGIN_DETAILS, [token_id, user_id, created_by, created_date, device_id, device_type,]);
            //logger.info("Credential details update");
            return cb(null, 'success');
        } else {
            const insertdetails = await dbManager.executeNonQuery(constants.INSERT_LOGIN_DETAILS, [token_id, device_id, device_type, user_id, created_by, created_date,]);
            //logger.info("Credential details save");
            return cb(null, 'success');
        }

    } catch (error) {
        logger.error("User Controller | Save Login Details Error", error);
        return cb(error, null);
    }
};

/**
 * Change User Password
 * @param password (required)
 * @param old_password (required)
 */
exports.changeUserPassword = async function (params, cb) {

    try {

        let response;

        let user = await userModel.get(params.authenticatedUser.user_id);

        // Check if current password matches with db
        if (user.user_password != appUtil.encryptPassword(params.old_password)) {
            response = appUtil.createErrorResponse({
                code: 400,
                message: "Invalid Current Password."
            });
            return cb(null, response);
        }

        if(params.old_password==params.password){
            response = appUtil.createErrorResponse({
                code: 400,
                message: "New password should not be same as current password."
            });
            return cb(null, response);
        }

        if(!params.password || params.password.length<6){
            response = appUtil.createErrorResponse({
                code: 400,
                message: "New Password should have atleast 6 characters."
            });
            return cb(null, response);
        }

        

        // Update the password in the database. 
        await userModel.update(params.authenticatedUser.user_id,
            {
                user_password: appUtil.encryptPassword(params.password)
            });

        // Logout user from other devices
        await loginModel.inactiveAllKeysExceptAuthKey(params.authenticatedUser.auth_key);
        inactiveAllUserKeys(params.authenticatedUser.user_id,params.authenticatedUser.auth_key);

        response = appUtil.createSuccessResponse(constants.responseCode.PROFILE_UPDATE);
        return cb(null, response);

    } catch (error) {
        logger.error('User Controller | Change User Password Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};

// Get User Profile Data
exports.getuserProfileInfo = async function (cb) {

    try {

        let response;

        // for getting user information.
        const finduserdata = await dbManager.all(constants.GET_USER_INFO, [homeControllerDeviceId]);
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, {
            userProfileData: finduserdata
        });

        return cb(null, response);
    } catch (error) {
        logger.error("User Controller | Get User Profile Info Error", error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};

// Save User Profile Data
// For purpose of changing user name, and other basic user information
exports.saveUserProfileDetails = async function (data, cb) {
    try {
        logger.info('saveUserProfileDetails ', data);
        let first_name = data.first_name,
            last_name = data.last_name,
            user_name = data.user_name, // please make sure we dont allow user_name to be changed in database under any condition
            user_phone = data.user_phone,
            user_email = data.user_email,
            user_password = data.user_password,

            modified_date = appUtil.currentDateTime(),
            profileSyncToStatus = 0, // setting it 0, when sync will happen on cloud it will be set to 1 in db
            user_id = data.user_id,
            phone_id = data.phone_id,
            phone_type = data.phone_type;

        if (first_name == '' || last_name == '' || user_name == '' || user_phone == '' || user_email == '') {
            logger.warn('All user details are required!');
            response = appUtil.createSuccessResponse(constants.responseCode.ALL_DETAILS_REQUIRE);
            return cb(null, response);
        }

        // user password validation
        if (user_password == '' || user_password == null || user_password == undefined) {
            // check if there is change in the user details: this checking can be removed for future. #optimization
            const sameUserDetails = await dbManager.all(constants.CHECK_SAME_USER_EXISTS, [first_name, last_name, user_name, user_phone, user_id,]);
            if (!sameUserDetails.length) await dbManager.executeNonQuery(constants.UPDATE_USER_PROFILE_DATA, [first_name, last_name, user_name, user_phone, profileSyncToStatus, user_id, modified_date, user_id,]);
        } else {

            //logger.info('Update password');
            // Update password and profile if password field is not empty
            userpassword = appUtil.encryptPassword(user_password);
            // update in the database. also check if user name is not updated. we are not allowing user_name to be updated under any condition
            const updateProfileAndPassword = await dbManager.executeNonQuery(constants.UPDATE_USER_PROFILE_DATA_PASSWORD, [first_name, last_name, user_name, userpassword, user_phone, profileSyncToStatus, user_id, modified_date, user_id,]);
        }

        const userlist = await dbManager.all(constants.GET_USER_DATA, [user_id]);

        //logger.info("GET_USER_DATA", userlist);
        let userIp = vpnURL + userlist[0].vpn_port;
        data = {
            ip: userIp,
            user_id: userlist[0].user_id,
            first_name: userlist[0].first_name,
            last_name: userlist[0].last_name,
            user_password: userlist[0].user_password
        };

        response = appUtil.createSuccessResponse(constants.responseCode.PROFILE_UPDATE, data);
        return cb(null, response);
    } catch (error) {
        logger.error('User Controller | Save User Profile Error ', error);
        response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);
    }
};

/**
 * Get user notification
 * @param user_id (required)
 */
exports.getUserNotificationList = async function(params, cb) {
    try {
        logger.info('Get User Notification List', params.authenticatedUser.user_id);
        let notificationList = [];
        const home_controller_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'home_controller_notification_enable', 1);
        const temperature_sensor_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'temperature_sensor_notification_enable', 1);
        const door_sensor_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'door_sensor_notification_enable', 1);
        const gas_sensor_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'gas_sensor_notification_enable', 1);
        const co2_sensor_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'co2_sensor_notification_enable', 1);
        const camera_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'camera_notification_enable', 1);
        const water_detector_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'water_detector_notification_enable', 1);
        const lock_notification_enable = await generalMetaModel.getMetaValue(params.authenticatedUser.user_id, 'lock_notification_enable', 1);

        notificationList.push({
            "id": 0,
            "title": "Home Controller",
            "value": parseInt(home_controller_notification_enable)
        }, {
            "id": 1,
            "title": "Temperature Sensor",
            "value": parseInt(temperature_sensor_notification_enable)
        }, {
            "id": 2,
            "title": "Door Sensor",
            "value": parseInt(door_sensor_notification_enable)
        }, {
            "id": 3,
            "title": "Gas Sensor",
            "value": parseInt(gas_sensor_notification_enable)
        }, {
            "id": 4,
            "title": "Camera",
            "value": parseInt(camera_notification_enable)
        }, {
            "id": 5,
            "title": "Water Sensor",
            "value": parseInt(water_detector_notification_enable)
        }, {
            "id": 6,
            "title": "Lock",
            "value": parseInt(lock_notification_enable)
        }, {
            "id": 7,
            "title": "Co2 Sensor",
            "value": parseInt(co2_sensor_notification_enable)
        });

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, notificationList);
        return cb(null, response);

    } catch (error) {
        logger.error('User Controller | Get user notification error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);

    }
}

/**
 * Save notification for user
 * 
 */
exports.saveUserNotificationList = async function(params, cb) {
    try {

        let json = JSON.parse(JSON.stringify(params));
        logger.info('User Controller Notification : ', json);
        if (json.data[0].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'home_controller_notification_enable', json.data[0].value);
        }

        if (json.data[1].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'temperature_sensor_notification_enable', json.data[1].value);
        }

        if (json.data[2].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'door_sensor_notification_enable', json.data[2].value);
        }

        if (json.data[3].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'gas_sensor_notification_enable', json.data[3].value);
        }

        if (json.data[4].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'camera_notification_enable', json.data[4].value);
        }

        if (json.data[5].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'water_detector_notification_enable', json.data[5].value);
        }

        if (json.data[6].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'lock_notification_enable', json.data[6].value);
        }

        if (json.data[7].value != null) {
            await generalMetaModel.update(params.authenticatedUser.user_id, 'user', 'co2_sensor_notification_enable', json.data[7].value);
        }

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
        return cb(null, response);

    } catch (error) {
        logger.error('User Controller | Save User Notification List', error);
        response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(null, response);

    }
}