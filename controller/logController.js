const appUtil = require('./../util/app-util');
const dbManager = require('./../util/db-manager');
const constants = require('./../util/constants');
const moment = require('moment');

// Models
const logModel = require('../models/logs');
const userDeviceCounterModel = require('../models/user_device_counter');
const logFormatter = require('./../util/log-formatter-util');
const generalUtil = require('../util/general-util');
const homeControllerDeviceId = generalUtil.appContext.viewerDetails.deviceID;

const Client = require('node-rest-client').Client;
const restClient = new Client();

/**
 * Find Logs API
 * @param filter_type (optional) 
 * @param filter_action
 * @param device_id
 * @param room_id
 * @param alert_id
 * @param schedule_id
 * @param panel_id
 * @param start_date
 * @param end_date
 * @param unseen
 */
// exports.find2 = async (params, cb) => {

//     logger.info('Find Log API', params);

//     try {
//         const param = JSON.parse(JSON.stringify(params).replace(/'/g, ""));

//         let categoryList = await logModel.categories();
//         let filterActionList = categoryList.map(item => item.log_action);
//         let seenQuery = null;
//         if (param.filter_type == 'all' && !param.filter_action) param.filter_action = 'add,alert_add,on,off,delete,alert_delete,enable,disable,panel_on,panel_off,alert_update,home_controller_active,home_controller_inactive, camera_active, camera_inactive';
//         if (!param.filter_action) param.filter_action = categoryList.filter(function (item) {
//             return item.log_parent_type == param.filter_type
//         }).map(item => item.log_action).join();


//         let query = `SELECT l.sub_description,l.user_id, l.log_id as id,l.image_url, l.log_type,slc.log_action as activity_action,slc.log_parent_type as activity_type,l.description as activity_description,l.created_at as activity_time,mu.user_name,l.seen_by from spike_logs l 
//         inner join spike_log_categories slc on l.log_type = slc.log_type 
//         left join mst_user mu on mu.user_id=l.user_id 
//         where l.log_id NOT NULL and l.description NOT NULL AND (l.show=1 OR l.show IS NULL) `;

//         // List of actions that are to be found in the list
//         let applicableActions = param.filter_action.split(",").filter(value => -1 !== filterActionList.indexOf(value));

//         let extraLogLypes = param.filter_action.split(",").filter(x => !applicableActions.includes(x));

//         if (param.filter_type == 'all') {

//             let applicableLogTypes = extraLogLypes;
//             query += " AND (slc.log_action IN ('" + applicableActions.join("','") + "')";
//             query += " OR l.log_type IN ('" + applicableLogTypes.join("','") + "'))"

//         } else if (param.filter_type == 'all-general-notifications') {

//             // General Notifications are retreived from
//             // notification icon in top bar of the app

//             let applicableLogTypes = generalUtil.allGeneralNotificationsType;
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')";

//             //TODO : Only show logs of users based rooms
//             // if (params.authenticatedUser.admin == 1) {

//                 // query += ` AND (l.log_object_id IN (
//                 //                                         SELECT device_id from spike_panel_devices where panel_id IN (
//                 //                                             SELECT panel_id from spike_panels where room_id IN (
//                 //                                                 SELECT room_id from spike_rooms where room_users LIKE '%${params.authenticatedUser.user_id}%'
//                 //                                             )
//                 //                                         )
//                 //                                     ) 
//                 //                 OR l.log_object_id IN ( SELECT camera_id from mst_camera_devices) 
//                 //                 OR l.log_object_id = '${homeControllerDeviceId}'
//                 //                 )`;

//             //                     query += ` AND ((l.log_object_id IN (
//             //                         SELECT device_id from spike_panel_devices where panel_id IN (
//             //                             SELECT panel_id from spike_panels where room_id IN (
//             //                                 SELECT room_id from spike_rooms where room_users LIKE '%${params.authenticatedUser.user_id}%'
//             //                             )
//             //                         )
//             //                     ) AND l.user_id='${params.authenticatedUser.user_id}')
//             // OR l.log_object_id IN ( SELECT camera_id from mst_camera_devices) 
//             // OR l.log_object_id = '${homeControllerDeviceId}'
//             // )`;


//             // } else {

//             if(params.device_id){

//                 let updateDeviceCountQuery = ``;
//                 userDeviceCounterModel.clearDeviceCounter(params.authenticatedUser.user_id,params.device_id);

//                 query += ` AND ((l.log_object_id ='${params.device_id}' AND l.user_id='${params.authenticatedUser.user_id}' AND l.log_type='alert_active' )

//                     )`;

//                     seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
//                     user_id='${params.authenticatedUser.user_id}'
//                     AND log_object_id='${param.device_id}'
//                     AND log_type='alert_active'
//                     AND (
//                         seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
//                         OR 
//                         seen_by IS NULL 
//                     )
//                     `;

//             }else{
//                 userDeviceCounterModel.clearAllDeviceCounter(params.authenticatedUser.user_id);

//                 query += ` AND ((l.log_object_id IN (
//                     SELECT device_id from spike_panel_devices where panel_id IN (
//                         SELECT panel_id from spike_panels where room_id IN (
//                             SELECT room_id from spike_rooms where room_users LIKE '%${params.authenticatedUser.user_id}%'
//                         )
//                     )
//                 ) AND l.user_id='${params.authenticatedUser.user_id}' AND l.log_type='alert_active' ) OR (l.log_object_id = '${homeControllerDeviceId}' AND  l.user_id='${params.authenticatedUser.user_id}')

//                     )`;

//                     seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
//                     user_id='${params.authenticatedUser.user_id}'
//                     AND (log_type='alert_active' OR log_object_id = '${homeControllerDeviceId}')
//                     AND (
//                         seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
//                         OR 
//                         seen_by IS NULL 
//                     )
//                     `;
//             }



//             // }


//         } else {
//             query += " AND slc.log_action IN ('" + applicableActions.join("','") + "')";
//         }

//         /**
//          * If need details of all rooms and all devices and all panels
//          */
//         if (param.filter_type == 'room') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['room', 'mood', 'panel', 'device'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (param.device_id && param.device_id.trim().length > 0) {

//                 query += ` AND (
//                     l.log_object_id='` + param.device_id + `'
//                 )`;

//             } else if (param.panel_id && param.panel_id.trim().length > 0) {

//                 query += ` AND (
//                     l.log_object_id ='` + param.panel_id + `'
//                     OR 
//                     l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id = '` + param.panel_id + `')
//                 )
//                 `;

//             } else if (param.room_id && param.room_id.trim().length > 0) {

//                 query += ` AND (
//                     l.log_object_id = '` + param.room_id + `' 
//                     OR 
//                     l.log_object_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `')
//                     OR 
//                     l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `'))
//                 )
//                 `
//             }
//             // 
//             // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

//         }


//         /**
//          * If need details of all rooms 
//          */
//         if (param.filter_type == 'mood') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['mood'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (param.mood_id && param.mood_id.trim().length > 0) {

//                 query += ` AND (
//                     l.log_object_id = '` + param.mood_id + `' 
//                 )
//                 `
//             }

//             // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

//         }

//         /**
//          * @param room_id - if need details of all panels of rooms
//          * @param panel_id -if need details of that particular panel
//          */
//         if (param.filter_type == 'panel') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['panel', 'device'].includes(item.log_parent_type);
//             }).map(item => item.log_type);

//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (!param.panel_id || param.panel_id.trim().length == 0) {


//                 query += ` AND (
//                                 l.log_object_id = '` + param.room_id + `' 
//                                 OR 
//                                 l.log_object_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `')
//                                 OR 
//                                 l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `'))
//                             )
//                 `

//             } else {


//                 query += ` AND (
//                                 l.log_object_id ='` + param.panel_id + `'
//                                 OR 
//                                 l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN '` + param.panel_id + `')
//                             )
//                 `;

//             }

//             // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

//         }


//         /**
//          * @param device_id if need details of specific device
//          * @param panel_id if need details of all devices of that specific panel
//          */
//         if (param.filter_type == 'device') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['device'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (!param.device_id || param.device_id.trim().length == 0) {

//                 query += ` AND (
//                                 l.log_object_id='` + param.panel_id + `'
//                                 OR 
//                                 l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN ='` + param.panel_id + `')
//                             )
//                 `

//             } else {

//                 query += ` AND (
//                                 l.log_object_id='` + param.device_id + `'
//                             )
//                 `;

//             }


//         }

//         /**
//          * If Filter is Related To Gas Sensor / Temp Sensor / Door Sensor
//          */

//         if (param.filter_type == 'all-sensor') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['device'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (!param.device_id) {

//                 query += ` AND (                               
//                                 l.log_object_id IN ( SELECT device_id from spike_devices where device_type IN ('door_sensor','temp_sensor','gas_sensor','water_detector','lock') ) OR
//                                 l.log_object_id IN (SELECT alert_id from spike_alerts WHERE device_id IN (SELECT device_id from spike_devices where device_type='` + param.filter_type + `'))
//                             )
//                 `

//             } else {

//                 query += ` AND (
//                                 l.log_object_id='` + param.device_id + `'
//                             )
//                 `;

//             }

//         }

//         if (['temp_sensor', 'gas_sensor', 'door_sensor', 'water_detector', 'lock'].includes(param.filter_type)) {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['device'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


//             if (!param.device_id) {

//                 query += ` AND (                               
//                                 l.log_object_id IN ( SELECT device_id from spike_devices where device_type='` + param.filter_type + `') OR
//                                 l.log_object_id IN (SELECT alert_id from spike_alerts WHERE device_id IN (SELECT device_id from spike_devices where device_type='` + param.filter_type + `'))
//                             )
//                 `

//             } else {

//                 query += ` AND (
//                                 l.log_object_id='` + param.device_id + `'
//                             )
//                 `;

//             }

//             // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

//         }

//         /**
//          * Camera Log File
//          */

//         if (['camera'].includes(param.filter_type)) {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['camera'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
//             // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

//             if (!param.camera_id) {
//                 query += ` AND  l.log_object_id IN ( SELECT camera_id from mst_camera_devices )`
//             } else {
//                 query += ` AND (
//                                 l.log_object_id='` + param.camera_id + `'
//                             )
//                 `;
//             }

//         }


//         /**
//          * @param alert_id
//          */
//         if (param.filter_type == 'alert') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['alert'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
//             // query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')"

//             if (param.alert_id) {

//                 query += ` AND (
//                     l.log_object_id='` + param.alert_id + `'
//                 )
//                     `;
//             }

//         }

//         /**
//          * @param alert_id
//          */
//         if (param.filter_type == 'schedule') {

//             let applicableLogTypes = categoryList.filter((item) => {
//                 return ['schedule'].includes(item.log_parent_type);
//             }).map(item => item.log_type);
//             applicableLogTypes.push(...extraLogLypes);
//             query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
//             // query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')"

//             if (params.schedule_id) {

//                 query += ` AND (
//                     l.log_object_id='` + param.schedule_id + `'
//                 )
//                     `;
//             }

//         }

//         if (param.start_date) {

//             let startTimestamp = moment(param.start_date, 'YYYY-MM-DD HH:mm:ss').toDate().valueOf().toString();
//             let endTimestamp = moment(param.end_date, 'YYYY-MM-DD HH:mm:ss').toDate().valueOf().toString();

//             logger.info('LOG FILTER', [
//                 startTimestamp, endTimestamp
//             ]);

//             query += ' AND CAST(l.created_at as REAL) > ' + startTimestamp;
//             query += ' AND CAST(l.created_at as REAL) < ' + endTimestamp;

//         } else {
//             // var startTimestamp = moment().startOf('day').toDate().valueOf().toString();
//             // query += ' AND CAST(l.created_at as REAL) > '+startTimestamp;
//         }

//         if (param.unseen && param.unseen == 1) {

//             query += ` AND (
//                 l.seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
//                 OR 
//                 l.seen_by IS NULL 
//             )`;

//             param.notification_number = 0;
//         }

//         if (!param.notification_number) {
//             param.notification_number = 0;
//         }


//         query += ' order by CAST(l.created_at as REAL) desc';
//         query += ' LIMIT ' + ((param.notification_number)) + ",25";    

//         logger.info('LOG QUERY', query);

//         let logs = await dbManager.all(query, []);

//         if(seenQuery!=null){
//             dbManager.executeNonQuery(seenQuery);
//         }

//         // dbManager.executeNonQuery(`UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE log_id in (SELECT id from (` + query + `)) 
//         // AND (
//         //     seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
//         //     OR 
//         //     seen_by IS NULL 
//         // )
//         // `);

//         logs = logFormatter.formatList(logs);
//         cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, logs));

//     } catch (error) {
//         logger.error('Log Controller | Log Find API', error);
//         return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
//     }
// }

/**
 * Find Logs API
 * @param filter_type (optional) 
 * @param filter_action
 * @param device_id
 * @param room_id
 * @param alert_id
 * @param schedule_id
 * @param panel_id
 * @param start_date
 * @param end_date
 * @param unseen
 */
exports.find = async (params, cb) => {

    logger.info('Find Log API', params);

    try {
        const param = JSON.parse(JSON.stringify(params).replace(/'/g, ""));

        let categoryList = await logModel.categories();
        let filterActionList = categoryList.map(item => item.log_action);
        let seenQuery = null;

        if (param.filter_type == 'all' && !param.filter_action) param.filter_action = 'add,alert_add,on,off,delete,alert_delete,enable,disable,panel_on,panel_off,alert_update,camera_active,camera_inactive';
        if (!param.filter_action) param.filter_action = categoryList.filter(function (item) {
            return item.log_parent_type == param.filter_type
        }).map(item => item.log_action).join();


        let query = `SELECT l.log_sub_type,l.sub_description,l.user_id, l.log_id as id,l.image_url, l.log_type,slc.log_action as activity_action,slc.log_parent_type as activity_type,l.description as activity_description,l.created_at as activity_time,mu.user_name,l.seen_by from spike_logs l 
        inner join spike_log_categories slc on l.log_type = slc.log_type 
        left join mst_user mu on mu.user_id=l.user_id 

        where l.log_id NOT NULL and l.description NOT NULL AND (l.show=1 OR l.show IS NULL) `;

        // List of actions that are to be found in the list
        if (param.filter_type == "" || param.filter_type == undefined || param.filter_type == null) {
            param.filter_type = "all"
        }
        
        let applicableActions = param.filter_action.split(",").filter(value => -1 !== filterActionList.indexOf(value));

        let extraLogLypes = param.filter_action.split(",").filter(x => !applicableActions.includes(x));

        logger.info("FILTER TYPE : ", param.filter_type)
        if (param.filter_type == 'all') {

            let applicableLogTypes = extraLogLypes;
            query += " AND (slc.log_action IN ('" + applicableActions.join("','") + "')";
            query += " OR l.log_type IN ('" + applicableLogTypes.join("','") + "'))"

        } else if (param.filter_type == 'all-general-notifications') {

            // General Notifications are retreived from
            // notification icon in top bar of the app

            let applicableLogTypes = generalUtil.allGeneralNotificationsType;
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')";

            let counterType = 'alert_counter';
            if (params.log_sub_type) {
                counterType = params.log_sub_type;
                query += ` AND l.log_sub_type='${params.log_sub_type}' `;
            }



            //TODO : Only show logs of users based rooms
            // if (params.authenticatedUser.admin == 1) {

            // query += ` AND (l.log_object_id IN (
            //                                         SELECT device_id from spike_panel_devices where panel_id IN (
            //                                             SELECT panel_id from spike_panels where room_id IN (
            //                                                 SELECT room_id from spike_rooms where room_users LIKE '%${params.authenticatedUser.user_id}%'
            //                                             )
            //                                         )
            //                                     ) 
            //                 OR l.log_object_id IN ( SELECT camera_id from mst_camera_devices) 
            //                 OR l.log_object_id = '${homeControllerDeviceId}'
            //                 )`;

            //                     query += ` AND ((l.log_object_id IN (
            //                         SELECT device_id from spike_panel_devices where panel_id IN (
            //                             SELECT panel_id from spike_panels where room_id IN (
            //                                 SELECT room_id from spike_rooms where room_users LIKE '%${params.authenticatedUser.user_id}%'
            //                             )
            //                         )
            //                     ) AND l.user_id='${params.authenticatedUser.user_id}')
            // OR l.log_object_id IN ( SELECT camera_id from mst_camera_devices) 
            // OR l.log_object_id = '${homeControllerDeviceId}'
            // )`;


            // } else {

            if (param.device_id) {

                let updateDeviceCountQuery = ``;

                if (['humidity_alert', 'temp_alert'].includes(counterType)) {
                    const specificCounter = await userDeviceCounterModel.clearDeviceCounter(param.authenticatedUser.user_id, params.device_id, counterType);

                } else {
                    userDeviceCounterModel.clearAllDeviceCounterForUser(param.authenticatedUser.user_id, params.device_id);
                }

                // if (counterType != 'alert_counter') {
                //     seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
                //     user_id='${params.authenticatedUser.user_id}'
                //     AND log_object_id='${params.device_id}'
                //     AND log_sub_type='${counterType}'
                //     AND log_type='alert_active'
                //     AND (
                //         seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
                //         OR 
                //         seen_by IS NULL 
                //     )
                //     `;
                // } else {
                //     seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
                //     user_id='${params.authenticatedUser.user_id}'
                //     AND log_object_id='${params.device_id}'
                //     AND log_type='alert_active'
                //     AND (
                //         seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
                //         OR 
                //         seen_by IS NULL 
                //     )
                //     `;
                // }



                query += ` AND ((l.log_object_id ='${param.device_id}' AND l.user_id='${param.authenticatedUser.user_id}' AND l.log_type='alert_active' )

                    )`;



            } else {
                userDeviceCounterModel.clearAllDeviceCounter(param.authenticatedUser.user_id);

                query += ` AND ((l.log_object_id IN (
                    SELECT device_id from spike_panel_devices where panel_id IN (
                        SELECT panel_id from spike_panels where room_id IN (
                            SELECT room_id from spike_rooms where room_users LIKE '%${param.authenticatedUser.user_id}%'
                        )
                    )
                ) AND l.user_id='${param.authenticatedUser.user_id}' AND l.log_type='alert_active' ) OR (l.log_object_id = '${homeControllerDeviceId}' AND  l.user_id='${param.authenticatedUser.user_id}')

                    )`;

                // seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + param.authenticatedUser.user_id + `' WHERE 
                // user_id='${param.authenticatedUser.user_id}'
                // AND (log_type='alert_active' OR log_object_id = '${homeControllerDeviceId}')
                // AND (
                //     seen_by NOT like '%` + param.authenticatedUser.user_id + `%'
                //     OR 
                //     seen_by IS NULL 
                // )
                // `;
            }



            // }


        } else {
            query += " AND slc.log_action IN ('" + applicableActions.join("','") + "')";
        }

        /**
         * If need details of all rooms and all devices and all panels
         */
        if (param.filter_type == 'room') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['room', 'mood', 'panel', 'device'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


            if (param.device_id && param.device_id.trim().length > 0) {

                query += ` AND (
                    l.log_object_id='` + param.device_id + `'
                )`;

            } else if (param.panel_id && param.panel_id.trim().length > 0) {

                query += ` AND (
                    l.log_object_id ='` + param.panel_id + `'
                    OR 
                    l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id = '` + param.panel_id + `')
                )`;

            } else if (param.room_id && param.room_id.trim().length > 0) {

                query += ` AND (
                    l.log_object_id = '` + param.room_id + `' 
                    OR 
                    l.log_object_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `')
                    OR 
                    l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `'))
                )`;
            }
            // 
            // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

        }


        /**
         * If need details of all rooms 
         */
        if (param.filter_type == 'mood') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['mood'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


            if (param.mood_id && param.mood_id.trim().length > 0) {

                query += ` AND (
                    l.log_object_id = '` + param.mood_id + `' 
                )`
            }

            // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

        }

        /**
         * @param room_id - if need details of all panels of rooms
         * @param panel_id -if need details of that particular panel
         */
        if (param.filter_type == 'panel') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['panel', 'device'].includes(item.log_parent_type);
            }).map(item => item.log_type);

            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


            if (!param.panel_id || param.panel_id.trim().length == 0) {


                query += ` AND (
                                l.log_object_id = '` + param.room_id + `' 
                                OR 
                                l.log_object_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `')
                                OR 
                                l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN (SELECT panel_id from spike_panels where room_id='` + param.room_id + `'))
                            )`

            } else {


                query += ` AND (
                                l.log_object_id ='` + param.panel_id + `'
                                OR 
                                l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN '` + param.panel_id + `')
                            )`;

            }

            // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

        }


        /**
         * @param device_id if need details of specific device
         * @param panel_id if need details of all devices of that specific panel
         */
        if (param.filter_type == 'device') {
            logger.info('FILTER TYPE : ', param.filter_type);
            logger.info('FILTER ACTION : ', param.filter_action)
            logger.info('LOG SUB TYPE : ', param.log_sub_type)
            
            let applicableLogTypes = categoryList.filter((item) => {
                return ['device'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"

            if(param.log_sub_type && param.log_sub_type!=''){
                query+= `AND l.log_sub_type IN ('${param.log_sub_type}') `;
            }


            if (!param.device_id || param.device_id.trim().length == 0) {

                query += ` AND (
                                l.log_object_id='` + param.panel_id + `'
                                OR 
                                l.log_object_id IN ( SELECT device_id from spike_panel_devices where panel_id IN ='` + param.panel_id + `')
                            )
                `

            } else {

                query += ` AND (
                                l.log_object_id='` + param.device_id + `'
                            )
                `;

            }


        }

        /**
         * If Filter is Related To Gas Sensor / Temp Sensor / Door Sensor
         */

        if (param.filter_type == 'all-sensor') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['device'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


            if (!param.device_id) {

                query += ` AND (                               
                                l.log_object_id IN ( SELECT device_id from spike_devices where device_type IN ('door_sensor','temp_sensor','gas_sensor','co2_sensor','water_detector','lock') ) OR
                                l.log_object_id IN (SELECT alert_id from spike_alerts WHERE device_id IN (SELECT device_id from spike_devices where device_type='` + param.filter_type + `'))
                            )
                `

            } else {

                query += ` AND (
                                l.log_object_id='` + param.device_id + `'
                            )
                `;
            }

        }

        if (['temp_sensor', 'gas_sensor', 'co2_sensor','door_sensor', 'water_detector', 'lock'].includes(param.filter_type)) {
            logger.info('IN DOOR NOTIFICATION');
            let applicableLogTypes = categoryList.filter((item) => {
                return ['device'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"


            if (!param.device_id) {

                query += ` AND (                               
                                l.log_object_id IN ( SELECT device_id from spike_devices where device_type='` + param.filter_type + `') OR
                                l.log_object_id IN (SELECT alert_id from spike_alerts WHERE device_id IN (SELECT device_id from spike_devices where device_type='` + param.filter_type + `'))
                            )
                `

            } else {

                query += ` AND (
                                l.log_object_id='` + param.device_id + `'
                            )
                `;

            }

            // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

        }

        /**
         * Camera Log File
         */

        if (['camera'].includes(param.filter_type)) {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['camera'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
            // if (extraLogLypes.length > 0) query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')";

            if (!param.camera_id) {
                query += ` AND  l.log_object_id IN ( SELECT camera_id from mst_camera_devices )`
            } else {
                query += ` AND (
                                l.log_object_id='` + param.camera_id + `'
                            )
                `;
            }

        }


        /**
         * @param alert_id
         */
        if (param.filter_type == 'alert') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['alert'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
            // query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')"

            if (param.alert_id) {

                query += ` AND (
                    l.log_object_id='` + param.alert_id + `'
                )
                    `;
            }

        }

        /**
         * @param alert_id
         */
        if (param.filter_type == 'schedule') {

            let applicableLogTypes = categoryList.filter((item) => {
                return ['schedule'].includes(item.log_parent_type);
            }).map(item => item.log_type);
            applicableLogTypes.push(...extraLogLypes);
            query += " AND l.log_type IN ('" + applicableLogTypes.join("','") + "')"
            // query += " OR l.log_type IN ('" + extraLogLypes.join("','") + "')"

            if (params.schedule_id) {

                query += ` AND (
                    l.log_object_id='` + param.schedule_id + `'
                )
                    `;
            }

        }

        if (param.start_date) {

            logger.info('Start Date : ', param.start_date);

            let startTimestamp = moment(param.start_date, 'YYYY-MM-DD HH:mm:ss').toDate().valueOf().toString();
            let endTimestamp = moment(param.end_date, 'YYYY-MM-DD HH:mm:ss').toDate().valueOf().toString();



            query += ' AND CAST(l.created_at as REAL) > ' + startTimestamp;
            query += ' AND CAST(l.created_at as REAL) < ' + endTimestamp;

        } else {
            // var startTimestamp = moment().startOf('day').toDate().valueOf().toString();
            // query += ' AND CAST(l.created_at as REAL) > '+startTimestamp;
        }

        if (param.unseen && param.unseen == 1) {

            query += ` AND (
                l.seen_by NOT like '%` + param.authenticatedUser.user_id + `%'
                OR 
                l.seen_by IS NULL 
            )`;

            param.timestamp = new Date().valueOf();
        }

        if (!param.timestamp) {
            param.timestamp = new Date().valueOf();
        }

        query += ' AND CAST(l.created_at as REAL) < ' + param.timestamp;
        if (param.filter_type === 'all-general-notifications') {
            query += ' order by l.seen_by ASC, CAST(l.created_at as REAL) DESC LIMIT 0,25'
        } else {
            query += ' order by CAST(l.created_at as REAL) DESC LIMIT 0,25';
        }

        logger.info('LOG QUERY', query);

        let logs = await dbManager.all(query, []);

        if (logs.length == 0) {

            if (param.start_date) {
                param.start_date = moment(param.start_date, 'YYYY-MM-DD HH:mm:ss').utc().format('YYYY-MM-DD HH:mm:ss');
            }

            if (param.end_date) {
                param.end_date = moment(param.end_date, 'YYYY-MM-DD HH:mm:ss').utc().format('YYYY-MM-DD HH:mm:ss');
            }


            let args = {
                data: param,
                headers: {
                    "Content-Type": "application/json"
                }
            };

            restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/logs/find", args, async function (data, response) {
                logger.info('[LOG CONTROLLER] - LOGS RECEIVED FROM CLOUD', data);
                if (data.code == 200) {
                    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data.data)) : null;
                } else {
                    return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
                }
            });

        } else {

            logger.info('[LOG CONTROLLER] - LOGS RECEIVED FROM LOCAL', logs.filter(function(item){
                return item.seen_by==null;
            }));

            logs = logFormatter.formatList(logs);
            cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, logs));
        }

        // if (seenQuery != null) {
        //     dbManager.executeNonQuery(seenQuery);
        // }




    } catch (error) {
        logger.error('Log Controller | Log Find API', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Mark Seen API
 * @param log_type (room,device,camera)
 * @param room_id - if log_type is room
 * @param camera_id - if log_type is room
 * @param device_id  - if log_type is device
 * @param log_sub_type - for temp_alert and humidity_alert
 */
exports.markSeen = async (params, cb) => {

    logger.info('Log Mark Seen API', params);
    try {
        const param = JSON.parse(JSON.stringify(params).replace(/'/g, ""));
        let seenQuery = null;

        if (param.log_type = "all-general-notifications") {
            let counterType = 'alert_counter';
            if (params.log_sub_type) {
                counterType = params.log_sub_type;
            }

            if (param.device_id) {
                if (['humidity_alert', 'temp_alert'].includes(counterType)) {
                    const specificCounter = await userDeviceCounterModel.clearDeviceCounter(param.authenticatedUser.user_id, params.device_id, counterType);

                } else {
                    userDeviceCounterModel.clearAllDeviceCounterForUser(param.authenticatedUser.user_id, params.device_id);
                }

                if (counterType != 'alert_counter') {
                    seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
                    user_id='${params.authenticatedUser.user_id}'
                    AND log_object_id='${params.device_id}'
                    AND log_sub_type='${counterType}'
                    AND log_type='alert_active'
                    AND (
                        seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
                        OR 
                        seen_by IS NULL 
                    )
                    `;
                } else {
                    seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + params.authenticatedUser.user_id + `' WHERE 
                    user_id='${params.authenticatedUser.user_id}'
                    AND log_object_id='${params.device_id}'
                    AND log_type='alert_active'
                    AND (
                        seen_by NOT like '%` + params.authenticatedUser.user_id + `%'
                        OR 
                        seen_by IS NULL 
                    )
                    `;
                }
            } else {
                userDeviceCounterModel.clearAllDeviceCounter(param.authenticatedUser.user_id);
                seenQuery = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || '` + param.authenticatedUser.user_id + `' WHERE 
                user_id='${param.authenticatedUser.user_id}'
                AND (log_type='alert_active' OR log_object_id = '${homeControllerDeviceId}')
                AND (
                    seen_by NOT like '%` + param.authenticatedUser.user_id + `%'
                    OR 
                    seen_by IS NULL 
                )
                `;
            }
        }

        if (seenQuery != null) {
            dbManager.executeNonQuery(seenQuery);
        }
        // const param = {
        //     "user_id": params.user_id.replace(/'/g, ""),
        //     "log_type": params.log_type.replace(/'/g, ""),
        //     "room_id": params.room_id.replace(/'/g, ""),
        //     "camera_id": params.camera_id.replace(/'/g, ""),
        //     "device_id": params.device_id.replace(/'/g, ""),            
        // }

        logger.info('Sanitized Params Mark Seen', param);
        // logModel.markSeen(param.user_id, param);
        // userDeviceCounterModel.clearDeviceCounter(params.user_id, params.device_id);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Log Controller | Mark Seen API', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Log Categories List API
 */
exports.listLogCategories = async (params, cb) => {

    try {
        let categoryList = await logModel.categories();
        let response = [];
        response.push({
            filter_name: "All",
            action_name: "add,alert_add,on,off,delete,alert_delete,enable,disable,panel_on,panel_off,alert_update"
        });

        parentList = [...new Set(categoryList.map(item => item.log_parent_type))];

        for (let i = 0; i < parentList.length; i++) {

            if (parentList[i] == 'camera') {
                continue;
            }

            let actions = categoryList.filter((item) => {
                return item.log_parent_type == parentList[i]
            });

            actions = actions.filter((item) => {
                return item.log_subtype != 'sensor';
            }).map(item => item.log_action);

            response.push({
                filter_name: capitalizeFirstLetter(parentList[i]),
                action_name: actions.join(",")
            });

        }

        /**
         * Sensor Categories
         */
        let sensorResponse = []

        // Gas Sensor
        sensorResponse.push({
            filter_name: capitalizeFirstLetter("all"),
            action_name: "add,update,delete,door_open,door_close,gas_detected,water_detected,door_lock,door_unlock"
        });

        sensorResponse.push({
            filter_name: capitalizeFirstLetter("gas sensor"),
            action_name: "add,update,delete,gas_detected"
        });

        sensorResponse.push({
            filter_name: capitalizeFirstLetter("temp sensor"),
            action_name: "add,update,delete,temp_alert,alert_add,alert_delete"
        });

        sensorResponse.push({
            filter_name: capitalizeFirstLetter("door sensor"),
            action_name: "add,update,delete,door_open,door_close,alert_add,alert_delete"
        });

        sensorResponse.push({
            filter_name: capitalizeFirstLetter("lock"),
            action_name: "add,update,delete,door_lock,door_unlock"
        });

        sensorResponse.push({
            filter_name: capitalizeFirstLetter("water detector"),
            action_name: "add,update,delete,water_detected"
        });

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, {
            general: response,
            sensor: sensorResponse,
            camera: [{
                filter_name: capitalizeFirstLetter("camera"),
                action_name: "add,update,delete,person_detected,inactive,active"
            }]
        }));

    } catch (error) {
        logger.error('Log Controller | Log Categories List API', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}