const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const constants = require('./../util/constants');

// Models
const deviceModel = require('../models/device');
const logModel = require('../models/logs');
const userModel = require('../models/user');
const alertModel = require('../models/device_alerts');
const generalMetaModel = require('../models/general_meta');
const firebaseUtil = require('../util/firebase-util');

// const { param } = require('express-validator');


/**
 * Add Alert API
 * @param device_id
 * @param user_id
 * @param days (comma seperated string)
 * @param start_time (24Hr Format) - Only Door Sensor
 * @param end_time (24Hr Format) - Only Door Sensor
 * @param alert_type (temperature , humidity, door_open_close, water_detected)
 */
exports.addAlert = async (params, cb) => {

    try {

        // Added to match old code
        params.days = params.days ? params.days.split(",") : null;

        // Check if device exists
        let device = await deviceModel.get(params.device_id);
        if (device == null) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Device doesnt exist."
            }));
        }

        // Temperature Sensor On Open Close
        if (device.device_type == generalUtil.deviceTypes.temp_sensor.device_type) {

            /**
             * alert_type
             * min_temp if alert_type =='temperature'
             * max_temp if alert_type =='temperature'
             * min_humidity if alert_type =='humidity'
             * max_humidity if alert_type =='humidity'
             */

            if (params.alert_type == generalUtil.alertTypes.temp_sensor.temperature) {

                let config = {};

                if (params.min_temp) config.min_temp = params.min_temp;
                if (params.max_temp) config.max_temp = params.max_temp;

                const alertData = await alertModel.add({
                    device_id: device.device_id,
                    user_id: params.authenticatedUser.user_id,
                    alert_type: generalUtil.alertTypes.temp_sensor.temperature,
                    days: params.days,
                    meta: {},
                    config: config
                });

                try {

                    logModel.add({
                        log_object_id: alertData.alert_id,
                        log_type: "alert_add"
                    }, params.authenticatedUser);

                } catch (error) {
                    logger.error('Alert Controller | Add Alert', error);
                }


            } else if (params.alert_type == generalUtil.alertTypes.temp_sensor.humidity) {

                let config = {};

                if (params.min_humidity) config.min_humidity = params.min_humidity;
                if (params.max_humidity) config.max_humidity = params.max_humidity;

                const alertData = await alertModel.add({
                    device_id: device.device_id,
                    user_id: params.authenticatedUser.user_id,
                    alert_type: generalUtil.alertTypes.temp_sensor.humidity,
                    days: params.days,
                    meta: {},
                    config: config
                });

                try {

                    logModel.add({
                        log_object_id: alertData.alert_id,
                        log_type: "alert_add"
                    }, params.authenticatedUser);

                } catch (error) {
                    logger.error('Alert Controller | Add Alert', error);
                }
            }

        }

        // Door Sensor Alert On Open Close
        if (device.device_type == generalUtil.deviceTypes.door_sensor.device_type) {

            let meta = {};

            const alertData = await alertModel.add({
                device_id: device.device_id,
                user_id: params.authenticatedUser.user_id,
                alert_type: generalUtil.alertTypes.door_sensor.door_open_close,
                days: params.days,
                start_time: params.start_time.replace(":", ""),
                end_time: params.end_time.replace(":", ""),
                meta: meta,
                config: {}
            });

            try {

                logModel.add({
                    log_object_id: alertData.alert_id,
                    log_type: "alert_add"
                }, params.authenticatedUser);

            } catch (error) {
                logger.info('Alert Controller | Add Log', error);
            }

        }

        // Door Sensor Alert On Open Close
        if (device.device_type == generalUtil.deviceTypes.water_detector.device_type) {

            const alertList = await alertModel.getAlertsByDeviceId(device.device_id);
            const userAlerts = alertList.filter(function (item) {
                return item.user_id == params.authenticatedUser.user_id
            });

            if (userAlerts.length > 0) {
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Alert is already enabled for this sensor"
                }));
            }

            let meta = {};

            const alertData = await alertModel.add({
                device_id: device.device_id,
                user_id: params.authenticatedUser.user_id,
                alert_type: generalUtil.alertTypes.water_detector.water_detected,
                days: ['0', '1', '2', '3', '4', '5', '6'],
                start_time: '0000',
                end_time: '2359',
                meta: meta,
                config: {}
            });

            logModel.add({
                log_object_id: alertData.alert_id,
                log_type: "alert_add"
            }, params.authenticatedUser);


        }

        // Door Lock / Unlock Alert
        if (device.device_type == generalUtil.deviceTypes.yale_lock.device_type) {

            let meta = {};

            const alertData = await alertModel.add({
                device_id: device.device_id,
                user_id: params.authenticatedUser.user_id,
                alert_type: generalUtil.alertTypes.lock.door_lock_unlock,
                days: params.days,
                start_time: params.start_time.replace(":", ""),
                end_time: params.end_time.replace(":", ""),
                meta: meta,
                config: {}
            });

            logModel.add({
                log_object_id: alertData.alert_id,
                log_type: "alert_add"
            }, params.authenticatedUser);


        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Alert Controller | Add Alert Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

/**
 * Edit Alert API
 * @param alert_id
 * @param days (comma seperated string)
 * @param start_time (24Hr Format) - Only Door Sensor
 * @param end_time (24Hr Format) - Only Door Sensor
 * @param is_active ('y','n')
 */
exports.editAlert = async (params, cb) => {

    //logger.info('edit alert API', params);

    try {

        let alert = await alertModel.get(params.alert_id);
        if (alert == null) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Alert not Found."
            }));
        }

        if (!params.authenticatedUser.admin && alert.user_id != params.authenticatedUser.user_id) {
            return cb(null, appUtil.createErrorResponse({
                code: 420,
                message: 'You don\'t have access to perform this task.'
            }))
        }

        let device = await deviceModel.get(alert.device_id);

        let updateData = {};

        if (params.days) {
            updateData.days = params.days;
        }
        if (params.is_active) updateData.is_active = params.is_active;

        // Temperature Sensor : Temperature Alert
        if (device && device.device_type == generalUtil.deviceTypes.temp_sensor.device_type && alert.alert_type == generalUtil.alertTypes.temp_sensor.temperature) {

            let config = {};
            if (params.min_temp) config.min_temp = params.min_temp;
            if (params.max_temp) config.max_temp = params.max_temp;

            updateData.config = config;

        }

        // Temperature Sensor : Humidity Alert
        if (device && device.device_type == generalUtil.deviceTypes.temp_sensor.device_type && alert.alert_type == generalUtil.alertTypes.temp_sensor.humidity) {

            let config = {};
            if (params.min_humidity) config.min_humidity = params.min_humidity;
            if (params.max_humidity) config.max_humidity = params.max_humidity;

            updateData.config = config;

        }

        if (device && device.device_type == generalUtil.deviceTypes.door_sensor.device_type && alert.alert_type == generalUtil.alertTypes.door_sensor.door_open_close) {

            // Nothing to change in meta at the moment
            if (params.start_time) updateData.start_time = params.start_time.replace(":", "");
            if (params.end_time) updateData.end_time = params.end_time.replace(":", "");

        }

        if (device && device.device_type == generalUtil.deviceTypes.yale_lock.device_type && alert.alert_type == generalUtil.alertTypes.lock.door_lock_unlock) {

            // Nothing to change in meta at the moment
            if (params.start_time) updateData.start_time = params.start_time.replace(":", "");
            if (params.end_time) updateData.end_time = params.end_time.replace(":", "");

        }

        await alertModel.update(alert.alert_id, updateData);
        // Update Alert Log
        logModel.add({
            log_object_id: alert.alert_id,
            log_type: "alert_update"
        }, params.authenticatedUser);

        // If alert is enabled or disabled logs
        if (params.is_active == 'y') {

            logModel.add({
                log_object_id: params.alert_id,
                log_type: "alert_enable"
            }, params.authenticatedUser);

        } else if (params.is_active == 'n') {

            logModel.add({
                log_object_id: params.alert_id,
                log_type: "alert_disable"
            }, params.authenticatedUser);

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Alert Controller | Edit Alert Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete Alert API
 * @param alert_id (required)
 * @param device_id
 * @param authenticatedUser (Optional : computed from authMiddleware)
 */
exports.deleteAlert = async (params, cb) => {

    try {

        // If wants to delete by device id
        if (params.device_id) {
            await alertModel.deleteByUserIdAndDeviceId(params.authenticatedUser.user_id, params.device_id);
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
        }

        // If wants to delete by alert id
        let alert = await alertModel.get(params.alert_id);
        if (alert == null) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Alert not Found."
            }));
        }

        if (!params.authenticatedUser.admin && alert.user_id != params.authenticatedUser.user_id) {
            return cb(null, appUtil.createErrorResponse({
                code: 420,
                message: 'You don\'t have access to pertform this task.'
            }))
        }

        await alertModel.delete(params.alert_id);
        logModel.add({
            // alert_id: alert.alert_id,
            log_type: "alert_delete",
            deleted_data: JSON.stringify(alert)
        }, params.authenticatedUser);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error('Alert Controller | Delete Alert Error', error);
        return cb(null, appUtil.createErrorResponse(constants.r.esponseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * This Sends Push Notifications to Devices - Privately Called 
 * @param device (deviceModel)
 * @param status (device_status, device_sub_status)
 * @param activity_type (only in case of yale lock)
 */
exports.handleStatusChange = async (params, cb) => {

    //logger.info('Alert Controller | Handle Status Change', params);

    let device = params.device;
    device.device_meta = device.device_meta ? JSON.parse(device.device_meta) : {};

    // Temperature Sensor
    if (device.device_type == generalUtil.deviceTypes.temp_sensor.device_type) {

        /**
         * Temperature Push Notification Logic
         */
        let temperatureAlerts = await alertModel.getDeviceAlertByDeviceType(
            device, {
            alert_type: generalUtil.alertTypes.temp_sensor.temperature,
            status: params.status.device_status
        });

        // Only Get Alerts that are enabled by user at master level
        temperatureAlerts = temperatureAlerts.filter(function (item) {
            return item.is_notification_enable == null || item.is_notification_enable.toString() == '1';
        });

        // Send Temperature Notifications
        if (temperatureAlerts.length > 0) {

            for (let tempAlert of temperatureAlerts) {

                let alertConfig = await alertModel.config(tempAlert.alert_id);

                let min_temp = alertConfig.find((item) => {
                    return item.param_name == 'min_temp';
                }).param_value;
                let max_temp = alertConfig.find((item) => {
                    return item.param_name == 'max_temp';
                }).param_value;


                // if(min_temp < params.status.device_status && max_temp > params.status.device_status){
                //     continue;
                // }



                let message = '';
                if (device.meta_unit == 'F') {

                    let convertedStatus = (params.status.device_status * 1.8 + 32).toFixed(1);

                    let minTemp = (min_temp * 1.8 + 32).toFixed(1);
                    let maxTemp = (max_temp * 1.8 + 32).toFixed(1);

                    message = device.device_name + ' ' +
                        convertedStatus + ' °F exceeded limit of ' + minTemp +
                        '°F~' + maxTemp + '°F';

                    // message = device.device_name + ' [' + tempAlert.room_name + '] : ' +
                    //     convertedStatus + '°F exceeded limit of ' + minTemp +
                    //     '°F~' + maxTemp + '°F';

                } else {

                    message = device.device_name + ' ' +
                        params.status.device_status + ' °C exceeded limit of ' +
                        min_temp + '°C~' + max_temp + '°C';
                }



                // logModel.add({
                //     log_type: "alert_active",
                //     log_object_id: tempAlert.alert_id
                // });

                await logModel.add({
                    log_sub_type: 'temp_alert',
                    log_type: "alert_active",
                    log_object_id: device.device_id,
                    sub_description: message
                }, {
                    user_id: tempAlert.user_id,
                    phone_id: "0000",
                    phone_type: "SYSTEM"
                }, {
                    sub_description: message
                });

                await userModel.increaseBadgeCountByUserId(tempAlert.user_id);

                firebaseUtil.sendMessageNotification({
                    message: message,
                    user_id: tempAlert.user_id,
                    data: {},
                    badge_count: 1
                    // badge_count: tempAlert.badge_count + 1
                });


                // await logModel.add({
                //     log_type: "temp_alert",
                //     log_object_id: device.device_id
                // });

            }

            // for(let alert of temperatureAlerts)
            // {
            //     socketio.emit('updateDeviceBadgeCounter', {
            //         user_id:alert.user_id,
            //         device_id:alert.device_id,
            //         counter: await logModel.getUnseenDeviceAlertsCount(alert.user_id,alert.device_id)
            //     });
            // }

        }

        /**
         * Humidity Push Notification Logic
         */
        let humidityAlerts = await alertModel.getDeviceAlertByDeviceType(device, {
            alert_type: generalUtil.alertTypes.temp_sensor.humidity,
            status: params.status.device_sub_status
        });

        // Only Get Alerts that are enabled by user at master level
        humidityAlerts = humidityAlerts.filter(function (item) {
            return item.is_notification_enable == null || item.is_notification_enable.toString() == '1';
        });

        // Send Humidity Alerts
        if (humidityAlerts.length > 0) {
            logger.info('IN HUMIDITY ALERT');

            let message = device.device_name + ' humidity :  ' + params.status.device_sub_status + '% ';
            // let message = device.device_name + ' [' + humidityAlerts[0].room_name + '] humidity :  ' + params.status.device_sub_status + '% ';

            // Log That Humidity Alert was active
            for (let humidityAlert of humidityAlerts) {

                // await logModel.add({
                //     log_type: "alert_active",
                //     log_object_id: humidityAlert.alert_id
                // },null,{
                //     sub_description:message
                // });

                await logModel.add({
                    log_sub_type: 'humidity_alert',
                    log_type: "alert_active",
                    log_object_id: device.device_id,
                    sub_description: message
                }, {
                    user_id: humidityAlert.user_id,
                    phone_id: "0000",
                    phone_type: "SYSTEM"
                }, {
                    sub_description: message
                });

                await userModel.increaseBadgeCountByUserId(humidityAlert.user_id);

                firebaseUtil.sendMessageNotification({
                    message: message,
                    user_id: humidityAlert.user_id,
                    data: {},
                    badge_count: 1
                });

            }
        }
    }

    /**
     * Door Sensor Push Notification Logic
     */
    if (device.device_type == generalUtil.deviceTypes.door_sensor.device_type) {

        let doorAlerts = await alertModel.getDeviceAlertByDeviceType(device, {
            alert_type: generalUtil.alertTypes.door_sensor.door_open_close
        });

        // Only Get Alerts that are enabled by user at master level
        doorAlerts = doorAlerts.filter(function (item) {
            return item.is_notification_enable == null || item.is_notification_enable.toString() == '1';
        });

        if (doorAlerts.length > 0) {

            // Send Push Notification

            let message;
            if (params.status.device_status == 1) {
                message = 'Unexpected Activity: ' + device.device_name + " was closed. ";
            } else {
                message = 'Unexpected Activity: ' + device.device_name + " was opened. ";
            }


            for (let doorAlert of doorAlerts) {

                await logModel.add({
                    log_sub_type: params.status.device_status == 1 ? 'door_closed' : 'door_open',
                    log_type: "alert_active",
                    log_object_id: device.device_id,
                    sub_description: message
                }, {
                    user_id: doorAlert.user_id,
                    phone_id: "0000",
                    phone_type: "SYSTEM"
                }, {
                    sub_description: message
                });

                await userModel.increaseBadgeCountByUserId(doorAlert.user_id);

                firebaseUtil.sendMessageNotification({
                    message: message,
                    user_id: doorAlert.user_id,
                    data: {},
                    badge_count: 1
                });

            }

        }

        await logModel.add({
            log_type: params.status.device_status == 1 ? "door_close" : "door_open",
            log_object_id: device.device_id
        });


    }

    /**
     * Gas Sensor Push Notification Logic
     */
    if (device.device_type == generalUtil.deviceTypes.gas_sensor.device_type) {

        if (params.status.device_status == 1) {

            const devicePanelRoomName = await deviceModel.getDevicePanelRoomName(device.device_id);

            if (devicePanelRoomName && devicePanelRoomName.room_name != null) {
                const message = device.device_name + ' Gas Detected';
                // const message = device.device_name + ' [' + devicePanelRoomName.room_name + '] : Gas Detected';
                const userList = await userModel.list();
                for (let user of userList) {

                    if (await generalMetaModel.getMetaValue(user.user_id, generalUtil.userMetaList.gas_sensor_notification_enable, '1') == '1') {

                        await logModel.add({
                            log_sub_type: 'gas_detected',
                            log_type: "alert_active",
                            log_object_id: device.device_id,
                            sub_description: message
                        }, {
                            user_id: user.user_id,
                            phone_id: "0000",
                            phone_type: "SYSTEM"
                        }, {
                            sub_description: message
                        });

                        await userModel.increaseBadgeCountByUserId(user.user_id);

                        firebaseUtil.sendMessageNotification({
                            message: message,
                            user_id: user.user_id,
                            data: {},
                            // badge_count: user.badge_count + 1
                            badge_count: 1
                        });
                    }
                }
            }

            await logModel.add({

                log_type: "gas_detected",
                log_object_id: device.device_id
            });
        }
    }

    /**
     * Co2 Sensor Push Notification Logic
    */
    if (device.device_type == generalUtil.deviceTypes.co2_sensor.device_type) {

        if (params.status.device_status == 1) {

            const devicePanelRoomName = await deviceModel.getDevicePanelRoomName(device.device_id);

            if (devicePanelRoomName && devicePanelRoomName.room_name != null) {
                const message = device.device_name + ' Co2 Detected';
                // const message = device.device_name + ' [' + devicePanelRoomName.room_name + '] : Gas Detected';
                const userList = await userModel.list();
                for (let user of userList) {

                    if (await generalMetaModel.getMetaValue(user.user_id, generalUtil.userMetaList.co2_sensor_notification_enable, '1') == '1') {

                        await logModel.add({
                            log_sub_type: 'co2_detected',
                            log_type: "alert_active",
                            log_object_id: device.device_id,
                            sub_description: message
                        }, {
                            user_id: user.user_id,
                            phone_id: "0000",
                            phone_type: "SYSTEM"
                        }, {
                            sub_description: message
                        });

                        await userModel.increaseBadgeCountByUserId(user.user_id);

                        firebaseUtil.sendMessageNotification({
                            message: message,
                            user_id: user.user_id,
                            data: {},
                            // badge_count: user.badge_count + 1
                            badge_count: 1
                        });
                    }
                }
            }

            await logModel.add({

                log_type: "co2_detected",
                log_object_id: device.device_id
            });
        }
    }

    /**
     * Water Detectir Push Notification Logic
     */
    if (device.device_type == generalUtil.deviceTypes.water_detector.device_type) {

        if (params.status.device_status == 1) {

            let waterDetectedAlerts = await alertModel.getDeviceAlertByDeviceType(device, {
                alert_type: generalUtil.alertTypes.water_detector.water_detected
            });

            // Only Get Alerts that are enabled by user at master level
            waterDetectedAlerts = waterDetectedAlerts.filter(function (item) {
                return item.is_notification_enable == null || item.is_notification_enable.toString() == '1';
            });


            // for (let waterAlert of waterDetectedAlerts) {



            //     // await logModel.add({
            //     //     log_type: "alert_active",
            //     //     show: 0,
            //     //     log_object_id: waterAlert.alert_id
            //     // });
            // }

            const devicePanelRoomName = await deviceModel.getDevicePanelRoomName(device.device_id);

            if (devicePanelRoomName && devicePanelRoomName.room_name != null) {

                const message = device.device_name + ': Water Detected';
                // const message = device.device_name + ' [' + devicePanelRoomName.room_name + '] : Water Detected';

                if (waterDetectedAlerts.length > 0) {

                    for (let waterDetectedAlert of waterDetectedAlerts) {

                        await logModel.add({
                            log_sub_type: 'water_detected',
                            log_type: "alert_active",
                            log_object_id: device.device_id,
                            sub_description: message
                        }, {
                            user_id: waterDetectedAlert.user_id,
                            phone_id: "0000",
                            phone_type: "SYSTEM"
                        }, {
                            sub_description: message
                        });

                        await userModel.increaseBadgeCountByUserId(waterDetectedAlert.user_id);

                        firebaseUtil.sendMessageNotification({
                            message: message,
                            user_id: waterDetectedAlert.user_id,
                            data: {},
                            badge_count: 1
                        });
                    }
                }
            }

            await logModel.add({
                log_type: "water_detected",
                log_object_id: device.device_id
            });
        }
    }

    /**
     * Door Lock Push Notification Logic
     */
    if (device.device_type == generalUtil.deviceTypes.yale_lock.device_type) {

        let doorAlerts = await alertModel.getDeviceAlertByDeviceType(device, {
            alert_type: generalUtil.alertTypes.lock.door_lock_unlock
        });

        // Only Get Alerts that are enabled by user at master level
        doorAlerts = doorAlerts.filter(function (item) {
            return item.is_notification_enable == null || item.is_notification_enable.toString() == '1';
        });

        if (doorAlerts.length > 0) {

            // Send Push Notification
            let message;
            if (params.status.device_status == 1) {

                if (params.activity_type && params.activity_type == 'keypad') {
                    message = 'Unexpected Activity: ' + device.device_name + " was locked using keypad. ";
                } else if (params.activity_type && params.activity_type == 'autolock') {
                    message = 'Unexpected Activity: ' + device.device_name + " was locked by autolock. ";
                } else if (params.activity_type && params.activity_type == 'fingerprint_or_key') {
                    message = 'Unexpected Activity: ' + device.device_name + " was locked using finerprint or key. ";
                } else {
                    message = 'Unexpected Activity: ' + device.device_name + " was locked using app. ";
                    // } else {
                    //     message = 'Unexpected Activity: ' + device.device_name + " [" + doorAlerts[0].room_name + "] was locked using app. ";
                }

            } else {
                logger.info('ALERT PARAMS : ', params.activity_type)
                if (params.activity_type && params.activity_type == 'master_pin_code') {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using Master password.";
                } else if (params.activity_type && params.activity_type == 'user_pin_code') {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using Admin password.";
                } else if (params.activity_type && params.activity_type == 'onetime_pin_code') {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using Guest password.";
                } else if (params.activity_type && params.activity_type == 'fingerprint') {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using fingerprint.";
                } else if (params.activity_type && params.activity_type == 'key') {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using key.";
                } else {
                    message = 'Unexpected Activity: ' + device.device_name + " was unlocked using App.";
                }

            }

            for (let doorAlert of doorAlerts) {

                await logModel.add({
                    log_type: "alert_active",
                    log_object_id: device.device_id,
                    sub_description: message,
                    log_sub_type: params.activity_type
                }, {
                    user_id: doorAlert.user_id,
                    phone_id: "0000",
                    phone_type: "SYSTEM"
                }, {
                    sub_description: message
                });


                // await logModel.add({
                //     log_type: "alert_active",
                //     show: 0,
                //     log_object_id: doorAlert.alert_id
                // });

                await userModel.increaseBadgeCountByUserId(doorAlert.user_id);

                firebaseUtil.sendMessageNotification({
                    message: message,
                    user_id: doorAlert.user_id,
                    data: {},
                    // badge_count: doorAlert.badge_count + 1
                    badge_count: 1
                });
            }
        }
    }


    // Send alert counter of that specific device based on user id
    // const userDeviceBadgeCountList = await logModel.getUnseenDeviceAlertsCountofAllUsers(device.device_id);
    // for (let userBadge of userDeviceBadgeCountList) {
    //     socketio.to(userBadge.user_id).emit('updateDeviceBadgeCounter', {
    //         user_id: userBadge.user_id,
    //         device_id: device.device_id,
    //         counter: userBadge.total_unseen_count
    //     });
    //     logger.info('updateDeviceBadgeCounter', userBadge);
    // }

    // send log alert counter of the rooms based on user id
    // const userRoomUnreadAlertCounterList = await logModel.getUnseenRoomAlertCounterForAllUsersForSpecificDevice(device.device_id);
    // for (let userRoomUnreadAlert of userRoomUnreadAlertCounterList) {
    // socketio.to(userRoomUnreadAlert.user_id).emit('updateRoomAlertCounter', userRoomUnreadAlert);
    //logger.info('updateRoomAlertCounter', userRoomUnreadAlert);
    // }

    // const users = await userModel.list();

    // for (let user of users) {
    //     let count = await logModel.getAllUnseenAlertsCounter({user});
    //     socketio.to(user.user_id).emit('generalNotificationCounter', count);
    // }
}

/**
 * List of alerts by device id and alert type (API)
 * @param device_id (device_id of device)
 * @param alert_type (optional) (temperature, humidity, door_open_close)
 */
exports.listAlert = async (params, cb) => {

    try {
        let response = {};

        if (params.alert_type) {
            response = await alertModel.getAlertsByDeviceId(params.device_id, null, params.alert_type);
        } else {
            response = await alertModel.getAlertsByDeviceId(params.device_id);
        }

        for (let i = 0; i < response.length; i++) {
            response[i].start_time = response[i].start_time ? response[i].start_time.slice(0, 2) + ":" + response[i].start_time.slice(2) : null;
            response[i].end_time = response[i].end_time ? response[i].end_time.slice(0, 2) + ":" + response[i].end_time.slice(2) : null;
        }

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));

    } catch (error) {
        logger.error('Alert Controller | Alert List Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}