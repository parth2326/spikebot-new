const network = require('network');
// Shivam Added the general Util
const {
    homeAutomationModule,
    appContext,
    shared
} = require('./../util/general-util');

// module.exports = function (homeAutomationModule, appContextParam, shared) {
let appUtil = homeAutomationModule.appUtil;
let constants = homeAutomationModule.constants;
let sqliteDB = shared.db.sqliteDB;
let systemUtil = homeAutomationModule.systemUtil;
let unixUtil = homeAutomationModule.unixUtil;
// var appContext = appContextParam;

let viewerDetails = appContext.viewerDetails;
let homeControllerDeviceId = viewerDetails.deviceID;
let homeControllerIP = viewerDetails.deviceIP;
let currentDateTime = appUtil.currentDateTime();

const dbManager = require('./../util/db-manager');

exports.fixUser = function() {
    try {
        let vpn_details = require('/camera/vpnport.json');
        console.log('VPN DETAILS : ', vpn_details.profile_vpn_port);

        dbManager.run(`update mst_user set vpn_port = ${vpn_details.profile_vpn_port}, home_controller_device_id = ${homeControllerDeviceId}, is_sync = 0`);
        dbManager.run(`update mst_home_controller_list set home_controller_device_id = ${homeControllerDeviceId}, is_sync = 0`);
        
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
        return (null, response);
    } catch (error) {
        logger.error('System Controller |FIX USER API Error', error);
        return (null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

// Get Network Information
exports.getDHCPInfo = function (param) {
    try {
        let cb = param.cb;
        systemUtil.getDHCPInfo(function (error, result) {
            let response;
            if (error) {
                logger.error('getDHCPInfo Error while fetching dhcp info. Error: ', error);
                response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(response, null);
            } else {
                //logger.info("getDHCPInfo dhcp info fethed successfully");
                let data = {
                    dhcpInfo: result
                };
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);
            }
        });
    } catch (error) {
        logger.error('System Controller | Get DHCP Info Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

/**
 * This function will basically update the code from github and upload it to server
 */
exports.updateCode = async (params) => {

    try {
        let exec = require('child_process').exec;

        exec("sudo git --git-dir=/home/pi/node/homeController/.git --work-tree=/home/pi/node/homeController/.git pull", function (error, stdout, stderr) {

            //logger.info('pi reboot exec stdout: ', stdout);

            exec("sudo reboot now", function (error, stdout, stderr) {
                //logger.info('pi reboot exec stdout: ', stdout);
                //logger.info('pi reboot exec stderr: ', stderr);
                //logger.info('pi reboot exec error: ', error);
            });

        });
    } catch (error) {
        logger.error('System Controller | Update Code Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

// Get Network Info
exports.getNetworkInfo = function (param) {
    try {
        let cb = param.cb;
        sqliteDB.all(constants.SELECT_SYSTEM_INFO, function (error, result) {
            let response;
            if (error) {
                logger.error('getNetworkInfo Error while fetching system info. SELECT_SYSTEM_INFO Error: ', error);
                response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {
                systemUtil.getDHCPInfo(function (error, dhcpResult) {
                    let response;
                    if (error) {
                        logger.error('getDHCPInfo Error while fetching dhcp info. Error: ', error);
                        response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                        return cb(null, response);
                    } else {
                        //logger.info("getDHCPInfo dhcp info fethed successfully");
                        let data = {
                            networkInfo: result,
                            dhcpInfo: dhcpResult
                        };
                        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                        return cb(null, response);
                    }
                });
            }
        });
    } catch (error) {
        logger.error('System Controller | Get Network Info Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

exports.savestaticIP = function (device, cb) {
    try {
        let ip_address = device.ip_address,
            // net_mask = data.net_mask,
            // gateway_address = data.gateway_address,
            is_static = '1',
            is_dhcp = '0',
            deviceToSyncStatus = 0,
            currentDateTime = appUtil.currentDateTime();

        let updateHomeControllerIP = sqliteDB.prepare(constants.UPDATE_HOME_CONTROLLER_DEVICE_ID);
        updateHomeControllerIP.run(ip_address, is_static, is_dhcp, deviceToSyncStatus, currentDateTime, userEmail, function (error, result) {
            if (error) {
                logger.error('updateSystemInfo Error while updating controller id info. UPDATE_HOME_CONTROLLER_DEVICE_ID Error: ', error);
                response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {
                //logger.info("updateSystemInfo System info updated successfully");

                network.get_active_interface(function (error, ip) {
                    if (error) {
                        console.log(error);
                    } else {
                        exec("sudo chmod 777 /etc/network/interfaces", function (error1, stdout, stderr) {
                            if (error1 !== null) {
                                console.log("error");
                            } else {
                                fs.readFile('/etc/network/interfaces', 'utf8', function (err, data) {
                                    if (err) {
                                        console.log("fail 1");
                                    } else {
                                        let new_data = `auto lo\niface lo inet loopback\n\nauto eth0\niface eth0 inet static\naddress ${new_address}\nnetmask ${ip.netmask}\ngateway ${ip_address}\n\nauto wlan0\nallow-hotplug wlan0\niface wlan0 inet manual\nwpa-conf /etc/wpa_supplicant/wpa_supplicant.conf\niface default inet dhcp\n\n`;

                                        fs.writeFile('/etc/network/interfaces', new_data, 'utf8', function (error) {
                                            if (error) {
                                                console.log("fail 2");
                                            } else {
                                                exec("sudo reboot", function (error1, stdout, stderr) {
                                                    if (err) {
                                                        console.log("fail 3");
                                                    } else {
                                                        console.log("success");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        updateHomeControllerIP.finalize();
    } catch (error) {
        logger.error('System Controller | Save Static IP Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

exports.getLocalMacAddress = function (cb) {
    try {
        let data = {
            mac_address: homeControllerDeviceId
        };
        let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
        return cb(null, response);
    } catch (error) {
        logger.error('System Controller | Get Local MAC Address Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

exports.deletePreviousRecords = async function(cb) {
    try {
        const data = await dbManager.all("SELECT max(timestamp) as timestamp,created_date,device_id from spike_device_status_mapping group by created_date,device_id;");
        for (let i=0; i<data.length; i++){
            await dbManager.executeNonQuery('delete from spike_device_status_mapping where device_id = ? and timestamp < ? and created_date = ?', [data[i].device_id, data[i].timestamp, data[i].created_date]);
        }       
        return cb(null, responseCode.SUCCESS);
    } catch(error) {
        logger.error('System Controller | Delete Previous Record Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}