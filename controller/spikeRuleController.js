// const appUtil = require('./../util/app-util');
// const constants = require('./../util/constants');
// const dbManager = require('./../util/db-manager');

// exports.listInfo = async (params, cb) => {
//     try {
//         let query = `SELECT sd.device_id, sd.device_name, sp.panel_id, sp.panel_name, sr.room_id, sr.room_name, sd.device_type, sd. device_sub_type, 
//             ('[ ' || sr.room_name || ' ] - ' || sp.panel_name || ' - ' || sd.device_name) AS room_panel_name
//             FROM spike_devices sd
//             INNER JOIN spike_panel_devices spd ON spd.device_id = sd.device_id
//             INNER JOIN spike_panels sp ON spd.panel_id = sp.panel_id
//             INNER JOIN spike_rooms sr ON sp.room_id = sr.room_id
//             INNER JOIN spike_modules sm ON sd.module_id = sm.module_id
//             WHERE sr.room_users LIKE ? 
//                 and sm.is_configured = "y" and sm.is_active = "y" 
//                 and device_type IN ("switch", "fan", "temp_sensor",  "water_detector", "remote", "door_sensor", "curtain", "lock"); `;
//         const param = ['%' + params.authenticatedUser.user_id + '%'];
//         let response = await dbManager.all(query, param);

//         return cb(null, appUtil.createErrorResponse(constants.responseCode.SUCCESS, response));
//     } catch(error) {
//         logger.error('Spike Rule Controller | List Info Error', error);
//         return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
//     }
// }