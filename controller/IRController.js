const Client = require('node-rest-client').Client;
const restClient = new Client();
const mqtt = require('mqtt');
const deviceModel = require('./../models/device');
const eventsEmitter = require("./../util/event-manager");
const command_set = require('../IR/frontend_command.json');
let headers = {
    "Content-Type": "application/json"
};

// Shivam Added the general Util
const {
    homeAutomationModule,
    appContext,
    shared
} = require('./../util/general-util');


// module.exports = function (homeAutomationModule, appContext, shared, clientSocketObj) {
let appUtil = homeAutomationModule.appUtil,
    deviceUtil = homeAutomationModule.deviceUtil,
    cloudURL = homeAutomationModule.cloudURL,
    constants = homeAutomationModule.constants,
    sqliteDB = shared.db.sqliteDB,
    moment = shared.util.moment,
    notificationUtil = shared.util.notificationUtil,
    // socketio = shared.util.socket_io,
    CronJobManager = shared.util.CronJobManager,
    manager = new CronJobManager(),
    viewerDetails = appContext.viewerDetails,
    homeControllerDeviceId = viewerDetails.deviceID,
    homeControllerIP = viewerDetails.deviceIP;


//Send On off command
exports.sendOnOffCommand = async function (data, cb) {

    logger.info(data);

    try {
        let device = await deviceModel.get(data.ir_blaster_module_id);
        //   //logger.info("sendOnOffCommand", data);
        let ir_blaster_module_id = device.device_identifier,
            ir_code = data.ir_code,
            mode = data.mode;


        let codeset_command = JSON.parse(ir_code);

        let command;
        if(['tv','dth'].includes(data.remote_type)){

            const onOffCommand = codeset_command.find(function(item){
                return item.name == (mode=='ON'?'turn_on':'turn_off');
            })
            
            command = onOffCommand.value;

        }else{
            command = codeset_command.commands[mode];
        }
       

        if (ir_blaster_module_id.length == 0 || command.length == 0) {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else {
            let publish_module_id = 'PI/' + ir_blaster_module_id;

            let client = mqtt.connect('mqtt://127.0.0.1:1883');
            client.on('connect', () => {

                client.publish(publish_module_id, command, function () {
                    console.log("Message is published");
                    //client.end(); // Close the connection when published
                });
            });

            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
            return cb(null, response);
        }
    } catch (error) {
        logger.error('IR Controller | Send ON OFF Command Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};

//Execute Command From Remote
exports.sendRemoteCommand = function (data, cb) {
    try {
        let remote_id = data.remote_id,
            power = data.power, //On, Off
            mode = data.mode, //AUTO,LOW,MEDIUM,LIGH,DRY
            temperature = data.temperature,
            room_device_id = data.room_device_id,

            device_status = power == 'ON' ? 1 : 0,

            created_date = appUtil.currentDateTime(),
            currentDateTime = appUtil.currentDateTime(),
            command;

        if (power.length != 0 && mode.length != 0 && temperature.length != 0 && remote_id.length != 0) {
            let remoteDetailList = sqliteDB.prepare(constants.GET_REMOTE_DETAILS);
            remoteDetailList.all(remote_id, function (error, result) {
                if (error) {
                    logger.error("Get remote list details error ", error);
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(response, null);
                } else {
                    if (result.length > 0) {

                        let ir_blaster_module_id = result[0].ir_blaster_module_id,
                            remote_device_id = result[0].remote_device_id,
                            ir_code = JSON.parse(result[0].ir_code);

                        if (power == 'OFF') {
                            command = ir_code.commands[power];
                            sendCommand(ir_blaster_module_id, command, room_device_id, remote_device_id);
                        } else {
                            temp_command_set = ir_code.commands[mode];
                            command = temp_command_set[temperature];
                            sendCommand(ir_blaster_module_id, command, room_device_id, remote_device_id);
                        }

                    } else {
                        //logger.info("No Remote Found!");
                        response = appUtil.createErrorResponse(constants.responseCode.NO_REMOTE_FOUND);
                        return cb(response, null);
                    }
                }
            });
            remoteDetailList.finalize();
        } else {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        }

        function sendCommand(ir_blaster_module_id, command, room_device_id, remote_device_id) {
            if (command.length != 0 && ir_blaster_module_id.length != 0) {
                let checkRemotestatus = sqliteDB.prepare(constants.FIND_REMOTE_CHECK);
                checkRemotestatus.all(room_device_id, function (error, result) {
                    if (error) {
                        logger.error("Device Status Change Error : ", error);
                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                        return cb(null, response);
                    } else {
                        if (result.length > 0) {
                            let is_active = result[0].is_active;
                            if (is_active == 0) {
                                logger.warn("Module is dead! " + room_device_id);
                                return cb(null, 'dead');
                            } else {
                                let module_id = result[0].module_id,
                                    device_specific_value = result[0].device_specific_value,
                                    device_on_time = result[0].device_on_time,
                                    device_off_time = result[0].device_off_time,

                                    deviceOnOffTime = result[0].currentDateTime,
                                    userEmail = result[0].userEmail,
                                    deviceOffTime = device_status == 0 ? currentDateTime : device_off_time,
                                    device_sync = 0;

                                let time_diff = moment.utc(moment(device_on_time, "YYYY/MM/DD HH:mm:ss").diff(moment(deviceOffTime, "YYYY/MM/DD HH:mm:ss"))).format("HH:mm:ss");

                                let publish_module_id = 'PI/' + ir_blaster_module_id;
                                let subscribe_module_id = 'ESP/' + ir_blaster_module_id;

                                let client = mqtt.connect('mqtt://127.0.0.1:1883');

                                client.on('connect', () => {

                                    client.publish(publish_module_id, command, function () {
                                        console.log("Message is published");
                                        //client.end(); // Close the connection when published
                                    });

                                    client.subscribe(subscribe_module_id, rap = false, function () {
                                        client.on('message', function (topic, message, packet) {

                                            console.log("Received '" + message + "' on '" + topic + "'");

                                            if (message == '54321' && topic == subscribe_module_id) {

                                                //logger.info("remote_device_id", remote_device_id);

                                                eventsEmitter.emit('emitSocket', {
                                                    topic: 'ReloadDeviceStatusApp',
                                                    data: {
                                                        "module_id": ir_blaster_module_id,
                                                        "device_id": remote_device_id,
                                                        "device_status": device_status,
                                                        "is_locked": 0
                                                    }
                                                });

                                                // socketio.emit('ReloadDeviceStatusApp', {
                                                //     "module_id": ir_blaster_module_id,
                                                //     "device_id": remote_device_id,
                                                //     "device_status": device_status,
                                                //     "is_locked": 0
                                                // });
                                                let updateRemoteDb = sqliteDB.prepare(constants.UPDATE_IN_REMOTE_DB);
                                                updateRemoteDb.run(power, mode, temperature, created_date, remote_id, function (error, result) {
                                                    if (error) {
                                                        logger.error(" Update Remote Details Error ", error);
                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                        return cb(null, response);
                                                    } else {
                                                        let remoteDetailList = sqliteDB.prepare(constants.GET_REMOTE_DETAILS);
                                                        remoteDetailList.all(remote_id, function (error, remoteDetails) {
                                                            if (error) {
                                                                logger.error("Get remote list details error ", error);
                                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                return cb(response, null);
                                                            } else {
                                                                let updatedevicestatus = sqliteDB.prepare(constants.UPDATE_DEVICE_STATUS_FOR_REMOTE);
                                                                updatedevicestatus.run(deviceOnOffTime, time_diff, device_on_time, deviceOffTime, device_status, device_specific_value, userEmail, currentDateTime, device_sync, module_id, remote_device_id, function (error, result) {
                                                                    if (error) {
                                                                        logger.error("Change Device Status DB Error : ", error);
                                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                        return cb(null, response);
                                                                    } else {
                                                                        //Check Room & Panel Status and Update!
                                                                        let checkDuplicatedevices = sqliteDB.prepare(constants.FIND_PANEL_ROOM_DETAILS_FOR_REMOTE);
                                                                        checkDuplicatedevices.all(module_id, remote_device_id, function (error, result) {
                                                                            if (error) {
                                                                                logger.error('Change room panel status error ', error);
                                                                                response = appUtil.createSuccessResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                return cb(null, response);
                                                                            } else {
                                                                                if (result.length > 0) {
                                                                                    //  //logger.info("check room panel status");
                                                                                    result.forEach(function (device) {
                                                                                        homeAutomationController.checkRoomPanelStatus(device.panel_id, device.room_id);
                                                                                    });

                                                                                    let return_data = {
                                                                                        remote_currentStatus_details: remoteDetails[0]
                                                                                    };

                                                                                    client.unsubscribe(subscribe_module_id);

                                                                                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, return_data);
                                                                                    return cb(null, response);
                                                                                }
                                                                            }
                                                                        });
                                                                        checkDuplicatedevices.finalize();
                                                                    }
                                                                });
                                                                updatedevicestatus.finalize();
                                                            }
                                                        });
                                                        remoteDetailList.finalize();
                                                    }
                                                });
                                                updateRemoteDb.finalize();
                                            }
                                        });
                                    });

                                    setTimeout(function () {
                                        //logger.info("client ended");
                                        client.end();
                                    }, 10000);
                                });
                            }
                        } else {
                            //logger.info('Device Not Found Error');
                        }
                    }
                });
                checkRemotestatus.finalize();
            } else {
                //No Command Found for a remote
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(response, null);
            }
        }
    } catch (error) {
        logger.error('IR Controller | Send Remote Command Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};


//Add New Remote v2
exports.AddRemote = function (devices, cb) {
    // //logger.info("AddRemote", devices);

    /* room_device_id == remote_id
         device_id == remote_device_id
         device_name == remote_name
         module_id == ir_blaster_module_id
         room_panel_id == panel_id
         device_type == remote_type
         original_room_device_id == remote_id
    
        panel_name == ir_blaster name for blaster panel
        panel_id == ir_blaster_id */

    try {
        let remote_id = appUtil.generateRandomId(),
            device_type = devices.device_type,
            brand_name = devices.brand_name,
            remote_codeset_id = devices.remote_codeset_id,
            ir_code = devices.ir_code,
            model_number = devices.model_number,
            remote_name = devices.remote_name,
            ir_blaster_id = devices.ir_blaster_id,
            ir_blaster_module_id = devices.ir_blaster_module_id,
            room_id = devices.room_id,
            mode = devices.mode,
            temperature = devices.temperature,
            panel_name = 'Sensor Panel',
            panel_type = 2,
            is_original = 1,
            power = 'OFF',
            device_icon = 'Remote_AC',
            remote_type = 2,
            module_id = '',

            panel_id = appUtil.generateRandomId(),
            phone_id = devices.phone_id,
            phone_type = devices.phone_type,
            created_date = appUtil.currentDateTime(),
            user_id = devices.user_id,
            created_by = user_id,
            user_name = '';

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });


        if (ir_blaster_module_id.length == 0 || remote_name.length == 0 || ir_blaster_id.length == 0 || room_id.length == 0 || mode.length == 0 || temperature.length == 0 || brand_name.length == 0 || model_number.length == 0 || remote_codeset_id.length == 0 || ir_code.length == 0 || device_type.length == 0) {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
            // } else if (device_id == '1') {
        } else {
            let checkSensorPanelExist = sqliteDB.prepare(constants.CHECK_SENSOR_PANEL_EXIST);
            checkSensorPanelExist.all(room_id, panel_type, function (error, result) {
                if (error) {
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(null, response);
                } else {
                    if (result.length > 0) {
                        panel_id = result[0].panel_id;
                        insertRemoteDetails();
                    } else {
                        let insertPanelData = sqliteDB.prepare(constants.INSERT_PANEL_DATA);
                        insertPanelData.run(panel_id, module_id, room_id, panel_name, panel_type, user_id, homeControllerDeviceId, created_by, created_date, function (error, result) {
                            if (error) {
                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                return cb(null, response);
                            } else {
                                insertRemoteDetails();
                            }
                        });
                        insertPanelData.finalize();
                    }
                }
            });


            function insertRemoteDetails() {
                //logger.info('Panel ID ', panel_id);

                let duplicateRemoteName = sqliteDB.prepare(constants.CHECK_DUPLICATE_REMOTE_NAME);
                duplicateRemoteName.all(remote_name, function (error, result) {
                    if (error) {
                        logger.error("Duplicate Remote Name Error ", error);
                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                        return cb(null, response);
                    } else {
                        if (result.length > 0) {
                            logger.warn("Remote with Same Name Already Exist");
                            response = appUtil.createSuccessResponse(constants.responseCode.DUPLICATE_REMOTE_NAME);
                            return cb(null, response);
                        } else {
                            //logger.info('Adding new Remote...');
                            let remote_device_id = appUtil.generateRandomId();

                            let addACRemote = sqliteDB.prepare(constants.ADD_AC_REMOTE);
                            addACRemote.run(remote_id, power, mode, temperature, remote_device_id, device_type, brand_name, remote_codeset_id, ir_code, model_number, remote_name, device_icon, ir_blaster_id, room_id, panel_id, mode, temperature, created_by, created_date, function (error, result) {
                                if (error) {
                                    logger.error("Add Remote Error " + error);
                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                    return cb(null, response);
                                } else {
                                    let insertRemoteInDeviceMapping = sqliteDB.prepare(constants.INSERT_ROOM_REMOTE_MAPPING_WITH_DEVICE_TYPE);
                                    insertRemoteInDeviceMapping.run(remote_id, panel_id, remote_device_id, remote_name, ir_blaster_module_id, panel_id, device_icon, remote_type, is_original, remote_id, user_id, homeControllerDeviceId, created_by, created_date, function (error, result) {
                                        if (error) {
                                            logger.error("Insert Room Device Mapping Data Error ", error);
                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                            return cb(null, response);
                                        } else {
                                            let activity_type = 'Remote',
                                                activity_description = remote_name,
                                                activity_action = 'Added';
                                            //logger.info(activity_type, activity_action, activity_description);
                                            notificationUtil.addActivity({
                                                room_id: room_id,
                                                panel_id: '',
                                                module_id: remote_id, // modeule_id == remote_id for remote
                                                activity_type: activity_type,
                                                activity_description: activity_description,
                                                activity_action: activity_action,
                                                sensor_type: constants.sensor_type.remote,
                                                is_unread: 0,
                                                phone_id: phone_id,
                                                phone_type: phone_type,
                                                image_url: '',
                                                user_id: user_id,
                                                user_name: user_name,
                                                home_controller_device_id: homeControllerDeviceId
                                            });
                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_ADDED);
                                            return cb(null, response);
                                        }
                                    });
                                    insertRemoteInDeviceMapping.finalize();
                                }
                            });
                            addACRemote.finalize();
                        }
                    }
                });
            }
        }
    } catch (error) {
        logger.error('IR Controller | Add Remote Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};

//Update Remote Details v2
exports.updateRemoteDetails = function (devices, cb) {

    //logger.info("updateRemoteDetails", devices);

    try {
        let remote_id = devices.remote_id,
            remote_name = devices.remote_name,
            room_id = devices.room_id,
            ir_blaster_id = devices.ir_blaster_id,
            ir_blaster_name = devices.ir_blaster_name,
            mode = devices.mode,
            temperature = devices.temperature,
            user_id = devices.user_id,

            panel_name = 'Sensor Panel',
            panel_id = appUtil.generateRandomId(),
            panel_type = 2,
            is_active = 1,
            module_id = '',
            updateRemoteNewRoomId = 0,

            phone_id = devices.phone_id,
            phone_type = devices.phone_type,
            created_by = user_id,
            created_date = appUtil.currentDateTime(),


            userEmail = user_id;

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });


        if (remote_id.length == 0 || remote_name.length == 0 || room_id.length == 0 || ir_blaster_id.length == 0 || mode.length == 0 || temperature.length == 0 || ir_blaster_name.length == 0) {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else {
            let findRemoteDetails = sqliteDB.prepare(constants.FIND_REMOTE_DETAILS);
            findRemoteDetails.all(remote_id, function (error, result) {
                if (error) {
                    logger.error("Find Remote details error ", error);
                } else {
                    if (result.length > 0) {

                        let original_remote_name = result[0].remote_name,
                            original_room_id = result[0].room_id,
                            original_ir_blaster_id = result[0].ir_blaster_id,
                            original_mode = result[0].mode,
                            original_temperature = result[0].temperature,
                            original_panel_id = result[0].panel_id;


                        if (original_remote_name == remote_name && original_room_id == room_id && original_ir_blaster_id == ir_blaster_id && original_mode == mode && original_temperature == temperature) {
                            logger.warn('Same Details FOUND');
                            response = appUtil.createErrorResponse(constants.responseCode.NO_MSG);
                            return cb(null, response);
                        } else if ((original_remote_name != remote_name || original_mode != mode || original_temperature != temperature) && (original_room_id == room_id && original_ir_blaster_id == ir_blaster_id)) {
                            let checkDuplicateRemotename = sqliteDB.prepare(constants.CHECK_SAME_REMOTE_NAME);
                            checkDuplicateRemotename.all(remote_name, remote_id, function (error, result) {
                                if (error) {
                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                    return cb(null, response);
                                } else {
                                    if (result.length > 0) {
                                        logger.warn('Same Remote name detected!');
                                        response = appUtil.createErrorResponse(constants.responseCode.DUPLICATE_REMOTE_NAME);
                                        return cb(null, response);
                                    } else {
                                        let updateRemoteName = sqliteDB.prepare(constants.UPDATE_REMOTE_NAME_WITH_SCHEDULE_DETAILS);
                                        updateRemoteName.run(remote_name, mode, temperature, user_id, created_date, remote_id, function (error, result) {
                                            if (error) {
                                                logger.error(" Update Remote Details Error ", error);
                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                return cb(null, response);
                                            } else {
                                                let updateDeviceName = sqliteDB.prepare(constants.UPDATE_ROOM_REMOTE_MAPPING_WITH_REMOTE_NAME);
                                                updateDeviceName.run(remote_name, is_active, remote_id, function (error, result) {
                                                    if (error) {
                                                        logger.error(" Update Remote Details In Room Mapping Error ", error);
                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                        return cb(null, response);
                                                    } else {
                                                        let activity_type = 'Remote',
                                                            activity_description = remote_name,
                                                            activity_action = 'Updated';
                                                        //logger.info(activity_type, activity_action, activity_description);
                                                        notificationUtil.addActivity({
                                                            room_id: room_id,
                                                            panel_id: '',
                                                            module_id: remote_id,
                                                            activity_type: activity_type,
                                                            activity_description: activity_description,
                                                            activity_action: activity_action,
                                                            sensor_type: constants.sensor_type.remote,
                                                            is_unread: 0,
                                                            phone_id: phone_id,
                                                            phone_type: phone_type,
                                                            image_url: '',
                                                            user_id: user_id,
                                                            user_name: user_name,
                                                            home_controller_device_id: homeControllerDeviceId
                                                        });
                                                        response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                        return cb(null, response);
                                                        // var getRemoteInfo = sqliteDB.prepare(constants.GET_REMOTE_INFO);
                                                        // getRemoteInfo.all(remote_id, function (error, result) {
                                                        //     if (error) {
                                                        //         logger.error("Find IR Blaster Info Error ", error);
                                                        //         response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                        //         return cb(null, response);
                                                        //     } else {
                                                        //         var return_data = {
                                                        //             remote_currentStatus_details: result[0]
                                                        //         };

                                                        //     }
                                                        // });
                                                        // getRemoteInfo.finalize();
                                                    }
                                                });
                                                updateDeviceName.finalize();
                                            }
                                        });
                                        updateRemoteName.finalize();
                                    }
                                }
                            });
                            checkDuplicateRemotename.finalize();
                        } else {
                            let checkDuplicateRemoteName = sqliteDB.prepare(constants.CHECK_SAME_REMOTE_NAME);
                            checkDuplicateRemoteName.all(remote_name, remote_id, function (error, result) {
                                if (error) {
                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                    return cb(null, response);
                                } else {
                                    if (result.length > 0) {
                                        logger.warn('Same Remote name detected!');
                                        response = appUtil.createErrorResponse(constants.responseCode.DUPLICATE_REMOTE_NAME);
                                        return cb(null, response);
                                    } else {
                                        let checkSensorPanelExist = sqliteDB.prepare(constants.CHECK_SENSOR_PANEL_EXIST);
                                        checkSensorPanelExist.all(room_id, panel_type, function (error, result) {
                                            if (error) {
                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                return cb(null, response);
                                            } else {
                                                if (result.length > 0) {
                                                    panel_id = result[0].panel_id;
                                                    insertUpdateRemoteDetails();
                                                } else {
                                                    let insertPanelData = sqliteDB.prepare(constants.INSERT_PANEL_DATA);
                                                    insertPanelData.run(panel_id, module_id, room_id, panel_name, panel_type, user_id, homeControllerDeviceId, created_by, created_date, function (error, result) {
                                                        if (error) {
                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                            return cb(null, response);
                                                        } else {
                                                            insertUpdateRemoteDetails();
                                                        }
                                                    });
                                                    insertPanelData.finalize();
                                                }
                                            }
                                        });

                                        function insertUpdateRemoteDetails() {

                                            //logger.info("panel_id", panel_id);

                                            let updateRemoteDetails = sqliteDB.prepare(constants.UPDATE_REMOTE_DETAILS);
                                            updateRemoteDetails.run(remote_name, room_id, panel_id, ir_blaster_id, mode, temperature, is_active, user_id, created_date, remote_id, function (error, result) {
                                                if (error) {
                                                    logger.error(" Update Remote Details Error ", error);
                                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                    return cb(null, response);
                                                } else {
                                                    let updateRoomPanelId = sqliteDB.prepare(constants.UPDATE_REMOTE_MAPPING_PANEL_ID_FOR_ROOM);
                                                    updateRoomPanelId.run(remote_name, panel_id, panel_id, is_active, remote_id, function (error, result) {
                                                        if (error) {
                                                            logger.error(" Update Remote Details In Room Mapping Error ", error);
                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                            return cb(null, response);
                                                        } else {
                                                            let updatePanelId = sqliteDB.prepare(constants.UPDATE_REMOTE_MAPPING_PANEL_ID_FOR_MOOD);
                                                            updatePanelId.run(panel_id, remote_id, function (error, result) {
                                                                if (error) {
                                                                    logger.error("Update Remote Details In Room Mapping Error ", error);
                                                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                    return cb(null, response);
                                                                } else {
                                                                    //for updating rooms schedules details
                                                                    let getRemoteSchedules = sqliteDB.prepare(constants.GET_REMOTE_SCHEDULES_ON_ROOM);
                                                                    getRemoteSchedules.all(remote_id, function (error, remoteSchedules) {
                                                                        if (error) {
                                                                            logger.error("Update Remote Schedules On Room Error ", error);
                                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                            return cb(null, response);
                                                                        } else {
                                                                            if (remoteSchedules.length > 0) {

                                                                                let callBackFunction = function () {
                                                                                    if (updateRemoteNewRoomId == remoteSchedules.length) {
                                                                                        let checkDoorDevicesExist = sqliteDB.prepare(constants.CHECK_DOOR_SENSOR_EXIST_IN_PANEL);
                                                                                        checkDoorDevicesExist.all(original_panel_id, function (error, result) {
                                                                                            if (error) {
                                                                                                logger.error("check Door Sensor Error ", error);
                                                                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                return cb(null, response);
                                                                                            } else {
                                                                                                if (result.length > 0) {

                                                                                                    homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                    homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                    let activity_type = 'Remote',
                                                                                                        activity_description = remote_name,
                                                                                                        activity_action = 'Updated';
                                                                                                    //logger.info(activity_type, activity_action, activity_description);
                                                                                                    notificationUtil.addActivity({
                                                                                                        room_id: room_id,
                                                                                                        panel_id: '',
                                                                                                        module_id: remote_id,
                                                                                                        activity_type: activity_type,
                                                                                                        activity_description: activity_description,
                                                                                                        activity_action: activity_action,
                                                                                                        sensor_type: constants.sensor_type.remote,
                                                                                                        is_unread: 0,
                                                                                                        phone_id: phone_id,
                                                                                                        phone_type: phone_type,
                                                                                                        image_url: '',
                                                                                                        user_id: user_id,
                                                                                                        user_name: user_name,
                                                                                                        home_controller_device_id: homeControllerDeviceId
                                                                                                    });
                                                                                                    response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                    return cb(null, response);
                                                                                                } else {
                                                                                                    let checkMultiSensorExist = sqliteDB.prepare(constants.CHECK_MULTI_SENSOR_EXIST_IN_PANEL);
                                                                                                    checkMultiSensorExist.all(original_panel_id, function (error, result) {
                                                                                                        if (error) {
                                                                                                            logger.error("check Multi Sensor Error ", error);
                                                                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                            return cb(null, response);
                                                                                                        } else {
                                                                                                            if (result.length > 0) {
                                                                                                                homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                                homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                                let activity_type = 'Remote',
                                                                                                                    activity_description = remote_name,
                                                                                                                    activity_action = 'Updated';
                                                                                                                //logger.info(activity_type, activity_action, activity_description);
                                                                                                                notificationUtil.addActivity({
                                                                                                                    room_id: room_id,
                                                                                                                    panel_id: '',
                                                                                                                    module_id: remote_id,
                                                                                                                    activity_type: activity_type,
                                                                                                                    activity_description: activity_description,
                                                                                                                    activity_action: activity_action,
                                                                                                                    sensor_type: constants.sensor_type.remote,
                                                                                                                    is_unread: 0,
                                                                                                                    phone_id: phone_id,
                                                                                                                    phone_type: phone_type,
                                                                                                                    image_url: '',
                                                                                                                    user_id: user_id,
                                                                                                                    user_name: user_name,
                                                                                                                    home_controller_device_id: homeControllerDeviceId
                                                                                                                });
                                                                                                                response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                                return cb(null, response);
                                                                                                            } else {
                                                                                                                let checkOtherRemoteExist = sqliteDB.prepare(constants.CHECK_REMOTE_EXIST_IN_PANEL);
                                                                                                                checkOtherRemoteExist.all(original_panel_id, function (error, result) {
                                                                                                                    if (error) {
                                                                                                                        logger.error("check Multi Sensor Error ", error);
                                                                                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                                        return cb(null, response);
                                                                                                                    } else {
                                                                                                                        if (result.length > 0) {
                                                                                                                            homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                                            homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                                            let activity_type = 'Remote',
                                                                                                                                activity_description = remote_name,
                                                                                                                                activity_action = 'Updated';
                                                                                                                            //logger.info(activity_type, activity_action, activity_description);
                                                                                                                            notificationUtil.addActivity({
                                                                                                                                room_id: room_id,
                                                                                                                                panel_id: '',
                                                                                                                                module_id: remote_id,
                                                                                                                                activity_type: activity_type,
                                                                                                                                activity_description: activity_description,
                                                                                                                                activity_action: activity_action,
                                                                                                                                sensor_type: constants.sensor_type.remote,
                                                                                                                                is_unread: 0,
                                                                                                                                phone_id: phone_id,
                                                                                                                                phone_type: phone_type,
                                                                                                                                image_url: '',
                                                                                                                                user_id: user_id,
                                                                                                                                user_name: user_name,
                                                                                                                                home_controller_device_id: homeControllerDeviceId
                                                                                                                            });
                                                                                                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                                            return cb(null, response);
                                                                                                                        } else {
                                                                                                                            let deleteOriginalBlasterPanel = sqliteDB.prepare(constants.DELETE_ORIGINAL_BLASTER_PANEL_FOR_UPDATE);
                                                                                                                            deleteOriginalBlasterPanel.all(original_panel_id, original_room_id, function (error, result1) {
                                                                                                                                if (error) {
                                                                                                                                    logger.error(" Delete Original Blaster Panel Error ", error);
                                                                                                                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                                                    return cb(null, response);
                                                                                                                                } else {
                                                                                                                                    homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                                                    homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                                                    let activity_type = 'Remote',
                                                                                                                                        activity_description = remote_name,
                                                                                                                                        activity_action = 'Updated';
                                                                                                                                    //logger.info(activity_type, activity_action, activity_description);
                                                                                                                                    notificationUtil.addActivity({
                                                                                                                                        room_id: room_id,
                                                                                                                                        panel_id: '',
                                                                                                                                        module_id: remote_id,
                                                                                                                                        activity_type: activity_type,
                                                                                                                                        activity_description: activity_description,
                                                                                                                                        activity_action: activity_action,
                                                                                                                                        sensor_type: constants.sensor_type.remote,
                                                                                                                                        is_unread: 0,
                                                                                                                                        phone_id: phone_id,
                                                                                                                                        phone_type: phone_type,
                                                                                                                                        image_url: '',
                                                                                                                                        user_id: user_id,
                                                                                                                                        user_name: user_name,
                                                                                                                                        home_controller_device_id: homeControllerDeviceId
                                                                                                                                    });
                                                                                                                                    response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                                                    return cb(null, response);
                                                                                                                                }
                                                                                                                            });
                                                                                                                            deleteOriginalBlasterPanel.finalize();
                                                                                                                        }
                                                                                                                    }
                                                                                                                });
                                                                                                                checkOtherRemoteExist.finalize();
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                };

                                                                                remoteSchedules.forEach(function (data) {

                                                                                    //logger.info("original remote schdeule room ids", data.schedule_room_ids);
                                                                                    let scheduleRoomIds = data.schedule_room_ids.split(","),
                                                                                        schedule_id = data.schedule_id;

                                                                                    scheduleRoomIds = scheduleRoomIds.filter(function (item) {
                                                                                        return item !== original_room_id;
                                                                                    });
                                                                                    scheduleRoomIds.push(room_id);

                                                                                    uniqueArray = scheduleRoomIds.filter(function (item, pos) {
                                                                                        return scheduleRoomIds.indexOf(item) == pos;
                                                                                    });

                                                                                    let new_room_id = uniqueArray.join();

                                                                                    //logger.info("new remote schdeule room ids", new_room_id);

                                                                                    let updateRemoteScheduleRoomId = sqliteDB.prepare(constants.UPDATE_REMOTE_SCHEDULES_ROOM_ID);
                                                                                    updateRemoteScheduleRoomId.run(new_room_id, schedule_id, function (error, result) {
                                                                                        if (error) {
                                                                                            updateRemoteNewRoomId = updateRemoteNewRoomId + 1;
                                                                                            callBackFunction();
                                                                                        } else {
                                                                                            updateRemoteNewRoomId = updateRemoteNewRoomId + 1;
                                                                                            callBackFunction();
                                                                                        }
                                                                                    });
                                                                                });
                                                                            } else {

                                                                                let checkDoorDevicesExist = sqliteDB.prepare(constants.CHECK_DOOR_SENSOR_EXIST_IN_PANEL);
                                                                                checkDoorDevicesExist.all(original_panel_id, function (error, result) {
                                                                                    if (error) {
                                                                                        logger.error("check Door Sensor Error ", error);
                                                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                        return cb(null, response);
                                                                                    } else {
                                                                                        if (result.length > 0) {

                                                                                            homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                            homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                            let activity_type = 'Remote',
                                                                                                activity_description = remote_name,
                                                                                                activity_action = 'Updated';
                                                                                            //logger.info(activity_type, activity_action, activity_description);
                                                                                            notificationUtil.addActivity({
                                                                                                room_id: room_id,
                                                                                                panel_id: '',
                                                                                                module_id: remote_id,
                                                                                                activity_type: activity_type,
                                                                                                activity_description: activity_description,
                                                                                                activity_action: activity_action,
                                                                                                sensor_type: constants.sensor_type.remote,
                                                                                                is_unread: 0,
                                                                                                phone_id: phone_id,
                                                                                                phone_type: phone_type,
                                                                                                image_url: '',
                                                                                                user_id: user_id,
                                                                                                user_name: user_name,
                                                                                                home_controller_device_id: homeControllerDeviceId
                                                                                            });
                                                                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                            return cb(null, response);
                                                                                        } else {
                                                                                            let checkMultiSensorExist = sqliteDB.prepare(constants.CHECK_MULTI_SENSOR_EXIST_IN_PANEL);
                                                                                            checkMultiSensorExist.all(original_panel_id, function (error, result) {
                                                                                                if (error) {
                                                                                                    logger.error("check Multi Sensor Error ", error);
                                                                                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                    return cb(null, response);
                                                                                                } else {
                                                                                                    if (result.length > 0) {
                                                                                                        homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                        homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                        let activity_type = 'Remote',
                                                                                                            activity_description = remote_name,
                                                                                                            activity_action = 'Updated';
                                                                                                        //logger.info(activity_type, activity_action, activity_description);
                                                                                                        notificationUtil.addActivity({
                                                                                                            room_id: room_id,
                                                                                                            panel_id: '',
                                                                                                            module_id: remote_id,
                                                                                                            activity_type: activity_type,
                                                                                                            activity_description: activity_description,
                                                                                                            activity_action: activity_action,
                                                                                                            sensor_type: constants.sensor_type.remote,
                                                                                                            is_unread: 0,
                                                                                                            phone_id: phone_id,
                                                                                                            phone_type: phone_type,
                                                                                                            image_url: '',
                                                                                                            user_id: user_id,
                                                                                                            user_name: user_name,
                                                                                                            home_controller_device_id: homeControllerDeviceId
                                                                                                        });
                                                                                                        response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                        return cb(null, response);
                                                                                                    } else {
                                                                                                        let checkOtherRemoteExist = sqliteDB.prepare(constants.CHECK_REMOTE_EXIST_IN_PANEL);
                                                                                                        checkOtherRemoteExist.all(original_panel_id, function (error, result) {
                                                                                                            if (error) {
                                                                                                                logger.error("check Multi Sensor Error ", error);
                                                                                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                                return cb(null, response);
                                                                                                            } else {
                                                                                                                if (result.length > 0) {
                                                                                                                    homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                                    homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                                    let activity_type = 'Remote',
                                                                                                                        activity_description = remote_name,
                                                                                                                        activity_action = 'Updated';
                                                                                                                    //logger.info(activity_type, activity_action, activity_description);
                                                                                                                    notificationUtil.addActivity({
                                                                                                                        room_id: room_id,
                                                                                                                        panel_id: '',
                                                                                                                        module_id: remote_id,
                                                                                                                        activity_type: activity_type,
                                                                                                                        activity_description: activity_description,
                                                                                                                        activity_action: activity_action,
                                                                                                                        sensor_type: constants.sensor_type.remote,
                                                                                                                        is_unread: 0,
                                                                                                                        phone_id: phone_id,
                                                                                                                        phone_type: phone_type,
                                                                                                                        image_url: '',
                                                                                                                        user_id: user_id,
                                                                                                                        user_name: user_name,
                                                                                                                        home_controller_device_id: homeControllerDeviceId
                                                                                                                    });
                                                                                                                    response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                                    return cb(null, response);
                                                                                                                } else {
                                                                                                                    let deleteOriginalBlasterPanel = sqliteDB.prepare(constants.DELETE_ORIGINAL_BLASTER_PANEL_FOR_UPDATE);
                                                                                                                    deleteOriginalBlasterPanel.all(original_panel_id, original_room_id, function (error, result1) {
                                                                                                                        if (error) {
                                                                                                                            logger.error(" Delete Original Blaster Panel Error ", error);
                                                                                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                                                            return cb(null, response);
                                                                                                                        } else {
                                                                                                                            homeAutomationController.checkRoomPanelStatus(panel_id, room_id);
                                                                                                                            homeAutomationController.checkRoomPanelStatus(original_panel_id, original_room_id);

                                                                                                                            let activity_type = 'Remote',
                                                                                                                                activity_description = remote_name,
                                                                                                                                activity_action = 'Updated';
                                                                                                                            //logger.info(activity_type, activity_action, activity_description);
                                                                                                                            notificationUtil.addActivity({
                                                                                                                                room_id: room_id,
                                                                                                                                panel_id: '',
                                                                                                                                module_id: remote_id,
                                                                                                                                activity_type: activity_type,
                                                                                                                                activity_description: activity_description,
                                                                                                                                activity_action: activity_action,
                                                                                                                                sensor_type: constants.sensor_type.remote,
                                                                                                                                is_unread: 0,
                                                                                                                                phone_id: phone_id,
                                                                                                                                phone_type: phone_type,
                                                                                                                                image_url: '',
                                                                                                                                user_id: user_id,
                                                                                                                                user_name: user_name,
                                                                                                                                home_controller_device_id: homeControllerDeviceId
                                                                                                                            });
                                                                                                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_UPDATE);
                                                                                                                            return cb(null, response);
                                                                                                                        }
                                                                                                                    });
                                                                                                                    deleteOriginalBlasterPanel.finalize();
                                                                                                                }
                                                                                                            }
                                                                                                        });
                                                                                                        checkOtherRemoteExist.finalize();
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                });

                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                            updatePanelId.finalize();
                                                        }
                                                    });
                                                    updateRoomPanelId.finalize();
                                                }
                                            });
                                            updateRemoteDetails.finalize();
                                        }
                                    }
                                }
                            });
                            checkDuplicateRemoteName.finalize();
                        }
                    } else {
                        logger.warn('No Remote found!');
                        response = appUtil.createErrorResponse(constants.responseCode.NO_REMOTE_FOUND);
                        return cb(null, response);
                    }
                }
            });
            findRemoteDetails.finalize();
        }
    } catch (error) {
        logger.error('IR Controller | Update Remote Details Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};

//Delete Remote
exports.deleteRemote = function (devices, cb) {
    //logger.info("deleteRemote", devices);
    try {
        let remote_id = devices.remote_id,
            phone_id = devices.phone_id,
            phone_type = devices.phone_type,

            user_id = devices.user_id;

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });

        if (remote_id.length == 0) {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else {
            let checkRemoteDetails = sqliteDB.prepare(constants.CHECK_REMOTE_DETAILS);
            checkRemoteDetails.all(remote_id, function (error, result) {
                if (error) {
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(null, response);
                } else {
                    if (result.length > 0) {
                        let remote_name = result[0].remote_name,
                            ir_blaster_id = result[0].ir_blaster_id,
                            room_id = result[0].room_id,
                            panel_id = result[0].panel_id;

                        let deleteRemote = sqliteDB.prepare(constants.DELETE_REMOTE);
                        deleteRemote.all(remote_id, function (error, result) {
                            if (error) {
                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                return cb(null, response);
                            } else {
                                let deleteFromDeviceMapping = sqliteDB.prepare(constants.DELETE_ROOM_REMOTE_MAPPING);
                                deleteFromDeviceMapping.run(remote_id, function (error, result) {
                                    if (error) {
                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                        return cb(null, response);
                                    } else {
                                        let deleteSchedule = sqliteDB.prepare(constants.DELETE_SCHEDULE_REMOTE_MAPPING);
                                        deleteSchedule.run(remote_id, function (error, result) {
                                            if (error) {
                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                return cb(null, response);
                                            } else {
                                                let checkOtherPanelRemotes = sqliteDB.prepare(constants.FIND_BLASTER_PANEL_REMOTES);
                                                checkOtherPanelRemotes.all(ir_blaster_id, function (error, result) {
                                                    if (error) {
                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                        return cb(null, response);
                                                    } else {
                                                        if (result.length > 0) {
                                                            let activity_type = 'Remote',
                                                                activity_description = remote_name,
                                                                activity_action = 'Deleted';
                                                            //logger.info(activity_type, activity_action, activity_description);
                                                            notificationUtil.addActivity({
                                                                room_id: room_id,
                                                                panel_id: '',
                                                                module_id: remote_id,
                                                                activity_type: activity_type,
                                                                activity_description: activity_description,
                                                                activity_action: activity_action,
                                                                sensor_type: constants.sensor_type.remote,
                                                                is_unread: 0,
                                                                phone_id: phone_id,
                                                                phone_type: phone_type,
                                                                image_url: '',
                                                                user_id: user_id,
                                                                user_name: user_name,
                                                                home_controller_device_id: homeControllerDeviceId
                                                            });
                                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_DELETE);
                                                            return cb(null, response);
                                                        } else {
                                                            let checkDoorDevicesExist = sqliteDB.prepare(constants.CHECK_DOOR_SENSOR_EXIST_IN_PANEL);
                                                            checkDoorDevicesExist.all(panel_id, function (error, result) {
                                                                if (error) {
                                                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                    return cb(null, response);
                                                                } else {
                                                                    if (result.length > 0) {
                                                                        let activity_type = 'Remote',
                                                                            activity_description = remote_name,
                                                                            activity_action = 'Deleted';
                                                                        //logger.info(activity_type, activity_action, activity_description);
                                                                        notificationUtil.addActivity({
                                                                            room_id: room_id,
                                                                            panel_id: '',
                                                                            module_id: remote_id,
                                                                            activity_type: activity_type,
                                                                            activity_description: activity_description,
                                                                            activity_action: activity_action,
                                                                            sensor_type: constants.sensor_type.remote,
                                                                            is_unread: 0,
                                                                            phone_id: phone_id,
                                                                            phone_type: phone_type,
                                                                            image_url: '',
                                                                            user_id: user_id,
                                                                            user_name: user_name,
                                                                            home_controller_device_id: homeControllerDeviceId
                                                                        });
                                                                        response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_DELETE);
                                                                        return cb(null, response);
                                                                    } else {
                                                                        let checkMultiSensorExist = sqliteDB.prepare(constants.CHECK_MULTI_SENSOR_EXIST_IN_PANEL);
                                                                        checkMultiSensorExist.all(panel_id, function (error, result) {
                                                                            if (error) {
                                                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                return cb(null, response);
                                                                            } else {
                                                                                if (result.length > 0) {
                                                                                    let activity_type = 'Remote',
                                                                                        activity_description = remote_name,
                                                                                        activity_action = 'Deleted';
                                                                                    //logger.info(activity_type, activity_action, activity_description);
                                                                                    notificationUtil.addActivity({
                                                                                        room_id: room_id,
                                                                                        panel_id: '',
                                                                                        module_id: remote_id,
                                                                                        activity_type: activity_type,
                                                                                        activity_description: activity_description,
                                                                                        activity_action: activity_action,
                                                                                        sensor_type: constants.sensor_type.remote,
                                                                                        is_unread: 0,
                                                                                        phone_id: phone_id,
                                                                                        phone_type: phone_type,
                                                                                        image_url: '',
                                                                                        user_id: user_id,
                                                                                        user_name: user_name,
                                                                                        home_controller_device_id: homeControllerDeviceId
                                                                                    });
                                                                                    response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_DELETE);
                                                                                    return cb(null, response);
                                                                                } else {
                                                                                    let deleteOriginalBlasterPanel = sqliteDB.prepare(constants.DELETE_ORIGINAL_BLASTER_PANEL);
                                                                                    deleteOriginalBlasterPanel.all(panel_id, function (error, result) {
                                                                                        if (error) {
                                                                                            logger.error(" Delete Original Blaster Panel Error ", error);
                                                                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                                            return cb(null, response);
                                                                                        } else {
                                                                                            let activity_type = 'Remote',
                                                                                                activity_description = remote_name,
                                                                                                activity_action = 'Deleted';
                                                                                            //logger.info(activity_type, activity_action, activity_description);
                                                                                            notificationUtil.addActivity({
                                                                                                room_id: room_id,
                                                                                                panel_id: '',
                                                                                                module_id: remote_id,
                                                                                                activity_type: activity_type,
                                                                                                activity_description: activity_description,
                                                                                                activity_action: activity_action,
                                                                                                sensor_type: constants.sensor_type.remote,
                                                                                                is_unread: 0,
                                                                                                phone_id: phone_id,
                                                                                                phone_type: phone_type,
                                                                                                image_url: '',
                                                                                                user_id: user_id,
                                                                                                user_name: user_name,
                                                                                                home_controller_device_id: homeControllerDeviceId
                                                                                            });
                                                                                            response = appUtil.createSuccessResponse(constants.responseCode.REMOTE_DELETE);
                                                                                            return cb(null, response);
                                                                                        }
                                                                                    });
                                                                                    deleteOriginalBlasterPanel.finalize();
                                                                                }
                                                                            }
                                                                        });
                                                                        checkMultiSensorExist.finalize();
                                                                    }
                                                                }
                                                            });
                                                            checkDoorDevicesExist.finalize();
                                                        }
                                                    }
                                                });
                                                checkOtherPanelRemotes.finalize();
                                            }
                                        });
                                        deleteSchedule.finalize();
                                    }
                                });
                                deleteFromDeviceMapping.finalize();
                            }
                        });
                        deleteRemote.finalize();
                    } else {
                        logger.warn('No Remote Details found!');
                        response = appUtil.createSuccessResponse(constants.responseCode.NO_REMOTE_DETAILS_FOUND);
                        return cb(null, response);
                    }
                }
            });
        }
    } catch (error) {
        logger.error('IR Controller | Delete Remote Details Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};

// get Individual Remote Info
exports.getRemoteInfo = function (devices, cb) {
    try {
        let remote_id = devices.remote_id,
            remoteDetails = [],
            data;

        let getRemoteInfo = sqliteDB.prepare(constants.GET_REMOTE_INFO);
        getRemoteInfo.all(remote_id, function (error, result) {
            if (error) {
                logger.error("Find IR Blaster Info Error ", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {
                if (result.length > 0) {
                    remoteDetails.push(result[0]);

                    let commandsetList = command_set.remote_commands[result[0].device_type];

                    data = {
                        remote_command_list: commandsetList,
                        remote_currentStatus_details: result[0]
                    };

                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                    return cb(null, response);
                } else {
                    //logger.info('No Remote data found!');
                    data = {
                        remoteDetails: remoteDetails,
                    };
                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                    return cb(null, response);
                }
            }
        });
    } catch (error) {
        logger.error('IR Controller | Get Remote Info Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
};

// //Get IR blaster list with Room List and  from cloud device types
exports.getIRDeviceTypeList = function (data, cb) {

    try {
        restClient.get(cloudURL + "/getIRDeviceTypes", function (result, response) {

            if (result.code === constants.responseCode.SUCCESS.code) {

                let data = {
                    device_list: result.data.ir_devicetype_list
                };

                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);

            } else {
                logger.error("Device Type List error ", error);
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });

    } catch (error) {
        logger.error('IR Controller | Get IR Device Type List Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get Device type brands from cloud
exports.getIRDeviceTypeBrands = function (data, cb) {
    //logger.info("getIRDeviceTypeBrands", data);
    try {
        let device_id = data.device_id;
        let args = {
            data: {
                device_id: device_id
            },
            headers: headers
        };

        restClient.post(cloudURL + "/getIRBrands", args, function (result, response) {
            if (result.code === constants.responseCode.SUCCESS.code) {

                let data = {
                    device_id: device_id,
                    device_brand_list: result.data.ir_device_brand_list
                };

                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);
            } else {
                logger.error("Device Type List error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch (error) {
        logger.error('IR Controller | Get IR Device Type Brands Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get Device type brands from cloud
exports.getDeviceBrandRemoteList = function (data, cb) {
    //logger.info("getDeviceBrandRemoteList", data);
    try {
        let device_brand_id = data.device_brand_id,
            with_ir_code = data.with_ir_code;

        let args = {
            data: {
                device_brand_id: device_brand_id,
                with_ir_code: with_ir_code
            },
            headers: headers
        };

        restClient.post(cloudURL + "/getBrandRemoteList", args, function (result, response) {
            if (result.code === constants.responseCode.SUCCESS.code) {

                let data = {
                    device_brand_id: device_brand_id,
                    device_brand_remote_list: result.data.remote_list
                };
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);
            } else {
                logger.error("Get Remote List error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch (error) {
        logger.error('IR Controller | Get Device Brand Remote List Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get Remote IR Code JSON
exports.getRemoteIRJson = function (data, cb) {
    //logger.info("getRemoteIRJson", data);

    try {
        let remote_id = data.remote_id;
        let args = {
            data: {
                remote_id: remote_id
            },
            headers: headers
        };

        restClient.post(cloudURL + "/getRemoteDetails", args, function (result, response) {
            if (result.code === constants.responseCode.SUCCESS.code) {

                let data = {
                    remote_details: result.data.remote_details
                };

                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);
            } else {
                logger.error("Get Remote List error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch (error) {
        logger.error('IR Controller | Get Remote IR Json Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Add Remote to Master DB in Cloud
exports.AddRemoteToCloud = function (devices, cb) {
    //logger.info("AddRemoteToCloud", devices);

    try {
        let mst_ir_device_brand_id = devices.mst_ir_device_brand_id,
            model_number = devices.model_number,
            ir_code = devices.ir_code,

            created_date = appUtil.currentDateTime(),
            user_id = devices.user_id;

        let args = {
            data: {
                mst_ir_device_brand_id: mst_ir_device_brand_id,
                model_number: model_number,
                ir_code: ir_code,
                created_by: user_id,
                created_date: created_date
            },
            headers: headers
        };


        restClient.post(cloudURL + "/AddRemoteToCloud", args, function (result, response) {

            //logger.info("response from cloud", result);
            if (result.code === constants.responseCode.SUCCESS.code) {
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                return cb(null, response);
            } else {
                logger.error("Add Remote to Cloud In Master Db error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch (error) {
        logger.error('IR Controller | Add Remote To Cloud Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Update Existing Remote Of Master DB
exports.UpdateExistingRemote = function (devices, cb) {

    //logger.info("UpdateExistingRemote", devices);

    try {
        let model_number = devices.model_number,
            ir_code = devices.ir_code,
            id = devices.id,
            created_date = appUtil.currentDateTime();

        let args = {
            data: {
                id: id,
                model_number: model_number,
                ir_code: ir_code,
                modified_date: created_date
            },
            headers: headers
        };

        restClient.post(cloudURL + "/UpdateExistingRemote", args, function (result, response) {
            //logger.info("response from cloud", result);
            if (result.code === constants.responseCode.SUCCESS.code) {
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                return cb(null, response);
            } else {
                logger.error("Update Existing Remote In Master Db error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch (err) {
        logger.error('IR Controller | Update Existing Remote Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Delete Remote from Master DB
exports.deleteMasterRemote = function (devices, cb) {
    try {
        let remote_id = devices.remote_id;

        let args = {
            data: {
                remote_id: remote_id
            },
            headers: headers
        };
    
        restClient.post(cloudURL + "/deleteMasterRemote", args, function (result, response) {
            //logger.info("resposne from cloud", result);
            if (result.code === constants.responseCode.SUCCESS.code) {
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                return cb(null, response);
            } else {
                logger.error("Delete Remote From Master Db error ");
                let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(errorResponse, null);
            }
        });
    } catch(error) {
        logger.error('IR Controller | Delete Master Remote Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};