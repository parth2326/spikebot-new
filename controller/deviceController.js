const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const cc2530Util = require('./../util/cc2530-util');
const queueManagerUtil = require('./../util/queue-manager-util');
const dbManager = require('./../util/db-manager');
const constants = require('./../util/constants');
const moment = require('moment');

// Models
const moduleModel = require('../models/module');
const deviceModel = require('../models/device');
const deviceStatusModel = require('../models/device_status');
const roomModel = require('../models/room');
const moodModel = require('../models/mood');
const panelModel = require('../models/panel');
const logModel = require('../models/logs');
const alertModel = require('../models/device_alerts');
const jetsonModel = require('../models/jetson');
const userModel = require('../models/user');
const userDeviceCounterModel = require('../models/user_device_counter');
const generalMetaModel = require('../models/general_meta');
const cameraModel = require('../models/camera');
const panelDeviceModel = require('../models/panel_device');
const beaconListener = require('./../listener/beaconListener');
const eventsEmitter = require("./../util/event-manager");

const util = require('./../util/general-util').shared.util;
const cameraController = require('./../util/general-util').homeAutomationModule.cameraController;

const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;
const Client = require('node-rest-client').Client;
const restClient = new Client();


/**
 * @param deviceType (required)
 */
exports.configureRequest = async (deviceType, cb) => {

    try {
        // deviceType : curtain | doorSensor | gasSensor | multiSensor | smartRemote | tempSensor | yale_lock | pir_device | pir_detector

        // logger.info("configureRequest", deviceType);

        let params = {
            deviceId: undefined,
            deviceStatusTemp: undefined
        }

        params = generalUtil.getDeviceIdAndStatus(deviceType);
        if (!params) return cb(null, appUtil.createErrorResponse(constants.CUSTOM_MESSAGE('419', 'Invalid Device Type')));

        // Add to Queue Manager For Sending Socket Details
        queueManagerUtil.addToQueue({
            device_id: 'configuration',
            deviceType: deviceType,
            device_identifier: params.deviceId,
            device_status: params.deviceStatusTemp,
            command_type: "configuration",
            counter: 2
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Configure Request", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * @param module_type
 * @param moduleId 
 * @param deviceId
 */
exports.configureRequestCallback = async (devices) => {

    try {
        logger.info("configuring modules", devices);
        const existingConfigurationCommand = queueManagerUtil.commandQueue.configuration;
        const deviceType = generalUtil.getDeviceType(devices.module_type, devices.deviceId);

        logger.info('DEVICE TYPE OF CONFIGURATION COMMAND', existingConfigurationCommand.deviceType);
        logger.info('DEVICE TYPE RECEIVED', deviceType);

        if (existingConfigurationCommand && existingConfigurationCommand.deviceType == 'switch_board' && ['5', '5f', 'heavy_load', 'double_heavy_load'].indexOf(deviceType) == -1) {

            eventsEmitter.emit('emitSocket', {
                topic: 'configureDevice',
                data: {
                    module_type: deviceType,
                    message: `Attached device is not a switch board`,
                }
            });

            return;

        } else if (existingConfigurationCommand && existingConfigurationCommand.deviceType != 'switch_board' && existingConfigurationCommand.deviceType !== deviceType) {

            eventsEmitter.emit('emitSocket', {
                topic: 'configureDevice',
                data: {
                    module_type: deviceType,
                    message: `Attached device is not a ${existingConfigurationCommand.deviceType}`,
                }
            });

            return;
        }

        queueManagerUtil.removeFromQueue({
            device_id: "configuration"
        });


        const moduleData = await moduleModel.getByIdentifier(devices.moduleId);

        //logger.info("module_device ::: ", moduleData);

        if (moduleData && moduleData.is_configured === 'y') {
            //logger.info("Device already configured");

            const capitalize = (s) => {
                if (typeof s !== 'string') return ''
                return s.charAt(0).toUpperCase() + s.slice(1)
            }

            eventsEmitter.emit('emitSocket', {
                topic: 'configureDevice',
                data: {
                    module_type: deviceType,
                    message: capitalize(deviceType.replace("_", " ")) + ' is already configured before',
                }
            });

            return;

            // return socketio.emit('configureDevice', {
            //     module_type: deviceType,
            //     message: capitalize(deviceType.replace("_", " ")) + ' is already configured before',
            // });
        }

        /**
         * Delete old module that are not part of any room or panel.
         * Delete devices having same module id so that duplicate devices does not created.
         */
        if (moduleData) {

            logger.info("moduleData :::: ", moduleData)

            await moduleModel.deleteByIdentifier(moduleData.module_identifier);

            const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

            await deviceModel.deleteByModuleId(moduleData.module_id);
            for (let device of deviceList) await panelDeviceModel.deleteByDeviceId(device.device_id);
        }

        const newModule = {
            module_id: appUtil.generateRandomId(),
            module_identifier: devices.moduleId,
            module_type: deviceType,
            is_active: 'y',
            last_response_time: new Date().getTime().toString(),
            is_configured: 'n'
        }

        await moduleModel.add(newModule);

        newModule.device_identifier = devices.deviceId;

        if (deviceType === "5") await deviceModel.add5Device(newModule);
        else if (deviceType === "5f") await deviceModel.add5fDevice(newModule);
        else if (deviceType === "heavy_load") await deviceModel.addHeavyLoad(newModule, 1);
        else if (deviceType === "double_heavy_load") await deviceModel.addHeavyLoad(newModule, 2);
        else if (deviceType === "gas_sensor" || deviceType === 'temp_sensor' || deviceType === 'repeater' || deviceType === 'smart_remote' || deviceType === 'door_sensor' || deviceType === 'water_detector' || deviceType === 'curtain' || deviceType === 'co2_sensor') await deviceModel.addDevice(newModule, 1);
        else if (deviceType === "yale_lock") await deviceModel.addYaleLock(newModule, 1);
        else if (deviceType === "pir_device") await deviceModel.addPirDevice(newModule);
        else if (deviceType === "pir_detector") await deviceModel.addPirDetector(newModule);
        else logger.error("INVALID MODULE CONFIGURED. NO Such Module Found");

        const roomList = await roomModel.listByType('room', ['room_id', 'room_name']);
        const moodList = await moodModel.getAllAllocatedMoods();

        let totalDevices = 1;
        if (["5", "5f"].includes(deviceType)) {
            totalDevices = 5;
        } else if (['heavy_load'].includes(deviceType)) {
            totalDevices = 1;
        } else if (['double_heavy_load'].includes(deviceType)) {
            totalDevices = 2;
        }

        eventsEmitter.emit('emitSocket', {
            topic: 'configureDevice',
            data: {
                message: '',
                module_id: newModule.module_id,
                module_type: newModule.module_type,
                total_devices: totalDevices,
                room_list: roomList,
                mood_list: moodList,
            }
        });

        return;
        // return socketio.emit('configureDevice', {
        //     message: '',
        //     module_id: newModule.module_id,
        //     module_type: newModule.module_type,
        //     total_devices: totalDevices,
        //     room_list: roomList,
        //     mood_list: moodList,
        // });

    } catch (error) {
        logger.error(' Device Controller | Configure Request Callback Error', error);

        eventsEmitter.emit('emitSocket', {
            topic: 'configureDevice',
            data: {
                message: 'Try Again!'
            }
        });

        return;
        // return socketio.emit('configureDevice', {
        //     message: 'Try Again!'
        // });
    }
};

/**
 * Add Device Master Function
 * Only Administrator
 * @param room_id (optional)
 * @param module_id (required)
 * @param module_type (required)
 * @param panel_name (optional)
 * @param user_id (required)
 * @param device_name For Sensors Only
 */
exports.addDevice = async (params, cb) => {

    logger.info('Add Device API', params.module_type);

    try {

        if (params.module_type == '5' || params.module_type == '5f' || params.module_type == 'heavy_load' || params.module_type == 'double_heavy_load') {
            exports.addPanelDevices(params, cb);
        } else if (params.module_type == 'curtain') {
            exports.addCurtain(params, cb);
        } else if (params.module_type == 'ir_blaster') {
            exports.addIRBlaster(params, cb);
        } else if (params.module_type == 'energy_meter') {
            exports.addEnergyMeter(params, cb);
        } else if (params.module_type == 'beacon_scanner') {
            exports.addBeaconScanner(params, cb);
        } else if (params.module_type == 'repeater') {
            exports.addRepeater(params, cb);
        } else if (params.module_type == 'door_sensor' || params.module_type == 'temp_sensor' || params.module_type == 'gas_sensor' || params.module_type == 'water_detector' || params.module_type == 'co2_sensor') {
            exports.addSensorDevice(params, cb);
        } else if (params.module_type == 'remote') {
            exports.addRemote(params, cb);
        } else if (params.module_type == generalUtil.deviceTypes.tt_lock_bridge.device_type) {
            exports.addTTLockBridge(params, cb);
        } else if (params.module_type == generalUtil.deviceTypes.tt_lock.device_type) {
            exports.addTTLock(params, cb);
        } else if (params.module_type == 'smart_remote') {
            exports.addSmartRemote(params, cb);
        } else if (params.module_type == 'beacon') {
            exports.addBeacon(params, cb);
        } else if (params.module_type == 'yale_lock') {
            exports.addYaleLock(params, cb);
        } else if (params.module_type == 'pir_device') {
            exports.addPirDevice(params, cb);
        } else if (params.module_type == 'pir_detector') {
            exports.addPirDetector(params, cb);
        } else if (params.module_type == generalUtil.deviceTypes.smart_bulb.device_type) {
            exports.addSmartBulb(params, cb);
        } else if (params.module_type == generalUtil.deviceTypes.smart_plug.device_type) {
            exports.addSmartPlug(params, cb);
        } else if (!generalUtil.supported_module.includes(params.module_type)) {
            return cb(null, appUtil.createErrorResponse({
                code: 404,
                message: "Module type is not supported. Please update your PI."
            }))
        }
        else {
            return cb(null, appUtil.createErrorResponse({
                code: 400,
                message: "Invalid Module Type."
            }));
        }

    } catch (error) {
        logger.error("Device Controller | Add Device Error ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Panel / Switch Board Devices i.e 5,5F, HeavyLoad, Double Heavy Load
 * @param module_id
 * @param {*} params 
 * @param {*} cb 
 */
exports.addPanelDevices = async (params, cb) => {

    //logger.info('ADD PANEL DEVICES', params);

    try {
        const moduleData = await moduleModel.get(params.module_id);
        logger.info('Module Data', moduleData);

        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }
        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        let panel_type = generalUtil.getPanelType(moduleData.module_type);

        const newPanel = {
            panel_id: appUtil.generateRandomId('PANEL'),
            room_id: params.room_id,
            panel_name: params.panel_name || 'New Panel',
            panel_type: panel_type,
            module_id: moduleData.module_id
        }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        let panelData;
        await panelModel.add(newPanel);
        panelData = newPanel;

        //logger.info("panelData ::: ", panelData);

        logModel.add({
            log_object_id: newPanel.panel_id,
            log_type: "panel_add"
        }, params.authenticatedUser);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add Panel Devices Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Curtain
 * @param module_id (required)
 * @param room_id (required)
 */
exports.addCurtain = async (params, cb) => {

    try {
        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        // let panel_type = generalUtil.getPanelType(moduleData.module_type);

        // const newPanel = {
        //     panel_id: appUtil.generateRandomId(),
        //     room_id: params.room_id,
        //     panel_name: params.panel_name || 'New Curtain Panel',
        //     panel_type: panel_type,
        //     module_id: moduleData.module_id,
        //     meta: JSON.stringify({})
        // }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        // let panelData;
        // if (newPanel.panel_type === "curtain") {
        //     const existingPanel = await panelModel.getByRoomIdAndType(params.room_id, "curtain");
        //     if (existingPanel) panelData = existingPanel;
        //     else {
        //         newPanel.panel_name = "Curtain Panel";
        //         await panelModel.add(newPanel);
        //         panelData = newPanel;

        //         logModel.add({
        //             panel_id: newPanel.panel_id,
        //             log_type: "panel_add"
        //         }, params.authenticatedUser);
        //     }
        // }

        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            // Device name is provided in the curtain
            if (params.device_name) {
                await deviceModel.update(device.device_id, {
                    device_name: params.device_name
                });
            }

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add Curtain Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Smart Remote Privately Called From Add Device API
 * @param module_id (required)
 * @param device_name (required)
 */
exports.addSmartRemote = async (params, cb) => {

    try {

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            // Update Repeater Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });


        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add Smart Remote Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add Beacon
 * @param module_id (beacon mac if added for first time else module_id) (required)
 * @param device_name (required)
 * @param related_devices (beacon) (required) (array of string i.e array of panel_device_id)
 */
exports.addBeacon = async (params, cb) => {

    //logger.info('Add Beacon API', params);

    try {

        // Check if module of beacon already exists
        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id.toLowerCase(), generalUtil.deviceTypes.beacon.device_type);
        }

        //logger.info('moduleData', moduleData);

        // If module already exists
        if (moduleData && moduleData.is_configured == 'y') {

            const isUnassigned = await moduleModel.checkIfIsUnassigned(moduleData.module_id);

            if (false == isUnassigned) {

                //logger.info("Beacon already exists and is added before.");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'Beacon is already added'
                }));

            }

        } else {

            // Module Data Initialization
            moduleData = {
                module_id: appUtil.generateRandomId('BEACON'),
                module_identifier: params.module_id.toLowerCase(),
                module_type: 'beacon',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y'
            }

            // Adding Module to Databaselectse
            await moduleModel.add(moduleData);

            await deviceModel.addBeacon(moduleData);

        }

        // Adding device to panel device list
        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);
        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            // Update Repeater Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            if (params.related_devices) {

                let finalDBDevices = {};
                let panelDeviceRoomList = await panelDeviceModel.getPanelDeviceIdsWithRoomId(params.related_devices);
                let beaconScannerList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type]);
                for (let scanner of beaconScannerList) {
                    const scannerRoomId = await generalMetaModel.getMetaValue(scanner.device_id, "room_id", null);
                    const panelDevicesOfRoom = panelDeviceRoomList.filter(function (item) {
                        return item.room_id == scannerRoomId;
                    }).map(item => item.panel_device_id);
                    finalDBDevices[scanner.device_id] = panelDevicesOfRoom;
                }

                await generalMetaModel.update(device.device_id, 'device', 'related_devices', JSON.stringify(finalDBDevices));

            }

            await beaconListener.updateBeacon(device.device_id);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add Beacon Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add Repeater - Privately Called From Add Device
 * @param module_id (required)
 * @param device_name (required)
 */
exports.addRepeater = async (params, cb) => {

    try {

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null,
                meta: JSON.stringify({})
            }

            // Update Repeater Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | AddRepeater Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Remote - Privately called from add device
 * @param ir_blaster_id (required parameter)
 * @param module_id (optional Pass only if the remote is to be added from unassigned list)
 * @param device_name (name of remote) (required)
 * @param device_sub_type (ac, tv, dth)
 * @param device_brand (brand name of device that is controlled by remote) (required if adding new)
 * @param device_model (model no of device that is controller by remote) (required if adding new)
 * @param device_codes (ir codes of the device) (required if adding new)
 * @param device_codeset_id (ir codeset id of the device) (required if adding new)
 * @param device_default_status FORMAT (MODE-TEMPERATURE) EX. "LOW-25" (required if adding new)
 */
exports.addRemote = async (params, cb) => {

    //logger.info('Add Remote API', params);

    try {

        logger.info('PARAMS ADD REMOTE : ', params);
        let irBlaster = {};
        params.device_sub_type = params.device_sub_type ? params.device_sub_type.toLowerCase() : 'ac';

        // Check if the device is to be added from unassigned list
        let moduleData = {};

        if (params.module_id) {

            moduleData = await moduleModel.get(params.module_id);

            if (!moduleData) {
                //logger.info("Remote does not exists");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Invalid Remote Selected."
                }));
            }

            let remoteDevice = await deviceModel.getByModuleId(params.module_id);

            irBlaster = await deviceModel.getDetails(params.ir_blaster_id);
            if (!irBlaster) {
                //logger.info("IR Blaster does not exists");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Invalid IR Blaster Selected."
                }));
            }

            await generalMetaModel.update(remoteDevice.device_id, 'ir_blaster_id', params.ir_blaster_id);

        } else {

            // Get IR Blaster Details
            irBlaster = await deviceModel.getDetails(params.ir_blaster_id);
            if (!irBlaster) {
                //logger.info("IR Blaster does not exists");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Invalid IR Blaster Selected."
                }));
            }

            // When the remote is added for the first time
            moduleData = {
                module_id: appUtil.generateRandomId('REMOTE'),
                module_identifier: appUtil.generateRandomId('REMOTE-MODULE'),
                module_type: 'remote',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            logger.info('ADD REMOTE DEVICE CODES : ', params.device_codes);
            await moduleModel.add(moduleData);

            // Additional Paramater that are needed for the device status changes.
            moduleData.meta = {};
            moduleData.meta.device_default_status = params.device_default_status;
            moduleData.meta.device_brand = params.device_brand;
            moduleData.meta.device_model = params.device_model;
            moduleData.meta.device_codeset_id = params.device_codeset_id;
            moduleData.meta.ir_blaster_id = params.ir_blaster_id;
            moduleData.device_sub_type = params.device_sub_type;

            // For ac, tv, dth
            if (params.device_sub_type == 'tv') {
                moduleData.meta.tv_codes = params.device_codes;
            } else if (params.device_sub_type == 'dth') {
                moduleData.meta.dth_codes = params.device_codes;
            } else {
                moduleData.meta.device_codes = params.device_codes;
            }


            await deviceModel.addRemote(moduleData);

        }

        const irBlasterPanelDevice = await panelDeviceModel.get(params.ir_blaster_id);
        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        /**
         * Get Sensor Panel Of That Particular Room To Add Remote In That Panel
         */
        params.room_id = params.room_id ? params.room_id : await generalMetaModel.getMetaValue(irBlasterPanelDevice.device_id, 'room_id');
        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id,
            }

            // Update Remote Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }


        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add Remote Error", error, params);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/** GET REMOTE COMMAND NAME
* @param remote_type 
* @param device_id
*/
exports.getRemoteCommandName = async (params, cb) => {
    try {
        logger.info('Device Id : ', params.device_id);
        logger.info('Remote Type : ', params.remote_type);
        let deviceCodes = await generalMetaModel.getMetaValue(params.device_id, params.remote_type == 'tv' ? 'tv_codes' : 'dth_codes');
        if (deviceCodes == null) {
            return cb ? cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "Invalid Device Codes Set."
            })) : null;
        }

        let commands = (JSON.parse(deviceCodes));

        let command_name = commands.map(item => item).filter(function (item) {
            // console.log('GET REMOTE COMMAND NAME : ', item);
            return !generalUtil.excludeFromRemote.includes(item.name)
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, command_name));
    } catch (error) {
        logger.error("Device Controller | Get Remote Command Name Error", error, params);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Custom Remote Button
 * @param {*} device_id
 * @param {*} remote_type
 * @param {*} button_no 
 * @param {*} button_name
 * @param {*} button_code
 * @param {*} button_ir_code
 */
exports.addCustomeRemoteButton = async (params, cb) => {

    try {
        logger.info('ADD CUSTOME REMOTE BUTTON', params);
        let deviceCodes = await generalMetaModel.getMetaValue(params.device_id, 'tv_codes', '[]');
        let commands = (JSON.parse(deviceCodes));

        let newcustomCommand;

        if (params.button_code) {

            logger.info('ADD CUSTOME REMOTE BUTTON', commands.map(item => item.name));

            // get custom command name and value
            newcustomCommand = {
                ...commands.find(function (item) {
                    return item.name == params.button_code;
                })
            };

            if (!newcustomCommand) {
                return cb(null, appUtil.createErrorResponse({
                    code: 500,
                    message: "This feature is not available for this remote. Please contact system administrator!"
                }));
            }

        } else {
            // set custom command
            newcustomCommand.name = params.button_name.split(" ").join("_");
            newcustomCommand.value = params.button_ir_code;
        }

        // set custom command button name and button no
        newcustomCommand.button_name = params.button_name;
        newcustomCommand.button_no = params.button_no;


        let editedCommand = commands.find(function (item) {
            return item.button_no == params.button_no;
        });

        let filteredCommand = commands.filter(function (item) {
            return item.button_no;
        });

        let findAddedButton = filteredCommand.find((item) => {
            return item.name === params.button_code
        });

        if (findAddedButton) {
            return cb(null, appUtil.createErrorResponse({ code: '400', message: 'You have already added this b' }));
        }

        if (!editedCommand) {
            commands.push(newcustomCommand);
        } else {
            editedCommand = newcustomCommand;
            let index = commands.findIndex(item => item.button_no === params.button_no);
            commands[index] = editedCommand;
        }

        await generalMetaModel.update(params.device_id, 'device', 'tv_codes', JSON.stringify(commands));
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, commands));
    } catch (error) {
        logger.error("Device Controller | Add Custome Remote Button Error", error, params);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Custom Remote Button
 * @param {*} device_id
 * @param {*} remote_type
 * @param {*} button_no (required)
 * @param {*} button_name (required)
 * @param {*} button_code (required)
 * @param {*} button_ir_code (custom / not applicable at the moment)
 */
exports.addCustomRemoteButton = async (params, cb) => {

    try {
        let deviceCodes = await generalMetaModel.getMetaValue(params.device_id, 'tv_codes', '[]');
        let commands = (JSON.parse(deviceCodes));

        // find the command by button_code (i.e name in the commands)
        let commandApplicable = commands.find(function (item) {
            return item.name == params.button_code;
        });

        // check the existing button_no
        let existingButtonNo = commands.find(function (item) {
            return item.button_no == params.button_no;
        });

        if (existingButtonNo && existingButtonNo.name != params.button_code) {
            delete existingButtonNo.button_no;
            delete existingButtonNo.button_name;
        }

        // When button is already allocated to button no and trying to add again
        if (commandApplicable.button_no && commandApplicable.button_no != params.button_no) {
            return cb(null, appUtil.createErrorResponse({
                code: 400,
                message: "Button is already added."
            }));
        }

        commandApplicable.button_no = params.button_no;
        commandApplicable.button_name = params.button_name;

        await generalMetaModel.update(params.device_id, 'device', 'tv_codes', JSON.stringify(commands));
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, commands));

    } catch (error) {
        logger.error("Device Controller | Add Custome Remote Button Error", error, params);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Smart Bulb
 * @param module_id (required) 
 * @param device_name (required)
 * @param module_type (required)
 * @param device_sub_type (required)
 * @param room_id (required)
 * @param user_id (required)
 */
exports.addSmartBulb = async (params, cb) => {

    try {

        if (!generalUtil.deviceTypes.smart_bulb.device_sub_types.includes(params.device_sub_type)) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Device Sub Type Selected."
            }));
        }

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, generalUtil.deviceTypes.smart_bulb.device_type);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData && moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            } else {

                // Add ir blaster it is added from different unassigned list
                const deviceList = await deviceModel.listByModuleId(params.module_id);

                const panel = await panelModel.getSensorPanelByRoomId(params.room_id);

                for (let device of deviceList) {

                    await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);

                    const newPanelDeviceData = {
                        panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                        device_id: device.device_id,
                        panel_id: panel.panel_id
                    }

                    await panelDeviceModel.add(newPanelDeviceData);

                    // Update IR Blaster Name
                    await deviceModel.update(device.device_id, {
                        device_name: params.device_name
                    });

                }

                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
            }
        } else {

            moduleData = {
                module_id: appUtil.generateRandomId('SMART-BULB'),
                module_identifier: params.module_id,
                module_type: 'smart_bulb',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            await moduleModel.add(moduleData);
            moduleData.meta = {
                room_id: params.room_id
            }
            moduleData.device_sub_type = params.device_sub_type;
            await deviceModel.addSmartBulb(moduleData);

        }

        const panel = await panelModel.getSensorPanelByRoomId(params.room_id);
        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panel.panel_id
            }

            // Update IR Blaster Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add IRBlaster Error ", error);
    }
}

/**
 * Add Smart Plug
 * @param module_id (required) 
 * @param device_name (required)
 * @param module_type (required)
 * @param device_sub_type (required)
 * @param room_id (required)
 * @param user_id (required)
 */
exports.addSmartPlug = async (params, cb) => {

    try {

        if (!generalUtil.deviceTypes.smart_plug.device_sub_types.includes(params.device_sub_type)) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Device Sub Type Selected."
            }));
        }

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, generalUtil.deviceTypes.smart_plug.device_type);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData && moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            } else {

                // Add ir blaster it is added from different unassigned list
                const deviceList = await deviceModel.listByModuleId(params.module_id);

                const panel = await panelModel.getSensorPanelByRoomId(params.room_id);

                for (let device of deviceList) {

                    await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);

                    const newPanelDeviceData = {
                        panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                        device_id: device.device_id,
                        panel_id: panel.panel_id
                    }

                    await panelDeviceModel.add(newPanelDeviceData);

                    // Update IR Blaster Name
                    await deviceModel.update(device.device_id, {
                        device_name: params.device_name
                    });

                }

                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
            }
        } else {

            moduleData = {
                module_id: appUtil.generateRandomId('SMART-PLUG'),
                module_identifier: params.module_id,
                module_type: 'smart_plug',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            await moduleModel.add(moduleData);
            moduleData.meta = {
                room_id: params.room_id
            }
            moduleData.device_sub_type = params.device_sub_type;
            await deviceModel.addSmartPlug(moduleData);

        }

        const panel = await panelModel.getSensorPanelByRoomId(params.room_id);
        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panel.panel_id
            }

            // Update IR Blaster Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add IRBlaster Error ", error);
    }
}

/**
 * Add IR Blaster - Privately Called From Add Device
 * @param module_id (required)
 * @param device_name (required)
 * @param user_id (required)
 */
exports.addEnergyMeter = async (params, cb) => {

    try {

        // Check if room exists
        // const roomData = await roomModel.get(params.room_id, 'room');
        // if (!roomData) {
        //     //logger.info("Room does not exists");
        //     return cb(null, appUtil.createErrorResponse({
        //         code: 402,
        //         message: "Invalid Room Selected."
        //     }));
        // }

        const existingEnergyMeterListWithTheirRoomId = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.energy_meter.device_type], ['room_id']);
        if (existingEnergyMeterListWithTheirRoomId) {

            const existingEnergyMeter = existingEnergyMeterListWithTheirRoomId.find(function (item) {
                return item.meta_room_id == params.room_id;
            });

            if (existingEnergyMeter) {
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Room already has one Energy Meter. You cannot add more than one energy meter in the room."
                }));
            }

        }

        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, generalUtil.deviceTypes.energy_meter.device_type);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData && moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            } else {
                // Add Energy Meter it is added from different unassigned list
                const deviceList = await deviceModel.listByModuleId(params.module_id);
                for (let device of deviceList) {
                    // await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);

                    const newPanelDeviceData = {
                        panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                        device_id: device.device_id,
                        panel_id: null
                    }

                    await panelDeviceModel.add(newPanelDeviceData);

                    // Update Energy Meter Name
                    await deviceModel.update(device.device_id, {
                        device_name: params.device_name
                    });

                }

                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
            }
        } else {

            moduleData = {
                module_id: appUtil.generateRandomId('ENERGY-METER'),
                module_identifier: params.module_id,
                module_type: 'energy_meter',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            await moduleModel.add(moduleData);
            moduleData.meta = {
                room_id: params.room_id
            }
            await deviceModel.addEnergyMeter(moduleData);
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            // Update Energy Meter Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add Energy Meter Error ", error);
    }
}


/**
 * Add IR Blaster - Privately Called From Add Device
 * @param module_id (required)
 * @param device_name (required)
 * @param room_id (required)
 * @param user_id (required)
 */
exports.addIRBlaster = async (params, cb) => {

    try {

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, generalUtil.deviceTypes.ir_blaster.device_type);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData && moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            } else {

                // Add ir blaster it is added from different unassigned list
                const deviceList = await deviceModel.listByModuleId(params.module_id);
                for (let device of deviceList) {
                    await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);

                    const newPanelDeviceData = {
                        panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                        device_id: device.device_id,
                        panel_id: null
                    }

                    await panelDeviceModel.add(newPanelDeviceData);

                    // Update IR Blaster Name
                    await deviceModel.update(device.device_id, {
                        device_name: params.device_name
                    });

                }

                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
            }
        } else {

            moduleData = {
                module_id: appUtil.generateRandomId('IR-BLASTER'),
                module_identifier: params.module_id,
                module_type: 'ir_blaster',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            await moduleModel.add(moduleData);
            moduleData.meta = {
                room_id: params.room_id
            }
            await deviceModel.addIRBlaster(moduleData);

        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            // Update IR Blaster Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add IRBlaster Error ", error);
    }
}



/**
 * Add Beacon Scanner - Privately Called From Add Device
 * @param module_id (pass identifier if new or pass module_id if from unassigned) (required)
 * @param device_name (requried)
 * @param user_id (required)
 * @param room_id (required)
 * @param range (in meters(integer)) (required)
 * @param off_time (HH:mm) (24 Hour Format) (required)
 * @param on_time (HH:mm) (24 Hour Format) (required)
 */
exports.addBeaconScanner = async (params, cb) => {

    try {

        // // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        // Check if Room already has Scanner
        const existingScannerListWithTheirRoomId = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type], ['room_id']);
        if (existingScannerListWithTheirRoomId) {

            const existingScanner = existingScannerListWithTheirRoomId.find(function (item) {
                return item.meta_room_id == params.room_id;
            });

            if (existingScanner) {
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: "Room already has one beacon scanner. You cannot add more than one beacon scanner in the room."
                }));
            }

        }

        let moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, generalUtil.deviceTypes.beacon_scanner.device_type);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData && moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already added.'
                }));
            } else {

                // Add ir blaster it is added from different unassigned list
                const deviceList = await deviceModel.listByModuleId(params.module_id);

                for (let device of deviceList) {

                    await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);
                    await generalMetaModel.update(device.device_id, 'device', 'range', params.range);
                    await generalMetaModel.update(device.device_id, 'device', 'off_time', params.off_time);
                    await generalMetaModel.update(device.device_id, 'device', 'on_time', params.on_time);

                    const newPanelDeviceData = {
                        panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                        device_id: device.device_id,
                        panel_id: null
                    }

                    await panelDeviceModel.add(newPanelDeviceData);

                    await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);
                    await beaconListener.addBeaconScanner(device.device_id);

                    // Update IR Blaster Name
                    await deviceModel.update(device.device_id, {
                        device_name: params.device_name
                    });

                }

                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
            }
        } else {

            moduleData = {
                module_id: appUtil.generateRandomId('B-SCANNER'),
                module_identifier: params.module_id.toUpperCase(),
                module_type: 'beacon_scanner',
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y',
            }

            await moduleModel.add(moduleData);
            await deviceModel.addBeaconScanner(moduleData);
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            // Update Beacon Scanner Blaster Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            await generalMetaModel.update(device.device_id, 'device', 'range', params.range);
            await generalMetaModel.update(device.device_id, 'device', 'room_id', params.room_id);
            await generalMetaModel.update(device.device_id, 'device', 'off_time', params.off_time);
            await generalMetaModel.update(device.device_id, 'device', 'on_time', params.on_time);

            await beaconListener.addBeaconScanner(device.device_id);
            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);
        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add Beacon Scanner Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Add Yale Lock Device - Privately Called From Add Device API
 * @param module_id (required)
 * @param device_name (required)
 * @param room_id (required)
 */
exports.addYaleLock = async (params, cb) => {

    try {

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            moduleData = await moduleModel.getByIdentifierAndModuleType(params.module_id, 'yale_lock');
        }

        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        let panel_type = generalUtil.getPanelType(moduleData.module_type);

        // const newPanel = {
        //     panel_id: appUtil.generateRandomId(),
        //     room_id: params.room_id,
        //     panel_name: params.panel_name || 'New Panel',
        //     panel_type: panel_type,
        //     module_id: moduleData.module_id
        // }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id);
        if (params.panel_name != null) {
            panelModel.update(panelData.panel_id, {
                panel_name: params.panel_name
            })
        }

        // if (newPanel.panel_type === "sensor") {
        //     const existingPanel = await panelModel.getByRoomIdAndType(params.room_id, panel_type);
        //     if (existingPanel) panelData = existingPanel;
        //     else {
        //         newPanel.panel_name = params.panel_name ? params.panel_name : "Sensor Panel";
        //         await panelModel.add(newPanel);
        //         panelData = newPanel;
        //     }
        // }

        //logger.info("panelData ::: ", panelData);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            if (panelData.panel_type === 'sensor' && params.device_name) await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add Yale Lock Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}



/**
 * Add TT Lock Lock Device - Privately Called From Add Device API
 * @param module_id (represents lock id of lock if new / module_id of module if adding from unassigned)
 * @param device_name
 * @param room_id
 * @param tt_lock_bridge_id (device id of ttlock bridge)
 * @param lock_data
 */
exports.addTTLock = async (params, cb) => {

    try {

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        let moduleData = await moduleModel.get(params.module_id, generalUtil.deviceTypes.tt_lock.device_type);

        if (!moduleData) {

            moduleData = {
                module_id: appUtil.generateRandomId('TT-LOCK'),
                module_identifier: params.module_id,
                module_type: generalUtil.deviceTypes.tt_lock.device_type,
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y'
            }

            await moduleModel.add(moduleData);
            moduleData.meta = {
                lock_data: params.lock_data,
                tt_lock_bridge_id: params.tt_lock_bridge_id
            }
            await deviceModel.addTTLock(moduleData);

        }


        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id)

        //logger.info("panelData ::: ", panelData);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            if (panelData.panel_type === 'sensor' && params.device_name) await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add TT Lock Error ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add TT Lock Bridge Device - Privately Called From Add Device API
 * @param module_id (as bridge_id if adding new / module_id if adding from unassgined)
 * @param device_name name of bridge
 */
exports.addTTLockBridge = async (params, cb) => {

    try {

        let moduleData = await moduleModel.get(params.module_id, generalUtil.deviceTypes.tt_lock_bridge.device_type);

        if (!moduleData) {

            moduleData = {
                module_id: appUtil.generateRandomId('TT-LOCK-BRIDGE'),
                module_identifier: params.module_id,
                module_type: generalUtil.deviceTypes.tt_lock_bridge.device_type,
                is_active: 'y',
                last_response_time: new Date().getTime().toString(),
                is_configured: 'y'
            }

            await moduleModel.add(moduleData);
            await deviceModel.addTTlockBridge(moduleData);
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null,
                meta: JSON.stringify({})
            }

            // Update TT Block Bridge name Name
            await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Add TT Lock Bridge Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add Sensor Device - Privately Called From Add Device API
 * @param module_id (required)
 * @param device_name (required)
 */
exports.addSensorDevice = async (params, cb) => {

    try {

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id);

        // if (newPanel.panel_type === "sensor") {
        //     const existingPanel = await panelModel.getByRoomIdAndType(params.room_id, panel_type);
        //     if (existingPanel) panelData = existingPanel;
        //     else {
        //         newPanel.panel_name = params.panel_name ? params.panel_name : "Sensor Panel";
        //         await panelModel.add(newPanel);
        //         panelData = newPanel;
        //     }
        // }

        //logger.info("panelData ::: ", panelData);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            if (panelData.panel_type === 'sensor' && params.device_name) await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });


        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add Sensor Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add PIR Device - Privately Called From Add Device API
 * @param module_id (required)
 * @param device_name (required)
 * @param room_id (required)
 * @param pir_timer(required)
 */
exports.addPirDevice = async (params, cb) => {

    try {

        // if (!params.pir_timer || params.pir_timer < 30 || params.pir_timer > 300) {
        //     return cb(null, appUtil.createErrorResponse({
        //         code: 402,
        //         message: 'Pir time is required and should be between 30 to 300 seconds'
        //     }));
        // }

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        // Check if room exists
        const roomData = await roomModel.get(params.room_id, 'room');
        if (!roomData) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */

        let panelData = await panelModel.getSensorPanelByRoomId(params.room_id);
        // let panelData = await panelModel.getPirPanelByRoomId(params.room_id);

        // if (newPanel.panel_type === "sensor") {
        //     const existingPanel = await panelModel.getByRoomIdAndType(params.room_id, panel_type);
        //     if (existingPanel) panelData = existingPanel;
        //     else {
        //         newPanel.panel_name = params.panel_name ? params.panel_name : "Sensor Panel";
        //         await panelModel.add(newPanel);
        //         panelData = newPanel;
        //     }
        // }

        //logger.info("panelData ::: ", panelData);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: panelData.panel_id
            }

            if (params.device_name) await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await generalMetaModel.update(device.device_id, 'device', 'pir_timer', 30);
            await panelDeviceModel.add(newPanelDeviceData);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add PIR Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add PIR Device - Privately Called From Add Device API
 * @param module_id (required)
 * @param device_name (required)
 * @param mood_id (required)
 */
exports.addPirDetector = async (params, cb) => {

    try {
        const moduleData = await moduleModel.get(params.module_id);
        logger.info('Add PIR Detectot --> Module Data', moduleData);

        if (!moduleData) {
            //logger.info("Module does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.MODULE_NOT_FOUND));
        }

        // If Module is Already Configured Before
        // Also check if the module is in unassgined list
        if (moduleData.is_configured == 'y') {
            if (false == await moduleModel.checkIfIsUnassigned(params.module_id)) {
                //logger.info("Module Is Already Configured");
                return cb(null, appUtil.createErrorResponse({
                    code: 402,
                    message: 'This module is already configured before. Please check in unassgined list.'
                }));
            }
        }

        // Check if Mood exists
        const pirMood = await moodModel.get(params.mood_id);
        logger.info('Pir Mood', pirMood);
        // const roomData = await moodModel.get(params.room_id, 'room');
        if (!pirMood || pirMood.created_by == null || pirMood.created_by.trim().length == 0) {
            //logger.info("Room does not exists");
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Mood Selected."
            }));
        }

        const deviceList = await deviceModel.listByModuleId(moduleData.module_id);

        if (!deviceList.length) {
            //logger.info("DeviceData does not exists");
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));
        }

        /**
         * One room can have only one sensor panel so if exist then no need to create new one.
         */
        // let panelData = await panelModel.getPirDetectorByMoodId(params.mood_id);

        // if (newPanel.panel_type === "sensor") {
        //     const existingPanel = await panelModel.getByRoomIdAndType(params.room_id, panel_type);
        //     if (existingPanel) panelData = existingPanel;
        //     else {
        //         newPanel.panel_name = params.panel_name ? params.panel_name : "Sensor Panel";
        //         await panelModel.add(newPanel);
        //         panelData = newPanel;
        //     }
        // }

        //logger.info("panelData ::: ", panelData);

        for (let device of deviceList) {

            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device.device_id,
                panel_id: null
            }

            if (params.device_name) await deviceModel.update(device.device_id, {
                device_name: params.device_name
            });

            await panelDeviceModel.add(newPanelDeviceData);

            // Binding Mood With PIR Detector
            await generalMetaModel.update(device.device_id, 'device', 'mood_id', params.mood_id);

            logModel.add({
                log_object_id: device.device_id,
                log_type: "device_add"
            }, params.authenticatedUser);

        }

        await moduleModel.update(moduleData.module_id, {
            is_configured: 'y'
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Device Controller | Add PIR Detector Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Edit Device API
 * Only Administrator
 * @param device_id (required)
 * @param device_name (optional)
 * @param device_icon (optional)
 * @param device_sub_type (optional)
 * @param unit (optional - temp sensor)
 * @param device_default_status (optional - remote)
 * @param ir_blaster_id (optional - remote)
 * @param onetime_code(optional - only yale lock)
 * @param pass_code(optional - only yale lock)
 * @param enable_lock_unlock_from_app (optional - only yale lock)
 * @param related_devices (optional - only beacon)
 * @param on_time (optional - only beacon scanner) 
 * @param off_time (optional - only beacon scanner)
 * @param range (optional - only beacon scanner) 
 * @param energy_meter_voltage (optional - only energy meter)
 * @param energy_meter_current (optional - only energy meter)
 * @param energy_meter_power (optional - only energy meter)
 * @param energy_meter_power_factor (optional - only energy meter)
 * @param code_type (onetime_code,master_code)
 */
exports.editDevice = async (params, cb) => {

    logger.info('[DEVICE CONTROLLER] - EDIT DEVICE API', params);

    /**
     * device_id
     * device_name
     * device_icon
     * device_sub_type
     * 
     * For Temperature
     * unit
     * 
     * For Remote
     * device_default_status ex (LOW-23)
     * ir_blaster_id - device id of ir_blaster
     *
     * For Yale Lock
     * enable_lock_unlock_from_app (0/1)
     * pass_code (4 digit code)
     */

    try {

        const deviceData = await deviceModel.get(params.device_id);
        if (!deviceData) return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));

        const updateQuery = {}
        if (params.device_name) updateQuery.device_name = params.device_name;
        if (params.device_icon) updateQuery.device_icon = params.device_icon;
        if (params.device_sub_type) updateQuery.device_sub_type = params.device_sub_type.toLowerCase();

        // Update Meta For Temperature Sensor
        if (deviceData.device_type == generalUtil.deviceTypes.temp_sensor.device_type) {

            if (params.unit) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'unit', params.unit.toUpperCase());
            }

        } else if (deviceData.device_type == generalUtil.deviceTypes.yale_lock.device_type && deviceData.device_sub_type == "yale_lock") {

            if (params.enable_lock_unlock_from_app != null) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'enable_lock_unlock_from_app', params.enable_lock_unlock_from_app);
            }

            const deviceDetails = await deviceModel.getDetails(deviceData.device_id);

            // Set Passcode 
            if (params.pass_code) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'pass_code', params.pass_code);
                cc2530Util.yaleLockPincodeSet(deviceDetails.module_identifier, 3, 1, params.pass_code.padEnd(10, 'F'), function (err, resp) { });
                logModel.add({
                    log_object_id: deviceData.device_id,
                    log_type: "password_change",
                    log_sub_type: params.code_type
                }, {
                    phone_id: "SYSTEM",
                    phone_type: "SYSTEM",
                    user_id: params.authenticatedUser.user_id
                });
            }

            // Set onetime code
            if (params.onetime_code) {
                const pass = await generalMetaModel.getByTableId(deviceData.device_id, 'pass_code');
                if (pass.meta_value === params.onetime_code) {
                    return cb(null, appUtil.createErrorResponse({
                        code: 402,
                        message: 'Password already used. Please try again with another password.'
                    }));
                }

                await generalMetaModel.update(deviceData.device_id, 'device', 'onetime_code', params.onetime_code);
                cc2530Util.yaleLockPincodeSet(deviceDetails.module_identifier, 4, 240, params.onetime_code.padEnd(10, 'F'), function (err, resp) { });
                logModel.add({
                    log_object_id: deviceData.device_id,
                    log_type: "password_change",
                    log_sub_type: params.code_type
                }, {
                    phone_id: "SYSTEM",
                    phone_type: "SYSTEM",
                    user_id: params.authenticatedUser.user_id
                });
            }

        } else if (deviceData.device_type == generalUtil.deviceTypes.remote.device_type) {

            if (params.device_default_status) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'device_default_status', params.device_default_status);
            }

            if (params.ir_blaster_id) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'ir_blaster_id', params.ir_blaster_id);
            }

            if (params.device_codes) {
                let generalMetaName = deviceData.device_sub_type === 'tv' ? 'dth_codes' : 'tv_codes';
                logger.info('GENERAL META NAME : ', generalMetaName);
                await generalMetaModel.update(deviceData.device_id, 'device', 'device_sub_type', 'tv_dth');
                await generalMetaModel.update(deviceData.device_id, 'device', generalMetaName, params.device_codes);
            }

        } else if (deviceData.device_type == generalUtil.deviceTypes.beacon.device_type) {

            if (params.related_devices) {

                let finalDBDevices = {};

                let panelDeviceRoomList = await panelDeviceModel.getPanelDeviceIdsWithRoomId(params.related_devices);
                let beaconScannerList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type]);

                for (let scanner of beaconScannerList) {
                    const scannerRoomId = await generalMetaModel.getMetaValue(scanner.device_id, "room_id", null);
                    const panelDevicesOfRoom = panelDeviceRoomList.filter(function (item) {
                        return item.room_id == scannerRoomId;
                    }).map(item => item.panel_device_id);
                    finalDBDevices[scanner.device_id] = panelDevicesOfRoom;
                }

                await generalMetaModel.update(deviceData.device_id, 'device', 'related_devices', JSON.stringify(finalDBDevices));
                await beaconListener.updateBeacon(deviceData.device_id);

            }

        } else if (deviceData.device_type == generalUtil.deviceTypes.beacon_scanner.device_type) {
            if (params.off_time) await generalMetaModel.update(deviceData.device_id, 'device', 'off_time', params.off_time);
            if (params.on_time) await generalMetaModel.update(deviceData.device_id, 'device', 'on_time', params.on_time);
            if (params.range) await generalMetaModel.update(deviceData.device_id, 'device', 'range', params.range);

            beaconListener.updateBeaconScanner(deviceData.device_id);

        } else if (deviceData.device_type == generalUtil.deviceTypes.pir_device.device_type) {

            if (params.device_sub_type && deviceData.device_sub_type != params.device_sub_type) {

                // logger.info("PIR DEVICE EDIT", {
                //     device_id: params.device_id,
                //     module_identifier: deviceData.device_identifier,
                //     pir_mode: params.device_sub_type == 'pir' ? 1 : 2,
                //     command_type: "change-pir-device-mode"
                // });

                queueManagerUtil.addToQueue([{
                    device_id: params.device_id,
                    module_identifier: deviceData.device_identifier,
                    pir_mode: params.device_sub_type == 'pir' ? 1 : 2,
                    command_type: "change-pir-device-mode"
                }]);

            }

            const oldTimer = await generalMetaModel.getMetaValue(deviceData.device_id, 'pir_timer', 30);
            if (params.pir_timer && params.pir_timer <= 300 && params.pir_timer != oldTimer) {

                logger.info('TIMER CHANGED', params);

                queueManagerUtil.addToQueue([{
                    device_id: params.device_id,
                    module_identifier: deviceData.device_identifier,
                    pir_timer: params.pir_timer,
                    old_pir_timer: params.old_pir_timer,
                    command_type: "change-pir-device-interval"
                }]);

                await generalMetaModel.update(deviceData.device_id, 'device', 'pir_timer', params.pir_timer);

            }
        } else if (deviceData.device_type == generalUtil.deviceTypes.energy_meter.device_type) {
            if (params.energy_meter_voltage) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'energy_meter_voltage', params.energy_meter_voltage);
            }
            if (params.energy_meter_current) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'energy_meter_current', params.energy_meter_current);
            }
            if (params.energy_meter_power) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'energy_meter_power', params.energy_meter_power);
            }
            if (params.energy_meter_power_factor) {
                await generalMetaModel.update(deviceData.device_id, 'device', 'energy_meter_power_factor', params.energy_meter_power_factor);
            }
        }

        await deviceModel.update(params.device_id, updateQuery);

        logModel.add({
            log_object_id: params.device_id,
            log_type: "device_update"
        }, params.authenticatedUser);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Edit Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete Device (Only Administrator)
 * @param device_id : To delete devices from panel_devices ( rooms, moods ) (required)
 * @param authenticatedUser : Computed Via Middleware
 */
exports.deleteDevice = async (params, cb) => {

    try {

        const device = await deviceModel.get(params.device_id);
        if (!device) return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));

        // Delete from panel device mapping
        await panelDeviceModel.deleteByDeviceId(params.device_id);

        // Remotes are Deleted Permanently
        if ([generalUtil.deviceTypes.remote.device_type].includes(device.device_type)) {
            await moduleModel.delete(device.module_id);
        }

        // If Device is IR Blaster - All Remotes of that ir blaster are also deleted
        if (device.device_type == generalUtil.deviceTypes.ir_blaster.device_type) {

            // Get all Remote List
            // var remoteList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.remote.device_type]);

            // Filter Remotes that belong to that ir blaster
            const irBlasterRemotes = await generalMetaModel.getByTableAndMeta(params.device_id, 'ir_blaster_id', 'device');
            logger.info('IR BLASTER LIST : ', irBlasterRemotes);
            // Delete Remote Related To That IR Blaster             
            for (let i = 0; i < irBlasterRemotes.length; i++) {
                logger.info('irBlaster Table Id : ', irBlasterRemotes[i].table_id);

                await moduleModel.deleteByDeviceId(irBlasterRemotes[i].table_id);
            }

        } else if (device.device_type == generalUtil.deviceTypes.beacon_scanner.device_type) {

            // When deleting the beacon scanner also remove the panel devices from beacon scanner.
            const beaconList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon.device_type]);
            for (let beacon of beaconList) {

                const relatedDevicesJson = await generalMetaModel.getMetaValue(beacon.device_id, 'related_devices', "{}");
                const relatedDevices = JSON.parse(relatedDevicesJson);

                if (relatedDevices && relatedDevices[device.device_identifier] && relatedDevices[device.device_identifier].length > 0) {
                    relatedDevices[device.device_identifier] = [];
                    const relatedDevicesJson = await generalMetaModel.update(beacon.device_id, 'device', "related_devices", JSON.stringify(relatedDevices));
                    beaconListener.updateBeacon(beacon.device_id);
                }

            }

            beaconListener.deleteBeaconScanner(device.device_identifier);

        } else if (device.device_type == generalUtil.deviceTypes.beacon.device_type) {

            // Remove beacon from beacon listener
            beaconListener.removeBeacon(device.device_identifier);
        } else if (device.device_type == generalUtil.deviceTypes.smart_remote.device_type) {

            let otherRemoteList = await panelDeviceModel.findByDeviceType([device.device_type]);

            if (otherRemoteList.length == 0) {
                await generalMetaModel.deleteByMetaName('smart_remote_no');
            }

        }

        await alertModel.deleteByDeviceId(params.device_id);

        logModel.add({
            log_object_id: params.device_id,
            log_type: "device_delete",
            deleted_data: JSON.stringify(device)
        }, params.authenticatedUser);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Delete Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/** DELETE CUSTOME BUTTON
 * 
 * @param {*} button_no 
 * @param {*} device_id 
 */
exports.deleteCustomeButton = async (params, cb) => {
    try {

        let deviceCodes = await generalMetaModel.getMetaValue(params.device_id, 'tv_codes', '[]');
        if (!deviceCodes) return cb(null, appUtil.createErrorResponse(constants.responseCode.BUTTON_NOT_FOUND));

        let commands = JSON.parse(deviceCodes);
        let filteredDeviceCodes = commands.find(function (item) { return item.button_no == params.button_no });

        delete filteredDeviceCodes.button_no;
        delete filteredDeviceCodes.button_name;
        await generalMetaModel.update(params.device_id, 'device', 'tv_codes', JSON.stringify(commands));
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Delete Custome Button Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete From Unassigned List (Only Administrator)
 * Only Administrator
 * @param module_id (module_id of module) (required)
 */
exports.deleteModule = async (params, cb) => {

    try {

        const moduleData = await moduleModel.get(params.module_id);
        if (!moduleData) return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND));

        // If Its jetson delete all camera as well
        if (moduleData.module_type == generalUtil.deviceTypes.jetson.device_type) {
            cameraController = require('./../util/general-util').homeAutomationModule.cameraController;
            const deviceList = await deviceModel.listByModuleId(params.module_id);
            for (let device of deviceList) {
                const cameraList = await cameraModel.getJetsonCamera(device.device_id);
                for (let camera of cameraList) {
                    params.camera_id = camera.camera_id;
                    cameraController.deleteCamera(params, function () { })
                }
            }
        }

        await moduleModel.delete(params.module_id);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Device Controller | Delete Module Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * List By Configuration Status
 * @param status
 * @value `y` : to list configured modules
 * @value `n` : to list un-configured modules
 * @default `y`
 */;
exports.listByStatus = async (params, cb) => {

    try {

        const moduleList = await moduleModel.listByConfiguration(params.status);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, moduleList || []));

    } catch (error) {
        logger.error("Device Controller | List By Status Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

exports.listPanelDeviceName = async (params, cb) => {
    logger.info("DEVICE CONTROLLER - [LIST PANEL DEVICE NAME API] - ", params);

    try {
        const user = await userModel.get(params.authenticatedUser.user_id);
        let responseWithPanelDeviceName = [];

        let [userList, roomList, allPanelList, allDeviceList, allIrBlasterList] = await Promise.all([
            dbManager.all(constants.GET_USER_DATA, [params.authenticatedUser.user_id]),
            // roomModel.listByUser(params.child_user_id ? params.child_user_id : params.authenticatedUser.user_id, params.room_type || 'room'),
            panelModel.list(),
            panelDeviceModel.allDetailedList(),
            // panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.ir_blaster.device_type]),
        ]);

        for (let i = 0; i < allPanelList.length; i++) {
            responseWithPanelDeviceName.push({
                "panel_device_id": allPanelList[i].panel_device_id,
                "panel_name": allPanelList[i].panel_name,
                "device_id": allPanelList[i].device_id,
                "device_name": allPanelList[i].device_name,
                "device_status": allPanelList[i].device_status,
                "device_sub_status": allPanelList[i].device_sub_status
            })
        }
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, responseWithPanelDeviceName || []));

    } catch (error) {
        logger.error("Device Controller | LIST PANEL DEVICE NAME API Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Device List API
 * @param room_type (room/mood)
 * @param only_rooms_of_beacon_scanner (true/false)
 * @param user_id
 * @param child_user_id
 * @param only_on_off_device (to show only on/off device ) (value in 0 or 1)
 */
exports.listDevice = async (params, cb) => {

    logger.info("Device Controller - [LIST DEVICE API] - ", params);

    /**
     * user_id,
     * room_type
     */

    if (params.only_on_off_device == null) params.only_on_off_device = false;

    try {

        let response = {
            userList: [],
            cameradeviceList: [],
            roomdeviceList: [],
            jetsonList: [],
            mac_address: home_controller_device_id,
            total_unread_count: 0
        };

        // const alert_count = await logModel.getAllUnseenAlertsCounter(params.authenticatedUser.user_id);
        // if (alert_count) {
        //     response.total_unread_count = alert_count;
        // }

        // If User is Admin : Give him all camera list 
        const user = await userModel.get(params.authenticatedUser.user_id);
        if (user) {

            // Get Camera List and Jetson List in parallel execution
            [allCameraList, response.jetsonList] = await Promise.all([
                user.admin == 1 ? cameraModel.getAdminUserCameras(params.user_id) : cameraModel.getChildUserCameras(params.authenticatedUser.user_id),
                jetsonModel.list()
            ]);

            // var allCameraList = user.admin == 1 ? await cameraModel.getAdminUserCameras(params.user_id) : await cameraModel.getChildUserCameras(params.user_id);

            // response.jetsonList = await jetsonModel.list();

            for (let jetson of response.jetsonList) {
                jetson.cameraList = allCameraList.filter(function (item) {
                    return item.jetson_device_id == jetson.jetson_id;
                });
            }

            response.cameradeviceList = allCameraList.filter(function (item) {
                return item.jetson_device_id == null;
            });

            // Not needed as of now so set it to static zero
            // response.total_unread_count = await logModel.getAllUnseenAlertsCounter(params.authenticatedUser);
            response.total_unread_count = 0;
        }

        let [userList, roomList, allPanelList, allDeviceList, allIrBlasterList] = await Promise.all([
            dbManager.all(constants.GET_USER_DATA, [params.authenticatedUser.user_id]),
            roomModel.listByUser(params.child_user_id ? params.child_user_id : params.authenticatedUser.user_id, params.room_type || 'room'),
            panelModel.list(),
            panelDeviceModel.allDetailedList(),
            panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.ir_blaster.device_type]),
        ]);


        // const beaconListener = require('./../listener/beaconListener');

        // var beaconRoomCounter = beaconListener.getAllRoomBeaconCounter();

        // const userList = await dbManager.all(constants.GET_USER_DATA, [params.user_id]);
        response.userList = userList || [];

        // Not needed now so room status and panel status will be set to zero
        // let alertStatusList = [];
        // alertStatusList.push(...roomList.map(p => async function () {
        //     return {
        //         "status": await roomModel.getRoomStatus(p.room_id),
        //         "type": "room_status",
        //         "id": p.room_id
        //     }
        // }));

        // alertStatusList.push(...allPanelList.map(p => async function () {
        //     return {
        //         "status": await panelModel.getPanelStatus(p.panel_id),
        //         "type": "panel_status",
        //         "id": p.panel_id
        //     }
        // }));

        // const alertStatusData = await Promise.all(alertStatusList.map(p => p()));

        // Check if requires only room of beacon scanner
        if (params.only_rooms_of_beacon_scanner == true) {
            let beaconScannerList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type]);
            let deviceIds = beaconScannerList.map(item => item.device_id);
            let metaListOfScanners = await generalMetaModel.getByTableIdArrayAndTablename(deviceIds, "device");
            //logger.info('Meta List Of Scanners', metaListOfScanners);
            let applicableRoomList = metaListOfScanners.filter(function (item) {
                return item.meta_name == "room_id";
            }).map(item => item.meta_value);

            //logger.info('Applicable Room List', applicableRoomList);

            roomList = roomList.filter(function (item) {
                return applicableRoomList.includes(item.room_id)
            });
        }


        for (let room of roomList) {

            const roomData = {
                room_id: room.room_id,
                room_name: room.room_name,
                // room_status: (alertStatusData.find(function (item) {
                //     return item.id == room.room_id && item.type == 'room_status';
                // }))['status'],
                room_status: 0,
                panelList: [],
                total_devices: 0,
                total_sensors: 0,
                total_curtain: 0,
                total_pir_devices: 0,
                total_beacons: 0,

                mood_name_id: null,
                is_unread: 0,
                // is_unread: (alertStatusData.find(function (item) {
                //     return item.id == room.room_id && item.type == 'room_alerts';
                // }))['status'],
                "meta_smart_remote_no": room.meta_smart_remote_no ? room.meta_smart_remote_no : null,
            }

            const panelList = allPanelList.filter(function (item) {
                return item.room_id == room.room_id;
            });

            for (let panel of panelList) {

                let panelData = panel;
                panel.deviceList = [];
                panel.curtainList = [];
                panel.sensorList = [];
                panel.panel_status = 0;
                // panel.panel_status = (alertStatusData.find(function (item) {
                //     return item.id == panel.panel_id && item.type == 'panel_status';
                // }))['status'];

                // const deviceList = await panelDeviceModel.detailedListByPanelId(panelData.panel_id);
                const deviceList = allDeviceList.filter(function (item) {
                    return item.panel_id == panel.panel_id;
                });

                // allDeviceList

                for (let device of deviceList) {

                    if (params.only_on_off_device == true && generalUtil.onOffDeviceTypes.indexOf(device.device_type) == -1) {
                        continue;
                    }

                    let deviceData = device;
                    // deviceData.is_unread = ['door_sensor', 'temp_sensor', 'gas_sensor', 'water_detector', 'lock'].includes(device.device_type) ? await logModel.getUnseenDeviceAlertsCount(params.authenticatedUser.user_id, device.device_id) : 0;
                    deviceData.is_unread = 0;
                    // deviceData.meta_pir_timer = ['pir_device'].includes(device.device_type) ? await generalMetaModel.getMetaValue(device.device_id, 'pir_timer', 30) : null;
                    deviceData.meta_pir_timer = null;

                    if (panelData.panel_type == "general") {
                        panelData.deviceList.push(deviceData);
                        roomData.total_devices += 1;
                    } else if (panelData.panel_type == "sensor") {

                        if (deviceData.device_type == generalUtil.deviceTypes.remote.device_type) {

                            // Get IR Blaster of the remove and find its temperature
                            const irBlaster = allIrBlasterList.find(function (item) {
                                return item.device_id == device.meta_ir_blaster_id;
                            });

                            deviceData.temperature = irBlaster && irBlaster.device_status ? irBlaster.device_status : null;
                        }
                        panelData.sensorList.push(deviceData);
                        roomData.total_sensors += 1;
                    }


                }

                // If there are no devices in the panel then dont return it
                if (deviceList.length > 0) {
                    roomData.panelList.push(panelData);
                }

                // Order the panel in order using comparator
                roomData.panelList.sort(generalUtil.panelOrderComparator);
            }
            // calculating room status based on panel status
            // roomData.room_status = roomData.panelList.length && roomData.panelList.every(panel => panel.panel_status === 'n') ? 'n' : 'y';
            response.roomdeviceList.push(roomData);
            // response.total_unread_count += roomData.is_unread;
        }

        logger.info('DEVICE LIST : ', response);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));

    } catch (error) {
        logger.error("Device Controller | List Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}



/**
 * List of icons
 */
exports.listIcons = async (params, cb) => {
    try {
        const iconList = await deviceModel.listIcons();
        //logger.info(iconList);
        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, iconList));
    } catch (error) {
        logger.error("Device Controller | List Icons Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Called by only general Event Listener 
 * when device status change is received there
 * @param preDevice (optional)
 * @param device_id
 * @param device_status
 * @param device_sub_status
 */
exports.handleDeviceStatusChangeListeners = async function (params, cb) {

    params.device_status = (params.device_status !== null && params.device_status !== undefined) ? params.device_status.toString() : null;


    try {

        const deviceData = params.preDevice ? params.preDevice : await deviceModel.getDetails(params.device_id);
        if (!deviceData) return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND)) : null;
        if (deviceData.is_configured === 'n') return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Device is not configured"))) : null;

        // Device is configured and added

        logger.info('HANDLE DEVICE STATUS CHANGE RECEIVED :', params);

        if (deviceData.device_type == "fan" || deviceData.device_type == "switch" || deviceData.device_type == "heavyload") {

            //logger.info('Change Device Received.', params);


            let updatedStatus = {
                device_id: params.device_id,
                device_status: params.device_status
            };

            // For Fan add sub status to the update request
            if (params.device_sub_status && deviceData.device_type == "fan") {
                updatedStatus.device_sub_status = params.device_sub_status;
            }

            await deviceStatusModel.updateMany(updatedStatus, {
                emitRoomPanelStatus: true
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status
                }
            });


        } else if (deviceData.device_type == "door_sensor") {

            //logger.info("Door Status Change : ", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });


        } else if (deviceData.device_type == "lock" && deviceData.device_sub_type == 'yale_lock') {

            //logger.info("Yale Lock Status Change : ", params);

            let updateStatus = {
                device_id: params.device_id
            };

            // status represents if the door is locked (1) or unlocked (0)
            if (params.device_status) updateStatus.device_status = params.device_status;

            // sub status here presents the door open db(0) hardware(2) or closed db(1) hardware(3)
            if (params.device_sub_status) {
                updateStatus.device_sub_status = params.device_sub_status == 2 ? 0 : 1;
            }

            deviceStatusModel.updateMany(updateStatus);

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status ? params.device_status : deviceData.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status
                }
            });

            logger.info('ACTIVITY TYPE : ', params.activity_type);
            // We are excluding app because app logs are already logged when user clicked and api was called.
            if (params.activity_type && params.activity_type != 'app') {
                logger.info("BEFORE LOGMODEL : ", params.activity_type);
                logModel.add({
                    log_object_id: params.device_id,
                    log_type: params.device_status == '0' ? "door_unlock" : "door_lock",
                    log_sub_type: params.activity_type
                }, {
                    phone_id: "SYSTEM",
                    phone_type: "SYSTEM",
                    user_id: params.activity_type
                });

            }


        } else if (deviceData.device_type == "water_detector") {

            logger.info("Water Sensor Status Change : ", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });

        } else if (deviceData.device_type == "curtain") {

            //logger.info("Curtain Status Changed", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status
            }, {
                emitRoomPanelStatus: true
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });

        } else if (deviceData.device_type == "temp_sensor") {

            //logger.info("Temperature Status Changed.", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status, // Temperature
                device_sub_status: params.device_sub_status // Humidity
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });


        } else if (deviceData.device_type == "gas_sensor") {

            //logger.info("Gas Status Changed.", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });

        } else if (deviceData.device_type == "co2_sensor") {

            //logger.info("Gas Status Changed.", params);

            deviceStatusModel.updateMany({
                device_id: params.device_id,
                device_status: params.device_status
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'changeDeviceStatus',
                data: {
                    device_id: params.device_id,
                    device_type: deviceData.device_type,
                    device_status: params.device_status,
                    device_sub_status: params.device_sub_status ? params.device_sub_status : 0
                }
            });

        }

        return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, {})) : null;

    } catch (error) {
        logger.error('Device Controller | Handle Device Status Change Listeners Error', error);
        return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
    }

}


/**
 * List of devices are unassigned but configured before
 * @param module_type
 */
exports.unAssignedDevices = async (params, cb) => {

    try {
        let unAssignedList = await moduleModel.unassignedList(params.module_type);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, unAssignedList));
    } catch (error) {
        logger.error('Device Controller | Unassigned Devices Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Called By Only Queue Manager To Return to old status of device 
 * in case if the module is down or failed to receive device status back
 * 
 * @param device_status
 * @param device_sub_status
 * @param device_old_status
 * @param device_old_sub_status
 * @param device_id
 * @param command_type
 */
// exports.reverseDeviceStatus = async (params, cb) => {


//     logger.info('reverseDeviceStatus', params);

//     // If Command Type is On Off Type
//     try {
//         if (params.command_type == "on-off") {

//             var device = await deviceModel.get(params.device_id);

//             if (device) {

//                 // Return the Status of Old Status 
//                 await deviceStatusModel.updateMany({
//                     device_id: params.device_id,
//                     device_status: params.device_old_status,
//                     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                 }, {
//                     emitRoomPanelStatus: true
//                 });

//                 eventsEmitter.emit('emitSocket', {
//                     topic: 'changeDeviceStatus',
//                     data: {
//                         device_id: params.device_id,
//                         device_type: device.device_type,
//                         device_status: params.device_old_status,
//                         device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                     }
//                 });

//                 // socketio.emit('changeDeviceStatus', {
//                 //     device_id: params.device_id,
//                 //     device_type: device.device_type,
//                 //     device_status: params.device_old_status,
//                 //     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                 // });

//             }

//         } else if (params.command_type == "lock-open-close") {

//             var device = await deviceModel.get(params.device_id);

//             if (device) {

//                 // Return the Status of Old Status 
//                 await deviceStatusModel.updateMany({
//                     device_id: params.device_id,
//                     device_status: params.device_old_status,
//                     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                 }, {
//                     emitRoomPanelStatus: false
//                 });

//                 eventsEmitter.emit('emitSocket', {
//                     topic: 'changeDeviceStatus',
//                     data: {
//                         device_id: params.device_id,
//                         device_type: device.device_type,
//                         device_status: params.device_old_status,
//                         device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                     }
//                 });

//                 // socketio.emit('changeDeviceStatus', {
//                 //     device_id: params.device_id,
//                 //     device_type: device.device_type,
//                 //     device_status: params.device_old_status,
//                 //     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
//                 // });

//             }

//         } else if (params.command_type == "change-pir-device-interval") {
//             await generalMetaModel.update(device.device_id, 'device', 'pir_timer', params.old_pir_timer);
//         }
//     } catch (error) {
//         logger.error('Device Controller | Reverse Device Status Error', error);
//         return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
//     }
// }

/**
 * Find Device By Device Type 
 * @param device_type
 * @param device_sub_type
 */
exports.findDevices = async (params, cb) => {

    try {
        let deviceList = await panelDeviceModel.findByDeviceType([params.device_type]);

        if (params.device_sub_type) {
            deviceList = deviceList.filter(function (item) {
                return item.device_sub_type == params.device_sub_type;
            });
        }

        // Add Room Data Device Type is IR Blaster
        if ([generalUtil.deviceTypes.ir_blaster.device_type, generalUtil.deviceTypes.beacon_scanner.device_type].includes(params.device_type)) {

            let remoteList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.remote.device_type]);

            for (let i = 0; i < deviceList.length; i++) {

                const room_id = await generalMetaModel.getMetaValue(deviceList[i].device_id, "room_id");

                if (params.device_type == generalUtil.deviceTypes.beacon_scanner.device_type) {
                    deviceList[i].range = await generalMetaModel.getMetaValue(deviceList[i].device_id, "range", 15);
                    deviceList[i].on_time = await generalMetaModel.getMetaValue(deviceList[i].device_id, "on_time", null);
                    deviceList[i].off_time = await generalMetaModel.getMetaValue(deviceList[i].device_id, "off_time", null);
                }


                if (room_id) {
                    // Get Room Details of the IR Blaster
                    deviceList[i].room = await roomModel.get(room_id);
                    // If Room is null then delete the device
                    if (deviceList[i].room == null) {
                        panelDeviceModel.deleteByDeviceId(deviceList[i].device_id);
                    }
                }

                deviceList[i].remote_list = remoteList.filter((remote) => {
                    return remote.meta_ir_blaster_id == deviceList[i].device_id;
                });

            }

            // Remove the unassgined ir blasters from the room
            deviceList = deviceList.filter(function (item) {
                return item.room != null;
            });

        } else if (params.device_type == generalUtil.deviceTypes.beacon.device_type) {

            for (let i = 0; i < deviceList.length; i++) {
                deviceList[i].related_devices = [];
                const relatedDevices = await generalMetaModel.getMetaValue(deviceList[i].device_id, "related_devices");
                if (relatedDevices) {
                    for (let scannerDevices of Object.values(JSON.parse(relatedDevices))) {
                        deviceList[i].related_devices.push(...scannerDevices);
                    }
                }
            }
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, deviceList));

    } catch (error) {
        logger.error('Device Controller | Find Device Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Get Device Info
 * @param device_id (required)
 */
exports.getDeviceInfo = async (params, cb) => {

    try {

        let startTime = new Date().valueOf();

        let response = {};

        let [device, deviceMeta, deviceAlerts, unreadCounter] = await Promise.all([
            deviceModel.getDetails(params.device_id),
            generalMetaModel.getByTableIdAndTablename(params.device_id, 'device'),
            alertModel.getAlertsByDeviceId(params.device_id),
            userDeviceCounterModel.getTotalUserDeviceCounter(params.authenticatedUser.user_id, params.device_id)
        ]);

        logger.info('[DEVICE INFO API] - DETAIL LIST', startTime - new Date().valueOf());

        startTime = new Date().valueOf();

        if (!device) {
            // Device not found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        if (device.device_type == generalUtil.deviceTypes.remote.device_type) {
            device.ir_blaster_id = await generalMetaModel.getMetaValue(device.device_id, 'ir_blaster_id');
            if (device.ir_blaster_id) {
                device.ir_blaster = await deviceModel.getDetails(device.ir_blaster_id);
                device.room_id = await generalMetaModel.getMetaValue(device.ir_blaster.device_id, 'room_id');
                if (device.room_id) {
                    device.room = await roomModel.get(device.room_id);
                }
            }
        } else if (device.device_type == generalUtil.deviceTypes.beacon_scanner.device_type) {
            device.room_id = await generalMetaModel.getMetaValue(device.device_id, 'room_id');
            if (device.room_id) {
                device.room = await roomModel.get(device.room_id);
            }
        } else if (device.device_type == "temp_sensor") {
            const humidityCounter = await userDeviceCounterModel.getUserDeviceCounter(params.authenticatedUser.user_id, params.device_id, "humidity_alert");
            device.humidity_counter = humidityCounter;
            device.temp_counter = await userDeviceCounterModel.getUserDeviceCounter(params.authenticatedUser.user_id, params.device_id, "temp_alert");
        }


        // [deviceMeta,response.alerts,response.unseen_logs]
        // const deviceMeta = await generalMetaModel.getByTableIdAndTablename(params.device_id, 'device');
        for (let meta of deviceMeta) {
            device['meta_' + meta.meta_name] = meta.meta_value;
        }

        logger.info('[DEVICE INFO API] - DEVICE META', startTime - new Date().valueOf());
        startTime = new Date().valueOf();

        response.device = device;
        response.alerts = deviceAlerts;
        response.unread_count = unreadCounter;
        response.alerts = response.alerts.filter(function (item) {
            return item.user_id == params.authenticatedUser.user_id;
        });


        logger.info('[DEVICE INFO API] - DEVICE ALERTS', startTime - new Date().valueOf());

        response.is_alert_set = response.alerts.map(item => item.user_id).includes(params.user_id);

        for (let i = 0; i < response.alerts.length; i++) {
            response.alerts[i].start_time = response.alerts[i].start_time ? response.alerts[i].start_time.slice(0, 2) + ":" + response.alerts[i].start_time.slice(2) : null;
            response.alerts[i].end_time = response.alerts[i].end_time ? response.alerts[i].end_time.slice(0, 2) + ":" + response.alerts[i].end_time.slice(2) : null;
        }

        response.unseen_logs = [];

        logger.info('GET DEVICE INFO : ', response)
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));

    } catch (error) {
        logger.error('Device Controller | Get Device Info Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }


}

/**
 * Get Beacons scanned by scanner 
 * @param device_id (required)
 */
exports.getBeaconsScannedByScanner = async (params, cb) => {

    try {
        //logger.info('Device Controller | Beacons Scanned by Scanner ', params);

        let device = await deviceModel.getDetails(params.device_id);

        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, beaconListener.getBeaconsDetectedInScanner(device.module_identifier)));

    } catch (error) {
        logger.error('Device Controller | Get Beacons Scanned by Scanner Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Get Heavy load Values
 * @param device_id (required)
 */
exports.getHeavyLoadValues = async (params, cb) => {

    try {
        //logger.info('Device Controller | Heavy Load Ping ', params);


        let device = await deviceModel.getDetails(params.device_id);

        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        cc2530Util.getHeavyLoadValues({
            module_id: device.module_identifier,
            device_id: '0' + device.device_identifier
        }, function (error, result) {
            // if (error) {
            // logger.error("Socket change device error ", error);
            // } else {
            //  //logger.info("Error Sending Heavy Load Values ");         
            // }
        });

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Device Controller | Get Heavy Load Values Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * 
 * @param device_id (required)
 * @param filter_type (day, month, year)
 * @param filter_value (01,11,2020 | 11,2020 | 2020) 
 * @param graph_type ("temp_value | temp_humidity")
 */
exports.getTempratureUsage = async (params, cb) => {
    try {
        let device = await deviceModel.getDetails(params.device_id);
        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        // If Filter Type is null set the default month data
        if (!params.filter_type) params.filter_type = 'day';
        if (!params.filter_value) params.filter_value = moment().format('DD') + "," + moment().format('MM') + "," + moment().format('YYYY');

        let mappingList = [];
        if (params.filter_type == 'day') {
            let monthYear = params.filter_value.split(",");
            const allMonthData = await dbManager.all('select status_value as meta1, created_date, timestamp from spike_device_status_mapping where device_id=? and created_date like ? and status_type = ? order by timestamp ASC', [
                params.device_id, '%' + monthYear[2] + "-" + monthYear[1] + "-" + monthYear[0] + '%', params.graph_type
            ]);

            let currentMoment = moment().set({ year: monthYear[2], month: monthYear[1] - 1, date: monthYear[0] });
            // const previousDate = moment(currentMoment).subtract('1', 'days');

            // const previousData = await dbManager.all('select status_value as meta1, created_date, timestamp from spike_device_status_mapping where device_id=? and created_date like ? and status_type = ? order by timestamp DESC limit 1', [
            //     params.device_id, '%' + previousDate.format('YYYY') + "-" + previousDate.format('MM') + "-" + previousDate.format('DD') + '%', params.graph_type
            // ]);

            // // logger.info('PREVIOUS DATA : ', previousData[0].meta1, previousData[0].timestamp)

            // if (previousData.length > 0) {
            //     mappingList.push({
            //         temprature: previousData[0].meta1,
            //         value: previousData[0].timestamp
            //     })
            // } else {
            //     return cb(null, appUtil.createErrorResponse({
            //         code: 200,
            //         message: "No Data Found"
            //     }));
            // }

            for (let i = 0; i < allMonthData.length; i++) {
                mappingList.push({
                    temprature: allMonthData[i].meta1 ? parseInt(allMonthData[i].meta1).toString() : "0",
                    value: allMonthData[i].timestamp
                });
            }
        }
        else if (params.filter_type == 'month') {

            let monthYear = params.filter_value.split(",");

            let totalDaysInMonth = new Date(monthYear[1], monthYear[0], 0).getDate();

            let currentMoment = moment().set({ year: monthYear[1], month: monthYear[0] - 1, date: 1 });

            if(currentMoment.toDate().valueOf() > new Date().valueOf()){
                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, []));
            }

            for (let i = 0; i < totalDaysInMonth; i++) {

                let day = (i + 1).toString().padStart(2, "0");

                let allMonthData = await dbManager.get("SELECT avg(status_value) as meta1, count(0) as total_values from spike_device_status_mapping where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and strftime('%d', created_date)=? and status_type=? order by timestamp desc", [params.device_id, currentMoment.format('YYYY'), currentMoment.format('MM'), currentMoment.format('DD'), params.graph_type]);

                if (allMonthData.total_values > 0) {
                    mappingList.push({
                        temprature: allMonthData.meta1 ? parseInt(allMonthData.meta1).toString() : "0",
                        value: day
                    })
                }


                currentMoment = currentMoment.add('1', 'days');
            }

        } else if (params.filter_type == 'year') {

            if(params.filter_value > moment(new Date()).format('yyyy')){
                return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, []));
            }


            for (let i = 0; i < 12; i++) {

                let date = new Date(params.filter_value, i, 1);
                let month = moment(date).format('MM');

                let mapping = await dbManager.get(`SELECT avg(status_value) as meta1 ,min(timestamp) as timestamp, count(0) as total_values
                                                from spike_device_status_mapping 
                                                where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and status_type=?
                                                order by timestamp desc`, [params.device_id, params.filter_value, month, params.graph_type]);

                if (mapping && mapping.total_values > 0) {
                    mappingList.push({
                        temprature: mapping.meta1 ? parseInt(mapping.meta1).toString() : "0",
                        value: month
                    })
                }

                // Get Previous Mapping
                // if (mapping) {
                //     logger.info('MAPPING TIMESTAMP : ', mapping.timestamp)
                //     let previousMapping = await dbManager.get("SELECT avg(status_value) as meta1 from spike_device_status_mapping where device_id=? and timestamp<? and status_type=? order by timestamp desc", [params.device_id, mapping.timestamp, generalUtil.deviceStatusMappingTypes.temp_sensor.temp_battery]);
                //     logger.info('PREVIOUS MAPPING  : ', previousMapping);
                //     // if (previousMapping) {
                //     //     currentTotal = previousMapping.meta1;
                //     // }

                // }

                // if (mapping.meta1 != null) {
                //     mappingList.push({
                //         energy: mapping.meta1 - currentTotal,
                //         month: month
                //     });
                //     currentTotal = mapping.meta1;
                // } else {
                //     mappingList.push({
                //         energy: 0,
                //         month: month
                //     });
                // }
            }

        }

        let zeroEnergyList = mappingList.filter(function (item) {
            return item.energy == 0;
        }).length;

        if (zeroEnergyList == mappingList.length) {
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, []));
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, mappingList));
    } catch (error) {
        logger.error('Device Controller | Get Heavy Load Usage Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Heavy Load Graph API
 * @param device_id (required)
 * @param filter_type (month, year)
 * @param filter_value (11,2019 | 2019)
 */
exports.getHeavyLoadUsage = async (params, cb) => {

    try {
        let response = {};
        let device = await deviceModel.getDetails(params.device_id);
        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        // If Filter Type is null set the default month data
        if (!params.filter_type) params.filter_type = 'month';
        if (!params.filter_value) params.filter_value = moment().format('MM') + "," + moment().format('YYYY');

        let mappingList = [];
        if (params.filter_type == 'month') {

            monthYear = params.filter_value.split(",");

            let currentTotal = 0;

            let totalDaysInMonth = new Date(monthYear[1], monthYear[0], 0).getDate();
            // var promiseList = [];
            const allMonthData = await dbManager.all('select status_value as meta1,created_date from spike_device_status_mapping where device_id=? and created_date like ? and status_type = ?', [
                params.device_id, '%' + monthYear[1] + "-" + monthYear[0] + '%', generalUtil.deviceStatusMappingTypes.heavy_load.total_energy
            ]);

            // for (var i = 0; i < totalDaysInMonth; i++) {

            //     // var date = new Date();
            //     // date.setFullYear(monthYear[1]);
            //     // date.setMonth(monthYear[0] - 1);
            //     // date.setDate(i + 1);
            //     day = (i + 1).toString().padStart(2, "0");
            //     //logger.info('GETTING DAY', i, day, date);

            //     promiseList.push(dbManager.get("SELECT max(status_value) as meta1 from spike_device_status_mapping where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and strftime('%d', created_date)=? and status_type=? order by timestamp desc limit 1", [params.device_id, monthYear[1], monthYear[0], day, generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]));

            // }

            // const allMappingListResponse = await Promise.all(promiseList);

            let currentMoment = moment().set({ year: monthYear[1], month: monthYear[0] - 1, date: 1 });

            for (let i = 0; i < totalDaysInMonth; i++) {

                // var date = new Date();
                // date.setFullYear(monthYear[1]);
                // date.setMonth(monthYear[0] - 1);
                // date.setDate(i + 1);
                let day = (i + 1).toString().padStart(2, "0");

                let mapping = allMonthData.find(function (item) {
                    return item.created_date == currentMoment.format('YYYY-MM-DD');
                });

                // var mapping = await dbManager.execut("SELECT max(status_value) as meta1,min(timestamp) as timestamp  from spike_device_status_mapping where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and strftime('%d', created_date)=? and status_type=? order by timestamp desc", [params.device_id, monthYear[1], monthYear[0], day, generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]);    
                // var mapping = currentMonthReading;

                // Get Previous Total Value

                if (i == 0 && mapping) {
                    let previousDate = moment(currentMoment).subtract('1', 'days');

                    logger.info('PREVIOUS DAY', previousDate.toISOString());
                    let previousMapping = await dbManager.get("SELECT max(status_value) as meta1 from spike_device_status_mapping where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and strftime('%d', created_date)=? and status_type=? order by timestamp desc", [params.device_id, previousDate.format('YYYY'), previousDate.format('MM'), previousDate.format('DD'), generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]);
                    // var previousMapping = await dbManager.get("SELECT max(status_value) as meta1 from spike_device_status_mapping where device_id=? and timestamp<? and status_type=? order by timestamp desc", [params.device_id, mapping.timestamp, generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]);
                    if (previousMapping) {
                        currentTotal = previousMapping.meta1;
                    }
                }

                if (mapping && mapping.meta1 != null) {
                    mappingList.push({
                        energy: parseInt(mapping.meta1 - currentTotal).toString(),
                        day: day
                    });
                    currentTotal = mapping.meta1;
                } else {
                    mappingList.push({
                        energy: 0,
                        day: day
                    });
                }

                currentMoment = currentMoment.add('1', 'days');
            }

        } else if (params.filter_type == 'year') {

            let currentTotal = 0;

            for (let i = 0; i < 12; i++) {

                let date = new Date(params.filter_value, i + 1, 1);
                month = moment(date).format('MM');

                let mapping = await dbManager.get(`SELECT max(status_value) as meta1 ,min(timestamp) as timestamp 
                                                from spike_device_status_mapping 
                                                where device_id=? and strftime('%Y', created_date)=? and strftime('%m', created_date)=? and status_type=?
                                                order by timestamp desc`, [params.device_id, params.filter_value, month, generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]);


                // Get Previous Mapping
                if (i == 0 && mapping.meta1 != null) {
                    let previousMapping = await dbManager.get("SELECT max(status_value) as meta1 from spike_device_status_mapping where device_id=? and timestamp<? and status_type=? order by timestamp desc", [params.device_id, mapping.timestamp, generalUtil.deviceStatusMappingTypes.heavy_load.total_energy]);
                    if (previousMapping) {
                        currentTotal = previousMapping.meta1;
                    }
                }

                if (mapping.meta1 != null) {
                    mappingList.push({
                        energy: mapping.meta1 - currentTotal,
                        month: month
                    });
                    currentTotal = mapping.meta1;
                } else {
                    mappingList.push({
                        energy: 0,
                        month: month
                    });
                }
            }

        }

        let zeroEnergyList = mappingList.filter(function (item) {
            return item.energy == 0;
        }).length;

        if (zeroEnergyList == mappingList.length) {
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, []));
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, mappingList));

    } catch (error) {
        logger.error('Device Controller | Get Heavy Load Usage Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}


/**
 * Get Custom Panel Details (Only Administrator)
 * 
 */
exports.getCustomPanelDetails = async (params, cb) => {

    try {
        let response = [];
        const roomList = await roomModel.listByType('room', ['room_id', 'room_name', 'room_users', 'created_by']);

        for (let room of roomList) {

            const panelList = await panelModel.lisByRoomIdAndType(room.room_id, 'general');

            for (let panel of panelList) {

                let roomObj = {
                    room_id: room.room_id,
                    module_id: panel.module_id,
                    module_name: panel.panel_name,
                    roomList: {
                        room_id: room.room_id,
                        module_id: panel.module_id,
                        panel_name: panel.panel_name,
                        room_name: room.room_name
                    }
                };

                if (panel.module_id) {
                    let deviceList = await deviceModel.listByModuleId(panel.module_id);
                    roomObj.deviceList = deviceList;
                    response.push(roomObj);
                }

            }

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, {
            "roomdeviceList": response
        }));

    } catch (error) {
        logger.error('Device Controller | Get Custom Panel Details Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }

}


/**
 * Get Lock Status
 * @param device_id (required)
 */
exports.getYaleLockStatus = async (params, cb) => {

    try {
        //logger.info('Device Controller | Get Yale Lock Status API ', params);

        let device = await deviceModel.getDetails(params.device_id);
        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        cc2530Util.lockOpenClose(device.module_identifier, 2, 0, function (err, resp) { });
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Device Controller | Get Yale Lock Status Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}


/**
 * Set yale lock user pincode
 * @param device_id (required)
 * @param user_code (required based on code_type)
 * @param pincode  (required based on code_type)
 * @param code_type (user_code,onetime_code,master_code)
 */
exports.setYaleLockPincode = async (params, cb) => {

    try {

        //logger.info('Device Controller | Set Yale Lock CODE API ', params);

        let device = await deviceModel.getDetails(params.device_id);

        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        if (params.code_type == 'user_code') {
            cc2530Util.yaleLockPincodeSet(device.module_identifier, 3, params.user_code, params.pincode.padEnd(10, 'F'), function (err, resp) { });
            logModel.add({
                log_object_id: params.device_id,
                log_type: "password_change",
                log_sub_type: params.code_type
            }, {
                phone_id: "SYSTEM",
                phone_type: "SYSTEM",
                user_id: params.activity_type
            });
        } else if (params.code_type == 'onetime_code') {
            cc2530Util.yaleLockPincodeSet(device.module_identifier, 4, 240, params.pincode.padEnd(10, 'F'), function (err, resp) { });
            logModel.add({
                log_object_id: params.device_id,
                log_type: "password_change",
                log_sub_type: params.code_type
            }, {
                phone_id: "SYSTEM",
                phone_type: "SYSTEM",
                user_id: params.activity_type
            });
        } else if (params.code_type == 'master_code') {
            cc2530Util.yaleLockPincodeSet(device.module_identifier, 5, 251, params.pincode.padEnd(10, 'F'), function (err, resp) { });
            logModel.add({
                log_object_id: params.device_id,
                log_type: "password_change",
                log_sub_type: params.code_type
            }, {
                phone_id: "SYSTEM",
                phone_type: "SYSTEM",
                user_id: params.activity_type
            });
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Device Controller | Set Yale Lock Pincode Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Delete yale lock user pincode
 * @param device_id (required)
 * @param user_code 
 * @param code_type (user_code,onetime_code,master_code)
 */
exports.deleteYaleLockPincode = async (params, cb) => {

    try {
        //logger.info('Device Controller | Delete Yale Lock CODE API ', params);


        let device = await deviceModel.getDetails(params.device_id);

        if (!device) {
            // Device Not Found in the database
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND))
        }

        if (params.code_type == 'user_code') {
            cc2530Util.yaleLockPincodeDelete(device.module_identifier, 6, params.user_code, null, function (err, resp) { });
        } else if (params.code_type == 'onetime_code') {
            cc2530Util.yaleLockPincodeDelete(device.module_identifier, 7, 240, null, function (err, resp) { });
        } else if (params.code_type == 'master_code') {
            cc2530Util.yaleLockPincodeDelete(device.module_identifier, 8, 251, null, function (err, resp) { });
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Device Controller | Delete Yale Lock Pincode Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Get Beacon Locations To Map Out which beacon is in which room
 * @param authenticatedUser 
 * @param room_id (optional)
 */
exports.getBeaconLocations = async (params, cb) => {

    try {

        let [beaconScannerList, beaconList, roomList] = await Promise.all([
            panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type]),
            panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon.device_type]),
            roomModel.listByUser(params.authenticatedUser.user_id, 'room')
        ]);

        if (params.room_id) {
            roomList = roomList.filter(function (item) {
                return item.room_id == params.room_id;
            });
        }

        for (let i = 0; i < beaconScannerList.length; i++) {

            const scanner = beaconScannerList[i];
            const scannerRoomId = await generalMetaModel.getMetaValue(scanner.device_id, "room_id", null);

            if (scannerRoomId && roomList.map(item => item.room_id).includes(scannerRoomId)) {

                // Get Currently Detected Beacon in the scanner
                const beaconListInScanner = beaconListener.getApplicableBeaconsInScanner(scanner.module_identifier);
                let beaconDetailListOfScanner = beaconList.filter(function (item) {
                    return beaconListInScanner.includes(item.device_identifier);
                });

                // Check if room is already added in response
                const isRoomAddedInResponse = response.findIndex(function (item) {
                    return item.room_id == scannerRoomId;
                });

                // if room is not added in response
                // if (isRoomAddedInResponse == -1) {

                const room = roomList.find(function (item) {
                    return item.room_id == scannerRoomId;
                });

                if (room) {
                    room.deviceList = beaconDetailListOfScanner;
                    response.push(room);
                }

                // } else {

                //     const existingDeviceIds = response[isRoomAddedInResponse].deviceList.map(item => item.device_id);
                //     for (let beacon of beaconDetailListOfScanner) {
                //         if (!existingDeviceIds.includes(beacon.device_id)) {
                //             response[isRoomAddedInResponse].deviceList.push(beacon);
                //         }
                //     }
                // }

            }

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));

    } catch (error) {
        logger.error('Device Controller | Get Beacons Locations Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR))
    }
}
