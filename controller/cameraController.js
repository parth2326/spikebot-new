const fs_extra = require('fs-extra');
// Shivam Added the general Util
const {
    homeAutomationModule,
    appContext,
    shared
} = require('./../util/general-util');

const firebaseUtil = require('./../util/firebase-util');
const logModel = require('./../models/logs');
const cameraModel = require('./../models/camera');
const userModel = require('./../models/user');
const generalMetaModel = require('./../models/general_meta');
const homeControllerModel = require('./../models/home_controller');
const dbManager = require('./../util/db-manager');
const syncManager = require('./../util/sync-manager');

let videoStitch = require('video-stitch');
const { constant } = require('underscore');

// module.exports = function (homeAutomationModule, appContext, shared) {
let appUtil = homeAutomationModule.appUtil,
    constants = homeAutomationModule.constants,
    sqliteDB = shared.db.sqliteDB,
    camera_vpn_port = constants.camera_vpn_port,
    notificationUtil = shared.util.notificationUtil,
    viewerDetails = appContext.viewerDetails,
    homeControllerDeviceId = viewerDetails.deviceID,
    time_format = appUtil.timeFormat(),
    moment = shared.util.moment,
    fs = shared.util.fs,
    exec = shared.util.exec,
    unixUtil = homeAutomationModule.unixUtil,
    finder = shared.util.finder,
    camera_dir = '/home/pi/node/homeController/static/storage/volume/pi/';

const appendPromise = (path, data) => {
    new Promise((resolve, reject) => {
        fs.appendFile(path, data, (err) => {
            if (err) reject(err)
            else resolve()
        })
    })
}

// Fix Camera
exports.fixCamera = async function (param, cb) {
    try {
        let cameraList = await cameraModel.list();

        for (let camera of cameraList) {
            let url = camera.camera_ip + camera.camera_videopath;
            let srs = srsfile(camera.camera_id, url);
            await appendPromise("/usr/local/srs/conf/srs.conf", srs);
        }
        //Service Restart
        exec("sudo /etc/init.d/srs restart", function (error, stdout, stderr) {
            //logger.info('Camera Restarted!');
        });

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
        return res.send(response);

    } catch (error) {
        logger.error('Camera Controller | FIX Camera Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

//Add a New Camera
/**
 * Add Camera
 * @param camera_name (required)
 * @param camera_ip (required)
 * @param video_path (required)
 * @param user_name (required)
 * @param password (required)
 */
exports.addCamera = function (devices, cb) {

    //logger.info('addCamera ', devices);
    try {
        let camera_name = devices.camera_name,
            camera_ip = devices.camera_ip,
            video_path = devices.video_path,
            cameraUserName = devices.user_name,
            cameraPassword = devices.password,
            phone_id = devices.authenticatedUser.phone_id,
            phone_type = devices.authenticatedUser.phone_type,

            url = camera_ip + video_path,
            camera_id = appUtil.generateRandomId(),
            created_date = appUtil.currentDateTime(),
            cameraIcon = "camera",

            // user_id = devices.user_id,
            // userEmail = user_id,
            // user_name = '',
            camera_url = '/live/livestream' + camera_id;

        // appUtil.getUserName(devices.user_id, function (result) {
        //     user_name = result;
        // });

        if (camera_id == '' || camera_name == '' || camera_ip == '' || video_path == '' || cameraUserName == '' || cameraPassword == '' || phone_id == '' || phone_type == '') {
            //logger.info('All Details required!');
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        }

        // Check if duplicate camera name exists
        // var duplicateCameraName = await dbManager.all(constants.CHECK_DUPLICATE_CAMERA_NAME, [camera_name]);
        // if (duplicateCameraName.length > 0) {
        //     logger.error("Duplicate Camera Name Error ", error);
        //     response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        //     return cb(null, response);
        // }

        let duplicateCameraName = sqliteDB.prepare(constants.CHECK_DUPLICATE_CAMERA_NAME);
        duplicateCameraName.all(camera_name, async function (error, result) {
            if (error) {
                logger.error("Duplicate Camera Name Error ", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {

                if (result.length > 0) {
                    logger.warn("Camera With Same Name Already Exist");
                    response = appUtil.createSuccessResponse(constants.responseCode.SAME_CAMERA_NAME);
                    return cb(null, response);
                } else {

                    const camera = await cameraModel.add({
                        camera_id: camera_id,
                        user_id: devices.authenticatedUser.user_id,
                        home_controller_device_id: homeControllerDeviceId,
                        camera_name: camera_name,
                        camera_ip: camera_ip,
                        camera_videopath: video_path,
                        camera_vpn_port: camera_vpn_port,
                        user_name: cameraUserName,
                        password: cameraPassword,
                        camera_url: camera_url,
                        jetson_device_id: devices.jetson_device_id
                    });

                    if (devices.confidence_score_day != null) {
                        await generalMetaModel.update(camera_id, 'camera', 'confidence_score_day', devices.confidence_score_day);
                    }

                    if (devices.confidence_score_night != null) {
                        await generalMetaModel.update(camera_id, 'camera', 'confidence_score_night', devices.confidence_score_night);
                    }


                    // var addcameradetails = sqliteDB.prepare(constants.ADD_CAMERA_DETAILS);
                    // addcameradetails.run(camera_id, user_id, homeControllerDeviceId, camera_name, camera_ip, video_path, cameraIcon, camera_vpn_port, cameraUserName, cameraPassword, camera_url, created_date, userEmail, function (error, response) {
                    // 	if (error) {
                    // 		logger.error("ADD_CAMERA_DETAILS Error ", error);
                    // 		response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    // 		return cb(null, response);
                    // 	} else {
                    // if (fs.existsSync(dir)) {
                    // 	console.log("exists");
                    // 	dir = camera_dir + '' + camera_id;
                    // 	fs.mkdirSync(dir);
                    // }

                    // //logger.info('Storage Camera Dir Added ', dir);

                    let srs = srsfile(camera.camera_id, url);

                    let rcd = "\n" +
                        "sudo /usr/bin/ffmpeg -i rtmp://127.0.0.1/live/livestream" + camera.camera_id + " -t 900 -vcodec copy /media/pi/" + camera.camera_id + "-$name.mp4";
                    // var rcd = "\n" +
                    //     "sudo /usr/bin/ffmpeg -i rtmp://127.0.0.1/live/livestream" + camera.camera_id + ' -c:v "libx264"  -vf "scale=640:480" -map 0 -f segment -segment_time 900  -strftime 1  /media/pi/' + camera.camera_id + "-$name.mp4";

                    fs.appendFile("/usr/local/srs/conf/srs.conf", srs, function (error) {
                        if (error) {
                            response = appUtil.createErrorResponse(constants.responseCode.FILE_APPEND_FAILED);
                            return cb(null, response);
                        } else {

                            // //logger.info('srs camera details added!');
                            fs.appendFile("/camera/rcd/rcd.sh", rcd, function (error) {
                                if (error) {
                                    response = appUtil.createErrorResponse(constants.responseCode.FILE_APPEND_FAILED);
                                    return cb(null, response);
                                } else {

                                    // Log Added to the System
                                    logModel.add({
                                        log_object_id: camera.camera_id,
                                        log_type: "camera_add",
                                        description: camera_name
                                    }, params.authenticatedUser);

                                    appUtil.generateCameraToken(camera.camera_id, function (error, result) {
                                        if (error) {
                                            //logger.info("Error in generating camera token");
                                        } else {
                                            let token = result.data.append_token,
                                                camera_id = result.data.camera_id;

                                            constants.camera_tokens[camera.camera_id] = {
                                                camera_id: camera.camera_id,
                                                token: token
                                            };
                                        }
                                    });

                                    //Service Restart
                                    exec("sudo /etc/init.d/srs restart", function (error, stdout, stderr) {
                                        //logger.info('Camera Restarted!');
                                    });

                                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                                    return cb(null, response);
                                }
                            });
                        }
                    });
                }
            }
        });
    } catch (error) {
        logger.error('Camera Controller | Add Camera Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

};

exports.updateRecordingFile = async (params, cb) => {

    try {
        let cameraList = await cameraModel.list();
        for (let camera of cameraList) {
            let rcd = "\n" +
                "sudo /usr/bin/ffmpeg -i rtmp://127.0.0.1/live/livestream" + camera.camera_id + " -t 900 -vcodec copy /media/pi/" + camera.camera_id + "-$name.mp4";
            fs.appendFile("/camera/rcd/rcd.sh", rcd, function (error) {

            });
        }

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Update recording File Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}


//Get Camera Details for View and edit
exports.editCamera = async function (camera_id, cb) {
    try {
        let result = await dbManager.all(constants.GET_CAMERA_INFO, [camera_id]);
        if (result.length > 0) {
            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, result);
            return cb(null, response);
        } else {
            response = appUtil.createErrorResponse(constants.responseCode.NOT_FOUND);
            return cb(null, response);
        }
    } catch (error) {
        logger.error('Camera Controller | Edit Camera Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

// Save Edit Camera
exports.updateCamera = function (devices, cb) {

    //logger.info('updateCamera ', devices);
    try {
        let camera_id = devices.camera_id,
            camera_name = devices.camera_name,
            camera_ip = devices.camera_ip,
            video_path = devices.camera_videopath,
            user_name = devices.user_name,
            password = devices.password,
            phone_id = devices.phone_id,
            phone_type = devices.phone_type,
            confidence_score = devices.confidence_score,

            user_id = devices.user_id,
            camera_sync = 0;

        let new_url = camera_ip + video_path;

        if (camera_id == '' || camera_name == '' || camera_ip == '' || video_path == '' || user_name == '' || password == '' || phone_id == '' || phone_type == '') {
            //logger.info('All Details required!');
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else {
            let checkDuplicateCameraname = sqliteDB.prepare(constants.CHECK_SAME_CAMERA_NAME);
            checkDuplicateCameraname.all(camera_name, camera_id, function (error, result) {
                if (error) {
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(null, response);
                } else {
                    if (result.length > 0) {
                        logger.warn('Same Camera name detected!');
                        response = appUtil.createErrorResponse(constants.responseCode.SAME_CAMERA_NAME);
                        return cb(null, response);
                    } else {
                        let getOriginalCameraDetails = sqliteDB.prepare(constants.GET_CAMERA_DETAILS_BY_ID);
                        getOriginalCameraDetails.all(camera_id, function (error, result) {
                            if (error) {
                                logger.error("Get Camera Details for Update Error ", error);
                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                return cb(null, response);
                            } else {
                                if (result.length > 0) {

                                    let original_camera_ip = result[0].camera_ip,
                                        original_cameravideo_path = result[0].camera_videopath,
                                        original_camera_name = result[0].camera_name;
                                    original_url = original_camera_ip + original_cameravideo_path;
                                    devices.phone_type = result[0].jetson_device_id;

                                    if (original_camera_name != camera_name && original_camera_ip == camera_ip && original_cameravideo_path == video_path) {
                                        let saveCameraDetails = sqliteDB.prepare(constants.UPDATE_CAMERA);
                                        saveCameraDetails.all(camera_name, camera_ip, video_path, user_name, password, camera_sync, camera_id, async function (error, result) {
                                            if (error) {
                                                logger.error("Update Camera Details Error ", error);
                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                return cb(null, response);
                                            } else {

                                                if (devices.confidence_score_day != null) {
                                                    await generalMetaModel.update(camera_id, 'camera', 'confidence_score_day', devices.confidence_score_day);
                                                }

                                                if (devices.confidence_score_night != null) {
                                                    await generalMetaModel.update(camera_id, 'camera', 'confidence_score_night', devices.confidence_score_night);
                                                }

                                                logModel.add({
                                                    log_object_id: camera_id,
                                                    log_type: "camera_update",
                                                    description: camera_name
                                                }, {
                                                    user_id: user_id,
                                                    phone_id: phone_id,
                                                    phone_type: phone_type
                                                });

                                                // generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);

                                                await syncManager.updateCameraTables();
                                                setTimeout(async function () {
                                                    if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                                        await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
                                                        await syncManager.updateTable('spike_general_meta');
                                                    }
                                                }, 15000);

                                                let data = {
                                                    camera_id: camera_id,
                                                    camera_name: camera_name,
                                                    camera_ip: camera_ip,
                                                    video_path: video_path,
                                                    user_name: user_name,
                                                    password: password
                                                };

                                                response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_UPDATE, data);
                                                return cb(null, response);
                                            }
                                        });
                                        saveCameraDetails.finalize();
                                    } else {
                                        fs.readFile('/usr/local/srs/conf/srs.conf', 'utf8', function (err, data) {
                                            if (err) {
                                                response = appUtil.createErrorResponse(constants.responseCode.FILE_READ_FAILED);
                                                return cb(null, response);
                                            } else {
                                                let original_srs = srsfile(camera_id, original_url);
                                                let new_srs = srsfile(camera_id, new_url);

                                                let replacement = data.replace(original_srs, new_srs);
                                                fs.writeFile('/usr/local/srs/conf/srs.conf', replacement, 'utf8', function (error) {
                                                    if (error) {
                                                        response = appUtil.createErrorResponse(constants.responseCode.FILE_WRITE_FAILED);
                                                        return cb(null, response);
                                                    } else {
                                                        //logger.info("new url ", new_url);
                                                        let saveCameraDetails = sqliteDB.prepare(constants.UPDATE_CAMERA);
                                                        saveCameraDetails.all(camera_name, camera_ip, video_path, user_name, password, camera_sync, camera_id, async function (error, result) {
                                                            if (error) {
                                                                logger.error("Update Camera Details Error ", error);
                                                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                                                return cb(null, response);
                                                            } else {

                                                                if (devices.confidence_score_day != null) {
                                                                    generalMetaModel.update(camera_id, 'camera', 'confidence_score_day', devices.confidence_score_day);
                                                                }

                                                                if (devices.confidence_score_night != null) {
                                                                    generalMetaModel.update(camera_id, 'camera', 'confidence_score_night', devices.confidence_score_night);
                                                                }

                                                                logModel.add({
                                                                    log_object_id: camera_id,
                                                                    log_type: "camera_update",
                                                                    description: camera_name
                                                                }, {
                                                                    user_id: user_id,
                                                                    phone_id: phone_id,
                                                                    phone_type: phone_type
                                                                });

                                                                await syncManager.updateCameraTables();
                                                                setTimeout(async function () {
                                                                    if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                                                        await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);

                                                                        await syncManager.updateTable('spike_general_meta');
                                                                    }
                                                                }, 15000);



                                                                appUtil.generateCameraToken(camera_id, function (error, result) {
                                                                    if (error) {
                                                                        //logger.info("Error in generating camera token");
                                                                    } else {
                                                                        let token = result.data.append_token,
                                                                            camera_id = result.data.camera_id;

                                                                        constants.camera_tokens[camera_id] = {
                                                                            camera_id: camera_id,
                                                                            token: token
                                                                        };
                                                                    }
                                                                });

                                                                //Camera Service Restart
                                                                exec("sudo /etc/init.d/srs restart", function (error, stdout, stderr) {
                                                                    //logger.info('Camera Restarted!');
                                                                });


                                                                let data = {
                                                                    camera_id: camera_id,
                                                                    camera_name: camera_name,
                                                                    camera_ip: camera_ip,
                                                                    video_path: video_path,
                                                                    user_name: user_name,
                                                                    password: password
                                                                };

                                                                response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_UPDATE, data);
                                                                return cb(null, response);
                                                            }
                                                        });
                                                        saveCameraDetails.finalize();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                } else {
                                    logger.error('Camera not found');
                                    response = appUtil.createErrorResponse(constants.responseCode.CAMERA_NOT_FOUND);
                                    return cb(null, response);
                                }
                            }
                        });
                    }
                }
            });
        }
    } catch (error) {
        logger.error('Camera Controller | Update Camera Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

/**
 * Delete Camera
 * @param {*} camera_id 
 * @param {*} cb 
 */
exports.deleteCamera = function (device, cb) {
    logger.info('deleteCamera ', device);
    try {
        let camera_id = device.camera_id,
            phone_id = device.authenticatedUser.phone_id,
            phone_type = device.authenticatedUser.phone_type,

            user_id = device.authenticatedUser.user_id,
            user_name = '';

        appUtil.getUserName(user_id, function (result) {
            user_name = result;
        });

        if (camera_id == '' || phone_id == '' || phone_type == '') {
            //logger.info('All Details required!');
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        }

        let checkCameraDetails = sqliteDB.prepare(constants.GET_CAMERA_INFO);
        checkCameraDetails.all(camera_id, function (error, result) {
            if (error) {
                logger.error("Delete camera error ", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                return cb(null, response);
            } else {
                if (result.length > 0) {
                    let camera_ip = result[0].camera_ip,
                        cameravideo_path = result[0].camera_videopath,
                        url = camera_ip + cameravideo_path;

                    let dir = camera_dir + '' + camera_id;
                    fs_extra.remove(dir, function (err) {

                        if (err) {
                            console.log(err);
                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, err);
                            return cb(null, response);
                        } else {
                            let camera_name = result[0].camera_name;
                            let deletecamera = sqliteDB.prepare(constants.DELETE_CAMERA);
                            deletecamera.run(camera_id, function (error, result) {
                                if (error) {
                                    logger.error("Delete camera error ", error);
                                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                                    return cb(null, response);
                                } else {
                                    let rcd = "\n" +
                                        "ffmpeg -i rtmp://127.0.0.1/live/livestream" + camera_id + " -vcodec copy /camera/storage/" + camera_id + "/$name.mp4 </dev/null >/dev/null 2>/tmp/" + camera_id + "$name.log &";

                                    fs.readFile('/camera/rcd/rcd.sh', 'utf8', function (err, rcd_data) {
                                        if (err) {
                                            response = appUtil.createErrorResponse(constants.responseCode.FILE_READ_FAILED);
                                            return cb(null, response);
                                        } else {
                                            let rcd_replacement = rcd_data.replace(rcd, '');
                                            fs.writeFile('/camera/rcd/rcd.sh', rcd_replacement, 'utf8', function (error) {
                                                if (error) {
                                                    response = appUtil.createErrorResponse(constants.responseCode.FILE_WRITE_FAILED);
                                                    return cb(null, response);
                                                }
                                                //logger.info("Deleted from rcd.sh");

                                                let srs = srsfile(camera_id, url);
                                                fs.readFile('/usr/local/srs/conf/srs.conf', 'utf8', function (err, srs_data) {
                                                    if (err) {
                                                        response = appUtil.createErrorResponse(constants.responseCode.FILE_READ_FAILED);
                                                        return cb(null, response);
                                                    } else {
                                                        let srs_replacement = srs_data.replace(srs, '');
                                                        fs.writeFile('/usr/local/srs/conf/srs.conf', srs_replacement, 'utf8', function (error) {
                                                            if (error) {
                                                                response = appUtil.createErrorResponse(constants.responseCode.FILE_WRITE_FAILED);
                                                                return cb(null, response);
                                                            } else {
                                                                //logger.info("Deleted from srs.config");

                                                                //Restart Cameras
                                                                exec("sudo /etc/init.d/srs restart", function (error, stdout, stderr) {
                                                                    //logger.info('Camera Restarted!');
                                                                });

                                                                let deleteCameraAlert = sqliteDB.prepare(constants.DELETE_CAMERA_ALERT);
                                                                deleteCameraAlert.run(camera_id, function (error, result) {
                                                                    if (error) {
                                                                        logger.error("Delete camera error ", error);
                                                                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                                                                        return cb(null, response);
                                                                    } else {

                                                                        generalMetaModel.deleteByTableIdAndTableName(camera_id, 'camera');

                                                                        logModel.add({
                                                                            log_type: "camera_delete",
                                                                            description: camera_name
                                                                        }, {
                                                                            user_id: user_id,
                                                                            phone_id: phone_id,
                                                                            phone_type: phone_type
                                                                        });

                                                                        setTimeout(function () {
                                                                            homeControllerModel.update(homeControllerDeviceId, {
                                                                                camera_sync_status: '0'
                                                                            });
                                                                        }, 15000);


                                                                        response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_DELETE);
                                                                        return cb(null, response);
                                                                    }
                                                                });
                                                                deleteCameraAlert.finalize();
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                            deletecamera.finalize();
                        }
                    });
                } else {
                    logger.error('Camera not found');
                    response = appUtil.createErrorResponse(constants.responseCode.CAMERA_NOT_FOUND);
                    return cb(null, response);
                }
            }
        });
    } catch (error) {
        logger.error('Camera Controller | Delete Camera Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};


//Get Camera list to sync with cloud
exports.getCameraToBySync = function (cb) {
    try {
        let cameralist = sqliteDB.prepare(constants.GET_CAMERA_LIST_TO_SYNC);
        cameralist.all(0, function (error, result) {
            if (error) {
                logger.error("Sync Camera list error ", error);
            } else {
                if (result.length > 0) {
                    let data = {
                        cameraList: result
                    };
                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                    return cb(null, response);
                }
            }
        });
        cameralist.finalize();
    } catch (error) {
        logger.error('Camera Controller | Get Camera To By Sync Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

// update activity status which are sync with cloud success
exports.updateCameraListFromCloud = function (deviceList, cb) {
    try {
        let cameraSync = '1';
        let totalCameralist = deviceList.length,
            totalCameraProcessed = 0,
            isError = false;

        let callBackFunction = function () {
            if (totalCameraProcessed == totalCameralist) {
                if (isError == true) {
                    let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(errorResponse, null);
                } else {
                    let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                    return cb(null, response);
                }
            }
        };

        deviceList.forEach(function (device) {
            let updateCameralist = sqliteDB.prepare(constants.UPDATE_CAMERA_LIST_SYNC);
            updateCameralist.run(cameraSync, device.camera_id, function (error, result) {
                if (error) {
                    totalCameraProcessed = totalCameraProcessed + 1;
                    logger.error("UPDATE Camera From Local Error ", error);
                    callBackFunction();
                } else {
                    totalCameraProcessed = totalCameraProcessed + 1;
                    callBackFunction();
                }
            });
            updateCameralist.finalize();
        });
    } catch (error) {
        logger.error('Camera Controller | Update Camera List From Cloud Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get Camera Recording list
exports.getCameraRecording = function (devices, cb) {
    //logger.info("getCameraRecording", devices);
    try {
        let cameraList = [];
        let totalFiles = 0,
            totalProcessFile = 0;
        let camera_id = devices.camera_id;

        //	fs.readdir('/camera/storage/' + camera_id, function (error, result) {
        fs.readdir('/home/pi/node/homeController/static/storage/volume/pi/' + camera_id, function (error, result) {
            //fs.readdir('/home/pi/node/homeController/static/storage/volume/pi', function (error, result) {
            if (error) {
                logger.error(error);
            } else {
                if (result.length > 0) {
                    totalFiles = result.length;
                    //callBack();

                    let callBack = function () {
                        if (totalFiles === totalProcessFile) {
                            let data = {
                                cameraList: cameraList
                            };
                            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                            return cb(null, response);
                        }
                    };

                    result.forEach(function (data) {
                        cameraList.push(data);
                        totalProcessFile++;
                        callBack();
                    });

                } else {
                    //logger.info('No Files found on this path!');
                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                    return cb(null, response);
                }
            }
        });
    } catch (error) {
        logger.error('Camera Controller | Get Camera Recording Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get camera List by Date
/* 
	Change in Library fs-finder : Base.prototype.getPathsSync in base.js -- added var temp_path = _path; to include only file name not PATH
	Reason : all video file modified after 15 minutes. So add 15 minutes in both time to send correct file list.
	start date : add 15 minute and keep 00 sec //end date : add 15 minute and keep 00 sec
 */

exports.getCameraRecordingByDate = function (devices, cb) {
    //logger.info('getCameraRecordingByDate ', devices);
    try {
        let camera_ids = devices.camera_details,
            camera_start_date = moment(devices.camera_start_date, time_format).add(15, 'minute').format(time_format),
            camera_end_date = moment(devices.camera_end_date, time_format).add(15, 'minute').format(time_format),
            camera_id = [],
            cameraList = [],
            i = 0,
            cameraArray = [],
            camera_files_name, process = 0,
            camera_files = [];



        for (let k = 0; k < camera_ids.length; k++) {
            //var baseDir = '/home/pi/node/homeController/static/storage/volume/pi/' + camera_ids[k].camera_id;					


            let baseDir = '/home/pi/node/homeController/static/storage/volume/pi/';
            camera_files_name = finder.from(baseDir).date('>', camera_start_date).date('<', camera_end_date).findFiles(camera_ids[k].camera_id);
            cameraArray.push({
                camera_id: camera_ids[k].camera_id,
                camera_name: camera_ids[k].camera_name,
                camera_files: camera_files_name
            });
            camera_files_name = '';
            i++;
        }

        callBackFunction();

        function callBackFunction() {
            if (i >= camera_ids.length) {
                //logger.info('Total Files ', cameraArray.length);
                let data = {
                    cameraList: cameraArray
                };
                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);
            }
        }
    } catch (error) {
        logger.error('Camera Controller | Get Camera Recording By Date Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

};

exports.getCameraVideo = function (params, cb) {
    try {
        params.timestamp = params.timestamp.slice(0, 10);

        let mins = moment.unix(params.timestamp).format('mm');
        //logger.info('CAMERA', mins, moment.unix(params.timestamp).format(time_format));

        let startTime;
        let endTime;
        let skipTime = 0;

        skipTime = mins % 15;

        if (mins >= 58) {
            //     skipTime = (mins > 58) ? (15 - (60 - mins)) : (15 - (2 - mins));
            startTime = moment.unix(params.timestamp).subtract(1, 'hour').set({ m: 45 });
            endTime = moment.unix(params.timestamp).set({ m: 15 });
        } else if (mins < 13) {
            startTime = moment.unix(params.timestamp).set({ m: 0 });
            endTime = moment.unix(params.timestamp).set({ m: 15 });
        } else if (mins < 15) {
            //     skipTime = (mins <= 15) ? (15 - (17 - mins)) : (15 - (17 - mins));
            startTime = moment.unix(params.timestamp).set({ m: 0 });
            endTime = moment.unix(params.timestamp).set({ m: 30 });
        } else if (mins < 28) {
            //     skipTime = (mins - 17);
            startTime = moment.unix(params.timestamp).set({ m: 15 });
            endTime = moment.unix(params.timestamp).set({ m: 30 });
        } else if (mins < 30) {
            //     skipTime = (15 - (32 - mins));
            startTime = moment.unix(params.timestamp).set({ m: 15 });
            endTime = moment.unix(params.timestamp).set({ m: 45 });
        } else if (mins < 43) {
            //     skipTime = (mins - 32);
            startTime = moment.unix(params.timestamp).set({ m: 30 });
            endTime = moment.unix(params.timestamp).set({ m: 45 });
        } else if (mins < 45) {
            //     skipTime = (15 - (47 - mins));
            startTime = moment.unix(params.timestamp).set({ m: 30 });
            endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
        } else if (mins < 58) {
            //     skipTime = (mins - 47);
            startTime = moment.unix(params.timestamp).set({ m: 45 });
            endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
        }


        // if (mins < 2 || mins > 58) {
        // //     skipTime = (mins > 58) ? (15 - (60 - mins)) : (15 - (2 - mins));
        //     startTime = moment.unix(params.timestamp).subtract(1, 'hour').set({ m: 45 });
        //     endTime = moment.unix(params.timestamp).set({ m: 15 });
        // } else if (mins >= 2 && mins <= 13) {
        // //     skipTime = (mins - 2);
        //     startTime = moment.unix(params.timestamp).set({ m: 0 });
        //     endTime = moment.unix(params.timestamp).set({ m: 15 });
        // } else if (mins > 13 && mins < 17) {
        // //     skipTime = (mins <= 15) ? (15 - (17 - mins)) : (15 - (17 - mins));
        //     startTime = moment.unix(params.timestamp).set({ m: 0 });
        //     endTime = moment.unix(params.timestamp).set({ m: 30 });
        // } else if (mins >= 17 && mins <= 28) {
        // //     skipTime = (mins - 17);
        //     startTime = moment.unix(params.timestamp).set({ m: 15 });
        //     endTime = moment.unix(params.timestamp).set({ m: 30 });
        // } else if (mins > 28 && mins < 32) {
        // //     skipTime = (15 - (32 - mins));
        //     startTime = moment.unix(params.timestamp).set({ m: 15 });
        //     endTime = moment.unix(params.timestamp).set({ m: 45 });
        // } else if (mins >= 32 && mins <= 43) {
        // //     skipTime = (mins - 32);
        //     startTime = moment.unix(params.timestamp).set({ m: 30 });
        //     endTime = moment.unix(params.timestamp).set({ m: 45 });
        // } else if (mins > 43 && mins < 47) {
        // //     skipTime = (15 - (47 - mins));
        //     startTime = moment.unix(params.timestamp).set({ m: 45 });
        //     endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
        // } else if (mins >= 47 && mins <= 58) {
        // //     skipTime = (mins - 47);
        //     startTime = moment.unix(params.timestamp).set({ m: 45 });
        //     endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
        // }

        startTime = startTime.add(15, 'minute').format(time_format);
        endTime = endTime.add(5, 'minute').format(time_format);

        cameraArray = [];
        //logger.info('startTime', startTime);
        //logger.info('endTime', endTime);

        let baseDir = '/home/pi/node/homeController/static/storage/volume/pi/';
        let camera_files_name = finder.from(baseDir).date('>', startTime).date('<', endTime).findFiles(params.camera_id);

        if (camera_files_name.length == 0) {

            let data = {
                skip_time: 0,
                camera_id: params.camera_id,
                camera_files: []
            }
            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);
        }

        if (camera_files_name.length == 1) {

            let data = {
                skip_time: skipTime > 0 ? (skipTime * 60) - 5 : 0,
                camera_id: params.camera_id,
                camera_files: [camera_files_name[0].toString().replace(baseDir, "static/storage/volume/pi/")]
            }

            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);

        }

        let clips = [];
        for (let i = 0; i < camera_files_name.length; i++) {
            clips.push({
                fileName: camera_files_name[i]
            })
        }

        let videoConcat = videoStitch.concat;


        videoConcat({
            silent: false, // optional. if set to false, gives detailed output on console
            overwrite: true // optional. by default, if file already exists, ffmpeg will ask for overwriting in console and that pause the process. if set to true, it will force overwriting. if set to false it will prevent overwriting.
        })
            .clips(clips)
            .output("/home/pi/node/homeController/static/storage/volume/pi/" + params.timestamp + ".mp4") //optional absolute file name for output file
            .concat()
            .then((outputFileName) => {

                let data = {
                    skip_time: skipTime > 0 ? (skipTime * 60) - 5 : 0,
                    camera_id: params.camera_id,
                    camera_files: [outputFileName.toString().replace(baseDir, "static/storage/volume/pi/")]
                }

                response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
                return cb(null, response);

            });
    } catch (error) {
        logger.error('Camera Controller | Get Camera Video Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * 
 * @param {*} camera_id 
 * @param {*} timestamp 
 */
exports.getNotificationGifByTimestamp = function (params, cb) {
    try {
        console.log('Params : ', params);
        
        const date = new Date(parseInt(params.timestamp));
        const datePrefix = date.getFullYear()+''+(date.getMonth()+1)+''+date.getDate();
        const prefix = `backup/${homeControllerDeviceId}/python_images/${datePrefix}/${params.camera_id}`;

        

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, prefix));
    } catch(error) {
        logger.error('Camera Controller | Get Notification GIF Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }     
}

/** 
 * Get Recording From Timestamp
 * @param camera_id (required)
 * @param timestamp (Required)
 */
exports.getCameraRecordingByTimestamp = function (params, cb) {

    //logger.info('PARAMS FOR VIDEO', params);

    params.timestamp = params.timestamp.slice(0, 10);

    let mins = moment.unix(params.timestamp).format('mm');
    //logger.info('CAMERA', mins, moment.unix(params.timestamp).format(time_format));

    let startTime;
    let endTime;
    let skipTime = 0;

    if (mins < 2 || mins > 58) {
        skipTime = (mins > 58) ? (15 - (60 - mins)) : (15 - (2 - mins));
        startTime = moment.unix(params.timestamp).subtract(1, 'hour').set({ m: 45 });
        endTime = moment.unix(params.timestamp).set({ m: 15 });
    } else if (mins >= 2 && mins <= 13) {
        skipTime = (mins - 2);
        startTime = moment.unix(params.timestamp).set({ m: 0 });
        endTime = moment.unix(params.timestamp).set({ m: 15 });
    } else if (mins > 13 && mins < 17) {
        skipTime = (mins <= 15) ? (15 - (17 - mins)) : (15 - (17 - mins));
        startTime = moment.unix(params.timestamp).set({ m: 0 });
        endTime = moment.unix(params.timestamp).set({ m: 30 });
    } else if (mins >= 17 && mins <= 28) {
        skipTime = (mins - 17);
        startTime = moment.unix(params.timestamp).set({ m: 15 });
        endTime = moment.unix(params.timestamp).set({ m: 30 });
    } else if (mins > 28 && mins < 32) {
        skipTime = (15 - (32 - mins));
        startTime = moment.unix(params.timestamp).set({ m: 15 });
        endTime = moment.unix(params.timestamp).set({ m: 45 });
    } else if (mins >= 32 && mins <= 43) {
        skipTime = (mins - 32);
        startTime = moment.unix(params.timestamp).set({ m: 30 });
        endTime = moment.unix(params.timestamp).set({ m: 45 });
    } else if (mins > 43 && mins < 47) {
        skipTime = (15 - (47 - mins));
        startTime = moment.unix(params.timestamp).set({ m: 45 });
        endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
    } else if (mins >= 47 && mins <= 58) {
        skipTime = (mins - 47);
        startTime = moment.unix(params.timestamp).set({ m: 45 });
        endTime = moment.unix(params.timestamp).add(1, "hour").set({ m: 0 });
    }

    startTime = startTime.add(15, 'minute').format(time_format);
    endTime = endTime.add(5, 'minute').format(time_format);

    cameraArray = [];
    //logger.info('startTime', startTime);
    //logger.info('endTime', endTime);

    let baseDir = '/home/pi/node/homeController/static/storage/volume/pi/';
    let camera_files_name = finder.from(baseDir).date('>', startTime).date('<', endTime).findFiles(params.camera_id);

    for (let i = 0; i < camera_files_name.length; i++) {
        camera_files_name[i] = camera_files_name[i].toString().replace(baseDir, "static/storage/volume/pi/");
    }

    let data = {
        skip_time: skipTime,
        camera_id: params.camera_id,
        camera_files: camera_files_name
    }

    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    return cb(null, response);
};

function srsfile(camera_id, url) {
    let srs = "\n" +
        "vhost __defaultVhost" + camera_id + "__ {\n\n" +
        "\t ingest livestream" + camera_id + "{\n" +
        "\t\t enabled on;\n" +
        "\t\t input {\n" +
        "\t\t\t  type stream;\n" +
        "\t\t\t  url " + url + ";\n" +
        "\t\t}\n" +
        "\t\t ffmpeg /usr/bin/ffmpeg;\n" +
        "\t\t engine {\n" +
        "\t\t \t enabled off;\n" +
        "\t\t \t output rtmp://127.0.0.1/live/livestream" + camera_id + ";\n" +
        "\t\t }\n" +
        "\t} \n" +
        "}";

    return srs;
}

function checkCameraStatus() {
    logger.info('CHECKING CAMERA STATUS');
    let checkAllCameraList = sqliteDB.prepare(constants.CHECK_CAMERA_LIST);
    checkAllCameraList.all(homeControllerDeviceId, function (error, result) {
        if (error) {
            //logger.info('Check camera status error ', error);
        } else {
            function timeout(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }

            if (result.length > 0) {
                result.forEach(async function (device) {
                    //	//logger.info('Camera ', device);

                    // Wait for 1 minute and run again
                    await timeout(60000);

                    let camera_command = 'timeout 60 ffprobe -v quiet -print_format json -show_streams ' + device.camera_ip + '' + device.camera_videopath;
                    ////logger.info("command", camera_command);
                    // setTimeout(() => {
                    exec(camera_command, function (error, stdout, stderr) {
                        if (stdout.length > 0) {
                            //if (!(JSON.stringify(JSON.parse(stdout)) == '{}')) {
                            if (device.is_active == 0) {
                                updateCameraStatus(1, device.camera_id, device.camera_name);
                            }

                        } else {

                            if (device.is_active == 0) {
                                logger.warn('Camera is already stopped!');
                            } else {
                                logger.warn("Camera is not working ", device.camera_id, device.camera_name);
                                updateCameraStatus(0, device.camera_id, device.camera_name);
                            }
                        }
                    });
                    // }, 61000);
                });
            }
        }
    });

    async function updateCameraStatus(camera_status, camera_id, camera_name) {

        // Update camera details
        await cameraModel.update(camera_id, {
            is_active: camera_status,
            is_sync: 0
        });

        // Update camera status in logs
        logModel.add({
            log_object_id: camera_id,
            log_type: camera_status === 0 ? "camera_inactive" : 'camera_active',
            description: camera_name
        }, {});

        // Getting all users for sending push notifications
        const userList = await userModel.list();

        for (let user of userList) {

            // check if the notification is enabled for that user
            const camera_notification_enable = await generalMetaModel.getMetaValue(user.user_id, 'camera_notification_enable', '1');
            logger.info('GOT NOTIFICATION STATUS AS', user.user_id, camera_notification_enable.toString());

            // Check if the user has priviledge for that camera
            let priviledge = false;
            if (user.admin == 1) {
                priviledge = true
            } else {
                priviledge = await cameraModel.checkIfUserHasCameraPriviledge(user.user_id, camera_id);
            }

            // if the user has priviledge of that camera and notification is enabled for that users
            if (camera_notification_enable == '1' && priviledge == true) {

                let disMessage = user.first_name + ' ' + user.last_name + ', your [' + camera_name + '] camera disconnected!';
                let connMessage = user.first_name + ' ' + user.last_name + ', your [' + camera_name + '] camera connected, back!';
                message = camera_status === 0 ? disMessage : connMessage;

                firebaseUtil.sendMessageNotification({
                    message: message,
                    user_id: user.user_id,
                    data: {}
                });

            }
        }

    }
}

exports.getCameraIds = async function (cb) {
    try {
        result = await dbManager.all(constants.GET_CAMERA_IDS, []);
        let data = {
            cameraIdList: result
        };
        let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Get Camera Ids Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

//Get Camera Token for Individual Camera
exports.getCameraToken = async function (devices, cb) {
    try {
        let camera_id = devices.camera_id;
        let generated_token = constants.camera_tokens[camera_id];

        if (generated_token) {

            let result = await dbManager.all(constants.GET_CAMERA_DETAILS_BY_ID, [camera_id]);
            // camera_vpn_port = result[0].camera_vpn_port;
            camera_url = result[0].camera_url + "" + generated_token.token;
            data = {
                camera_vpn_port: camera_vpn_port,
                camera_url: camera_url
            };

            logger.warn('CAMERA ACCESS', data);
            let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);

        } else {
            let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
            return cb(errorResponse, null);
        }

    } catch (error) {
        logger.error('Camera Controller | Get Camera Token Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

/**
 * @param jetson_id
 */
exports.getAllCameraToken = async function (params, cb) {
    try {
        let user_id = params.authenticatedUser.user_id,
            admin = params.authenticatedUser.admin,
            cameraList = [];

        let query = admin == 1 ? constants.GET_CAMERA_LIST : constants.GET_CHILD_CAMERA_LIST;
        let result = await dbManager.all(query, [user_id]);

        if (params.jetson_id) {
            result = result.filter(function (item) {
                return item.jetson_device_id == params.jetson_id;
            });
        } else {
            result = result.filter(function (item) {
                return item.jetson_device_id == null;
            });
        }

        for (let i = 0; i < result.length; i++) {
            let generated_token = constants.camera_tokens[result[i].camera_id];
            result[i].camera_url = result[i].camera_url + "" + generated_token.token;
            cameraList.push(result[i]);
        }

        // logger.info('RESULT : ', result)
        let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, result);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Get All Camera Token Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

exports.validatecamerakey = async function (devices, cb) {
    try {
        let key = appUtil.encryptPassword(devices.key);
        let camera_encrypted_key = '83fe5cb1f03c308c'; // (spike123)

        if (camera_encrypted_key == key) {
            let result = await dbManager.all(constants.UPDATE_CAMERA_KEY_STATUS, [1, homeControllerDeviceId]);
            let response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_KEY_SUCCESS);
            return cb(null, response);
        } else {
            response = appUtil.createErrorResponse(constants.responseCode.INVALID_KEY);
            return cb(null, response);
        }
    } catch (error) {
        logger.error('Camera Controller | Validate Camera key Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};

setTimeout(() => {
    // process.nextTick(function () {
    checkCameraStatus();
    // });

}, 20000);

setInterval(() => {
    // process.nextTick(function () {
    checkCameraStatus();
    // });
}, 900000);


//Add Camera Notification
exports.addCameraNotification = function (devices, cb) {
    //logger.info("addCameraNotification ", devices);
    try {
        let camera_notification_id = appUtil.generateRandomId(),
            cameraList = devices.cameraList,
            start_time = devices.start_time,
            end_time = devices.end_time,
            alert_interval = devices.alert_interval,

            phone_id = devices.authenticatedUser.phone_id,
            phone_type = devices.authenticatedUser.phone_type,

            totalDeviceProcessed = 0,
            isError = false,

            created_date = appUtil.currentDateTime(),
            user_id = devices.authenticatedUser.user_id,
            user_name = '';

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });

        if (cameraList.length == 0 || start_time.length == 0 || end_time.length == 0 || phone_id == '' || phone_type == '') {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else if (start_time == end_time) {
            response = appUtil.createErrorResponse(constants.responseCode.SAME_START_END_DATE);
            return cb(null, response);
        } else {
            let notification_action = 0;
            devices.camera_notification_id = '';
            checkDuplicateCameraNotification(notification_action, devices, function (error, result) {
                if (error) {
                    logger.error('Check Duplicate Device error ', error);
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                    return cb(null, response);
                } else {
                    if (result.code === 200) { //add New Schedule                        
                        let addNotification = sqliteDB.prepare(constants.ADD_CAMERA_NOTIFICATION);
                        addNotification.run(camera_notification_id, start_time, end_time, alert_interval, user_id, user_id, created_date, function (error, result) {
                            if (error) {
                                logger.error('Add Camera Notification Error ', error);
                                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                                return cb(null, response);
                            } else {

                                let callBackFunction = async function () {
                                    if ((totalDeviceProcessed == cameraList.length)) {
                                        if (isError === true) {
                                            logger.error("Add Schedule Devices Error ");
                                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                                            return cb(response, null);
                                        } else {
                                            await syncManager.updateCameraTables();
                                            setTimeout(async function () {
                                                if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                                    await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
                                                    await syncManager.updateTable('spike_general_meta');
                                                }
                                            }, 15000);

                                            response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_ALERT_ADDED);
                                            return cb(null, response);
                                        }
                                    }
                                };

                                cameraList.forEach(function (device) {

                                    let insertNotificationCameras = sqliteDB.prepare(constants.INSERT_CAMERA_NOTIFICATION_MAPPING);
                                    insertNotificationCameras.run(appUtil.generateRandomId('NOTI-MAP'), camera_notification_id, device.camera_id, function (error, result) {
                                        if (error) {
                                            isError = true;
                                            totalDeviceProcessed = totalDeviceProcessed + 1;
                                            callBackFunction();
                                        } else {
                                            // //logger.info(room_device_id, ' added in a list!');
                                            totalDeviceProcessed = totalDeviceProcessed + 1;
                                            callBackFunction();
                                        }
                                    });
                                });
                            }
                        });
                    } else {
                        return cb(null, result);
                    }
                }
            });
        }
    } catch (error) {
        logger.error('Camera Controller | Add Camera Notification Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

};

// Update Camera Notification
exports.updateCameraNotification = function (devices, cb) {

    try {
        let camera_notification_id = devices.camera_notification_id,
            cameraList = devices.cameraList,
            start_time = devices.start_time,
            end_time = devices.end_time,
            alert_interval = devices.alert_interval,
            phone_id = devices.authenticatedUser.phone_id,
            phone_type = devices.authenticatedUser.phone_type,
            user_id = devices.authenticatedUser.user_id,
            user_name = '',

            created_date = appUtil.currentDateTime(),
            newAddingDevices = devices.camera_ids,
            originalCameraDevices = [],
            deleteUniqueDevices = '',
            addUniqueDevice = '',
            notification_action = 1;

        appUtil.getUserName(user_id, function (result) {
            user_name = result;
        });

        if (cameraList.length == 0 || start_time.length == 0 || end_time.length == 0 || camera_notification_id.length == 0 || phone_id == '' || phone_type == '') {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        } else if (start_time == end_time) {
            response = appUtil.createErrorResponse(constants.responseCode.SAME_START_END_DATE);
            return cb(null, response);
        } else {
            let checkOriginalDetails = sqliteDB.prepare(constants.CHECK_ORIGINAL_CAMERA_ALERT_DATA_FOR_UPDATE);
            checkOriginalDetails.all(camera_notification_id, function (error, result) {
                if (error) {
                    logger.error('Check Schedule details error ', error);
                } else {
                    if ((result.length > 0) || (result[0].camera_id != null)) {

                        let cameras = result[0].camera_id.split(',');
                        //logger.info("cameras", cameras);

                        if (cameras.length > 0) {
                            originalCameraDevices = originalCameraDevices.concat(cameras);
                        } else {
                            originalCameraDevices.push(cameras);
                        }

                        newAddingDevices = newAddingDevices;

                        deleteUniqueDevices = originalCameraDevices.filter(function (val) {
                            return newAddingDevices.indexOf(val) === -1;
                        });

                        addUniqueDevice = newAddingDevices.filter(function (val) {
                            return originalCameraDevices.indexOf(val) === -1;
                        });

                        //logger.info("deleteUniqueDevices", deleteUniqueDevices);
                        //logger.info("addUniqueDevice", addUniqueDevice);

                        if ((deleteUniqueDevices.length == 0) && (addUniqueDevice.length == 0) && (start_time == result[0].start_time) && ((end_time == result[0].end_time) && (alert_interval == result[0].alert_interval))) {
                            response = appUtil.createSuccessResponse(constants.responseCode.NO_MSG);
                            return cb(null, response);
                        } else if ((start_time != result[0].start_time) || (end_time != result[0].end_time) || (deleteUniqueDevices.length != 0) || (addUniqueDevice.length != 0) || (alert_interval != result[0].alert_interval)) {
                            updateProcess(deleteUniqueDevices, addUniqueDevice);
                        }

                    } else {
                        updateProcess(deleteUniqueDevices, addUniqueDevice);
                    }
                }
            });


            function updateProcess(deleteUniqueDevices, addUniqueDevice) {
                if ((deleteUniqueDevices.length > 0 || addUniqueDevice.length > 0)) {
                    checkDuplicateCameraNotification(notification_action, devices, function (error, result) {
                        if (error) {
                            logger.error('Check Duplicate Device error ', error);
                            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                            return cb(null, response);
                        } else {
                            if (result.code === 200) { //update notification alert
                                updateSchedule(deleteUniqueDevices, addUniqueDevice);
                            } else {
                                return cb(null, result);
                            }
                        }
                    });
                } else {
                    updateSchedule(deleteUniqueDevices, addUniqueDevice);
                }
            }


            function updateSchedule(deleteUniqueDevices, addUniqueDevice) {

                let updateschedule = sqliteDB.prepare(constants.UPDATE_CAMERA_NOTIFICATION_ALERT);
                updateschedule.run(start_time, end_time, alert_interval, user_id, created_date, camera_notification_id, async function (error, result) {
                    if (error) {
                        logger.error('Update Camera Notification alert details error ', error);
                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
                        return cb(null, response);
                    } else {
                        let addProcess = 0,
                            deleteProcess = 0;


                        await syncManager.updateCameraTables();
                        setTimeout(async function () {
                            if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
                                await syncManager.updateTable('spike_general_meta');
                            }
                        }, 15000);

                        function promise1() {
                            return new Promise((resolve, reject) => {
                                if (deleteUniqueDevices.length > 0) {
                                    let deleteCallBack = function () {
                                        if (deleteUniqueDevices.length === deleteProcess) {
                                            resolve(true);
                                        }
                                    };

                                    deleteUniqueDevices.forEach(function (device) {
                                        let camera_id = device;
                                        let deleteMoodDevice = sqliteDB.prepare(constants.DELETE_CAMERA_NOTIFICATION_MAPPING);
                                        deleteMoodDevice.run(camera_id, camera_notification_id, function (error, data) {
                                            if (error) {
                                                logger.error('Delete Camera Notification Mapping ', error);
                                                deleteProcess++;
                                                deleteCallBack();
                                            } else {
                                                deleteProcess++;
                                                deleteCallBack();
                                            }
                                        });
                                    });
                                } else {
                                    resolve(true);
                                }
                            });
                        }


                        function promise2() {
                            return new Promise((resolve, reject) => {
                                if (addUniqueDevice.length > 0) {
                                    let addCallBack = function () {
                                        if (addUniqueDevice.length === addProcess) {
                                            resolve(true);
                                        }
                                    };

                                    addUniqueDevice.forEach(function (device) {
                                        let camera_id = device;

                                        let insertScheduleDevices = sqliteDB.prepare(constants.INSERT_CAMERA_NOTIFICATION_MAPPING);
                                        insertScheduleDevices.run(appUtil.generateRandomId('NOTI-MAP'), camera_notification_id, camera_id, function (error, result) {
                                            if (error) {
                                                logger.error('Insert Device error ', error);
                                                addProcess++;
                                                addCallBack();
                                            } else {
                                                addProcess++;
                                                addCallBack();
                                            }
                                        });
                                    });
                                } else {
                                    resolve(true);
                                }
                            });
                        }


                        let callBackFunction = async function () {

                            // syncManager.updateCameraTables();
                            await syncManager.updateCameraTables();
                            setTimeout(async function () {
                                if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                    await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
                                    await syncManager.updateTable('spike_general_meta');
                                }
                            }, 15000);

                            response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_ALERT_UPDATED);
                            return cb(null, response);
                        };

                        function main() {
                            promise1()
                                .then(promise2)
                                .then(callBackFunction);
                        }

                        if ((deleteUniqueDevices.length == 0) && (addUniqueDevice.length == 0)) {
                            callBackFunction();
                        } else {
                            main();
                        }
                    }
                });
            }
        }
    } catch (error) {
        logger.error('Camera Controller | Update Camera Notification Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};


function checkDuplicateCameraNotification(notification_action, devices, cb) {
    try {
        //logger.info("into checkDuplicateCameraNotification" + notification_action);
        let camera_notification_id = devices.camera_notification_id,
            cameraList = devices.cameraList,


            processResult = 0,
            duplicateSchedule = 0,
            cameraNames = [],
            newAddingDevices = [],
            originalCameras = [],
            query1, param1;

        cameraList.forEach(function (device) {
            let id = device.camera_id;
            newAddingDevices.push(id);
        });

        //logger.info('Total New Devices are ', newAddingDevices, cameraList.length);

        if (notification_action === 0) { //add new camera notification query           
            query1 = constants.CHECK_DUPLICATE_CAMERA_NOTIFICATION;
            param1 = [cameraList.length];
        } else if (notification_action === 1) { //update exist camera notification query            
            query1 = constants.CHECK_DUPLICATE_CAMERA_NOTIFICATION_BY_ID;
            param1 = [cameraList.length, camera_notification_id];
        } else {
            response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
            return cb(null, response);
        }

        let checkRoomDevice = sqliteDB.prepare(query1);
        checkRoomDevice.all(param1, function (error, result) {
            if (error) {
                logger.error("Get Room Data Error ", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {
                //logger.info("result of add camera alert", result);
                if ((result.length > 0) && result[0].notification_cameras != null) {
                    //logger.info('Same Number of Devices Found! ', result.length);

                    let callBackFunction = function () {
                        if (processResult === result.length) {
                            if (duplicateSchedule > 0) {
                                let msg = 'Other Notification Alert have same cameras!';
                                let res = {
                                    code: 3001,
                                    message: msg
                                };
                                return cb(null, res);
                            } else {
                                response = appUtil.createErrorResponse(constants.responseCode.SUCCESS);
                                return cb(null, response);
                            }
                        }
                    };

                    result.forEach(function (device) {

                        originalCameras = [];

                        let camera = device.camera_id.split(',');
                        if (camera.length > 0) {
                            originalCameras = originalCameras.concat(camera);
                        } else {
                            originalCameras.push(camera);
                        }

                        let duplicateData = originalCameras.filter(function (val) {
                            return newAddingDevices.indexOf(val) != -1;
                        });


                        if (duplicateData.length == newAddingDevices.length) {
                            duplicateSchedule++;
                        }
                        processResult = processResult + 1;
                        callBackFunction();
                    });
                } else {
                    response = appUtil.createErrorResponse(constants.responseCode.SUCCESS);
                    return cb(null, response);
                }
            }
        });
    } catch (error) {
        logger.error('Camera Controller | Check Duplicate Camera Notification Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

//Delete Camera Notification
exports.deleteCameraNotification = function (devices, cb) {
    //logger.info("deleteCameraNotification", devices);
    try {
        let camera_notification_id = devices.camera_notification_id,
            phone_id = devices.phone_id,
            phone_type = devices.phone_type,
            user_id = devices.user_id,
            user_name = '';

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });

        if (camera_notification_id.length == 0 || phone_id == '' || phone_type == '') {
            response = appUtil.createErrorResponse(constants.responseCode.ALL_DETAILS_REQUIRED);
            return cb(null, response);
        }

        let deleteschedulerecord = sqliteDB.prepare(constants.DELETE_CAMERA_NOTIFICATION_ALERT);
        deleteschedulerecord.all(camera_notification_id, function (error, result) {
            if (error) {
                logger.error("Deleting Camera Notification Error ", error);
                response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                return cb(null, response);
            } else {
                let deletemappingrecord = sqliteDB.prepare(constants.DELETE_CAMERA_MAPPING_RECORDS);
                deletemappingrecord.all(camera_notification_id, function (error, result) {
                    if (error) {
                        logger.error("Deleting Camera Notification Mapping Error ", error);
                        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                        return cb(null, response);
                    } else {

                        setTimeout(function () {
                            if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
                                generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
                            }
                        }, 15000);

                        response = appUtil.createSuccessResponse(constants.responseCode.CAMERA_NOTIFICATION_DELETED);
                        return cb(null, response);
                    }
                });
            }
        });
    } catch (error) {
        logger.error('Camera Controller | Delete Camera Notification Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

};


/**
 * Change Alert Status
 * @param jetson (true / false)
 */
exports.changeCameraAlertStatus = async function (devices, cb) {
    try {
        logger.info("changeCameraAlertStatus", devices);
        let camera_notification_id = devices.camera_notification_id,
            is_active = devices.is_active,

            phone_id = devices.phone_id,
            phone_type = devices.phone_type,
            user_id = devices.user_id,
            user_name = '';

        appUtil.getUserName(devices.user_id, function (result) {
            user_name = result;
        });

        let result = await dbManager.executeNonQuery(constants.UPDATE_CAMERA_ALERT_STATUS, [is_active, camera_notification_id]);

        await syncManager.updateCameraTables();
        if (devices.jetson_id && devices.jetson_id != null && devices.jetson_id.length > 0) {
            await generalMetaModel.update(devices.jetson_id, 'jetson', 'is_changed', 0);
            await syncManager.updateTable('spike_general_meta');
        }

        let res = {
            code: 200,
            message: ' Notification Alert Updated',
        };

        let data = {
            is_active: is_active
        };

        setTimeout(function () {
            homeControllerModel.update(homeControllerDeviceId, {
                camera_sync_status: '0'
            });
        }, 15000);

        response = appUtil.createSuccessResponse(res, data);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Change Camera Alert Status Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
};


// exports.getCameraNotificationAlertList = function (devices, cb) {

// 	var alertList = [],
// 		unreadLogs = [],
// 		cameraIdList = [];


// 	var getAlertDetails = sqliteDB.prepare(constants.GET_CAMERA_ALERT_DETAILS);
// 	getAlertDetails.all(function (error, alertDevices) {
// 		if (error) {
// 			response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
// 			return cb(null, response);
// 		} else {
// 			if (alertDevices.length > 0) {
// 				var processRecord = 0,
// 					unreadprocessRecord = 0,
// 					cameraIdRecord = 0;

// 				var callBackAlertData = function () {
// 					if (processRecord === alertDevices.length) {
// 						var getPushLogs = sqliteDB.prepare(constants.GET_CAMERA_PUSH_LOGS);
// 						getPushLogs.all(function (error, logresult) {
// 							if (error) {
// 								logger.error("Get Camera Push Logs error ", error);
// 								var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
// 								return cb(errorResponse, null);
// 							} else {
// 								var callBackUnread = function () {

// 									if (unreadprocessRecord === logresult.length) {

// 										var getCameraIdNames = sqliteDB.prepare(constants.GET_CAMERA_ID_NAMES);
// 										getCameraIdNames.all(function (error, cameraResult) {
// 											if (error) {
// 												logger.error("Get Camera ID List error ", error);
// 												var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
// 												return cb(errorResponse, null);
// 											} else {
// 												var callBackCamera = function () {
// 													if (cameraIdRecord == cameraResult.length) {
// 														var data = {
// 															cameraAlertList: alertList,
// 															cameraPushLogs: unreadLogs,
// 															cameraIdList: cameraIdList,
// 															unreadCount: logresult.length
// 														};
// 														response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
// 														return cb(null, response);
// 													}
// 												};

// 												for (var p = 0; p < cameraResult.length; p++) {
// 													cameraIdList.push(cameraResult[p]);
// 													cameraIdRecord = cameraIdRecord + 1;
// 													callBackCamera();
// 												}
// 											}
// 										});
// 									}
// 								};
// 								if (logresult.length > 0) {
// 									for (var p = 0; p < logresult.length; p++) {
// 										unreadLogs.push(logresult[p]);
// 										unreadprocessRecord = unreadprocessRecord + 1;
// 										callBackUnread();
// 									}
// 								} else {
// 									callBackUnread();
// 								}
// 							}
// 						});
// 					}
// 				};

// 				for (var p = 0; p < alertDevices.length; p++) {
// 					alertList.push(alertDevices[p]);
// 					processRecord = processRecord + 1;
// 					callBackAlertData();
// 				}

// 			} else {
// 				var getCameraIdNames = sqliteDB.prepare(constants.GET_CAMERA_ID_NAMES);
// 				getCameraIdNames.all(function (error, cameraResult) {
// 					if (error) {
// 						logger.error("Get Camera ID List error ", error);
// 						var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
// 						return cb(errorResponse, null);
// 					} else {
// 						var cameraIdRecord = 0;

// 						var callBackCamera = function () {
// 							if (cameraIdRecord == cameraResult.length) {
// 								var data = {
// 									cameraIdList: cameraIdList
// 								};
// 								response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
// 								return cb(null, response);
// 							}
// 						};

// 						for (var p = 0; p < cameraResult.length; p++) {
// 							cameraIdList.push(cameraResult[p]);
// 							cameraIdRecord = cameraIdRecord + 1;
// 							callBackCamera();
// 						}
// 					}
// 				});
// 			}
// 		}
// 	});
//};

/**
 * Get Camera Notification Alert List for All the Camera
 * @param user_id (required)
 * @param admin (0/1)
 * @param jetson_id (optional)
 */
exports.getCameraNotificationAlertList = async function (params, cb) {
    //logger.info("getCameraNotificationAlertList", params);

    try {
        let user_id = params.user_id,
            admin = params.admin,
            alertList = [],
            unreadLogs = [],
            cameraIdList = [];

        // Get Camera of users based on their priviledge
        cameraIdList = admin == 1 ? await cameraModel.getAdminUserCameras() : await cameraModel.getChildUserCameras(user_id);

        // Filter camera by jetson id if jetson id is provided
        if (params.jetson_id && params.jetson_id.length > 0) {
            cameraIdList = cameraIdList.filter(function (item) {
                return item.jetson_device_id == params.jetson_id
            });
        } else {
            cameraIdList = cameraIdList.filter(function (item) {
                return item.jetson_device_id == null
            });
        }
        alertList = [];
        unreadLogs = [];
        // If jetson id is provided then return only cameras of that specific jetson
        for (let camera of cameraIdList) {
            cameraAlerts = await dbManager.all(constants.GET_CHILD_CAMERA_ALERT_DETAILS, [camera.camera_id]);
            unseenCameraLogs = await logModel.getUnseenCameraAlerts(user_id, camera.camera_id);
            alertList.push(...cameraAlerts);
            unreadLogs.push(...unseenCameraLogs);
        }

        const key = 'camera_notification_id';
        const uniqueAlertList = [...new Map(alertList.map(item => [item[key], item])).values()];

        let data = {
            cameraAlertList: uniqueAlertList,
            cameraPushLogs: unreadLogs,
            cameraIdList: cameraIdList,
            unreadCount: unreadLogs.length
        };

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
        return cb(null, response);

    } catch (error) {
        logger.error('Camera Controller | Get Camera Notification Alert List Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
        return cb(null, response);
    }



    // if (admin == 1) {

    //     // alertList = await dbManager.all(constants.GET_CAMERA_ALERT_DETAILS,[] );
    //     // unreadLogs =  await logModel.getAllUnseenCameraAlerts(user_id);
    //     alertList = [];
    //     unreadLogs = [];
    //     cameraIdList = await cameraModel.getAdminUserCameras();

    //     if (devices.jetson_id && devices.jetson_id.length > 0) {
    //         cameraIdList = cameraIdList.filter(function (item) {
    //             return item.jetson_device_id == devices.jetson_id
    //         });
    //     }

    //     // If jetson id is provided then return only cameras of that specific jetson
    //     for (let camera of cameraIdList) {
    //         cameraAlerts = await dbManager.all(constants.GET_CHILD_CAMERA_ALERT_DETAILS, [camera.camera_id]);
    //         unseenCameraLogs = await logModel.getUnseenCameraAlerts(user_id, camera.camera_id);
    //         alertList.push(...cameraAlerts);
    //         unreadLogs.push(...unseenCameraLogs);
    //     }

    //     var data = {
    //         cameraAlertList: alertList,
    //         cameraPushLogs: unreadLogs,
    //         cameraIdList: cameraIdList,
    //         unreadCount: unreadLogs.length
    //     };

    //     response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //     return cb(null, response);

    // } else {

    //     alertList = [];
    //     unreadLogs = [];
    //     cameraIdList = await cameraModel.getChildUserCameras(user_id);

    //     // If jetson id is provided then return only cameras of that specific jetson
    //     if (devices.jetson_id && devices.jetson_id.length > 0) {
    //         cameraIdList = cameraIdList.filter(function (item) {
    //             return item.jetson_device_id == devices.jetson_id
    //         });
    //     }

    //     for (let camera of cameraIdList) {
    //         cameraAlerts = await dbManager.all(constants.GET_CHILD_CAMERA_ALERT_DETAILS, [camera.camera_id]);
    //         unseenCameraLogs = await logModel.getUnseenCameraAlerts(user_id, camera.camera_id);
    //         alertList.push(...cameraAlerts);
    //         unreadLogs.push(...unseenCameraLogs);
    //     }

    //     var data = {
    //         cameraAlertList: alertList,
    //         cameraPushLogs: unreadLogs,
    //         cameraIdList: cameraIdList,
    //         unreadCount: unreadLogs.length
    //     };

    //     response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //     return cb(null, response);

    //     // childCameraNotificationAlert(user_id);
    // }


    // function adminCameraNotificationAlert() {

    //     var getAlertDetails = sqliteDB.prepare(constants.GET_CAMERA_ALERT_DETAILS);
    //     getAlertDetails.all(function (error, alertDevices) {
    //         if (error) {
    //             response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR, error);
    //             return cb(null, response);
    //         } else {
    //             if (alertDevices.length > 0) {
    //                 var processRecord = 0,
    //                     unreadprocessRecord = 0,
    //                     cameraIdRecord = 0;

    //                 var callBackAlertData = function () {
    //                     if (processRecord === alertDevices.length) {
    //                         var getPushLogs = sqliteDB.prepare(constants.GET_CAMERA_PUSH_LOGS);
    //                         getPushLogs.all(function (error, logresult) {
    //                             if (error) {
    //                                 logger.error("Get Camera Push Logs error ", error);
    //                                 var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                                 return cb(errorResponse, null);
    //                             } else {
    //                                 var callBackUnread = function () {

    //                                     if (unreadprocessRecord === logresult.length) {

    //                                         var getCameraIdNames = sqliteDB.prepare(constants.GET_CAMERA_ID_NAMES);
    //                                         getCameraIdNames.all(function (error, cameraResult) {
    //                                             if (error) {
    //                                                 logger.error("Get Camera ID List error ", error);
    //                                                 var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                                                 return cb(errorResponse, null);
    //                                             } else {

    //                                                 var callBackCamera = function () {
    //                                                     if (cameraIdRecord == cameraResult.length) {
    //                                                         var data = {
    //                                                             cameraAlertList: alertList,
    //                                                             cameraPushLogs: unreadLogs,
    //                                                             cameraIdList: cameraIdList,
    //                                                             unreadCount: logresult.length
    //                                                         };
    //                                                         response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //                                                         return cb(null, response);
    //                                                     }
    //                                                 };

    //                                                 for (var p = 0; p < cameraResult.length; p++) {
    //                                                     cameraIdList.push(cameraResult[p]);
    //                                                     cameraIdRecord = cameraIdRecord + 1;
    //                                                     callBackCamera();
    //                                                 }
    //                                             }
    //                                         });
    //                                     }
    //                                 };
    //                                 if (logresult.length > 0) {
    //                                     for (var p = 0; p < logresult.length; p++) {
    //                                         unreadLogs.push(logresult[p]);
    //                                         unreadprocessRecord = unreadprocessRecord + 1;
    //                                         callBackUnread();
    //                                     }
    //                                 } else {
    //                                     callBackUnread();
    //                                 }
    //                             }
    //                         });
    //                     }
    //                 };

    //                 for (var p = 0; p < alertDevices.length; p++) {
    //                     alertList.push(alertDevices[p]);
    //                     processRecord = processRecord + 1;
    //                     callBackAlertData();
    //                 }
    //             } else {
    //                 var getCameraIdNames = sqliteDB.prepare(constants.GET_CAMERA_ID_NAMES);
    //                 getCameraIdNames.all(function (error, cameraResult) {
    //                     if (error) {
    //                         logger.error("Get Camera ID List error ", error);
    //                         var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                         return cb(errorResponse, null);
    //                     } else {
    //                         var cameraIdRecord = 0;

    //                         var callBackCamera = function () {
    //                             if (cameraIdRecord == cameraResult.length) {
    //                                 var data = {
    //                                     cameraIdList: cameraIdList
    //                                 };
    //                                 response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //                                 return cb(null, response);
    //                             }
    //                         };

    //                         for (var p = 0; p < cameraResult.length; p++) {
    //                             cameraIdList.push(cameraResult[p]);
    //                             cameraIdRecord = cameraIdRecord + 1;
    //                             callBackCamera();
    //                         }
    //                     }
    //                 });
    //             }
    //         }
    //     });
    // }


    // function childCameraNotificationAlert(user_id) {

    //     var cameraList = [],
    //         process = 0;

    //     var getChildCameraList = sqliteDB.prepare(constants.GET_CHILD_USER_CAMERAS);
    //     getChildCameraList.all(user_id, function (error, cameraResult) {
    //         if (error) {
    //             response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //             return cb(null, response);
    //         } else {
    //             for (var j = 0; j < cameraResult.length; j++) {
    //                 cameraList.push(cameraResult[j]);
    //             }

    //             var CallBackFunction = function () {
    //                 if (cameraList.length == process) {
    //                     alertList = getUnique(alertList, 'camera_notification_id');

    //                     var getCameraIdNames = sqliteDB.prepare(constants.GET_CHILD_USER_CAMERAS);
    //                     getCameraIdNames.all(user_id, function (error, cameraResult) {
    //                         if (error) {
    //                             logger.error("Get Camera ID List error ", error);
    //                             var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                             return cb(errorResponse, null);
    //                         } else {
    //                             for (var p = 0; p < cameraResult.length; p++) {
    //                                 cameraIdList.push(cameraResult[p]);

    //                             }

    //                             var data = {
    //                                 cameraAlertList: alertList,
    //                                 cameraPushLogs: unreadLogs,
    //                                 cameraIdList: cameraIdList
    //                             };
    //                             response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //                             return cb(null, response);
    //                         }
    //                     });
    //                     getCameraIdNames.finalize();
    //                 }
    //             };

    //             cameraResult.forEach(function (data) {

    //                 var camera_id = data.camera_id;

    //                 var getAlertDetails = sqliteDB.prepare(constants.GET_CHILD_CAMERA_ALERT_DETAILS);
    //                 getAlertDetails.all(camera_id, function (error, alertDevices) {
    //                     if (error) {
    //                         process = process + 1;
    //                         CallBackFunction();
    //                     } else {
    //                         //logger.info("alertDevices", alertDevices);
    //                         if (alertDevices.length > 0) {
    //                             var processRecord = 0,
    //                                 unreadprocessRecord = 0,
    //                                 cameraIdRecord = 0;

    //                             var callBackAlertData = function () {
    //                                 if (processRecord === alertDevices.length) {
    //                                     var getPushLogs = sqliteDB.prepare(constants.GET_CHILD_CAMERA_PUSH_LOGS);
    //                                     getPushLogs.all(camera_id, function (error, logresult) {
    //                                         if (error) {
    //                                             logger.error("Get Camera Push Logs error ", error);
    //                                             var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                                             return cb(errorResponse, null);
    //                                         } else {
    //                                             var callBackUnread = function () {

    //                                                 if (unreadprocessRecord === logresult.length) {
    //                                                     process = process + 1;
    //                                                     CallBackFunction();
    //                                                 }
    //                                             };
    //                                             if (logresult.length > 0) {
    //                                                 for (var p = 0; p < logresult.length; p++) {
    //                                                     unreadLogs.push(logresult[p]);
    //                                                     unreadprocessRecord = unreadprocessRecord + 1;
    //                                                     callBackUnread();
    //                                                 }
    //                                             } else {
    //                                                 callBackUnread();
    //                                             }
    //                                         }
    //                                     });
    //                                 }
    //                             };

    //                             for (var p = 0; p < alertDevices.length; p++) {
    //                                 alertList.push(alertDevices[p]);
    //                                 processRecord = processRecord + 1;
    //                                 callBackAlertData();
    //                             }
    //                         } else {
    //                             process = process + 1;
    //                             CallBackFunction();
    //                         }
    //                     }
    //                 });
    //             });
    //         }
    //     });
    //     getChildCameraList.finalize();
    // }
    // };

    // function getUnique(arr, comp) {

    //     const unique = arr
    //         .map(e => e[comp])
    //         // store the keys of the unique objects
    //         .map((e, i, final) => final.indexOf(e) === i && i)
    //         // eliminate the dead keys & store unique objects
    //         .filter(e => arr[e]).map(e => arr[e]);

    //     return unique;
};


/**
 * Get Jetson Camera List
 * @param jetson_device_id (device id of jetson)
 */
exports.getCameraList = async (params, cb) => {

    try {
        // logger.info('Get Camera List : ', params.jetson_id);
        let response = [];
        let cameraList = [];
        if (params.authenticatedUser.admin == 1) {
            cameraList = await cameraModel.getAdminUserCameras(params.authenticatedUser.user_id);
            // logger.info('Camera List : ', cameraList);
        } else {
            cameraList = await cameraModel.getChildUserCameras(params.authenticatedUser.user_id);
            // logger.info('Camera List : ', cameraList);
        }

        if (params.jetson_id) {
            response = cameraList.filter(function (item) {
                return item.jetson_device_id == params.jetson_id;
            });
        } else {
            response = cameraList.filter(function (item) {
                return item.jetson_device_id == null || item.jetson_device_id.trim().length == 0;
            });
        }

        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Get Camera List By JSON Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(response, null);
    }
}

/**
 * Get Jetson Camera List
 * @param jetson_device_id (device id of jetson)
 */
exports.getCameraListByJetson = async (params, cb) => {

    //logger.info('getCameraListByJetson', params);

    try {
        let cameraList = await cameraModel.getJetsonCamera(params.jetson_device_id);
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, cameraList);
        return cb(null, response);
    } catch (error) {
        logger.error('Camera Controller | Get Camera List By JSON Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(response, null);
    }
}

exports.updateUnReadCameraLogs = function (data, cb) {
    //logger.info('updateUnReadCameraLogs', data);

    try {
        let camera_id = data.camera_id;

        if (camera_id.length > 0) {
            let updateLogsCountByID = sqliteDB.prepare(constants.UPDATE_CAMERA_LOGS_BY_ID);
            updateLogsCountByID.run(camera_id, function (error, result) {
                if (error) {
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(null, response);
                } else {
                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                    return cb(null, response);
                }
            });
        } else {
            let updateLogsCount = sqliteDB.prepare(constants.UPDATE_CAMERA_LOGS);
            updateLogsCount.run(function (error, result) {
                if (error) {
                    response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
                    return cb(null, response);
                } else {
                    response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
                    return cb(null, response);
                }
            });
        }
    } catch (error) {
        logger.error('Camera Controller | Update UnRead Camera Logs Error', error);
        response = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(response, null);
    }
};

/**
 * Get Python Camera Details
 * API Called By Python Jeton For Face Detection
 */
exports.getPythonCameraDetails = async (params, cb) => {

    try {
        let cameradetails = [],
            cameraIdList = [];

        let cameraResult = await dbManager.all(constants.GET_CAMERA_DETAILS_FOR_PYTHON, []);
        if (cameraResult.length > 0) {

            for (let p = 0; p < cameraResult.length; p++) {

                camera_ids_array = cameraResult[p].camera_ids != null ? cameraResult[p].camera_ids.split(',') : [];
                camera_names_array = cameraResult[p].camera_names != null ? cameraResult[p].camera_names.split(',') : [];
                camera_videopaths_array = cameraResult[p].camera_videopaths != null ? cameraResult[p].camera_videopaths.split(',') : [];
                camera_ips_array = cameraResult[p].camera_videopaths != null ? cameraResult[p].camera_ips.split(',') : [];
                camera_active_array = cameraResult[p].camera_active != null ? cameraResult[p].camera_active.split(',') : [];

                cameraResult[p].camera_ids = camera_ids_array;
                cameraResult[p].camera_names = camera_names_array;
                cameraResult[p].camera_videopaths = camera_videopaths_array;
                cameraResult[p].camera_ips = camera_ips_array;
                cameraResult[p].camera_active = camera_active_array;

                cameradetails.push(cameraResult[p]);

            }

            let result = await dbManager.all(constants.GET_ALL_CAMERA_IDS, []);
            cameraIdList = result.map(item => item.camera_id);

            let data = {
                cameradetails: cameradetails,
                cameraIdList: cameraIdList
            };

            response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);

        } else {

            let result = await dbManager.all(constants.GET_ALL_CAMERA_IDS, []);
            cameraIdList = result.map(item => item.camera_id);

            let data = {
                cameradetails: cameradetails,
                cameraIdList: cameraIdList
            };

            let response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);

        }

    } catch (error) {
        logger.error('Camera Controller | Get Python Camera Details Error', error);
        let errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
        return cb(errorResponse, null);
    }

    // ankur code
    // var pythonCameraDetails = sqliteDB.prepare(constants.GET_CAMERA_DETAILS_FOR_PYTHON);
    // pythonCameraDetails.all(function (error, cameraResult) {
    //     if (error) {
    //         logger.error("Get Camera Detils For Python error ", error);
    //         var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //         return cb(errorResponse, null);
    //     } else {
    //         if (cameraResult.length > 0) {
    //             var unreadprocessRecord = 0;
    //             var callBackUnread = function () {

    //                 if (unreadprocessRecord === cameraResult.length) {
    //                     var getCameraIds = sqliteDB.prepare(constants.GET_ALL_CAMERA_IDS);
    //                     getCameraIds.all(function (error, result) {
    //                         if (error) {
    //                             logger.error("Get Camera IDs For Python error ", error);
    //                             var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                             return cb(errorResponse, null);
    //                         } else {
    //                             for (var p = 0; p < result.length; p++) {
    //                                 cameraIdList.push(result[p].camera_id);
    //                             }
    //                             var data = {
    //                                 cameradetails: cameradetails,
    //                                 cameraIdList: cameraIdList
    //                             };
    //                             response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //                             return cb(null, response);
    //                         }
    //                     });
    //                     getCameraIds.finalize();
    //                 }
    //             };

    //             for (var p = 0; p < cameraResult.length; p++) {

    //                 if (cameraResult[p].camera_ids == null) {
    //                     unreadprocessRecord = unreadprocessRecord + 1;
    //                     continue;
    //                 }
    //                 camera_ids_array = cameraResult[p].camera_ids.split(',');
    //                 camera_names_array = cameraResult[p].camera_names.split(',');
    //                 camera_videopaths_array = cameraResult[p].camera_videopaths.split(',');
    //                 camera_ips_array = cameraResult[p].camera_ips.split(',');
    //                 camera_active_array = cameraResult[p].camera_active.split(',');

    //                 cameraResult[p].camera_ids = camera_ids_array;
    //                 cameraResult[p].camera_names = camera_names_array;
    //                 cameraResult[p].camera_videopaths = camera_videopaths_array;
    //                 cameraResult[p].camera_ips = camera_ips_array;
    //                 cameraResult[p].camera_active = camera_active_array;

    //                 cameradetails.push(cameraResult[p]);
    //                 unreadprocessRecord = unreadprocessRecord + 1;
    //                 callBackUnread();
    //             }
    //         } else {
    //             var getCameraIds = sqliteDB.prepare(constants.GET_ALL_CAMERA_IDS);
    //             getCameraIds.all(function (error, result) {
    //                 if (error) {
    //                     logger.error("Get Camera IDs For Python error ", error);
    //                     var errorResponse = appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR);
    //                     return cb(errorResponse, null);
    //                 } else {
    //                     if (result.length > 0) {
    //                         for (var p = 0; p < result.length; p++) {
    //                             cameraIdList.push(result[p].camera_id);
    //                         }
    //                         var data = {
    //                             cameradetails: cameradetails,
    //                             cameraIdList: cameraIdList
    //                         };
    //                         response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, data);
    //                         return cb(null, response);
    //                     } else {
    //                         response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS);
    //                         return cb(null, response);
    //                     }
    //                 }
    //             });
    //             getCameraIds.finalize();
    //         }
    //     }
    // });
};

