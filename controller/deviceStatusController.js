const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const queueManagerUtil = require('./../util/queue-manager-util');
const constants = require('./../util/constants');
const mqtt = require('mqtt');
const deviceModel = require('../models/device');
const panelModel = require('../models/panel');
const logModel = require('../models/logs');
const userModel = require('../models/user');
const generalMetaModel = require('../models/general_meta');
const eventsEmitter = require("./../util/event-manager");
const deviceStatusModel = require('../models/device_status');
const Client = require('node-rest-client').Client;
const moment = require('moment');
const restClient = new Client();

let generalCommandQueue = {};

/**
 * Change Device Status API And Also Privately Called
 * @param device_id (required)
 * @param panel_id (optional)
 * @param device_status
 * @param device_sub_status
 * @param device_swing (Optional, onle require for ac swing)
 * @param broadcast_room_panel_status
 * @param log (true/false)
 * @param preDevice (optional)
 * @param command_type (Optional, only need for tv/dth remotes)
 */
exports.changeDeviceStatus = async (params, cb = null) => {


    //  Initializing Default Values Necessary
    params.broadcast_room_panel_status = (params.broadcast_room_panel_status !== true && params.broadcast_room_panel_status !== undefined && params.broadcast_room_panel_status !== null) ? params.broadcast_room_panel_status : true;
    params.device_status = params.device_status.toString();

    try {

        const deviceData = params.preDevice ? params.preDevice : await deviceModel.getDetails(params.device_id);

        if (!deviceData) return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.DEVICE_NOT_FOUND)) : null;
        if (deviceData.is_configured === 'n') return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Device is not configured"))) : null;

        // Device is configured and added

        if ((deviceData.device_type == "fan" || deviceData.device_type == "switch" || deviceData.device_type == "heavyload") && ['5', '5f', 'heavy_load', 'double_heavy_load'].includes(deviceData.module_type.toString())) {
            return exports.changeFanSwitchHeavyloadStatus(params, deviceData, cb);
        } else if ((deviceData.device_type == "fan" || deviceData.device_type == "switch" || deviceData.device_type == "heavyload") && ['5-wifi', '5f-wifi', 'heavy_load-wifi', 'double_heavy_load-wifi'].includes(deviceData.module_type.toString())) {
            return exports.changeFanSwitchHeavyloadWifiStatus(params, deviceData, cb);
        } else if (deviceData.device_type == generalUtil.deviceTypes.curtain.device_type) {
            return exports.changeCurtainStatus(params, deviceData, cb);
        } else if (deviceData.device_type == "lock" && deviceData.device_sub_type == 'yale_lock') {
            return exports.changeYaleLockStatus(params, deviceData, cb);
        } else if (deviceData.device_type == "remote" && ['tv', 'dth', 'tv_dth'].includes(deviceData.device_sub_type.toLowerCase())) {
            return exports.changeTVDTHRemoteStatus(params, deviceData, cb);
        } else if (deviceData.device_type == "remote") {
            return exports.changeACRemoteStatus(params, deviceData, cb);
        } else if (deviceData.device_type == generalUtil.deviceTypes.pir_device.device_type) {
            return exports.changePirDeviceStatus(params, deviceData, cb);
        } else if (deviceData.device_type == generalUtil.deviceTypes.smart_bulb.device_type) {
            return exports.changeSmartBulbStatus(params, deviceData, cb);
        } else if (deviceData.device_type == generalUtil.deviceTypes.smart_plug.device_type) {
            return exports.changeSmartPlugStatus(params, deviceData, cb);
        }

    } catch (error) {
        logger.error('Device Controller | changeDeviceStatus Error', error);
        return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
    }
}

/**
 * Internally Called Change Device Status For TV/DTH Remote
 * @param device_id
 * @param device_status (0/1)
 * @param remote_type (tv/dth)
 * @param command_type (turn_on, turn_off etc....)
 * @param deviceData (data of device)
 */
exports.changeTVDTHRemoteStatus = async function (params, deviceData, cb) {

    logger.info('CHANGE TV STATUS', params);
    // logger.info('Device Data', deviceData);
    if (params.device_status != 0 && params.device_status != 1) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    // if the command is being executed for the first time
    if (params.isallow) {
        if (generalCommandQueue[deviceData.device_id] === null || generalCommandQueue[deviceData.device_id] === undefined) {
            generalCommandQueue[deviceData.device_id] = {
                lastExecutionTime: new Date().valueOf()
            };

            // if device being operated again in 10 seconds of last operation
        } else if (generalCommandQueue[deviceData.device_id].lastExecutionTime > moment().subtract('3', 'seconds').toDate().valueOf()) {

            return cb ? cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "Device is performing task. Please wait."
            })) : null;

        } else {
            generalCommandQueue[deviceData.device_id] = {
                lastExecutionTime: new Date().valueOf()
            };
        }
    }

    if (params.remote_type == "tv_dth") {
        params.remote_type = "tv"
    } else if (!params.remote_type) {
        params.remote_type = deviceData.device_sub_type == 'dth' ? 'dth' : 'tv';
    }

    let deviceCodes = await generalMetaModel.getMetaValue(deviceData.device_id, params.remote_type == 'tv' ? 'tv_codes' : 'dth_codes');

    if (deviceCodes == null) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Codes Set."
        })) : null;
    }

    let commands = (JSON.parse(deviceCodes));

    let updateStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    let tvActionCommand;
    if (params.command_type) {
        tvActionCommand = commands.find(function (item) {
            return item.name == params.command_type;
        });


        if (!tvActionCommand) {
            return cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "This feature is not available for this remote. Please contact system administrator!"
            }));
        }
    } else if (params.button_no) {
        tvActionCommand = commands.find(function (item) {
            return item.button_no == params.button_no;
        })
    } else {
        tvActionCommand = commands.find(function (item) {
            return item.name == (params.device_status == 1 ? "turn_on" : "turn_off");
        });
    }

    let ir_blaster_id = await generalMetaModel.getMetaValue(deviceData.device_id, 'ir_blaster_id');
    const irBlaster = await deviceModel.getDetails(ir_blaster_id);

    let client = mqtt.connect('mqtt://127.0.0.1:1883');
    let sendResponse = false;

    client.on('connect', () => {
        client.subscribe('ESP/' + irBlaster.module_identifier, rap = false, function () {
            client.on('message', async function (topic, message, packet) {
                if (message == '54321' && topic == 'ESP/' + irBlaster.module_identifier) {
                    if (!sendResponse) {
                        sendResponse = true;
                        // Update Only When IR Blaster Gives Response
                        await deviceStatusModel.updateMany(updateStatus, {
                            emitRoomPanelStatus: true
                        });

                        logger.info('TV ACTION COMMAND NAME : ', tvActionCommand.name);

                        if (tvActionCommand.name == 'turn_on' || tvActionCommand.name == "turn_off") {
                            eventsEmitter.emit('emitSocket', {
                                topic: 'changeDeviceStatus',
                                data: {
                                    device_id: params.device_id,
                                    device_type: deviceData.device_type,
                                    device_status: params.device_status
                                }
                            });
                        }

                        // logger.info('Change Device Status socket emmit');
                        return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
                    } else {
                        return;
                    }
                }
            })
        });

        // if (params.command_type === null || params.command_type === undefined) {
        //     const onOffCommand = commands.find(function (item) {
        //         return item.name == deviceData.device_status == 1 ? "turn_on" : 'turn_off';
        //     });

        //     logger.info('Command Fired ON/OFF: ', onOffCommand.value);
        //     client.publish('PI/' + irBlaster.module_identifier, onOffCommand.value, function () { });
        // } else {

        // let result = tvActionCommand.find((item) => {
        //     return item.name === params.command_type
        // });

        if (tvActionCommand) {
            let currentTime = new Date();

            if (currentTime <= moment(currentTime).subtract('3', 'second')) {
                return cb(null, appUtil.createErrorResponse({
                    code: 400,
                    message: "Please wait! Device is performing task."
                }));
            }
            logger.info('TV/DTH Command Fired: ', tvActionCommand.value);
            client.publish('PI/' + irBlaster.module_identifier, tvActionCommand.value, function () { });
        }

        // }


        // if (params.command_type === 'turn_on' || params.command_type === 'turn_off') {
        //     const onOffCommand = commands.find(function (item) {
        //         return item.name == deviceData.device_status == 1 ? "turn_on" : 'turn_off';
        //     });

        //     logger.info('Command Fired ON/OFF: ', onOffCommand.value);
        //     client.publish('PI/' + irBlaster.module_identifier, onOffCommand.value, function () {});
        // } else {

        //     logger.info('PARAM COMMAND TYPE : ', params.command_type);
        //     let result = tvActionCommand.find((item) => {
        //         return item.name === params.command_type
        //     }); 

        //     logger.info('RESULT FOR TV ACTION COMMAND : ', result);
        //     if (tvActionCommand) {
        //         logger.info('Command Fired IN TVACTION: ', result[0]);
        //         client.publish('PI/' + irBlaster.module_identifier, result[0], function () {});
        //     }

        // }
        // if (deviceData.device_status === params.device_status) {
        //     logger.info('Device Status : ', deviceData.device_status);


        // }


    });

    setTimeout(function () {

        client.end();
        if (!sendResponse) {
            return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
        }

    }, 2000);


    if (params.log === null || params.log == true || params.log == undefined) {

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on"
        }, params.authenticatedUser);
    }

}

/**
 * Internally Called Change Device Status For AC Remote
 * @param device_id
 * @param device_status (0/1)
 * @param device_sub_status (AUTO-19, LOW-19  etc..)
 * @param device_swing (1)
 */
exports.changeACRemoteStatus = async function (params, deviceData, cb) {


    logger.info('CHANGE AC REMOTE PARAMS : ', params)
    if (params.device_status != 0 && params.device_status != 1) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    // const deviceMeta = JSON.parse(deviceData.device_meta);
    // var commands = deviceMeta.device_codes.commands;

    // if the command is being executed for the first time
    if (params.isallow) {
        if (generalCommandQueue[deviceData.device_id] === null || generalCommandQueue[deviceData.device_id] === undefined) {
            generalCommandQueue[deviceData.device_id] = {
                lastExecutionTime: new Date().valueOf()
            };

            // if device being operated again in 10 seconds of last operation
        } else if (generalCommandQueue[deviceData.device_id].lastExecutionTime > moment().subtract('3', 'seconds').toDate().valueOf()) {

            return cb ? cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "Device is performing task. Please wait."
            })) : null;

        } else {
            generalCommandQueue[deviceData.device_id] = {
                lastExecutionTime: new Date().valueOf()
            };
        }
    }


    let deviceCodes = await generalMetaModel.getMetaValue(deviceData.device_id, 'device_codes');
    // logger.info('IN AC DEVICE CODES : ', deviceCodes);
    if (deviceCodes == null) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Codes Set."
        })) : null;
    }

    let commands = (JSON.parse(deviceCodes)).commands;

    let updateStatus = {
        device_id: params.device_id,
        device_status: params.device_status,
        device_sub_status: deviceData.device_sub_status
    };

    if (params.device_sub_status) {
        params.device_sub_status = params.device_sub_status.toUpperCase();
        updateStatus.device_sub_status = params.device_sub_status;
    }

    let mode = updateStatus.device_sub_status.split("-")[0].trim().toUpperCase();
    let temp = updateStatus.device_sub_status.split("-")[1].trim();

    // var command = params.device_status == 0 ? commands['OFF'] : commands[mode][temp];
    let command;

    // if not swing is not setup, then send error response

    if (params.device_status == 0) {
        command = commands['OFF'];
    } else if (params.device_status == 1 && params.device_swing == 1) {
        if (commands['SWING'] == null) {
            return cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "This functionality is not added. Please contact system administrator!"
            }));
        }

        const deviceSwingStatus = await deviceStatusModel.getStatus(params.device_id, 'device_swing_status');

        if (deviceSwingStatus == '0') {
            command = commands['SWINGOFF'];
            updateStatus.device_swing_status = 1;
        } else {
            command = commands['SWING'];
            updateStatus.device_swing_status = 0;
        }

    } else {

        if (!commands[mode]) {
            return cb(null, appUtil.createErrorResponse({
                code: 500,
                message: "This functionality is not added. Please contact system administrator!"
            }));
        }

        command = commands[mode][temp];

    }


    let ir_blaster_id = await generalMetaModel.getMetaValue(deviceData.device_id, 'ir_blaster_id');
    const irBlaster = await deviceModel.getDetails(ir_blaster_id);

    let client = mqtt.connect('mqtt://127.0.0.1:1883');
    let sendResponse = false;

    client.on('connect', () => {
        // logger.info('IN CONNECT AC');
        client.subscribe('ESP/' + irBlaster.module_identifier, rap = false, function () {
            // logger.info('IN SUBSCRIBE AC');
            client.on('message', async function (topic, message, packet) {
                if (message == '54321' && topic == 'ESP/' + irBlaster.module_identifier) {
                    // logger.info('IN MESSAGE AC');
                    if (!sendResponse) {
                        sendResponse = true;

                        // Update Only When IR Blaster Gives Response
                        await deviceStatusModel.updateMany(updateStatus, {
                            emitRoomPanelStatus: true
                        });

                        eventsEmitter.emit('emitSocket', {
                            topic: 'changeDeviceStatus',
                            data: {
                                device_id: params.device_id,
                                device_type: deviceData.device_type,
                                device_status: params.device_status,
                                device_sub_status: updateStatus.device_sub_status
                            }
                        });

                        // logger.info('Change Device Status socket emmit');
                        return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
                    } else {
                        return;
                    }
                }
            })
        });

        // if (params.device_status == 1 && params.device_swing != 1) {

        //     client.publish('PI/' + irBlaster.module_identifier, commands['ON'], function () {

        //     });

        // }

        if (params.device_swing == 1) {

            // let tempCommand = commands[mode][temp];

            // client.publish('PI/' + irBlaster.module_identifier, tempCommand, function () {
            // });if (currentTime <= moment(currentTime).subtract('3', 'second')) {
            let currentTime = new Date();
            if (currentTime <= moment(currentTime).subtract('3', 'second')) {
                return cb(null, appUtil.createErrorResponse({
                    code: 400,
                    message: "Please wait! Device is performing task."
                }));
            }
            logger.info('AC SWING Command Fired: ', command);
            client.publish('PI/' + irBlaster.module_identifier, command, function () {

            });

        } else {
            logger.info('AC Command Fired: ', command);
            client.publish('PI/' + irBlaster.module_identifier, command, function () {
            });
        }
    });

    setTimeout(function () {
        if (!sendResponse) {
            logger.info('BLASTER DID NOT RESPONDE');
            return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
        }
        client.end();
    }, 2000);


    if (params.log === null || params.log == true || params.log == undefined) {

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on"
        }, params.authenticatedUser);
    }


}

/**
 * Internally Called
 * @param device_id
 * @param device_status
 * @param broadcast_room_panel_status
 * @param deviceData 
 * @param cb (callback) 
 */
exports.changeCurtainStatus = async function (params, deviceData, cb) {

    if (params.device_status != 0 && params.device_status != 1 && params.device_status != 2) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    await deviceStatusModel.updateMany({
        device_id: params.device_id,
        device_status: params.device_status
    }, {
        emitRoomPanelStatus: params.broadcast_room_panel_status
    });

    // Add to Queue Manager For Sending Socket Details
    let queueCommand = {
        device_id: params.device_id,
        module_identifier: deviceData.module_identifier,
        command_type: "curtain-open-close",
        device_old_status: deviceData.device_status,
        device_old_sub_status: deviceData.device_sub_status,
        counter: 2
    };

    if (params.device_status == 1) {

        // open Curtain 
        queueCommand.device_status = 1;
        queueCommand.device_identifier = 2;
        queueManagerUtil.addToQueue(queueCommand);

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: 'curtain_open'
        }, params.authenticatedUser);


    } else if (params.device_status == 2) {

        // Curtain stop in between
        queueCommand.device_status = 0;
        queueCommand.device_identifier = 1;
        queueManagerUtil.addToQueue(queueCommand);

        queueCommand.device_status = 0;
        queueCommand.device_identifier = 2;
        queueManagerUtil.addToQueue(queueCommand);

    } else if (params.device_status == 0) {

        // close Curtain 
        queueCommand.device_status = 1;
        queueCommand.device_identifier = 1;
        queueManagerUtil.addToQueue(queueCommand);

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: 'curtain_close'
        }, params.authenticatedUser);

    }

    eventsEmitter.emit('emitSocket', {
        topic: 'changeDeviceStatus',
        data: {
            device_id: params.device_id,
            device_type: deviceData.device_type,
            device_status: params.device_status,
            device_sub_status: 0
        }
    });

    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
}

exports.changeFanSwitchHeavyloadWifiStatus = async function (params, deviceData, cb) {

    logger.info('WIFI PARAMS : ', params)
    if (params.device_status != 0 && params.device_status != 1) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    // If Sub Status is not provided for fan then add old sub status
    if (deviceData.device_type == 'fan' && (params.device_sub_status == null || params.device_sub_status == undefined || params.device_sub_status == "0")) {
        if (params.device_status == 1) {
            // logger.info('DEVICE SUB TYPE : ', deviceData.device_sub_type)
            if (deviceData.device_sub_type == 'normal') {

                params.device_sub_status = 5;
            } else if (deviceData.device_sub_status != null && deviceData.device_sub_status > 0) {
                params.device_sub_status = deviceData.device_sub_status;
            } else {
                //if sub status is not set in database then by default set it to 1
                params.device_sub_status = 1;
            }
        }

    } else if (deviceData.device_type == 'fan' && deviceData.device_sub_type == 'normal') {
        params.device_sub_status = 5;
    } else if (deviceData.device_type == 'fan' && params.device_sub_status == null) {
        params.device_sub_status = params.device_status;
    }

    let updatedStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    if (params.device_sub_status) updatedStatus.device_sub_status = params.device_sub_status;

    // logger.info('FAN STEP 5!', params);

    eventsEmitter.emit('emitSocket', {
        topic: 'changeDeviceStatus',
        data: {
            device_id: params.device_id,
            device_type: deviceData.device_type,
            device_status: params.device_status,
            device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status
        }
    });

    await deviceStatusModel.updateMany(updatedStatus, {
        emitRoomPanelStatus: params.broadcast_room_panel_status
    });

    queueManagerUtil.addToQueue({
        device_id: params.device_id,
        counter:1,
        device: { ...deviceData, ...updatedStatus },
        module_identifier: deviceData.module_identifier,
        device_identifier: deviceData.device_identifier,
        device_status: (deviceData.device_type == "fan" && params.device_status == 1) ? params.device_sub_status : params.device_status,
        device_old_status: deviceData.device_status,
        device_old_sub_status: deviceData.device_sub_status,
        command_type: "on-off-wifi"
    });

    if (params.panel_id) {

        let panelDetails = await panelModel.getPanelWithRoomDetails(params.panel_id);
        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on",
            description: `${deviceData.device_name}|${panelDetails.panel_name}|${panelDetails.room_name}`
        }, params.authenticatedUser);

    } else {
        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on"
        }, params.authenticatedUser);

    }

    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
}

exports.changeFanSwitchHeavyloadStatus = async function (params, deviceData, cb) {

    // Validate the Device Status Before Updating
    // logger.info('changeFanSwitchHeavyloadStatus', params, deviceData)

    logger.info('DEVICE MODULE TYPE : ', deviceData.module_type)
    if (params.device_status != 0 && params.device_status != 1) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    // If Sub Status is not provided for fan then add old sub status
    if (deviceData.device_type == 'fan' && (params.device_sub_status == null || params.device_sub_status == undefined || params.device_sub_status == "0")) {
        if (params.device_status == 1) {
            // logger.info('DEVICE SUB TYPE : ', deviceData.device_sub_type)
            if (deviceData.device_sub_type == 'normal') {

                params.device_sub_status = 5;
            } else if (deviceData.device_sub_status != null && deviceData.device_sub_status > 0) {
                params.device_sub_status = deviceData.device_sub_status;
            } else {
                //if sub status is not set in database then by default set it to 1
                params.device_sub_status = 1;
            }
        }

    } else if (deviceData.device_type == 'fan' && deviceData.device_sub_type == 'normal') {
        params.device_sub_status = 5;
    } else if (deviceData.device_type == 'fan' && params.device_sub_status == null) {
        params.device_sub_status = params.device_status;
    }

    let updatedStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    if (params.device_sub_status) updatedStatus.device_sub_status = params.device_sub_status;

    // logger.info('FAN STEP 5!', params);

    eventsEmitter.emit('emitSocket', {
        topic: 'changeDeviceStatus',
        data: {
            device_id: params.device_id,
            device_type: deviceData.device_type,
            device_status: params.device_status,
            device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status
        }
    });

    await deviceStatusModel.updateMany(updatedStatus, {
        emitRoomPanelStatus: params.broadcast_room_panel_status
    });

    queueManagerUtil.addToQueue({
        device_id: params.device_id,
        device: { ...deviceData, ...updatedStatus },
        module_identifier: deviceData.module_identifier,
        device_identifier: deviceData.device_identifier,
        device_status: (deviceData.device_type == "fan" && params.device_status == 1) ? params.device_sub_status : params.device_status,
        device_old_status: deviceData.device_status,
        device_old_sub_status: deviceData.device_sub_status,
        command_type: "on-off"
    });

    if (params.panel_id) {

        let panelDetails = await panelModel.getPanelWithRoomDetails(params.panel_id);
        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on",
            description: `${deviceData.device_name}|${panelDetails.panel_name}|${panelDetails.room_name}`
        }, params.authenticatedUser);

    } else {
        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on"
        }, params.authenticatedUser);

    }

    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
}


exports.changeYaleLockStatus = async function (params, deviceData, cb) {

    // Check if lock is allowed to open from the app
    const isLockAllowedToOpen = await generalMetaModel.getMetaValue(deviceData.device_id, 'enable_lock_unlock_from_app', 1);
    if (isLockAllowedToOpen == 0) {
        return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(419, "This door cannot be locked or unlocked from the app."))) : null;
    }

    // Check if status is valid
    if (params.device_status != 0 && params.device_status != 1) {
        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Invalid Device Status"
        })) : null;
    }

    await deviceStatusModel.updateMany({
        device_id: params.device_id,
        device_status: params.device_status
    }, {
        emitRoomPanelStatus: false
    });


    // if the command is being executed for the first time
    if (generalCommandQueue[deviceData.device_id] === null || generalCommandQueue[deviceData.device_id] === undefined) {
        generalCommandQueue[deviceData.device_id] = {
            lastExecutionTime: new Date().valueOf()
        };

        // if device being operated again in 10 seconds of last operation
    } else if (generalCommandQueue[deviceData.device_id].lastExecutionTime > moment().subtract('10', 'seconds').toDate().valueOf()) {

        return cb ? cb(null, appUtil.createErrorResponse({
            code: 500,
            message: "Device is performing task. Please wait."
        })) : null;

    } else {
        generalCommandQueue[deviceData.device_id] = {
            lastExecutionTime: new Date().valueOf()
        };
    }




    // Add to Queue Manager For Sending Socket Details
    let queueCommand = {
        device_id: params.device_id,
        module_identifier: deviceData.module_identifier,
        command_type: "lock-open-close",
        device_status: params.device_status,
        device_old_status: deviceData.device_status,
        device_old_sub_status: deviceData.device_sub_status,
        counter: 5
    };

    queueManagerUtil.addToQueue(queueCommand);

    eventsEmitter.emit('emitSocket', {
        topic: 'changeDeviceStatus',
        data: {
            device_id: params.device_id,
            device_type: deviceData.device_type,
            device_status: params.device_status,
            device_sub_status: 0
        }
    });

    logModel.add({
        show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
        log_type: params.device_status == 1 ? "door_lock" : "door_unlock",
        log_object_id: params.device_id
    }, params.authenticatedUser);

    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;


}


exports.changePirDeviceStatus = async function (params, deviceData, cb) {

    let updatedStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    if (params.device_sub_status) updatedStatus.device_sub_status = params.device_sub_status;

    await deviceStatusModel.updateMany(updatedStatus, {
        emitRoomPanelStatus: params.broadcast_room_panel_status
    });

    eventsEmitter.emit('emitSocket', {
        topic: 'changeDeviceStatus',
        data: {
            device_id: params.device_id,
            device_type: deviceData.device_type,
            device_status: params.device_status,
            device_sub_status: params.device_sub_status ? params.device_sub_status : null
        }
    });

    // Add to Queue Manager For Sending Socket Details
    queueManagerUtil.addToQueue([{
        device_id: params.device_id,
        module_identifier: deviceData.device_identifier,
        device_identifier: deviceData.device_identifier,
        device_status: params.device_status,
        device_old_status: deviceData.device_status,
        device_old_sub_status: deviceData.device_sub_status,
        command_type: "change-pir-device-status"
    }]);

    if (params.panel_id) {

        panelDetails = await panelModel.getPanelWithRoomDetails(params.panel_id);

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on",
            description: `${deviceData.device_name}|${panelDetails.panel_name}|${panelDetails.room_name}`
        }, params.authenticatedUser);

    } else {

        logModel.add({
            show: (params.log === null || params.log === true || params.log === undefined) ? 1 : 0,
            log_object_id: params.device_id,
            log_type: params.device_status == '0' ? "device_off" : "device_on"
        }, params.authenticatedUser);

    }

    return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
}

exports.changeSmartBulbStatus = async function (params, deviceData, cb) {

    const adminUser = await userModel.getAdminUser();

    let updatedStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    // If device status is sent
    if (params.device_status && params.device_status != deviceData.device_status) {

        let args = {
            data: {
                device_sub_type: deviceData.device_sub_type,
                device_type: deviceData.device_type,
                user_id: adminUser.user_id,
                device_identifier: deviceData.device_identifier,
                device_status: params.device_status
            },
            headers: {
                "Content-Type": "application/json"
            }
        };

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/ifttt/v1/smart-bulb/turn-on-off-light", args, async function (data, response) {

            if (data.code == 200) {


                await deviceStatusModel.updateMany(updatedStatus, {
                    emitRoomPanelStatus: params.broadcast_room_panel_status
                });


                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: deviceData.device_type,
                        device_status: params.device_status,
                        device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status,
                        device_brightness: params.device_brightness
                    }
                });

                return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
            } else {
                return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
            }
        });
    }

    // If RGB Value for color is sent
    if (params.device_sub_status && params.device_sub_status != deviceData.device_sub_status) {
        let args = {
            data: {
                device_sub_type: deviceData.device_sub_type,
                device_type: deviceData.device_type,
                user_id: adminUser.user_id,
                device_identifier: deviceData.device_identifier,
                color: params.device_sub_status,
                brightness_level: params.device_brightness
            },
            headers: {
                "Content-Type": "application/json"
            }
        };

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/ifttt/v1/smart-bulb/change-device-color", args, async function (data, response) {
            if (data.code == 200) {

                updatedStatus.device_sub_status = params.device_sub_status;

                await deviceStatusModel.updateMany(updatedStatus, {
                    emitRoomPanelStatus: params.broadcast_room_panel_status
                });

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: deviceData.device_type,
                        device_status: params.device_status,
                        device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status,
                        device_brightness: params.device_brightness
                    }
                });

                return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
            } else {
                return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
            }
        });
    }

    // If Brightness level is sent
    if (params.device_brightness && params.device_brightness != deviceData.device_brightness) {
        let args = {
            data: {
                device_sub_type: deviceData.device_sub_type,
                device_type: deviceData.device_type,
                user_id: adminUser.user_id,
                device_identifier: deviceData.device_identifier,
                color: params.device_sub_status,
                brightness_level: params.device_brightness
            },
            headers: {
                "Content-Type": "application/json"
            }
        };

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/ifttt/v1/smart-bulb/change-brightness", args, async function (data, response) {
            if (data.code == 200) {

                await deviceStatusModel.updateMany(updatedStatus, {
                    emitRoomPanelStatus: params.broadcast_room_panel_status
                });

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: deviceData.device_type,
                        device_status: params.device_status,
                        device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status,
                        device_brightness: params.device_brightness
                    }
                });

                return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;
            } else {
                return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
            }
        });
    }

}

exports.changeSmartPlugStatus = async function (params, deviceData, cb) {

    const adminUser = await userModel.getAdminUser();

    let updatedStatus = {
        device_id: params.device_id,
        device_status: params.device_status
    };

    // If device status is sent
    if (params.device_status && params.device_status != deviceData.device_status) {
        let args = {
            data: {
                device_sub_type: deviceData.device_sub_type,
                device_type: deviceData.device_type,
                user_id: adminUser.user_id,
                device_identifier: deviceData.device_identifier,
                device_status: params.device_status
            },
            headers: {
                "Content-Type": "application/json"
            }
        };

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/ifttt/v1/smart-bulb/turn-on-off-light", args, async function (data, response) {
            if (data.code == 200) {

                await deviceStatusModel.updateMany(updatedStatus, {
                    emitRoomPanelStatus: params.broadcast_room_panel_status
                });

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: deviceData.device_type,
                        device_status: params.device_status,
                        device_sub_status: params.device_sub_status ? params.device_sub_status : deviceData.device_sub_status
                    }
                });

                return cb ? cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS)) : null;

            }
        });
    }

}