const appUtil = require('./../util/app-util');
const constants = require('./../util/constants');
const syncManager = require('./../util/sync-manager');
// Models
const userModel = require('../models/user');
const homeControllerModel = require('../models/home_controller');

const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;

_controller = {};

/**
 * Sync User Function
 * @param users array of users
 */
_controller.syncUser = async (params, cb) => {

    try {

        if (params.users) {

            for (let user of params.users) {

                if (user.home_controller_device_id != home_controller_device_id) {
                    return;
                }

                const dbUser = await userModel.get(user.user_id);

                if (dbUser) {
                    delete user.is_push_enable;
                    user.is_sync = 1;
                    await userModel.update(user.user_id, user);
                }

            }

            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, {
                home_controller_device_id: home_controller_device_id
            }));

        } else {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(402, "Users Not Found.")));
        }

    } catch (error) {
        logger.error('Stnc Controller | Sync User Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Sync Home Controller
 * @param homecontroller (object)
 */
_controller.syncHomeController = async (params, cb) => {

    console.log(params);

    try {

        if (params.home_controller) {

            if (params.home_controller.home_controller_device_id != home_controller_device_id) {
                return;
            }
            params.home_controller.is_sync = 1;
            await homeControllerModel.update(home_controller_device_id, params.home_controller);

            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, params.home_controller));

        } else {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(402, "Users Not Found.")));
        }

    } catch (error) {
        logger.error('Sync Controller | Sync Home Controller Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }


}

_controller.syncTable = async (params, cb) => {

    try {

        syncManager.updateTable(params.table);
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, null));

    } catch (error) {
        logger.error('Sync Controller | Sync Home Controller Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }


}


module.exports = _controller;