const appUtil = require('./../util/app-util');
const constants = require('./../util/constants');
const generalUtil = require('./../util/general-util');
const deviceUtil = generalUtil.homeAutomationModule.deviceUtil;

// viewerDetails = appContext.viewerDetails,
// homeControllerIP = viewerDetails.deviceIP;

// Models
const moduleModel = require('../models/module');
const deviceModel = require('../models/device');
const userModel = require('../models/user');
const roomModel = require('../models/room');
const panelModel = require('../models/panel');
const logModel = require('../models/logs');
const jetsonModel = require('../models/jetson');
const userDeviceCounterModel = require('../models/user_device_counter');
const moodModel = require('../models/mood');
const moodDeviceModel = require('../models/mood_device');
const generalMetaModel = require('../models/general_meta');
const panelDeviceModel = require('../models/panel_device');
const cameraModel = require('../models/camera');

const scheduleModel = require('../models/schedule');
const beaconListener = require('./../listener/beaconListener');

// const socketio = util.socket_io;
const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;
const gateway_ip = require('./../util/general-util').appContext.viewerDetails.deviceIP;
const deviceController = require('./deviceStatusController');

const eventsEmitter = require("./../util/event-manager");

/**
 * List All Rooms (Authenticated by User)
 * @param authenticatedUser (User object)
 */
exports.listRoom = async function (params, cb) {
    try {
        const rooms = await roomModel.listByUser(params.authenticatedUser.user_id, 'room');
        logger.info('LIST ROOMS : ', rooms);
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, rooms);
        return cb(null, response);
    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Add Room (Only Administrator)
 * @param room_name (required)
 * @param user_id
 */
exports.addRoom = async (params, cb) => {

    try {

        const body = {
            room_id: appUtil.generateRandomId('ROOM'),
            room_name: params.room_name.trim(),
            room_type: 'room',
            room_users: [params.user_id].join(),
            room_status: 'y',
            created_by: params.user_id, // TODO -> have to find loggedin user.
            meta: JSON.stringify({})
        }

        if (!body.room_name) {
            logger.warn('Add Room Name');
            response = appUtil.createErrorResponse(constants.responseCode.ADD_ROOM_NAME);
            return cb(null, response);
        }

        // Check if room with same name exists or not
        const isRoomNameExists = await roomModel.isRoomNameExists(body.room_name);
        if (isRoomNameExists && isRoomNameExists.count) {
            logger.warn('Same Room name detected!');
            return cb(null, appUtil.createErrorResponse(constants.responseCode.DUPLICATE_ROOM_NAME));
        }

        await roomModel.add(body);

        logModel.add({
            log_type: 'room_add',
            log_object_id: body.room_id
        }, params.authenticatedUser);

        response = appUtil.createSuccessResponse(constants.responseCode.ROOM_ADD, body.room_id);
        return cb(null, response);
    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * @params room_id 
 */
exports.getRoomStatus = async (params, cb) => {
    try {
        logger.info('GET ROOM STATUS : ', params.room_id);
        
        const roomStatus = await roomModel.getRoomStatus(params.room_id);
        logger.info('Room Status', roomStatus);
        return cb(null, roomStatus);
    } catch (error) {
        logger.error('Room Controller | Get Room Status', error);
    }
}

/**
 * Get Room 
 * @param room_id
 * @param room_type
 */
exports.getRoom = async (params, cb) => {

    try {

        const roomData = await roomModel.get(params.room_id, params.room_type);
        if (!roomData) return cb(null, appUtil.createErrorResponse(constants.responseCode.NO_ROOMS));
        const response = {
            room_id: roomData.room_id,
            room_name: roomData.room_name,
            room_type: roomData.room_type,
            room_status: await roomModel.getRoomStatus(params.room_id),
            panelList: [],
            total_devices: 0,
            total_sensors: 0,
            total_curtain: 0,
        };
        const panelList = await panelModel.lisByRoomId(roomData.room_id);

        for (let panel of panelList) {

            const panelData = {
                panel_id: panel.panel_id,
                panel_name: panel.panel_name,
                panel_type: panel.panel_type,
                panel_status: await panelModel.getPanelStatus(panel.panel_id), // TODO -> need to calculate dynamically weather the devices are enabled in the panels or not
                deviceList: [],
                curtainList: [],
                sensorList: [],
            }

            const deviceList = await panelDeviceModel.detailedListByPanelId(panelData.panel_id);

            for (let device of deviceList) {
                let deviceData = device;
                deviceData.is_unread = ['door_sensor', 'temp_sensor', 'gas_sensor', 'co2_sensor', 'water_detector', 'lock'].includes(device.device_type) ? await logModel.getUnseenDeviceAlertsCount(params.authenticatedUser.user_id, device.device_id) : 0;
                deviceData.meta_pir_timer = ['pir_device'].includes(device.device_type) ? await generalMetaModel.getMetaValue(device.device_id, 'pir_timer', 30) : null;
                if (panelData.panel_type == "general") {
                    panelData.deviceList.push(deviceData);
                    response.total_devices += 1;
                } else if (panelData.panel_type == "sensor") {
                    panelData.sensorList.push(deviceData);
                    response.total_sensors += 1;
                } else if (panelData.panel_type == "curtain") {
                    panelData.curtainList.push(deviceData);
                    response.total_curtain += 1;
                } else if (panelData.panel_type == "pir") {
                    panelData.deviceList.push(deviceData);
                    roomData.total_pir_devices += 1;
                } else if (panelData.panel_type == "smart_device") {
                    panelData.deviceList.push(deviceData);
                    roomData.total_devices += 1;
                }
            }
            // panelData.panel_status = deviceList && deviceList.every(device => device.device_status === '0') ? 'n' : 'y';
            response.panelList.push(panelData);
        }
        // response.room_status = response.panelList.length && response.panelList.every(panel => panel.panel_status === 'n') ? 'n' : 'y';
        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));
    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Get Total Unread Count
 * @param user_id
 */
exports.getTotalUnreadCount = async (params, cb) => {

    try {

        let total_unread_count = 0;
        // total_unread_count = await logModel.getAllUnseenAlertsCounter(params.authenticatedUser);   
        total_unread_count = await userDeviceCounterModel.getTotalCounterOfUser(params.authenticatedUser.user_id);   
            
        setTimeout(function(){
            eventsEmitter.emit('emitSocketTo', {
                topic: 'generalNotificationCounter',
                to: params.authenticatedUser.user_id,
                data: {
                    user_id:params.authenticatedUser.user_id,
                    counter:total_unread_count
                }
            });
        },200);

        return cb ?  cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, total_unread_count)) : null;

    } catch (error) {
        logger.error("Device Controller | Get Total Unread Count Error", error);
        return cb ? cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR)) : null;
    }

}

/**
 * Get All Room Details API
 * @param user_id
 */
exports.getAllRoomDetails = async (params, cb) => {

    logger.info("Room Controller - [GET All Room Details] - ", params);

    try {

        let response = {
            userList: [],
            cameradeviceList: [],
            roomdeviceList: [],
            mac_address: home_controller_device_id,
            total_unread_count: await userDeviceCounterModel.getTotalCounterOfUser(params.authenticatedUser.user_id)
        };


        // If User is Admin : Give him all camera list 
        let start = new Date();
        let user = await userModel.get(params.authenticatedUser.user_id);
        logger.info('User Request Time : ', new Date().valueOf() - start.valueOf(), 'ms');
        start = new Date();
        if (user) {

            // Get Camera List and Jetson List in parallel execution
            [allCameraList, response.jetsonList] = await Promise.all([
                user.admin == 1 ? cameraModel.getAdminUserCameras(params.user_id) : cameraModel.getChildUserCameras(params.authenticatedUser.user_id),
                jetsonModel.list()
            ]);

            for (let jetson of response.jetsonList) {
                jetson.cameraList = allCameraList.filter(function (item) {
                    return item.jetson_device_id == jetson.jetson_id;
                });
                // jetson.total_cameras = jetson.cameraList.length;
            }

            response.jetsonList = response.jetsonList.filter(function (item) {
                return item.cameraList.length > 0;
            });

            response.cameradeviceList = allCameraList.filter(function (item) {
                return item.jetson_device_id == null;
            });
        }

        logger.info('Camera Time', new Date().valueOf() - start.valueOf(), 'ms');
        start = new Date();
        
        user.mac_address = home_controller_device_id;
        user.gateway_ip = gateway_ip;

        let userList = [user];
        let roomList = await roomModel.getAllChildWithRoomDeviceCount(params.authenticatedUser.user_id);
     
        logger.info('OTHER DETAILS TIME', new Date().valueOf() - start.valueOf(), 'ms');
        start = new Date();

        // response.total_unread_count = 0;
        let beaconRoomCounter = beaconListener.getAllRoomBeaconCounter();

        response.userList = userList || [];

        let alertStatusList = [];
        alertStatusList.push(...roomList.map(p => async function () {
            return {
                "status": await roomModel.getRoomStatus(p.room_id),
                "type": "room_status",
                "id": p.room_id
            }
        }));

        const alertStatusData = await Promise.all(alertStatusList.map(p => p()));
        logger.info('ROOM STATUS TIME', new Date().valueOf() - start.valueOf(), 'ms');

        for (let room of roomList) {

            const roomData = {
                room_id: room.room_id,
                room_name: room.room_name,
                total_devices: room.total_devices,
                room_status: (alertStatusData.find(function (item) {
                    return item.id == room.room_id && item.type == 'room_status';
                }))['status'],
                total_beacons: beaconRoomCounter[room.room_id] ? beaconRoomCounter[room.room_id] : 0,
                mood_name_id: null,
                "meta_smart_remote_no": null,
            }

            response.roomdeviceList.push(roomData);

        }

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));
        exports.getTotalUnreadCount(params);
        return;
        
    } catch (error) {
        logger.error("Device Controller | List Device Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Get Room 
 * @param room_id
 * @param room_type
 */
exports.getRoomDetails = async (params, cb) => {

    try {

        logger.info("Room Controller - [GET Room Details] - ", params);

        let [roomData, allIrBlasterList, panelList, allDeviceList] = await Promise.all([
            roomModel.get(params.room_id, params.room_type),
            panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.ir_blaster.device_type]),
            panelModel.lisByRoomId(params.room_id),
            panelDeviceModel.detailedListByRoomId(params.room_id)
        ]);
        
        if (!roomData) return cb(null, appUtil.createErrorResponse(constants.responseCode.NO_ROOMS));

        const response = {
            room_id: roomData.room_id,
            room_name: roomData.room_name,
            room_type: roomData.room_type,
            room_status: 0,
            panelList: [],
            total_devices: 0,
        };

        for (let panel of panelList) {

            const panelData = {
                panel_id: panel.panel_id,
                panel_name: panel.panel_name,
                panel_type: panel.panel_type,
                deviceList: [],
            }

            const deviceList = allDeviceList.filter(function (item) {
                return item.panel_id == panel.panel_id;
            });


            // const deviceList = await panelDeviceModel.detailedListByPanelId(panelData.panel_id);
            response.total_devices = deviceList.length;

            let panel_status = 0;
            for (let device of deviceList) {
                
                let deviceData = device;
                
                // deviceData.is_unread = ['door_sensor', 'temp_sensor', 'gas_sensor', 'water_detector', 'lock'].includes(device.device_type) ? await logModel.getUnseenDeviceAlertsCount(params.authenticatedUser.user_id, device.device_id) : 0;
                deviceData.is_unread = ['door_sensor', 'temp_sensor', 'gas_sensor', 'co2_sensor', 'water_detector', 'lock'].includes(device.device_type) ? await userDeviceCounterModel.getTotalUserDeviceCounter(params.authenticatedUser.user_id, device.device_id) : 0;
                deviceData.meta_pir_timer = ['pir_device'].includes(device.device_type) ? await generalMetaModel.getMetaValue(device.device_id, 'pir_timer', 30) : null;

                if (deviceData.device_type == generalUtil.deviceTypes.remote.device_type) {
                    // Get IR Blaster of the remove and find its temperature
                    const irBlaster = allIrBlasterList.find(function (item) {
                        return item.device_id == device.meta_ir_blaster_id;
                    });

                    deviceData.temperature = irBlaster && irBlaster.device_status ? irBlaster.device_status : null;
                }

                if (device.device_status == 1) {
                    panel_status = 1;
                }

                panelData.deviceList.push(deviceData);
            }

            panelData.panel_status = panel_status;
            panelData.deviceList.length > 0 ? response.panelList.push(panelData) : null;
        }

        response.panelList.sort(generalUtil.panelOrderComparatorForHome);

        // response.room_status = response.panelList.length && response.panelList.every(panel => panel.panel_status === 'n') ? 'n' : 'y';
        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));
      
        if(params.refresh==1){
            eventsEmitter.emit('getRoomOnOffDetails', [...allDeviceList]);
        }
       
        return; 
    } catch (error) {
        logger.error("Room Controller | Get Room Details : ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Update Room (Only Administrator)
 * @param room_id 
 * @param room_name
 * @param panel_data  [ { panel_id:"",panel_name:""}]
 */
exports.editRoom = async (params, cb) => {

    //logger.info('Room Controller | Edit Room API', params);

    try {

        let roomData = await roomModel.get(params.room_id);
        if (!roomData) return cb(null, appUtil.createErrorResponse(constants.responseCode.NO_ROOMS));
        if (params.room_name) {
            await roomModel.update(roomData.room_id, {
                room_name: params.room_name
            });
            roomData.room_name = params.room_name;
        }

        // Iteration for updating panels name;
        if (params.panel_data && params.panel_data.length) {
            for (let panel of params.panel_data) {
                const panelData = await panelModel.get(panel.panel_id);
                if (panelData && panel.panel_name) await panelModel.update(panelData.panel_id, {
                    panel_name: panel.panel_name
                });
            }
        }

        await scheduleModel.deleteEmptySchedule();

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete Room (Only Administrator)
 * @param room_id: to delete the any type of room
 * by deleting room its respected panel and panel_devices will be deleted.
 */
exports.deleteRoom = async (params, cb) => {
    /**
     * @param room_id
     */
    try {

        // Check if the room exists in the database
        const room = await roomModel.get(params.room_id);
        if (!room) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        // Delete Panels of Room And Panel That Reference It
        const panelData = await panelModel.lisByRoomId(params.room_id);
        for (let i = 0; i < panelData.length; i++) {
            await panelModel.delete(panelData[i].panel_id);
        }

        await roomModel.delete(params.room_id);

        // Delete IR Blasters of the specific Room
        // Remember when room is deleted its IR Blaster should go to unassigned list
        const deviceRelatedToRooms = await generalMetaModel.listMetaByValue('device', 'room_id', params.room_id);
        for (let relatedRoomDevice of deviceRelatedToRooms) {
            const device = await deviceModel.get(relatedRoomDevice.table_id);
            if (device && device.device_type == generalUtil.deviceTypes.ir_blaster.device_type) {
                panelDeviceModel.deleteByDeviceId(device.device_id);
            } else if (device && device.device_type == generalUtil.deviceTypes.beacon_scanner.device_type) {
                panelDeviceModel.deleteByDeviceId(device.device_id);
                beaconListener.deleteBeaconScanner(device.device_identifier);
            }
        }

        logModel.add({
            log_type: 'room_delete',
            deleted_data: JSON.stringify(room)
        }, params.authenticatedUser);

        await scheduleModel.deleteEmptySchedule();

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Delete mood 
 * @param mood_id (required)
 */
exports.deleteMood = async (params, cb) => {

    try {

        const mood = await moodModel.get(params.mood_id);

        // Delete All Mood Devices
        await moodDeviceModel.deleteByMoodId(params.mood_id);

        // Empty Allocation of that mood
        await moodModel.update(params.mood_id, {
            created_by: null
        });

        await generalMetaModel.deleteByTableIdAndTableName(params.mood_id, 'mood');

        logModel.add({
            log_type: 'mood_delete',
            deleted_data: JSON.stringify(mood)
        }, params.authenticatedUser);

        await scheduleModel.deleteByMoodId(params.mood_id);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Change Room Status
 * @param room_id (required)
 * @param room_status (0/1) 0 means off | 1 means on 
 */
exports.changeRoomStatus = async (params, cb) => {

    logger.info('Change Room Status API: ', params);

    try {

        const room = await roomModel.get(params.room_id);
        if (!room) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        // If the request is via api and user is not allowed the room access then
        // user should not be able to change room status
        if (params.authenticatedUser && !room.room_users.includes(params.authenticatedUser.user_id)) {
            return cb(null, appUtil.createErrorResponse({
                code: 400,
                message: "Unauthorized Request"
            }));
        }

        // Validate Status
        if (!["0", "1"].includes(params.room_status.toString())) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Status Sent."
            }));
        }

        logger.info('Before CHANGE ROOM SOCKET', params.room_status);
        eventsEmitter.emit('emitSocket', {
            topic: 'changeRoomStatus',
            data: {
                room_id: params.room_id,
                room_status: params.room_status
            }
        });


        let [panelList, allDeviceList] = await Promise.all([
            panelModel.lisByRoomId(params.room_id),
            panelDeviceModel.detailedListByRoomId(params.room_id)
        ]);

        const deviceList = allDeviceList.filter(function (item) {
            return generalUtil.onOffDeviceTypes.includes(item.device_type);
        });

        for (let j = 0; j < deviceList.length; j++) {

            if (deviceList[j].is_active != 'y') {
                continue;
            }

            logger.info('Device Sub Status : ', deviceList[j].device_sub_status);

             deviceController.changeDeviceStatus({
                preDevice: deviceList[j],
                device_id: deviceList[j].device_id,
                device_status: params.room_status,
                device_sub_status: deviceList[j].device_sub_status,
                broadcast_room_panel_status: false,
                log: false,
                authenticatedUser: params.authenticatedUser
            });

        }

        for (let i = 0; i < panelList.length; i++) {

            if (panelList[i].panel_type == 'general') {

                eventsEmitter.emit('emitSocket', {
                    topic: 'changePanelStatus',
                    data: {
                        panel_id: panelList[i].panel_id,
                        panel_status: params.room_status
                    }
                });
            }

           
        }
        // });

        logModel.add({
            log_type: params.room_status == 1 ? 'room_on' : "room_off",
            log_object_id: params.room_id
        }, params.authenticatedUser);

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Room Controller | Change Room Status API ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add Custom Panel / Existing panel from other rooms to new / old room (Only Administrator)
 * @param panel_name (required)
 * @param devices (required)
 * @param room_id (required)
 */
exports.addExistingPanel = async (params, cb) => {

    try {

        // Check if devices are selected
        if (params.devices == null || params.devices.length == 0) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Please select atleast one panel"
            }));
        }

        // Check if panel name is provided
        if (params.panel_name == null) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Please provide panel name."
            }));
        }

        // Check if room exists
        const room = roomModel.get(params.room_id);
        if (!room) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        const newPanelId = appUtil.generateRandomId('PANEL');

        await panelModel.add({
            panel_id: newPanelId,
            room_id: params.room_id,
            panel_name: params.panel_name,
            panel_type: 'general'
        });

        for (let device of params.devices) {

            await panelDeviceModel.add({
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: device,
                panel_id: newPanelId
            });

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));


    } catch (error) {
        logger.error("Room Controller | Add Existiing Panel API ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Change Mood Status API
 * This api is called from api / alexa and google via cloud server as well.
 * @param mood_id (required)
 * @param mood_status (0/1)
 */
exports.changeMoodStatus = async (params, cb) => {

    logger.info('Change Mood Status API: ', params);

    try {

        const room = moodDeviceModel.getByMoodId(params.mood_id);

        if (!room) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Room Selected."
            }));
        }

        // Validate Status
        if (params.mood_status != 0 && params.mood_status != 1) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Mood Status Sent."
            }));
        }

        const deviceList = await moodDeviceModel.getDetailedListByMoodId(params.mood_id);
        
        for (let j = 0; j < deviceList.length; j++) {

            deviceController.changeDeviceStatus({
                preDevice: deviceList[j],
                device_id: deviceList[j].device_id,
                device_status: params.mood_status,
                device_sub_status: deviceList[j].device_sub_status,
                broadcast_room_panel_status: true,
                log: false,
                authenticatedUser: params.authenticatedUser
            });

        }

        eventsEmitter.emit('emitSocket', {
            topic: 'changeMoodStatus',
            data: {
                mood_id: params.mood_id,
                mood_status: params.mood_status
            }
        });

        logModel.add({
            log_type: params.mood_status == 1 ? 'mood_on' : "mood_off",
            log_object_id: params.mood_id
        }, params.authenticatedUser);

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error("Room Controller | Change Mood Status API ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Delete Panel (Only Administrator)
 * @param panel_id: to delete the any type of room
 * by deleting panel its respected panel devices will be deleted.
 */
exports.deletePanel = async (params, cb) => {

    try {

        const panelData = await panelModel.get(params.panel_id);
        if (!panelData) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Panel."
            }));
        }

        // Delete the panel 
        // Will delete the panel and reference panels of that panel
        await panelModel.delete(params.panel_id);
        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

        logModel.add({
            log_type: "panel_delete",
            log_object_id: panelData.room_id,
            deleted_data: JSON.stringify(panelData)
        }, params.authenticatedUser);

        // await scheduleModel.deleteEmptySchedule();

    } catch (error) {
        logger.error("Server Error :: ", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Change Panel Status. i.e turn on or turn off panel
 * @param panel_id : to change room / mood status
 * @param panel_status
 * @value `0`
 * @value `1`
 */
exports.changePanelStatus = async (params, cb) => {

    try {

        // Validate Status
        if (params.panel_status != 0 && params.panel_status != 1) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Panel Status Sent."
            }));
        }

        const panel = await panelModel.get(params.panel_id);
        if (!panel) {
            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Panel Id."
            }));
        }

        const room = await roomModel.get(panel.room_id);

        // If the request is via api and user is not allowed the room access then
        // user should not be able to change room status
        if (params.authenticatedUser && !room.room_users.includes(params.authenticatedUser.user_id)) {
            return cb(null, appUtil.createErrorResponse({
                code: 400,
                message: "Unauthorized Request"
            }));
        }


        if (panel) {

            eventsEmitter.emit('emitSocket', {
                topic: 'changePanelStatus',
                data: {
                    panel_id: params.panel_id,
                    panel_status: params.panel_status
                }
            });


            const deviceList = await panelDeviceModel.detailedListByPanelId(panel.panel_id);

            const devicePromiseList = await Promise.all(
                deviceList.map(device=> deviceController.changeDeviceStatus({
                    preDevice: device,
                    device_id: device.device_id,
                    device_status: params.panel_status,
                    device_sub_status: device.device_sub_status,
                    broadcast_room_panel_status: false,
                    log: false,
                    authenticatedUser: params.authenticatedUser
                }))
            )

            // for (let j = 0; j < deviceList.length; j++) {

            //     await deviceController.changeDeviceStatus({
            //         preDevice: deviceList[j],
            //         device_id: deviceList[j].device_id,
            //         device_status: params.panel_status,
            //         device_sub_status: deviceList[j].device_sub_status,
            //         broadcast_room_panel_status: false,
            //         log: false,
            //         authenticatedUser: params.authenticatedUser
            //     });

            // }

      
            logModel.add({
                log_type: params.panel_status == 0 ? "panel_off" : "panel_on",
                log_object_id: params.panel_id
            }, params.authenticatedUser);

            // Broadcast New Room Status
            eventsEmitter.emit('emitSocket', {
                topic: 'changeRoomStatus',
                data: {
                    room_id: panel.room_id,
                    room_status: params.panel_status == 1 ? 1 : await roomModel.getRoomStatus(panel.room_id)
                }
            });


        } else {

            return cb(null, appUtil.createErrorResponse({
                code: 402,
                message: "Invalid Panel Or Panel Type."
            }));

        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (ex) {
        logger.error("Server Error :: ", ex);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Add mood
 * @param mood_name (provide only if you want to create new mood)
 * @param mood_name_id (required)
 * @param panel_device_ids (required)
 */
exports.addMood = async (params, cb) => {

    // New Code
    try {

        // If we have create new mood based on mood name
        if (params.mood_name && (params.mood_name_id == null || params.mood_name_id == "")) {
            const newMood = await moodModel.createMoodIfNotExists(params.mood_name);
            params.mood_name_id = newMood.mood_id;
        }

        if (!params.panel_device_ids) return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Devices are not attached")));
        else if (!params.panel_device_ids.length) return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Devices are not attached")));

        // Check if mood is already created
        let moodDeviceList = await moodDeviceModel.getByMoodId(params.mood_name_id);
        if (moodDeviceList.length > 0) {
            return cb(null, appUtil.createErrorResponse(constants.responseCode.CUSTOM_MESSAGE(400, "Mood with same name already exists. Please select different mood.")));
        }

        await moodModel.add({
            mood_id: params.mood_name_id,
            created_by: params.authenticatedUser.user_id,
            devices: params.panel_device_ids
        });

        logModel.add({
            log_type: 'mood_add',
            log_object_id: params.mood_name_id
        }, params.authenticatedUser);

        await generalMetaModel.deleteByTableIdAndTableName(params.mood_name_id, "mood");

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));


    } catch (error) {
        logger.error(error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }

}

/**
 * Edit Mood
 * @param room_id
 * @param user_id
 * @param panel_device_ids
 * @param mood_id (required)
 * @param old_mood_id (required)
 * @param mood_name
 */
exports.editMood = async (params, cb) => {

    try {

        let createdByFlag = false;
        // If the user is not changing mood but only devices, then update only devices
        if (params.old_mood_id == params.mood_id) {

            await moodDeviceModel.deleteByMoodId(params.mood_id);

            const mood = await moodModel.get(params.mood_id);

            if (mood.created_by != params.authenticatedUser.user_id) {
                createdByFlag = true;
            } else {
                createdByFlag = false;
            }

            await moodModel.add({
                mood_id: params.mood_id,
                created_by: params.user_id,
                devices: params.panel_device_ids,
                createdByFlag: createdByFlag
            });

        } else {

            if (params.mood_name && params.mood_name.length > 0) {
                const newMood = await moodModel.createMoodIfNotExists(params.mood_name);
                params.mood_id = newMood.mood_id;
            }

            const mood = await moodModel.get(params.mood_id);
            const oldMood = await moodModel.get(params.old_mood_id);

            logger.info('Mood', mood);
            logger.info('Old Mood', oldMood);
            const newMoodDevices = await moodDeviceModel.getByMoodId(params.mood_id);
            logger.info('New Mood Devices', newMoodDevices);

            // if (newMoodDevices.length > 0) {
            //     return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Mood is already added.")));
            // }

            // Clear Old Mood Data
            await moodDeviceModel.deleteByMoodId(params.old_mood_id);
            await moodModel.update(params.old_mood_id, {
                created_by: null
            });

            if (mood.created_by == null) {
                await moodModel.update(params.mood_id, {
                    created_by: params.user_id
                });
            }

            // When allocated to different mood, you need to update the smart remote no also.
            const oldRemoteNo = await moodModel.getSmartRemoteNo(params.old_mood_id);
            await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(params.old_mood_id, 'mood', 'smart_remote_no');
            if (oldRemoteNo) {
                await generalMetaModel.update(params.mood_id, 'mood', 'smart_remote_no', oldRemoteNo);
            }

            await moodDeviceModel.deleteByMoodId(params.mood_id);
            await moodModel.add({
                mood_id: params.mood_id,
                created_by: oldMood.created_by,
                devices: params.panel_device_ids
            });
        }

        // When allocated to differet mood, you need to also update its schedule
        const scheduleList = await scheduleModel.getByMoodId(params.old_mood_id);
        for (let schedule of scheduleList) {

            await scheduleModel.update(schedule.schedule_id, {
                mood_id: params.mood_id
            });

            const scheduleDeviceList = await scheduleModel.getDeviceIdsByScheduleId(schedule.schedule_id);
            const panelDeviceIds = scheduleDeviceList.map(item => item.panel_device_id);

            const removedPanelDevices = panelDeviceIds.filter(function (item) {
                return !params.panel_device_ids.includes(item);
            });

            if (removedPanelDevices.length == panelDeviceIds.length) {
                await scheduleModel.delete(schedule.schedule_id);
            } else {
                for (let removedDevice of removedPanelDevices) {
                    await scheduleModel.deleteDeviceFromSchedule(schedule.schedule_id, removedDevice);
                }
            }
        }

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Room Controller | Edit Mood Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Update panel devices of specific panel. (Only Administrator)
 * API provides ability to add or remove device to specific panel
 * @param panel_id (required)
 * @param devices (required)
 */
exports.updatePanelDevices = async (params, cb) => {

    try {

        const panelData = await panelModel.get(params.panel_id);

        if (!panelData) {
            response = appUtil.createErrorResponse(constants.responseCode.PANEL_NOT_FOUND);
            return cb(null, response);
        }

        // Delete the devices that are not in panel
        let existingPanelDevices = await panelDeviceModel.listByPanelId(params.panel_id);
        let existingPanelDevicesIds = existingPanelDevices.map(item => item.device_id);
        let deletedPanelDevices = existingPanelDevicesIds.filter(x => !params.devices.includes(x));
        for (let deletedDevice of deletedPanelDevices) {
            await panelDeviceModel.deleteByPanelIdAndDeviceId(params.panel_id, deletedDevice);
        }


        for (let j = 0; j < params.devices.length; j++) {

            // If device is already in panel no need to add it
            if (existingPanelDevicesIds.includes(params.devices[j])) {
                continue;
            }

            // If device is not in panel add it
            const newPanelDeviceData = {
                panel_device_id: appUtil.generateRandomId('PANEL-DEVICE'),
                device_id: params.devices[j],
                panel_id: panelData.panel_id
            };
            await panelDeviceModel.add(newPanelDeviceData);

        }

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Room Controller | Update Panel Devices Error', error);
        cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Update smart remote number of the mood
 * @param mood_id (mood id of that mood)
 * @param smart_remote_no 
 */
exports.updateMoodSmartRemoteNumber = async (params, cb) => {

    try {

         let otherRemoteList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.smart_remote.device_type]);
         if(otherRemoteList.length ==0){
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(419, "No smart remote is added to the system.")));
         }

        // Unallocate the number from mood if the empty
        if (params.smart_remote_no == null || params.smart_remote_no == '') {
            await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(params.mood_id, 'mood', 'smart_remote_no');
            return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
        }

        // Check if number is allocated to some other mood
        const existingMoodNo = await generalMetaModel.getMetaByValue('mood', 'smart_remote_no', params.smart_remote_no);
        if (existingMoodNo && existingMoodNo.table_id != params.mood_id) {

            const deviceList = await moodDeviceModel.getByMoodId(existingMoodNo.table_id);
            if (deviceList.length > 0) {
                return cb(null, appUtil.createSuccessResponse(constants.responseCode.CUSTOM_MESSAGE(419, "Smart Remote No is already occupied by other mood.")));
            } else {
                await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(existingMoodNo.table_id, 'mood', 'smart_remote_no');
            }

        }

        // Update the smart remote no to mood 
        await generalMetaModel.update(params.mood_id, "mood", "smart_remote_no", params.smart_remote_no);

        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Room Controller | Update Mood Smart Remote Number Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Assign user to room (Only Administrator)
 * @param room_id (required)
 * @param assgined_user_id
 */
exports.assignUsers = async (params, cb) => {

    try {

        // Check if room exists
        const roomData = await roomModel.get(params.room__id);
        if (!roomData) {
            logger.warn('No Room Found');
            response = appUtil.createErrorResponse(constants.responseCode.NO_ROOMS);
            return cb(null, response);
        }

        // Calculate new user list and update the user room priviledges
        let roomUsers = roomData.room_users.split(",");
        if (roomUsers.includes(params.assgined_user_id)) {
            //logger.info('Room Controller | User is already assgined the particular room', params);
        } else {
            let newRoomUser = roomUsers.push(params.assgined_user_id).join();
            await roomModel.update(roomData.room_id, {
                room_users: newRoomUser
            });
        }

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));

    } catch (error) {
        logger.error('Room Controller | Assign Users Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * Privately Called -> In case there is change in room structure
 * Emit Status For All Panels Rooms and Moods
 */
exports.emitAllRoomPanelMoodStatus = async (params, cb) => {

    try {
    
        let roomList = await roomModel.list(['room_id', 'room_name']);

        for (let room of roomList) {

            eventsEmitter.emit('emitSocket', {
                topic: 'changeRoomStatus',
                data: {
                    room_id: room.room_id,
                    room_status: await roomModel.getRoomStatus(room.room_id)
                }
            });

            // socketio.emit('changeRoomStatus', {
            //     room_id: room.room_id,
            //     room_status: await roomModel.getRoomStatus(room.room_id)
            // });

            let panelList = await panelModel.lisByRoomId(room.room_id);

            for (let panel of panelList) {
                eventsEmitter.emit('emitSocket', {
                    topic: 'changePanelStatus',
                    data: {
                        panel_id: panel.panel_id,
                        panel_status: await panelModel.getPanelStatus(panel.panel_id)
                    }
                });

                // socketio.emit('changePanelStatus', {
                //     panel_id: panel.panel_id,
                //     panel_status: await panelModel.getPanelStatus(panel.panel_id)
                // });
            }
        }
        return cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS));
    } catch (error) {
        logger.error('Room Controller | Emit All Room Panel Mood Status Error', error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * List of moods of admin / user
 * @param authenticatedUser (required)
 */
exports.listMoodAlexa = async (params,cb) => {

    let moodList;

    if (params.authenticatedUser.admin == 1) {
        moodList = await moodModel.getAllAllocatedMoods();
    } else {
        moodList = await moodModel.getUserMoods(params.authenticatedUser.user_id);
    }

    cb(null, appUtil.createSuccessResponse({
        code:200,
        message:'success'
    }, moodList));

}

/**
 * @param mood_id
 */
exports.getMoodStatus = async (params, cb) => {

    const moodStatus = await moodModel.getMoodStatus(params.mood_id);
    
    cb(null, appUtil.createSuccessResponse({
        code:200,
        message:'success'
    }, moodStatus));

}
    

/**
 * List of moods of admin / user
 * @param authenticatedUser (required)
 * @param only_on_off_device
 */
exports.listMood = async (params, cb) => {

    try {

        // const user = await userModel.get(params.authenticatedUser.user_id);

        // if (!user) {

        //     return cb(null, appUtil.createErrorResponse({
        //         code: 402,
        //         message: "Invalid User."
        //     }));
        // }

        let moodList;

        if (params.authenticatedUser.admin == 1) {
            moodList = await moodModel.getAllAllocatedMoods();
        } else {
            moodList = await moodModel.getUserMoods(params.authenticatedUser.user_id);
        }

        const response = {
            userList: [],
            cameradeviceList: [],
            roomdeviceList: [],
            mac_address: home_controller_device_id
        }

        const pirDetectorList = await panelDeviceModel.findByDeviceType(['pir_detector'], ['mood_id']);

        for (let mood of moodList) {

            const roomData = {
                room_id: mood.mood_id,
                room_name: mood.mood_name,
                room_type: 'mood',
                created_by: mood.created_by,
                room_status: await moodModel.getMoodStatus(mood.mood_id),
                panelList: [],
                pirDetector: pirDetectorList.find(function (item) {
                    return item.meta_mood_id == mood.mood_id;
                }),
                total_devices: 0,
                total_sensors: 0,
                total_curtain: 0,
                mood_name_id: mood.mood_id,
                is_unread: 0,
                meta_smart_remote_no: mood.meta_smart_remote_no ? mood.meta_smart_remote_no : null
            }

            const panelList = await panelModel.getMoodPanels(mood.mood_id);

            for (let panel of panelList) {

                let panelData = panel;
                panel.panel_name = panel.room_name + " " + panel.panel_name;
                panel.deviceList = [];
                panel.curtainList = [];
                panel.sensorList = [];
                panel.panel_status = 0;

                const deviceList = await panelDeviceModel.detailedListByPanelIdAndMoodId(panelData.panel_id, mood.mood_id);

                for (let device of deviceList) {

                    if (params.only_on_off_device == true && generalUtil.onOffDeviceTypes.indexOf(device.device_type) == -1) {
                        continue;
                    }

                    let deviceData = device;
                    deviceData.is_unread = ['door_sensor', 'temp_sensor', 'gas_sensor', 'co2_sensor', 'water_detector', 'lock'].includes(device.device_type) ? await logModel.getUnseenDeviceAlertsCount(params.authenticatedUser.user_id, device.device_id) : 0;

                    if (panelData.panel_type == "general") {
                        panelData.deviceList.push(deviceData);
                        roomData.total_devices += 1;
                    } else if (panelData.panel_type == "sensor") {
                        panelData.sensorList.push(deviceData);
                        roomData.total_sensors += 1;
                    } else if (panelData.panel_type == "curtain") {
                        panelData.curtainList.push(deviceData);
                        roomData.total_curtain += 1;
                    }
                }
                // calculating panel status based on device status
                // panelData.panel_status = deviceList && deviceList.every(device => device.device_status === '0') ? 'n' : 'y';
                roomData.panelList.push(panelData);

                // Order the panel in order using comparator
                roomData.panelList.sort(generalUtil.panelOrderComparator);
            }

            // calculating room status based on panel status
            // roomData.room_status = roomData.panelList.length && roomData.panelList.every(panel => panel.panel_status === 'n') ? 'n' : 'y';
            response.roomdeviceList.push(roomData);
        }  

        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, response));

    } catch (error) {
        logger.error("Room Controller | List Mood Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}

/**
 * List All Unallocated Moods
 */
exports.listMoodNames = async (params, cb) => {
    try {
        const availibleNameList = await moodModel.getAllUnallocatedMoods();
        cb(null, appUtil.createSuccessResponse(constants.responseCode.SUCCESS, availibleNameList));
    } catch (error) {
        logger.error("Room Controller | List Mood Names Error", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}