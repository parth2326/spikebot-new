const appUtil = require('./../util/app-util');
const constants = require('./../util/constants');

const deviceModel = require('../models/device');

exports.getAllActiveDevices = async (params, cb) => {
    try {
        const devices = await deviceModel.listActiveDevices();
        logger.info('Devices : ', devices);
        response = appUtil.createSuccessResponse(constants.responseCode.SUCCESS, devices);
        return cb(null, response);
    } catch (error) {
        logger.error("Ifttt Controller | Get Devices Request", error);
        return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}