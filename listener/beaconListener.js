const mqtt = require('mqtt');
const moment = require('moment');
const moduleModel = require('./../models/module');
const generalMetaModel = require('./../models/general_meta');
const logModel = require('./../models/logs');
const generalUtil = require('./../util/general-util');
const panelDeviceModel = require('./../models/panel_device');
let mqttClient;
const deviceController = require('./../controller/deviceStatusController');
const eventsEmitter = require("./../util/event-manager");
let _beaconScannerInitialized = [];
let _beaconScannerList = {};
let _applicableBeaconList = [];
let _applicableBeaconDetailList = {};

/**
 * Add Beacon Scanner
 * @param device_id 
 */
exports.addBeaconScanner = async function (device_id) {

    let beaconScanner = await panelDeviceModel.detailedByDeviceId(device_id);

    if (!beaconScanner) {
        return;
    }

    if (_beaconScannerList[beaconScanner.module_identifier] == null) {

        const roomId = await generalMetaModel.getMetaValue(device_id, 'room_id', null);
        const offTime = await generalMetaModel.getMetaValue(device_id, 'off_time', null);
        const onTime = await generalMetaModel.getMetaValue(device_id, 'on_time', null);
        const range = await generalMetaModel.getMetaValue(device_id, 'range', 9);

        _beaconScannerList[beaconScanner.module_identifier] = {
            device_id: beaconScanner.device_id,
            room_id: roomId,
            off_time: offTime,
            on_time: onTime,
            range: range,
            module_identifier: beaconScanner.module_identifier,
            beacons: [],
            removedBeacons: []
        };
    }
}


/**
 * Update Beacon Scanner
 * @param device_id 
 */
exports.updateBeaconScanner = async function (device_id) {

    let beaconScanner = await panelDeviceModel.detailedByDeviceId(device_id);

    if (!beaconScanner) {
        return;
    }

    if (_beaconScannerList[beaconScanner.module_identifier] != null) {

        const offTime = await generalMetaModel.getMetaValue(device_id, 'off_time', null);
        const onTime = await generalMetaModel.getMetaValue(device_id, 'on_time', null);
        const range = await generalMetaModel.getMetaValue(device_id, 'range', 9);

        _beaconScannerList[beaconScanner.module_identifier] = {
            ..._beaconScannerList[beaconScanner.module_identifier], ...{
                off_time: offTime,
                on_time: onTime,
                range: range
            }
        };

        //logger.info('New Data Set', _beaconScannerList[beaconScanner.module_identifier]);
    }
}
/**
 * Delete Beacon Scanner
 * @param beacon_identifier
 */
exports.deleteBeaconScanner = async function (beacon_identifier) {
    delete _beaconScannerList[beacon_identifier];
}

/**
 * Initialize Beacon Listener
 */
exports.init = async () => {

    mqttClient = mqtt.connect('mqtt://127.0.0.1:1883');

    mqttClient.on('connect', () => {

        // //logger.info('MQTT CONNECTED');

        mqttClient.subscribe('ESP32/BECON');
        mqttClient.on('message', async function (topic, message, packet) {

            message = message.toString().replace(/\u0000/g, "").trim();
            // //logger.info('MQTT DATA RECEIVED', message);

            if (topic == 'ESP32/BECON') {

                let messageJson = JSON.parse(message.toString());
                messageJson.selfmac = messageJson.selfmac.toUpperCase();

                if (!_beaconScannerList[messageJson.selfmac]) {
                    return;
                }

                // Get All Mac Received
                let macList = messageJson.mac.split(",");
                let ssList = messageJson.ss.split(",");

                let beaconWithSSList = [];
                for (let i = 0; i < macList.length; i++) {
                    let item = {};
                    item.mac = macList[i];
                    item.ss = ssList[i];
                    beaconWithSSList.push(item);

                    eventsEmitter.emit('emitSocket', {
                        topic: 'changeBeaconRange',
                        data: {
                            module_identifier: item.mac,
                            range: item.ss                        
                        }
                    });

                    // socketio.emit('changeBeaconRange', {
                    //     module_identifier: item.mac,
                    //     range: item.ss
                    // });
                }

                // var signalStrengthList = ssList.filter(function (item) {
                //     return item <= 10;
                // });

                // Check the beacons within given range
                let inRangeMacList = beaconWithSSList.filter(function (item) {
                    return item.ss <= _beaconScannerList[messageJson.selfmac].range * process.env.BEACON_SCANNER_RANGE_MULTIPLIER
                }).map(item => item.mac);

                // for (let ss of signalStrengthList) {
                //     inRangeMacList.push(macList[ssList.indexOf(ss)])
                // }

                // do not on off device when beacon scanner sends the first data
                if (_beaconScannerInitialized.includes(messageJson.selfmac)) {
                    _beaconScannerList[messageJson.selfmac].allDetectedBeacons = beaconWithSSList;
                    _beaconScannerList[messageJson.selfmac].lastResponseTime = new Date().valueOf();
                    exports.handleBeanconChange(messageJson.selfmac, inRangeMacList.filter(x => _applicableBeaconList.includes(x)));
                } else {
                    _beaconScannerInitialized.push(messageJson.selfmac);
                    _beaconScannerList[messageJson.selfmac].beacons = inRangeMacList.filter(x => _applicableBeaconList.includes(x));
                    _beaconScannerList[messageJson.selfmac].allDetectedBeacons = beaconWithSSList;

                    eventsEmitter.emit('emitSocket', {
                        topic: 'beaconInRoomCountUpdate',
                        data: {
                            room_id: _beaconScannerList[messageJson.selfmac].room_id,
                            device_count: _beaconScannerList[messageJson.selfmac].beacons.length
                        }
                    });

                    // socketio.emit('beaconInRoomCountUpdate', {
                    //     room_id: _beaconScannerList[messageJson.selfmac].room_id,
                    //     device_count: _beaconScannerList[messageJson.selfmac].beacons.length
                    // });

                }
            }
        });
    });

    let beaconScannerList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon_scanner.device_type]);
    for (let scanner of beaconScannerList) {

        const roomId = await generalMetaModel.getMetaValue(scanner.device_id, 'room_id', null);
        const offTime = await generalMetaModel.getMetaValue(scanner.device_id, 'off_time', null);
        const onTime = await generalMetaModel.getMetaValue(scanner.device_id, 'on_time', null);
        const range = await generalMetaModel.getMetaValue(scanner.device_id, 'range', 9);

        _beaconScannerList[scanner.module_identifier] = {
            device_id: scanner.device_id,
            room_id: roomId,
            off_time: offTime,
            on_time: onTime,
            range: range,
            module_identifier: scanner.module_identifier,
            beacons: [],
            removedBeacons: []
        };

        //logger.info('scanner initialized',_beaconScannerList[scanner.module_identifier]);

    }

    exports.updateApplicableBeacons();
}

exports.canTurnOnDeviceNow = function (scanner) {

    const currentTime = moment().format('HHmm');

    //logger.info('CAN ON OFF CHECK',currentTime,scanner.on_time.replace(":", "") ,scanner.off_time.replace(":", "") );

    if (scanner.on_time != null && scanner.off_time != null) {

        // if on time is greater than off time 
        // it means time is going through mid night
        if (Number(scanner.on_time.replace(":", "")) > Number(scanner.off_time.replace(":", "")) && (Number(currentTime) > Number(scanner.on_time.replace(":", "")) || Number(currentTime) < Number(scanner.off_time.replace(":", "")))) {

            return true;

        } else if (Number(currentTime) > Number(scanner.on_time.replace(":", "")) && Number(currentTime) < Number(scanner.off_time.replace(":", ""))) {
            return true;
        }

        //logger.info('CAN ON OFF ',false );
        return false;

    } else {
        //logger.info('CAN ON OFF ',true );
        return true;
    }

}

/**
 * Handle Beacon Change in specific Beacon Scanner
 * @param beaconIdentifier
 * @param newBeaconList (beacon in range of 70)
 */
exports.handleBeanconChange = async function (beaconIdentifier, newBeaconList) {

    if (beaconIdentifier != null) await moduleModel.updateLastResponseTime(beaconIdentifier);

    // set the beacons
    const oldBeaconList = _beaconScannerList[beaconIdentifier] ? _beaconScannerList[beaconIdentifier].beacons : [];

    // beacon removed in this data
    const removedBeacons = oldBeaconList.filter(function (item) {
        return !newBeaconList.includes(item);
    });

    // new beacon added in this data
    const newBeacons = newBeaconList.filter(function (item) {
        return !oldBeaconList.includes(item);
    });

    if (!_beaconScannerList[beaconIdentifier].previousBeaconData) {
        _beaconScannerList[beaconIdentifier].previousBeaconData = [];
    }

    if (_beaconScannerList[beaconIdentifier].visibleBeacons == null) {
        _beaconScannerList[beaconIdentifier].visibleBeacons = [];
    }

    for (let newBeaon of newBeaconList) {
        if (_applicableBeaconDetailList[newBeaon]) {
            if (_beaconScannerList[beaconIdentifier].visibleBeacons.includes(newBeaon) == false) {

                _beaconScannerList[beaconIdentifier].visibleBeacons.push(newBeaon);

                eventsEmitter.emit('emitSocket', {
                    topic: 'beaconAddedToRoom',
                    data: {
                        room_id: _beaconScannerList[beaconIdentifier].room_id,
                        device_id: _applicableBeaconDetailList[newBeaon].device_id,
                        device: _applicableBeaconDetailList[newBeaon],
                    }
                });

                // socketio.emit('beaconAddedToRoom', {
                //     room_id: _beaconScannerList[beaconIdentifier].room_id,
                //     device_id: _applicableBeaconDetailList[newBeaon].device_id,
                //     device: _applicableBeaconDetailList[newBeaon],
                // });

                eventsEmitter.emit('emitSocket', {
                    topic: 'beaconInRoomCountUpdate',
                    data: {
                        room_id: _beaconScannerList[beaconIdentifier].room_id,
                        device_count: _beaconScannerList[beaconIdentifier].visibleBeacons.length
                    }
                });

                // socketio.emit('beaconInRoomCountUpdate', {
                //     room_id: _beaconScannerList[beaconIdentifier].room_id,
                //     device_count: _beaconScannerList[beaconIdentifier].visibleBeacons.length
                // });
            }
        }
    }
    // }


    // logger.info('BEACON SCANNER - [ROOM SOCKET]',{
    //     room_id: _beaconScannerList[beaconIdentifier].room_id,
    //     device_count: newBeaconList.length
    // });

    // socketio.emit('beaconInRoomCountUpdate', {
    //     room_id: _beaconScannerList[beaconIdentifier].room_id,
    //     device_count: newBeaconList.length
    // });


    // logger.info('PREVIOUS DATA',_beaconScannerList[beaconIdentifier].previousBeaconData);
    // logger.info('NEW DATA',newBeaconList);

    // if (_beaconScannerList[beaconIdentifier].previousBeaconData) {

    //     const removedBeaconInThisIteration = _beaconScannerList[beaconIdentifier].previousBeaconData.filter(function (item) {
    //         return !newBeaconList.includes(item);
    //     });

    //     for (let newBeaon of removedBeaconInThisIteration) {
    //         if (_applicableBeaconDetailList[newBeaon]) {

    //             // logger.info('BEACON SCANNER - [BEACON REMOVED SOCKET]',{
    //             //     room_id: _beaconScannerList[beaconIdentifier].room_id,
    //             //     device_id: _applicableBeaconDetailList[newBeaon].device_id
    //             // });

    //             socketio.emit('beaconRemovedFromRoom', {
    //                 room_id: _beaconScannerList[beaconIdentifier].room_id,
    //                 device_id: _applicableBeaconDetailList[newBeaon].device_id
    //             });
    //         }
    //     }

    //     socketio.emit('beaconInRoomCountUpdate', {
    //         room_id: _beaconScannerList[beaconIdentifier].room_id,
    //         device_count: newBeaconList.length
    //     });
    // }

    // _beaconScannerList[beaconIdentifier].previousBeaconData = newBeaconList;


    // unique removed beacons
    // const uniqueRemovedBeacons = [...new Set(removedBeacons)];

    // const applicableBeaconList = oldBeaconList.concat(newBeaconList).filter(function(item) {
    //     return !item.includes(removedBeacons);
    // });

    // _beaconScannerList[beaconIdentifier].beacons = applicableBeaconList;
    _beaconScannerList[beaconIdentifier].removedBeacons = _beaconScannerList[beaconIdentifier].removedBeacons.filter(function (item) {
        return !newBeaconList.includes(item);
    });

    _beaconScannerList[beaconIdentifier].removedBeacons.push(...removedBeacons);
    if (_beaconScannerList[beaconIdentifier].removedBeacons.length == 0 && newBeacons.length == 0) {
        return;
    }

    /// On off devices

    let offDevices = [];
    let finalRemovedBeaconList = [];
    const uniqueRemovedBeacons = [...new Set(_beaconScannerList[beaconIdentifier].removedBeacons)];

    for (let removedBeaconModuleIdentifier of uniqueRemovedBeacons) {

        const removeCounter = _beaconScannerList[beaconIdentifier].removedBeacons.filter(function (item) {
            return item == removedBeaconModuleIdentifier;
        }).length;

        // logger.info(process.env.BEACON_SCANNER_TIMEOUT);
        if (removeCounter > 15) {

            finalRemovedBeaconList.push(removedBeaconModuleIdentifier);

            if (_applicableBeaconDetailList[removedBeaconModuleIdentifier] && _applicableBeaconDetailList[removedBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]) {
                offDevices.push(..._applicableBeaconDetailList[removedBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]);
            }

            _beaconScannerList[beaconIdentifier].removedBeacons = _beaconScannerList[beaconIdentifier].removedBeacons.filter(function (item) {
                return item != removedBeaconModuleIdentifier;
            });

        } else if (removeCounter > 4) {

            eventsEmitter.emit('emitSocket', {
                topic: 'beaconRemovedFromRoom',
                data: {
                    room_id: _beaconScannerList[beaconIdentifier].room_id,
                    device_id: _applicableBeaconDetailList[removedBeaconModuleIdentifier].device_id
                }
            });

            // socketio.emit('beaconRemovedFromRoom', {
            //     room_id: _beaconScannerList[beaconIdentifier].room_id,
            //     device_id: _applicableBeaconDetailList[removedBeaconModuleIdentifier].device_id
            // });

            _beaconScannerList[beaconIdentifier].visibleBeacons = _beaconScannerList[beaconIdentifier].visibleBeacons.filter(function (item) {
                return item != removedBeaconModuleIdentifier;
            });

            eventsEmitter.emit('emitSocket', {
                topic: 'beaconInRoomCountUpdate',
                data: {
                    room_id: _beaconScannerList[beaconIdentifier].room_id,
                    device_count: _beaconScannerList[beaconIdentifier].visibleBeacons.length
                }
            });

            // socketio.emit('beaconInRoomCountUpdate', {
            //     room_id: _beaconScannerList[beaconIdentifier].room_id,
            //     device_count: _beaconScannerList[beaconIdentifier].visibleBeacons.length
            // });

            logger.info('BEACON LISTENER - [BEACON VISIBILITY REMOVED]', {
                room_id: _beaconScannerList[beaconIdentifier].room_id,
                device_count: _beaconScannerList[beaconIdentifier].visibleBeacons.length
            });
        }
    }

    const applicableBeaconList = oldBeaconList.concat(newBeaconList).filter(function (item) {
        return !finalRemovedBeaconList.includes(item);
    });


    // if ([...new Set(applicableBeaconList)].length != _beaconScannerList[beaconIdentifier].beacons.length) {
    //     socketio.emit('beaconInRoomCountUpdate', {
    //         room_id: _beaconScannerList[beaconIdentifier].room_id,
    //         device_count: [...new Set(applicableBeaconList)].length
    //     });
    // }

    _beaconScannerList[beaconIdentifier].beacons = [...new Set(applicableBeaconList)];

    if (!_beaconScannerList[beaconIdentifier].visibleBeacons) {
        _beaconScannerList[beaconIdentifier].visibleBeacons = [..._beaconScannerList[beaconIdentifier].beacons];
    }

    let doNotOffDevices = [];
    // const applicableBeaconList = 
    for (let newBeaconModuleIdentifier of applicableBeaconList) {
        if (_applicableBeaconDetailList[newBeaconModuleIdentifier] && _applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]) {
            doNotOffDevices.push(..._applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]);
        }
    }

    let doNotOnDevices = [];
    // const applicableBeaconList = 
    for (let newBeaconModuleIdentifier of oldBeaconList) {
        if (_applicableBeaconDetailList[newBeaconModuleIdentifier] && _applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]) {
            doNotOnDevices.push(..._applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]);
        }
    }

    let onDevices = [];
    // const applicableBeaconList = 
    for (let newBeaconModuleIdentifier of newBeacons) {
        if (_applicableBeaconDetailList[newBeaconModuleIdentifier] && _applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]) {
            onDevices.push(..._applicableBeaconDetailList[newBeaconModuleIdentifier].related_devices[_beaconScannerList[beaconIdentifier].device_id]);
        }
    }

    offDevices = offDevices.filter(function (item) {
        return !doNotOffDevices.includes(item);
    });

    onDevices = onDevices.filter(function (item) {
        return !doNotOnDevices.includes(item);
    });

    const panelDeviceList = onDevices.concat(offDevices);

    if (panelDeviceList.length == 0 || !exports.canTurnOnDeviceNow(_beaconScannerList[beaconIdentifier])) {
        return;
    }


    panelDevices = await panelDeviceModel.getMultipleWithStatus(panelDeviceList);

    for (let panelDevice of panelDevices) {

        isOnDevice = onDevices.includes(panelDevice.panel_device_id);

        // If the device was physically operated in last.. dont not do any action
        const canTurnOnDevice = await logModel.canBeaconTurnOnDevice(panelDevice.device_id);
        if (canTurnOnDevice == false) {
            continue;
        }

        if (isOnDevice == true && panelDevice.device_status == 0) {

            await deviceController.changeDeviceStatus({
                device_id: panelDevice.device_id,
                device_status: 1,
                broadcast_room_panel_status: true,
                log: true,
                authenticatedUser: {
                    user_id: "Beacon Activity",
                    phone_id: "0000",
                    phone_type: "spikebot"
                }
            }, function (err, response) { });
            await delay(50);

        } else if (isOnDevice == false && panelDevice.device_status == 1) {

            await deviceController.changeDeviceStatus({
                device_id: panelDevice.device_id,
                device_status: 0,
                broadcast_room_panel_status: true,
                log: true,
                authenticatedUser: {
                    user_id: "Beacon Activity",
                    phone_id: "0000",
                    phone_type: "spikebot"
                }
            }, function (err, response) { });
            await delay(50);

        }
    }



    // //logger.info('Beacons Removed ', removedBeacons);

    // Get devices of old beacons and devices of new beacons as well


    // Start changing status of beacon



}


exports.updateApplicableBeacons = async function () {

    // Get All The Beacons And Update their related devices
    let beaconList = await panelDeviceModel.findByDeviceType([generalUtil.deviceTypes.beacon.device_type]);
    _applicableBeaconList = beaconList.map(item => item.module_identifier);

    for (let beacon of beaconList) {
        _applicableBeaconDetailList[beacon.module_identifier] = beacon;
        const relatedDevices = await generalMetaModel.getMetaValue(beacon.device_id, 'related_devices', "[]");
        _applicableBeaconDetailList[beacon.module_identifier].related_devices = JSON.parse(relatedDevices);
    }

}


/**
 * Update On Beacon in the list
 * @param device_id
 */
exports.updateBeacon = async function (device_id) {

    let beacon = await panelDeviceModel.detailedByDeviceId(device_id);
    if (beacon) {

        // Add Beacon to applicable beacon list if not found
        if (!_applicableBeaconList.includes(beacon.module_identifier)) {
            _applicableBeaconList.push(beacon.module_identifier);
        }

        // Update the beacon in other detail array
        _applicableBeaconDetailList[beacon.module_identifier] = beacon;
        const relatedDevices = await generalMetaModel.getMetaValue(beacon.device_id, 'related_devices', "[]");
        _applicableBeaconDetailList[beacon.module_identifier].related_devices = JSON.parse(relatedDevices);

        // logger.info('BEACON LIS/TENER - [BEACON UPDATED] - ', JSON.parse(relatedDevices));
        // logger.info('applicable beacon list', _applicableBeaconList);
        // logger.info('applicable detail list', _applicableBeaconDetailList);

    }

}

/**
 * Remove Beacon From the list
 * @param beacon_identifier
 */
exports.removeBeacon = function (beacon_identifier) {

    _applicableBeaconList = _applicableBeaconList.filter(function (item) {
        return item != beacon_identifier;
    });

    try {

        const beaconDevice = _applicableBeaconDetailList[beacon_identifier];

        if (_applicableBeaconDetailList[beacon_identifier]) delete _applicableBeaconDetailList[beacon_identifier];

        for (let scannerIdentifer of Object.keys(_beaconScannerList)) {

            _beaconScannerList[scannerIdentifer].beacons = _beaconScannerList[scannerIdentifer].beacons.filter(function (item) {
                return item != beacon_identifier;
            });

            _beaconScannerList[scannerIdentifer].removedBeacons = _beaconScannerList[scannerIdentifer].removedBeacons.filter(function (item) {
                return item != beacon_identifier;
            });

            // Update Beacon count
            eventsEmitter.emit('emitSocket', {
                topic: 'beaconInRoomCountUpdate',
                data: {
                    room_id: _beaconScannerList[scannerIdentifer].room_id,
                    device_count: _beaconScannerList[scannerIdentifer].previousBeaconData ? _beaconScannerList[scannerIdentifer].previousBeaconData.length : 0
                }
            });

            // socketio.emit('beaconInRoomCountUpdate', {
            //     room_id: _beaconScannerList[scannerIdentifer].room_id,
            //     device_count: _beaconScannerList[scannerIdentifer].previousBeaconData ? _beaconScannerList[scannerIdentifer].previousBeaconData.length : 0
            // });

            // Remove beacon from from socket emit
            eventsEmitter.emit('emitSocket', {
                topic: 'beaconRemovedFromRoom',
                data: {
                    room_id: _beaconScannerList[scannerIdentifer].room_id,
                    device_id: beaconDevice.device_id
                }
            });

            // socketio.emit('beaconRemovedFromRoom', {
            //     room_id: _beaconScannerList[scannerIdentifer].room_id,
            //     device_id: beaconDevice.device_id
            // });

        }

    } catch (error) {
        logger.error('Error removing beacons from beacon listener', error);
    }

}

/**
 * Get List of actively scanned beacons in specific scanner
 * @param scanner_identifier (module_identifer of beacon scanner)
 */
exports.getBeaconsDetectedInScanner = function (scanner_identifier) {

    if (!_beaconScannerList[scanner_identifier]) {
        return [];
    }

    return _beaconScannerList[scanner_identifier].allDetectedBeacons;
}

/**
 * Get List of applicable beacons in specific scanner
 * @param scanner_identifier (module_identifer of beacon scanner)
 */
exports.getApplicableBeaconsInScanner = function (scanner_identifier) {

    if (!_beaconScannerList[scanner_identifier]) {
        return [];
    }

    return _beaconScannerList[scanner_identifier].visibleBeacons.filter(function (item) {
        return _applicableBeaconList.includes(item);
    });
}


/**
 * Get Count of Beacons in Each Room
 */
exports.getAllRoomBeaconCounter = function () {

    let response = {};
    for (const [scanner_identifier, scanner] of Object.entries(_beaconScannerList)) {
        if (scanner.room_id) {
            response[scanner.room_id] = _beaconScannerList[scanner_identifier].visibleBeacons ? _beaconScannerList[scanner_identifier].visibleBeacons.length : 0;
        }
    }

    //logger.info('beacon room list status', response);
    return response;
}

setInterval(function () {

    process.nextTick(async function () {

        for (const [scanner_identifier, scanner] of Object.entries(_beaconScannerList)) {
            if (_beaconScannerList[scanner_identifier] && _beaconScannerList[scanner_identifier].lastResponseTime < (new Date().valueOf() - 60000)) {
                _beaconScannerList.beacons = [];
                _beaconScannerList.visibleBeacons = [];

                eventsEmitter.emit('emitSocket', {
                    topic: 'beaconInRoomCountUpdate',
                    data: {
                        room_id: _beaconScannerList[scanner_identifier].room_id,
                        device_count: 0
                    }
                });

                // socketio.emit('beaconInRoomCountUpdate', {
                //     room_id: _beaconScannerList[scanner_identifier].room_id,
                //     device_count: 0
                // });

            }
        }
    });

}, 5000);

function delay(t, val) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve(val);
        }, t);
    });
}

