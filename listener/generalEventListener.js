const deviceController = require('./../controller/deviceController');
const roomController = require('./../controller/roomController');
const alertController = require('./../controller/alertController');
const cc2530Util = require('./../util/cc2530-util');
const moduleModel = require('./../models/module');
const deviceModel = require('./../models/device');
const logModel = require('./../models/logs');
const generalMetaModel = require('./../models/general_meta');
const statusMappingModel = require('./../models/device_status_mapping');
const queueManager = require('./../util/queue-manager-util');
const generalUtil = require('./../util/general-util');
const eventsEmitter = require("./../util/event-manager");
const cacheManager = require('./../util/cache-util');
const { connect } = require('socket.io-client');
const acknowledgeSensorData = true;
// For Setup And Configuration Of Modules
cc2530Util.registerDeviceSetupListener((response) => {
    //logger.info('General Event Listener | ', 'configureRequestCallback', response);
    deviceController.configureRequestCallback(response);
});

let doorSensorDebouncer = {};
let doorSensorValueCollector = {};

const onDeviceStatusListener = async (params, type) => {

    logger.info('General Event Listener | ', type, params);

    if (type == "registerDeviceListenerCallBack") {


    } else if (type == 'registerDoorSensorStatusListener') {

        // if (params.moduleId) {
        //     let arr = [];
        //     function debounce(i) {
        //         setTimeout(function() {
        //             arr.push(i);
        //             if (arr.length < 10) {
        //                 debounce(i);                    
        //             }
        //         }, 1000);
        //     }
        //     debounce(params.event);    
        // }

        // Set Debounce 
        if (!params.allowPass) {
            if (!doorSensorDebouncer[params.moduleId]) {

                logger.info('FIRST DEBOUNCE SET', params);

                doorSensorDebouncer[params.moduleId] = setTimeout(function () {
                    params.allowPass = true;
                    onDeviceStatusListener(params, 'registerDoorSensorStatusListener');
                }, 2000);
                return;
            } else {

                logger.info('ANOTHER DEBOUNCE SET', params);

                clearTimeout(doorSensorDebouncer[params.moduleId]);
                doorSensorDebouncer[params.moduleId] = setTimeout(function () {
                    params.allowPass = true;
                    onDeviceStatusListener(params, 'registerDoorSensorStatusListener');
                }, 2000);
            }

            return;
        }

        logger.info('FINAL OUTPUT ', params);
        delete doorSensorDebouncer[params.moduleId];
        moduleModel.updateLastResponseTime(params.moduleId);

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.panel_device_id) {

            await deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.event
            }, function (err, response) { });

            if (device.is_active == 'n') {
                //logger.info('Modules is marked active : ', device.device_name);
                // Emit Real Time Socket To Change Module Status

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Send Alert To The Users
            alertController.handleStatusChange({
                device: device,
                status: {
                    device_status: params.event
                }
            }, (err, response) => { });

            // statusMappingModel.add({
            //     device_id: device.device_id,
            //     status_type: generalUtil.deviceStatusMappingTypes.door_sensor.door_open_close,
            //     status_value: params.event
            // });

        }else{
            logger.error('[INVALID MODULE] - registerDoorSensorStatusListener',params);
        }

    } else if (type == 'registerWaterDetectorStatusListener') {

        moduleModel.updateLastResponseTime(params.moduleId);

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.panel_device_id) {

            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.event
            });

            if (device.is_active == 'n') {
                //logger.info('Water Sensor Module Marked Active', device);
                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Send Alert To The Users
            if (params.event.toString() == '1') {
                alertController.handleStatusChange({
                    device: device,
                    status: {
                        device_status: params.event
                    }
                }, (err, response) => { });
            }

            // statusMappingModel.add({
            //     device_id: device.device_id,
            //     status_type: generalUtil.deviceStatusMappingTypes.water_detector.water_detected,
            //     status_value: params.event
            // });

        }

        // registerWaterDetectorStatusListener

    } else if (type == 'registerDoorSensorVoltageListener') {

        moduleModel.updateLastResponseTime(params.moduleId);

        let totalVoltage = 3.4;
        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.device_type == generalUtil.deviceTypes.door_sensor.device_type) {

            // Update battery level of that sensor
            await generalMetaModel.update(device.device_id, 'device', 'battery_level', parseInt((params.status / totalVoltage).toFixed(2)));

            if (device.is_active == 'n') {
                //logger.info('Door Sensor Module Marked Active', device);
                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Log the change in the database
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.door_sensor.door_battery,
                status_value: parseInt((params.status / totalVoltage).toFixed(2)).toString()
            });

        }else{
            logger.error('[INVALID MODULE] - registerDoorSensorVoltageListener',params);
        }

    } else if (type == 'registerStartupDeviceStatusListener') {


    } else if (type == 'registerheavyLoadValueListener') {


        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.deviceId, params.moduleId);

        if (device) {

            moduleModel.updateLastResponseTime(params.moduleId);

            // await dbManager.executeNonQuery('DELETE from spik')

            // Log Total Energy
            statusMappingModel.addOrUpdate({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.heavy_load.total_energy,
                status_value: params.total_energy
            });

            // Log Load Current
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.heavy_load.load_current,
                status_value: params.load_current
            });

            // Log Real Power
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.heavy_load.real_power,
                status_value: params.real_power
            });

            // Log Load Voltage
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.heavy_load.load_voltage,
                status_value: params.load_voltage
            });

            // Send Real Power socket to the users
            eventsEmitter.emit('emitSocket', {
                topic: 'statusMapping:heavy_load_voltage',
                data: {
                    device_id: device.device_id,
                    real_power: params.real_power
                }
            });

            // socketio.emit('statusMapping:heavy_load_voltage', {
            //     device_id: device.device_id,
            //     real_power: params.real_power
            // });

            if (device.is_active == 'n') {
                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

        }


    } else if (type == 'registerSwitchModuleBootDeviceListener' || type == 'registerDeviceStatusACKListener' || type == 'registerPhysicalDeviceListener' || type == 'registerSwitchModuleStatusListener') {



        if (params.moduleId != null) moduleModel.updateLastResponseTime(params.moduleId);

        let deviceList = params.deviceStatusDetails;
        handlePanelDeviceStatusChange(deviceList, {
            "type": type
        });

    } else if (type == 'registerCurtainAcknowledgement') {

        let deviceList = params.deviceStatusDetails;
        let curtainDevice = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);

        if (!curtainDevice) {
            return;
        }

        // remove from queue on response received
        for (let device of deviceList) {
            queueManager.removeFromQueue({
                device_id: curtainDevice.device_id,
                device_status: device.status,
                device_identifier: device.deviceId
            });
        }

        const isOpen = deviceList.filter(function (item) {
            return item.deviceId == 2 && item.status == 1;
        });

        const isClose = deviceList.filter(function (item) {
            return item.deviceId == 1 && item.status == 1;
        });

        // if curtain is open and curtain status is not open in db
        if (isOpen.length > 0 && curtainDevice.device_status != 1) {

            deviceController.handleDeviceStatusChangeListeners({
                device_id: curtainDevice.device_id,
                device_status: 1
            });

            // if curtain is closed and curtain status is not closed in db
        } else if (isClose.length > 0 && curtainDevice.device_status != 0) {

            deviceController.handleDeviceStatusChangeListeners({
                device_id: curtainDevice.device_id,
                device_status: 0
            });

        } else {

            // if curtain is paused
            deviceController.handleDeviceStatusChangeListeners({
                device_id: curtainDevice.device_id,
                device_status: 2
            }, function (err, response) { });

        }

        moduleModel.updateLastResponseTime(params.moduleId);

        if (curtainDevice.is_active == 'n') {

            //logger.info('CurtianModule Marked Active', curtainDevice);

            // Emit Real Time Socket To Change Module Status
            eventsEmitter.emit('emitSocket', {
                topic: 'changeModuleStatus',
                data: {
                    module_id: curtainDevice.module_id,
                    is_active: 'y'
                }
            });

            // socketio.emit('changeModuleStatus', {
            //     module_id: curtainDevice.module_id,
            //     is_active: 'y'
            // });
        }

    } else if (type == 'registerTempSensorStatusListener') {

        /**
         * @param status_type 0 - temp value is negative | 1 - temp value is positive
         * @param humidity
         * @param moduleId
         * @param status
         */

        let status_type = params.status_type,
            humidity = params.humidity,
            module_id = params.moduleId;

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        logger.info('GENERAL EVENT LISTNER TEMPRATUER : ', device);
        if (device && device.panel_device_id) {

            if (params.status_type == 0) params.status = '-' + params.status;

            // Update temperature of temp sensor
            await generalMetaModel.update(device.device_id, 'device', 'temprature', parseInt((params.status)));
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.temp_sensor.temp_value,
                status_value: parseInt((params.status)).toString()
            });

            await generalMetaModel.update(device.device_id, 'device', 'humidity', parseInt((params.humidity)));
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.temp_sensor.temp_humidity,
                status_value: parseInt((params.humidity)).toString()
            });
        
            logger.error("Temprature : ", params.status);
            logger.error("Humidity : ", params.humidity);
            // Update the last response time of temperature sensor
            moduleModel.updateLastResponseTime(params.moduleId);

            // Handle Device Status Change For Temperature Sensor
            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.status,
                device_sub_status: params.humidity
            }, function (err, response) { });

            // If module is inactive, send active socket
            if (device.is_active == 'n') {

                //logger.info('Temp Sensor Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Send Alert to the User
            alertController.handleStatusChange({
                device: device,
                status: {
                    device_status: params.status,
                    device_sub_status: params.humidity
                }
            }, (err, response) => { });

        }else{
            logger.error('[INVALID MODULE] - registerTempSensorStatusListener',params);
        }

    } else if (type == 'registerTempSensorVoltageListener') {

        /**
         * @param moduleId
         * @param status
         */

        logger.info('Temperature Bttery : ', params.status);
        
        let totalVoltage = 3.4;
        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.device_type == generalUtil.deviceTypes.temp_sensor.device_type) {

            // Update last response time of that temp sensor
            moduleModel.updateLastResponseTime(params.moduleId);

            // Update battery level of temp sensor
            await generalMetaModel.update(device.device_id, 'device', 'battery_level', parseInt((params.status / totalVoltage).toFixed(2)));

            if (device.is_active == 'n') {

                //logger.info('Temp Sensor Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }


            // log Battery level
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.temp_sensor.temp_battery,
                status_value: parseInt((params.status / totalVoltage).toFixed(2)).toString()
            });

        }else{
            logger.error('[INVALID MODULE] - registerTempSensorVoltageListener',params);
        }

    } else if (type == 'registerWaterDetectorVoltageListener') {

        /**
         * @param moduleId
         * @param status
         */

        let totalVoltage = 3.4;
        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.device_type == generalUtil.deviceTypes.water_detector.device_type) {

            // Update last response time of that temp sensor
            moduleModel.updateLastResponseTime(params.moduleId);

            // Update battery level of temp sensor
            await generalMetaModel.update(device.device_id, 'device', 'battery_level', parseInt((params.status / totalVoltage).toFixed(2)));

            if (device.is_active == 'n') {

                //logger.info('Water Sensor Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // log Battery level
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.water_detector.water_detector_battery,
                status_value: parseInt((params.status / totalVoltage).toFixed(2)).toString()
            });

        }

    } else if (type == 'registerGasSensorValueListener') {


        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.panel_device_id) {

            // Update last response time of that gas sensor
            moduleModel.updateLastResponseTime(params.moduleId);

            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.is_gas_detect
            }, function (err, response) { });


            if (device.is_active == 'n') {

                //logger.info('Gas Sensor Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Send Alert to user if gas is detected
            if (params.is_gas_detect == 1) {
                alertController.handleStatusChange({
                    device: device,
                    status: {
                        device_status: params.is_gas_detect
                    }
                }, (err, response) => {

                });
            }

            // Log Status
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.gas_sensor.gas_detected,
                status_value: params.is_gas_detect
            });

        }
    } else if (type == 'registerCo2SensorValueListener') {


        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device && device.panel_device_id) {

            // Update last response time of that gas sensor
            moduleModel.updateLastResponseTime(params.moduleId);

            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.is_co2_detect
            }, function (err, response) { });


            if (device.is_active == 'n') {

                //logger.info('Gas Sensor Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }

            // Send Alert to user if gas is detected
            if (params.is_co2_detect == 1) {
                alertController.handleStatusChange({
                    device: device,
                    status: {
                        device_status: params.is_co2_detect
                    }
                }, (err, response) => {

                });
            }

            // Log Status
            statusMappingModel.add({
                device_id: device.device_id,
                status_type: generalUtil.deviceStatusMappingTypes.co2_sensor.co2_detected,
                status_value: params.is_co2_detect
            });

        }
    } else if (type == 'registerRepeatorStatusListener') {

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (device) {

            // Update last response time of that repeater
            moduleModel.updateLastResponseTime(params.moduleId);

            if (device.is_active == 'n') {

                //logger.info('Repeater Module Marked Active', device);

                // Emit Real Time Socket To Change Module Status
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });

                // socketio.emit('changeModuleStatus', {
                //     module_id: device.module_id,
                //     is_active: 'y'
                // });
            }
        }

    } else if (type == 'registerSmartRemoteNumberListener') {

        /**
         * @param remote_number
         * @param status
         */

        let status = params.status;

        let meta = await generalMetaModel.getMetaByValue('mood', 'smart_remote_no', params.remote_number);
        if (meta) {
            await roomController.changeMoodStatus({
                authenticatedUser: {
                    user_id: "Smart Remote",
                    phone_id: "0000",
                    phone_type: "System"
                },
                mood_id: meta.table_id,
                mood_status: status
            }, () => { });
        }

    } else if (type == 'registerYaleLockListener') {

        logger.error('yale lock details received from serial', params);

        

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(1, params.moduleId);
        if (!device || !device.panel_device_id) {
            logger.error('[INVALID MODULE] - registerYaleLockListener',params);
            return;
        }

        statusMappingModel.add({
            device_id: device.device_id,
            status_type: 'yale_lock_data',
            status_value: JSON.stringify(params)
        });

        moduleModel.updateLastResponseTime(params.moduleId);
        eventsEmitter.emit('emitSocket', {
            topic: 'changeModuleStatus',
            data: {
                module_id: device.module_id,
                is_active: 'y'
            }
        });

        // socketio.emit('changeModuleStatus', {
        //     module_id: device.module_id,
        //     is_active: 'y'
        // });

        if (params.lockCommandType == 0) {

            let activity_type = 'app';

            if (params.subCommandType == 1) {
                //logger.info('YALE LOCK - UNLOCK - MASTER PIN CODE');
                activity_type = 'master_pin_code';
            } else if (params.subCommandType == 2) {
                // logger.info('YALE LOCK - UNLOCK - USER PIN CODE');
                activity_type = 'user_pin_code';
                // await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(device.device_id, 'device', 'onetime_code');
            } else if (params.subCommandType == 3) {
                logger.info('YALE LOCK - UNLOCK - ONE TIME PIN CODE');
                activity_type = 'onetime_pin_code';
                await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(device.device_id, 'device', 'onetime_code');
                
                eventsEmitter.emit('emitSocket', {
                    topic: 'oneTimeCodeUsedStatus',
                    data: {
                        device_id: device.device_id,
                        one_time_code_used: 'y'
                    }
                });
            } else if (params.subCommandType == 4) {
                //logger.info('YALE LOCK - UNLOCK - FINGERPRINT');
                activity_type = 'fingerprint';
            } else if (params.subCommandType == 5) {
                //logger.info('YALE LOCK - UNLOCK - KEY');
                activity_type = 'key';
            }

            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.lockCommandType,
                activity_type: activity_type
            }, function (err, response) { });

            alertController.handleStatusChange({
                device: device,
                activity_type: activity_type,
                status: {
                    device_status: params.lockCommandType
                }
            }, (err, response) => { });

            queueManager.removeFromQueue({
                device_id: device.device_id,
                device_status: params.lockCommandType
            });

        } else if (params.lockCommandType == 1) {

            let activity_type = 'app';

            if (params.subCommandType == 6) {
                //logger.info('YALE LOCK - LOCK - KEYPAD');
                activity_type = 'keypad';
            } else if (params.subCommandType == 7) {
                //logger.info('YALE LOCK - LOCK - AUTOLOCK');
                activity_type = 'autolock';
            } else if (params.subCommandType == 8) {
                //logger.info('YALE LOCK - LOCK - FINGER PRINT OR KEY');
                activity_type = 'fingerprint_or_key';
            }

            deviceController.handleDeviceStatusChangeListeners({
                device_id: device.device_id,
                device_status: params.lockCommandType,
                activity_type: activity_type
            });

            alertController.handleStatusChange({
                device: device,
                activity_type: activity_type,
                status: {
                    device_status: params.lockCommandType
                }
            }, (err, response) => { });

            queueManager.removeFromQueue({
                device_id: device.device_id,
                device_status: params.lockCommandType
            });

        } else if (params.lockCommandType == 9) {

            //logger.info('YALE LOCK - USER PINCODE SET ', params.subCommandType, params.otherdata);
            let userPinCodeMeta = await generalMetaModel.getMetaValue(device.device_id, 'user_pincode', "{}");
            userPinCodeMeta = JSON.parse(userPinCodeMeta);
            userPinCodeMeta[params.subCommandType] = params.otherdata.replace("F", "");
            await generalMetaModel.update(device.device_id, 'device', 'user_pincode', JSON.stringify(userPinCodeMeta));

        } else if (params.lockCommandType == 10) {
            //logger.info('YALE LOCK - ONE TIME PINCODE SET ', params.otherdata);
            await generalMetaModel.update(device.device_id, 'device', 'onetime_pincode', params.otherdata);
        } else if (params.lockCommandType == 11) {

            //logger.info('YALE LOCK - USER PINCODE DELETE ', params.subCommandType, params.otherdata);

            let userPinCodeMeta = await generalMetaModel.getMetaValue(device.device_id, 'user_pincode', "{}");
            userPinCodeMeta = JSON.parse(userPinCodeMeta);

            if (userPinCodeMeta[params.subCommandType] != null) {
                delete userPinCodeMeta[params.subCommandType];
                await generalMetaModel.update(device.device_id, 'device', 'user_pincode', JSON.stringify(userPinCodeMeta));
            }

        } else if (params.lockCommandType == 12) {
            //logger.info('YALE LOCK - ONE TIME PINCODE DELETE ', params.otherdata);
            await generalMetaModel.deleteByTableIdAndTableNameAndMetaName(device.device_id, "device", "onetime_pincode");
        }

        return;

        // var deviceList = params.deviceStatusDetails;
        // // handlePanelDeviceStatusChange(deviceList);

        // for (var i = 0; i < deviceList.length; i++) {

        //     var device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(deviceList[i].deviceId, deviceList[i].moduleId);
        //     if (!device) {
        //         return;
        //     }

        //     // for lock open close status
        //     if (deviceList[i].status == 1 || deviceList[i].status == 0) {

        //         alertController.handleStatusChange({
        //             device: device,
        //             status: {
        //                 device_status: deviceList[i].status
        //             }
        //         }, (err, response) => {

        //         });

        //         // only change it if its different
        //         if(device.device_status!=deviceList[i].status){
        //             deviceController.handleDeviceStatusChangeListeners({
        //                 device_id: device.device_id,
        //                 device_status: deviceList[i].status
        //             }, function (err, response) {});
        //         }

        //         // Remove from the queue that matches the device details
        //         queueManager.removeFromQueue({
        //             device_id: device.device_id,
        //             device_status: deviceList[i].status
        //         });

        //      // for door open close status
        //     }else if(deviceList[i].status == 2 || deviceList[i].status==3) {

        //         deviceController.handleDeviceStatusChangeListeners({
        //             device_id: device.device_id,
        //             device_sub_status: deviceList[i].status
        //         }, function (err, response) {});

        //     }


        //     // check if the module is active if not activate it


        // }

    } else if (type == 'registerPirDeviceListener') {

        logger.info('GENERAL EVENT LISTENER - [PIR DEVICE] - ', params);

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (!device) {
            return;
        }

        moduleModel.updateLastResponseTime(params.moduleId);
        eventsEmitter.emit('emitSocket', {
            topic: 'changeModuleStatus',
            data: {
                module_id: device.module_id,
                is_active: 'y'
            }
        });

        // socketio.emit('changeModuleStatus', {
        //     module_id: device.module_id,
        //     is_active: 'y'
        // });


        // Change Mode
        if (params.commandType == 0) {

            queueManager.removeFromQueue({
                command_type: "change-pir-device-mode",
                device_id: device.device_id,
                pir_mode: params.subCommandType
            });

        } else if (params.commandType == 1) {
            // Change Device Status 

            queueManager.removeFromQueue({
                command_type: "change-pir-device-status",
                device_id: device.device_id,
                device_status: params.subCommandType
            });

        } else if (params.commandType == 2) {
            // Change Device Status 

            queueManager.removeFromQueue({
                command_type: "change-pir-device-interval",
                device_id: device.device_id,
                pir_timer: params.subCommandType
            });

            await generalMetaModel.update(device.device_id, 'device', 'pir_timer', params.subCommandType);

        }

    } else if (type == 'registerPirDetectorListener') {

        logger.info('GENERAL EVENT LISTENER  - [PIR DETECTOR]',params);

        let device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(params.moduleId, params.moduleId);
        if (!device) {
            return;
        }

        moduleModel.updateLastResponseTime(params.moduleId);

        if (device.is_active == 'n') {
            eventsEmitter.emit('emitSocket', {
                topic: 'changeModuleStatus',
                data: {
                    module_id: device.module_id,
                    is_active: 'y'
                }
            });

            // socketio.emit('changeModuleStatus', {
            //     module_id: device.module_id,
            //     is_active: 'y'
            // });
        }

        let pirMood = await generalMetaModel.getMetaValue(device.device_id, 'mood_id', null);
        if (pirMood) {
            roomController.changeMoodStatus(
                {
                    authenticatedUser: {
                        user_id: `PIR Activity - ${device.device_name} `,
                        phone_id: "0000",
                        phone_type: "System"
                    },
                    mood_id: pirMood,
                    mood_status: params.commandType
                },()=>{}
            )
        }

    }
};

const updateStatusCache = (object, key) => {
    const {[key]: deleteKey, ...otherKeys} = object;
    return otherKeys;
}


/**
 * Privately Called By Listener 
 * @param deviceId
 * @param status
 * @param moduleId
 */
let handlePanelDeviceStatusChange = async function (deviceList, options = {}) {

    /**
     * Each device should have the following params
     * deviceId
     * status
     * moduleId
     */

    //  logger.info('General Event Listner called!!!');

    // const dbDeviceList = await deviceModel.geTBy

    for (let i = 0; i < deviceList.length; i++) {

        let device;
        if(options && options.type=='registerDeviceStatusACKListener'){
            device = queueManager.deviceData[deviceList[i].moduleId+"-"+deviceList[i].deviceId];
        }

        if(device==null){            
            device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(deviceList[i].deviceId, deviceList[i].moduleId);
        }
        
        if (i == 0) {
            console.log('Device :: ', device);
            moduleModel.updateLastResponseTime(deviceList[i].moduleId);
            if(device.is_active=='n'){
                eventsEmitter.emit('emitSocket', {
                    topic: 'changeModuleStatus',
                    data: {
                        module_id: device.module_id,
                        is_active: 'y'
                    }
                });
            }
          
            // socketio.emit('changeModuleStatus', {
            //     module_id: device.module_id,
            //     is_active: 'y'
            // });
        }

        let updateStatus = {
            preDevice: device,
            device_id: device.device_id,
            device_status: deviceList[i].status
        };

        // Remove from the queue that matches the device details

        const queueCommand = { ...queueManager.commandQueue[device.device_id] };

        queueManager.removeFromQueue({
            device_id: device.device_id,
            device_status: deviceList[i].status
        });


        // Check if Module Type if 5f and device is fan and device status is not 0 or 1 
        if (["5f","5f-wifi"].includes(device.module_type) && deviceList[i].deviceId == 1 && (deviceList[i].status != 0 && deviceList[i].status != 1)) {

            
            updateStatus.device_sub_status = deviceList[i].status.toString();
            updateStatus.device_status = "1";

            cacheManager.actualDeviceStatusList[updateStatus.device_id] = updateStatusCache({...updateStatus}, 'preDevice');


            // Process the change if necessary (uncomment if not working)
            // if (device.device_status != deviceList[i].status || device.device_sub_status != updateStatus.device_sub_status) {
                
            //     deviceController.handleDeviceStatusChangeListeners(updateStatus);
            // }

            if (device.device_status != '1' || device.device_sub_status != updateStatus.device_sub_status) {
                if (queueCommand != null && queueCommand.device_status == deviceList[i].status) {
                    logger.info('General Event Listener | Fan Status Change | Condition 1', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else if (queueCommand == null || queueCommand == undefined) {
                    logger.info('General Event Listener | Fan Status Change | Condition 2', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else if (options.type!='registerDeviceStatusACKListener') {
                    logger.info('General Event Listener | Fan Status Change | Condition 3', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else{
                    logger.info('General Event Listener | Fan Status Change | No condition', deviceList[i]);
                }
            }else{
                logger.info('General Event Listener | Fan Status Change | Condition 5 (No Condition)', deviceList[i]);
            }



        } else {


            // logger.info('update Status',)

            cacheManager.actualDeviceStatusList[updateStatus.device_id] = updateStatusCache({...updateStatus}, 'preDevice');
            // logger.info('Update Status Cache : ', cacheManager.actualDeviceStatusList);

            // Process the change is necessary
            if (device.device_status != deviceList[i].status) {

                if (queueCommand != null && queueCommand.device_status == deviceList[i].status) {
                    logger.info('General Event Listener | Other Status Change | Condition 1', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else if (queueCommand === null || queueCommand === undefined) {
                    logger.info('General Event Listener | Other Status Change | Condition 2', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else if (options.type != 'registerDeviceStatusACKListener') {
                    logger.info('General Event Listener | Other Status Change | Condition 3', deviceList[i]);
                    deviceController.handleDeviceStatusChangeListeners(updateStatus);
                } else {
                    logger.info('General Event Listener | Other Status Change | Condition 4', deviceList[i]);
                }

            }else{
                logger.info('General Event Listener | Other Status Change | Condition 5 (No Condition)', deviceList[i]);
            }
        }

        // Physical Change Detected
        if (options.type == "registerPhysicalDeviceListener") {

            logModel.add({
                log_object_id: device.device_id,
                log_type: deviceList[i].status == '0' ? "device_off" : "device_on"
            }, {
                phone_id: "0000",
                phone_type: "SpikeBot",
                user_id: 'Physically'
            });

        }

    }

}


// Other Listeners of the System
cc2530Util.registerDeviceListenerCallBack(function (response) {
    onDeviceStatusListener(response, "registerDeviceListenerCallBack");
});

cc2530Util.registerPhysicalDeviceListener(function (response) {
    onDeviceStatusListener(response, "registerPhysicalDeviceListener");
});

cc2530Util.registerSwitchModuleBootDeviceListener(function (response) {
    onDeviceStatusListener(response, "registerSwitchModuleBootDeviceListener");
});

cc2530Util.registerDeviceStatusACKListener(function (response) {
    onDeviceStatusListener(response, "registerDeviceStatusACKListener");
});

cc2530Util.registerStartupDeviceStatusListener(function (response) {
    onDeviceStatusListener(response, "registerStartupDeviceStatusListener");
});

cc2530Util.registerSwitchModuleStatusListener(function (response) {
    onDeviceStatusListener(response, "registerSwitchModuleStatusListener");
});

// Temperature Status and Voltage Listener
cc2530Util.registerTempSensorStatusListener(function (response) {
    onDeviceStatusListener(response, "registerTempSensorStatusListener");
});

cc2530Util.registerTempSensorVoltageListener(function (response) {
    onDeviceStatusListener(response, "registerTempSensorVoltageListener");
});

// Door Sensor Status and Voltage Listener
cc2530Util.registerDoorSensorStatusListener(function (response) {
    onDeviceStatusListener(response, "registerDoorSensorStatusListener");
});

cc2530Util.registerWaterDetectorStatusListener(function (response) {
    onDeviceStatusListener(response, "registerWaterDetectorStatusListener");
});

cc2530Util.registerDoorSensorVoltageListener(function (response) {
    onDeviceStatusListener(response, "registerDoorSensorVoltageListener");
});

cc2530Util.registerWaterDetectorVoltageListener(function (response) {
    onDeviceStatusListener(response, "registerWaterDetectorVoltageListener");
});

cc2530Util.registerMainsVoltageListener(function (response) {
    onDeviceStatusListener(response, "registerMainsVoltageListener");
});

cc2530Util.registerSmartRemoteNumberListener(function (response) {
    onDeviceStatusListener(response, "registerSmartRemoteNumberListener");
});

cc2530Util.registerGasSensorValueListener(function (response) {
    onDeviceStatusListener(response, "registerGasSensorValueListener");
});

cc2530Util.registerheavyLoadValueListener(function (response) {
    onDeviceStatusListener(response, "registerheavyLoadValueListener");
});

cc2530Util.registerRepeatorStatusListener(function (response) {
    onDeviceStatusListener(response, "registerRepeatorStatusListener");
});

cc2530Util.registerYaleLockListener(function (response) {
    onDeviceStatusListener(response, "registerYaleLockListener");
});

cc2530Util.registerCurtainAcknowledgement(function (response) {
    onDeviceStatusListener(response, "registerCurtainAcknowledgement");
});

cc2530Util.registerPirDeviceListener(function (response) {
    onDeviceStatusListener(response, "registerPirDeviceListener");
});

cc2530Util.registerPirDetectorListener(function (response) {
    onDeviceStatusListener(response, "registerPirDetectorListener");
});

/** Received 5f data change */
eventsEmitter.on('/ESP32/5fACK',function(data){

    console.log('DATA RECEIVED FROM MQTT',data);

    const moduleId = data.substring(2,18);
    console.log('MODULE ID RECEIVED BY MQTT',moduleId);

    const commandTypeCode = data.substring(18,20);
    console.log('COMMAND TYPE IS ',commandTypeCode);

    const dataLength = data.substring(20,22);
    console.log('DATA LENGTH IS ',dataLength);

    const deviceStatusString = data.substring(22).match(/.{4}/g);

    let deviceStatusDetails = [];
    for(let deviceStatus of deviceStatusString){
        deviceStatusDetails.push({
            deviceId:parseInt(deviceStatus.substring(0,2)),
            status:parseInt(deviceStatus.substring(2,4)),
            moduleId:moduleId
        })
    }

    let commandType ='registerDeviceStatusACKListener';
    if(commandTypeCode=='06'){
        commandType = 'registerPhysicalDeviceListener';
    } else if (commandTypeCode=='0B'){
        commandType = 'registerSwitchModuleStatusListener';
    }

    onDeviceStatusListener({
        "moduleId":moduleId,
        "deviceStatusDetails":deviceStatusDetails
    }, commandType);



});