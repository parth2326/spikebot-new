process.env.PROJECT_PATH = __dirname;
try {
    process.chdir(process.env.PROJECT_PATH);
    process.env.PROJECT_PATH = process.cwd();
    //console.log('Changed directory to: ' + process.cwd());
} catch (error) {
    console.log('Error wile changing directory :', error);
}

const express = require('express'),
    app = express(),
    cors = require('cors'),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server, { origins: '*:*' }),
    // session = require('express-session'),
    bodyParser = require('body-parser'),
    // cookieParser = require('cookie-parser'),
    // cookieSession = require('cookie-session'),
    dotenv = require('dotenv').config(),
    path = require('path'),
    fs = require('fs'),
    ip = require('ip'),
    moment = require('moment'),
    __ = require("underscore"),
    // clientSocket = require('socket.io-client'),
    // helmet = require('helmet'),
    exec = require('child_process').exec,
    finder = require('fs-finder'),
    network = require('network'),
    CronJobManager = require('cron-job-manager'),
    shortid = require('shortid');

// sqlite3 = require('sqlite3').verbose(),
// database = new sqlite3.Database('spikeBotDB.db', () => {
//   database.run('PRAGMA foreign_keys=on');
//   database.get('PRAGMA foreign_keys', function(err, res) {
//       console.log('pragma res is ' + res.foreign_keys);
//   });
// });


let database;

let dbManager = require('./util/db-manager');
dbManager.open(process.env.PROJECT_PATH + '/spikeBotDB.db').then(function (result) {
    database = result;
});
// database = dbManager.db;

let mailUtil = require('./util/mail-util.js');
let constants = require('./util/constants.js');
logger = require('./util/logger.js');


deleteVideo = require('./scanner/deleteVideos');
deleteVideo.init();

//logger.info("-------- Starting Automation Application --------  ", new Date());

app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'views/admin')]);
app.set('view engine', 'ejs');

// app.use(helmet());
// app.use(cookieParser('keyboard cat'));
app.use(cors());

// const rateLimit = require("express-rate-limit");
// const limiter = rateLimit({
//     windowMs: 1000,
//     max: 10,
//     message: {
//         code: 500,
//         message: "You have exceeded api request limit."
//     }
// });

// app.use(limiter);

app.use((err, req, res, next) => {
    if (!err) {
        return next();
    }

    res.json({
        code: 500,
        message: "Internal Server Error"
    });

});

app.use(bodyParser.json({
    limit: 5242880
}));

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/static', express.static(__dirname + '/static'));
// app.use('/static/images', express.static(__dirname + '/static/images'));
// app.use("/node_modules", express.static(__dirname + '/node_modules'));

// app.use(session({
//     secret: 'keyboard cat',
//     resave: true,
//     saveUninitialized: true
// }));

//for Browser request
app.get('/', function (req, res) {
    //logger.info('In / request');
    let ipAddress = ip.address();
    let FIND_LOCAL_IP = "SELECT count(0) as data FROM mst_home_controller_list WHERE home_controller_ip = '" + ipAddress + "'";
    database.all(FIND_LOCAL_IP, function (err, result) {
        if (err) {
            logger.error("FOR BROWSER REQUEST ", err);
        } else {
            if (result[0].data == 0 || result[0].data == null) {
                res.redirect('/signup');
            } else {
                res.redirect('/profile');
            }
        }
    });
});

// var serverConfig = require('./config/server-config.js');
let appContext = {};
// appContext.serverConfig = serverConfig;
appContext.constants = constants;

shared = {};
shared.util = {};
shared.util.socket_io = io;
// shared.util.clientSocket = clientSocket;
shared.util.moment = moment;
shared.util.fs = fs;
shared.util.finder = finder;
shared.util.exec = exec;
shared.util.CronJobManager = CronJobManager;
shared.util.network = network;

shared.db = {};

//Device Information
let host = ip.address();
let viewerDetails = require('./config/viewer-config.js')();
server.listen(80, function () {
    // var port = server.address().port;
    // serverConfig.localIP = host;
    // serverConfig.localPort = port;

    logger.info("SPIKEBOT SERVER IS RUNNING AT http://" + host + ":" + '80');

    let vpn_details = require('/camera/vpnport.json');
    //logger.info('vpn_details', vpn_details);
    constants.profile_vpn_port = vpn_details.profile_vpn_port;
    constants.camera_vpn_port = vpn_details.camera_vpn_port;

    viewerDetails.init(function (error, result) {
        if (error) {
            logger.error("Error while initialising viewer details. Error: ", error);
        } else {
            viewerDetails.deviceIP = host;
            viewerDetails.deviceID = result;
            //logger.info("Viewer details initialized successsfully ", viewerDetails.deviceID);

            dbManager.executeNonQuery('UPDATE mst_home_controller_list SET home_controller_ip=?, is_sync=0', [viewerDetails.deviceIP]);

            appContext.viewerDetails = viewerDetails;
            appContext.app = app;
            shared.db.sqliteDB = database;

            if (constants.userId == null || constants.userId == "" || constants.userEmail == "" || constants.userEmail == null) {
                let finduserdata = database.prepare(constants.GET_USER_DATA);
                finduserdata.all(viewerDetails.deviceID, function (error, result) {
                    if (error) {
                        logger.error("User Details Not Found!");
                        return 0;
                    } else {
                        if (result.length > 0) {
                            constants.userEmail = result[0].user_email;
                            constants.userId = result[0].user_id;
                            appContext.userId = result[0].user_id;
                        } else {
                            constants.userEmail = null;
                            constants.userId = null;
                            appContext.userId = null;
                        }
                    }
                });
            } else {
                console.log("constants ", constants.userId, constants.userEmail);
            }

            require('./routes/module.js')(appContext, shared);
        }
    });
});

//For Local Only
var on = 1;
fs.exists('/sys/class/gpio/gpio18', function (exist) {
    if (exist) fs.writeFile('/sys/class/gpio/unexport', 18, function (err) {
        if (err) return logger.error('exist gpio ', err);
    });
    __.defer(function () {
        fs.writeFile('/sys/class/gpio/export', 18, function (err) {
            if (err) return logger.error('read gpio ', err);
            fs.writeFile('/sys/class/gpio/gpio18/direction', 'out', function (err) {
                if (err) return logger.error('set gpio18 ', err);
                //logger.info('Direction gpio 18 was set');
                fs.writeFile('/sys/class/gpio/gpio18/value', on, function (err) {
                    if (err) logger.error('write gpio18 ', err);
                    //logger.info('Starting App Running indication on GPIO');
                    setInterval(function () {
                        on = (on + 1) % 2;
                        fs.writeFile('/sys/class/gpio/gpio18/value', on, function (err) {
                            if (err) logger.error('write gpio18 ', err);
                        });
                    }, 800);
                });
            });
        });
    });
});

var on = 1;
fs.exists('/sys/class/gpio/gpio13', function (exist) {
    if (exist) fs.writeFile('/sys/class/gpio/unexport', 13, function (err) {
        if (err) return logger.error('exist gpio ', err);
    });
    __.defer(function () {
        fs.writeFile('/sys/class/gpio/export', 13, function (err) {
            if (err) return logger.error('read gpio ', err);
            fs.writeFile('/sys/class/gpio/gpio13/direction', 'out', function (err) {
                if (err) return logger.error('set gpio13 ', err);
                //logger.info('Direction gpio 13 was set');
                fs.writeFile('/sys/class/gpio/gpio13/value', on, function (err) {
                    if (err) logger.error('write gpio18 ', err);
                });
            });
        });
    });
});


process.on('unhandledRejection', error => {

    // console.log('unhandledRejection From Main File :', error.message);
    logger.error('unhandledRejection From Main File :', error);
    // dbManager.open('/home/pi/node/homeController/spikeBotDB.db');
    // Will print "unhandledRejection err is not defined"
    // console.log('unhandledRejection', error.message);
});


process.on("uncaughtException", function (err) {

    let exception_msg = err.message,
        exception_stack = err.stack;

    // logger.error('APP CRASHED - UNCAUGHT EXCEPTION ', err);

    if (exception_msg.toString().includes('locked')) {
        dbManager.close();
        dbManager.open(process.env.PROJECT_PATH + '/spikeBotDB.db');
        logger.error('DB REINITIATED ');
    }

    logger.error('[UNCAUGHT EXCEPTION] ', exception_msg);

});