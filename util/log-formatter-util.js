/**
 * This utility is create to format message of each logs. based on log type its message is returnedd.
 */
_logFormatter = {};

_logFormatter.formats = {
    'mood_add': '[[username]] added mood',
    'mood_update': '[[username]] updated mood',
    'mood_delete': '[[username]] deleted mood',
    'mood_on': '[[username]] turned mood on ',
    'mood_off': '[[username]] turned mood off',
    'room_add': '[[username]] added room',
    'room_update': '[[username]] updated room',
    'room_delete': '[[username]] deleted room',
    'room_on': '[[username]] turned room on',
    'room_off': '[[username]] turned room off',
    'schedule_add': '[[username]] added schedule',
    'schedule_update': '[[username]] updated schedule',
    'schedule_delete': '[[username]] deleted schedule',
    'schedule_on': '[[d1]] Schedule turned on ',
    'schedule_off': '[[d1]] Schedule turned off ',
    'schedule_enable': '[[username]] enabled schedule',
    'schedule_disable': '[[username]] disabled schedule',
    'alert_add': '[[username]] added alert on [[d1]] sensor',
    'alert_update': '[[username]] updated alert on [[d1]] sensor',
    'alert_delete': '[[username]] deleted alert on [[d1]] sensor',
    'alert_active': 'Alert on [[d1]] sensor activated',
    'alert_enable': '[[username]] enabled alert on [[d1]] sensor',
    'alert_disable': '[[username]] disabled alert on [[d1]] sensor',
    'device_add': '[[username]] added device',
    'device_update': '[[username]] updated device',
    'device_delete': '[[username]] deleted device',
    'device_on': ' [[username]] turned on',
    'device_off': '[[username]] turned off',
    'door_open': 'Door opened',
    'door_close': 'Door closed',
    'gas_detected': 'Gas detected',
    'temp_alert': '[[d1]] Temperature alert',
    'panel_add': '[[username]] added panel',
    'panel_update': '[[username]] updated panel',
    'panel_delete': '[[username]] deleted panel',
    'panel_on': '[[username]] turned on panel',
    'panel_off': '[[username]] turned off panel',
    'child_user_add': '[[username]] added child user',
    'child_user_update': '[[username]] updated child user',
    'child_user_delete': '[[username]] deleted child user',
    'camera_add': '[[username]] added camera',
    'camera_update': '[[username]] updated camera',
    'camera_delete': '[[username]] deleted camera',
    'camera_person_detected': 'Person detected',
    'camera_active': 'Camera is active',
    'camera_inactive': 'Camera is inactive',
    'water_detected': '[[d1]] detected water',
    'curtain_open': '[[username]] opened curtain',
    'curtain_close': '[[username]] closed curtain',
    'door_lock': {
        'keypad': '[[d1]] was locked using keypad',
        'autolock': '[[d1]] was locked by autolock feature',
        'fingerprint_or_key': '[[d1]] was locked by finger print or key',
        'default': '[[username]] locked [[d1]]',
    },
    'door_unlock': {
        'master_pin_code': '[[d1]] was unlocked using Master password',
        'user_pin_code': '[[d1]] was unlocked using Admin password',
        'onetime_pin_code': '[[d1]] was unlocked using Guest password',
        'fingerprint': '[[d1]] was unlocked using finger print',
        'key': '[[d1]] was unlocked using key',
        'default': '[[username]] unlocked [[d1]]',
    },
    'home_controller_active': 'Home Controller connected',
    'home_controller_inactive': 'Home Controller disconnected',
    'password_change': '[[log_sub_type]] password changed',
    'jetson_inactive': 'Jetson is inactive',
    'jetson_active': 'Jetson is active'
} 

// _logFormatter.formats = {
//     'mood_add': '[[username]] added [[d1]] mood',
//     'mood_update': '[[username]] updated [[d1]] mood',
//     'mood_delete': '[[username]] deleted [[d1]] mood',
//     'mood_on': '[[username]] turned [[d1]] mood on ',
//     'mood_off': '[[username]] turned [[d1]] mood off',
//     'room_add': '[[username]] added [[d1]] room',
//     'room_update': '[[username]] updated [[d1]] room',
//     'room_delete': '[[username]] deleted [[d1]] room',
//     'room_on': '[[username]] turned [[d1]] room on',
//     'room_off': '[[username]] turned [[d1]] room off',
//     'schedule_add': '[[username]] added [[d1]] schedule',
//     'schedule_update': '[[username]] updated [[d1]] schedule',
//     'schedule_delete': '[[username]] deleted [[d1]] schedule',
//     'schedule_on': '[[d1]] Schedule turned on ',
//     'schedule_off': '[[d1]] Schedule turned off ',
//     'schedule_enable': '[[username]] enabled [[d1]] schedule',
//     'schedule_disable': '[[username]] disabled [[d1]] schedule',
//     'alert_add': '[[username]] added alert on [[d1]] sensor',
//     'alert_update': '[[username]] updated alert on [[d1]] sensor',
//     'alert_delete': '[[username]] deleted alert on [[d1]] sensor',
//     'alert_active': 'Alert on [[d1]] sensor activated',
//     'alert_enable': '[[username]] enabled alert on [[d1]] sensor',
//     'alert_disable': '[[username]] disabled alert on [[d1]] sensor',
//     'device_add': '[[username]] added device',
//     'device_update': '[[username]] updated device',
//     'device_delete': '[[username]] deleted device',
//     'device_on': ' [[username]] turned on',
//     'device_off': '[[username]] turned off',
//     'door_open': '[[d1]] Door opened',
//     'door_close': '[[d1]] Door closed',
//     'gas_detected': '[[d1]] Sensor detected gas ',
//     'temp_alert': '[[d1]] Temperature alert',
//     'panel_add': '[[username]] added panel [[d1]] of room [[d2]]',
//     'panel_update': '[[username]] updated [[d1]] panel in [[d2]] room',
//     'panel_delete': '[[username]] deleted [[d1]] panel from [[d2]] room',
//     'panel_on': '[[username]] turned [[d1]] panel on in [[d2]] room',
//     'panel_off': '[[username]] turned [[d1]] panel off in [[d2]] room',
//     'child_user_add': '[[username]] added child user [[d2]]',
//     'child_user_update': '[[username]] updated [[d2]] child user',
//     'child_user_delete': '[[username]] deleted [[d2]] child user',
//     'camera_add': '[[username]] added [[d1]] camera',
//     'camera_update': '[[username]] updated [[d1]] camera',
//     'camera_delete': '[[username]] deleted [[d1]] camera',
//     'camera_person_detected': 'Person detected on camera [[d1]]',
//     'camera_active': 'Camera [[d1]] is active',
//     'camera_inactive': 'Camera [[d1]] is inactive',
//     'water_detected': '[[d1]] detected water',
//     'curtain_open': '[[username]] opened [[d1]]',
//     'curtain_close': '[[username]] closed [[d1]]',
//     'door_lock': {
//         'keypad': '[[d1]] was locked using keypad',
//         'autolock': '[[d1]] was locked by autolock feature',
//         'fingerprint_or_key': '[[d1]] was locked by finger print or key',
//         'default': '[[username]] locked [[d1]]',
//     },
//     'door_unlock': {
//         'master_pin_code': '[[d1]] was unlocked using master pin code',
//         'user_pin_code': '[[d1]] was unlocked using user pin code',
//         'onetime_pin_code': '[[d1]] was unlocked using one time pin code',
//         'fingerprint': '[[d1]] was unlocked using finger print',
//         'key': '[[d1]] was unlocked using key',
//         'default': '[[username]] unlocked [[d1]]',
//     },
//     'home_controller_active': 'Home Controller connected',
//     'home_controller_inactive': 'Home Controller disconnected'
// }

_logFormatter.formatList = function(logList) {

    for (log of logList) {

        if (typeof _logFormatter.formats[log.log_type] === 'string' || _logFormatter.formats[log.log_type] instanceof String) {
            log.message = _logFormatter.formats[log.log_type];
        } else {
            log.message = _logFormatter.formats[log.log_type][log.user_id] != null ? _logFormatter.formats[log.log_type][log.user_id] : _logFormatter.formats[log.log_type]['default'];
        }

        let description = log.activity_description ? log.activity_description.split('|') : [];
        // log.message = _logFormatter.formats[log.log_type];
        log.message = log.message.replace("[[username]]", log.user_name ? log.user_name : (log.user_id ? log.user_id : "System"));
        log.message = log.message.replace("[[d1]]", description[0] != null ? description[0].trim() : '');
        log.message = log.message.replace("[[d2]]", description[1] != null ? description[1].trim() : '');
        log.message = log.message.replace("[[d3]]", description[2] != null ? description[2].trim() : '');
        log.message = log.message.trim();

        if (['schedule', 'mood', 'room'].includes(log.activity_type)) {
            log.activity_description = log.activity_type.charAt(0).toUpperCase() + log.activity_type.slice(1) + ": " + log.activity_description;
        }else if(['onetime_pin_code','user_pin_code'].includes(log.log_sub_type)){
            log.message = log.message.replace('[[log_sub_type]]',log.log_sub_type=='user_pin_code'?'Admin':'One time');
        }

        if(log.sub_description){
            log.message=log.sub_description;
        }

    }

    return logList;
}

module.exports = _logFormatter;