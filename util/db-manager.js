const sqlite3 = require('sqlite3').verbose();

let db;
let readerDatabase;

exports.db = db;
exports.readerDatabase = readerDatabase;

exports.open = function (path) {
    return new Promise(function (resolve) {
        this.db = new sqlite3.Database(path,
            (err) => {
                if (err) reject("Open error: " + err.message)
                else {
                    // this.db.run('PRAGMA foreign_keys=on');
                    this.db.get("PRAGMA foreign_keys = ON");
                    this.db.get("PRAGMA journal_mode = WAL");
                    // this.db.get("PRAGMA read_uncommitted = 1");
                    // this.db.get("PRAGMA CACHE_SIZE = 10000");
                    // this.db.get("PRAGMA SYNCHRONOUS = 0");
                    this.db.get("PRAGMA TEMP_STORE = 2");
                    this.db.get("PRAGMA PAGE_SIZE = 10000");
                    // this.db.get("PRAGMA main.locking_mode=NORMAL ");
                   
                    
                    // this.db.run('PRAGMA busy_timeout = 50');
                    // this.db.configure("busyTimeout", 50);
                    resolve(this.db);
                }
            }
        )
    })
}

// any query: insert/delete/update
exports.run = function (query) {
    return new Promise(function (resolve, reject) {
        this.db.run(query,
            function (err) {
                if (err) reject(err.message)
                else resolve(true)
            })
    })
}

// any query: insert/delete/update
exports.executeNonQuery = function (query, params) {
    
    return new Promise(function (resolve, reject) {
        let stmt = this.db.prepare(query);
        stmt.run(params,
            function (err) {
                stmt.finalize();
                if (err) {
                    reject(err.message)
                } else {
                   
                    resolve(true);
                }
            });

    })
}


exports.getQuestionMarks = function (length) {

    let questionsMarks = [];
    for (let i = 0; i < length; i++) {
        questionsMarks.push('?');
    }
    return questionsMarks.join(",");
}

// any query: insert/delete/update
exports.executeInsert = function (table_name, params) {

    questionMarksLenth = Object.keys(params).length;
    questionsMarks = [];
    for (let i = 0; i < questionMarksLenth; i++) {
        questionsMarks.push('?');
    }

    return new Promise(function (resolve, reject) {
        let stmt = this.db.prepare(`INSERT INTO ${table_name} (${Object.keys(params).join()}) VALUES (${questionsMarks.join()})`);
        stmt.run(Object.values(params),
            function (err) {
                stmt.finalize();
                if (err) {
                    logger.error('SQlite Error', err);
                    reject(err.message)
                } else {
                  
                    resolve(true);
                }
            })
    });
}


// first row read
exports.get = function (query, params) {
    return new Promise(function (resolve, reject) {

        try {
            let preparedQuery = this.db.prepare(query);
            preparedQuery.get(params, function (err, row) {
                preparedQuery.finalize();
                if (err) {
                    logger.error('SQlite Error', err);
                    reject(null);
                }
                else {
                    resolve(row)
                }
            })
        } catch (error) {
            logger.error('SQlite Error', error);
            reject(null);
        }
    })
}

// set of rows read
exports.all = function (query, params) {
  
  

    return new Promise(function (resolve, reject) {
        if (params == undefined) params = []

        try {

            let userlist = this.db.prepare(query);
            userlist.all(params, function (err, rows) {
                userlist.finalize();
                if (err) {
                    logger.error('SQlite Error', err);
                    reject(null)
                } else {
                   
                    resolve(rows)
                }
            });

        } catch (error) {
            logger.error('SQlite Error', error);
            reject(null);
        }


    });
}

// each row returned one by one 
exports.each = function (query, params, action) {
    return new Promise(function (resolve, reject) {
        let db = this.db
        db.serialize(function () {
            db.each(query, params, function (err, row) {
                if (err) reject("Read error: " + err.message)
                else {
                    if (row) {
                        action(row)
                    }
                }
            })
            db.get("", function (err, row) {
                resolve(true)
            })
        })
    })
}

exports.close = function () {
    return new Promise(function (resolve, reject) {
        this.db.close()
        resolve(true)
    })
}

exports.generateUpdateQuery = function (obj = {}) {
    let s = '';
    for ([key, value] of Object.entries(obj)) {
        s += `${key}=`;
        s += typeof value === 'string' ? "'" : "";
        s += value
        s += typeof value === 'string' ? "'" : "";
        s += ",";
    }
    return s.substr(0, s.length - 1);
}

/**
 * @param {*} table_name 
 * @param {*} params (update Query)
 * @param {*} whereQuery 
 */
exports.executeUpdate = function (table_name, params, whereQuery) {
    //logger.info('params', params);
    let updateParams = Object.values(params);
    updateParams.push(...Object.values(whereQuery));
    //logger.info('UPDATE QUERY FIRED ', `UPDATE ${table_name} SET ${Object.keys(params).join('=?, ')+"=?"} WHERE ${Object.keys(whereQuery).join('=?, ')+"=?"} `);
    return new Promise(function (resolve, reject) {
        let stmt = this.db.prepare(`UPDATE ${table_name} SET ${Object.keys(params).join('=?, ')+"=?"} WHERE ${Object.keys(whereQuery).join('=?, ')+"=?"} `);
        stmt.run(updateParams,
            function (err) {
                stmt.finalize();
                if (err) {
                    logger.error('SQlite Error', err);
                    reject(err.message)
                } else {
                    resolve(true);
                }
            })
    });
}