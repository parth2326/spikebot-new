const request = require('request');
const fs = require('fs');

exports.uploadDbBackup = function (home_controller_device_id) {
    //logger.info('Home Controller Device Id : ', home_controller_device_id);
    //logger.info('File Path',process.env.PROJECT_PATH + '/spikeBotDB.db');
    const formData = {
        home_controller_device_id : home_controller_device_id,
        file: fs.createReadStream(process.env.PROJECT_PATH + '/spikeBotDB.db')
    };
    
    request.post({url: `${process.env.CLOUD_URL}:${process.env.CLOUD_PORT}/home-controller/db-backup`, formData: formData}, function (err, httpResponse, body) {
        if (err) {
            return console.error('Upload Failed: ', err)
        }    
        console.log('Upload successful! Server responded with :', body);
    });
}
