// this file is basically created to listen to zigbee events on serial port.
// there are various events that points to various functions in code.

let SerialPort = require('serialport'),
    cc2531Constants = require('./cc2531Constants'),

    readRespArray = [],
    tempCurrentResp = "",
    isCommandStarted = true,
    currentBufferData = null,
    isDis = true,
    serialPort,

    commandDataArray = [],
    ifJustConnected = false,
    isCommandProcessing = false;

let init = function() {
    serialPort = new SerialPort("/dev/ttyAMA0", {
        baudRate: 115200,
        buffersize: 1024
    });

    serialPort.on('open', function(err) {
        isDis = false;
        commandDataArray = [];
        tempCurrentResp = "";
        isCommandStarted = true;
        ifJustConnected = true;
        if (err) {
            return logger.error("Serial Port Open " + err);
        } else {
            //logger.info('Serial port opened successfully', new Date());
        }
    });

    // this is the function callled when any data is received in serial port of pi.

    serialPort.on('data', function(data) {
        logger.info("DATA RECV - [SERIAL PORT IO] - ", data);
        isDis = false;
        let tempCurrentDataStingHex = data.toString('hex');
        let tempCurrentDataSting = tempCurrentDataStingHex.toUpperCase();
        commandDataArray.push(tempCurrentDataSting);

        if (isCommandProcessing) {
            logger.warn('Return as isCommandProcessing');
            return;
        }
        if (commandDataArray.length === 0) {
            logger.warn('Return as commandDataArray');
            if (tempCurrentResp.length !== 0) {
                proocessCommand(tempCurrentResp);
                //  //logger.info('Forwared for proocessCommand 1');
            }
        } else {
            let cmd = commandDataArray.shift();
            tempCurrentResp = tempCurrentResp + cmd;
            proocessCommand(tempCurrentResp);
            // //logger.info('Forwared for proocessCommand 2');
        }
    });

    serialPort.on('close', function(error) {
        isDis = true;
        //logger.info('========== Closed serialPort ==========', error);
    });

    serialPort.on('error', function(error) {
        isDis = true;
        //logger.info('Serial Port Error ', error);
    });
};

setTimeout(function() {
    init();
}, 300);

let proocessCommand = function() {
    isCommandProcessing = true;
    let deepFlag;

    if (ifJustConnected) {
        //   //logger.info("ifJustConnected: tempCurrentResp length " + tempCurrentResp.length);
        if (tempCurrentResp.length < 2) {
            tempCurrentResp = "";
            ifJustConnected = false;
            isCommandProcessing = false;
            //    logger.error("Invalid length of command ifJustConnected :", deepFlag);
            return;
        }

        // check if the first two characters are DF then only allow to process ahead.
        deepFlag = tempCurrentResp.charAt(0) + tempCurrentResp.charAt(1);
        if (deepFlag !== "DF") {
            tempCurrentResp = "";
            isCommandStarted = true;
            ifJustConnected = false;
            isCommandProcessing = false;
            //   logger.error("DF Invalid start of command ifJustConnected :", deepFlag);
            return;
        }
    }

    if (isCommandStarted) {
        //  //logger.info("isCommandStarted: tempCurrentResp length " + tempCurrentResp.length);
        if (tempCurrentResp.length < 2) {
            logger.error("Invalid start of command ", tempCurrentResp);
            tempCurrentResp = "";
            isCommandStarted = true;
        } else {
            deepFlag = tempCurrentResp.charAt(0) + tempCurrentResp.charAt(1);
            if (deepFlag !== "DF") {
                tempCurrentResp = "";
                isCommandStarted = true;
                //     logger.error("DF Invalid start of command ", deepFlag);
            } else if (tempCurrentResp.length >= 22) {
                length = convertHexStringToInteger(tempCurrentResp.substring(20, 22));
                //   //logger.info("substring length", tempCurrentResp.substring(20, 22));
                if (length === 0) {
                    let expectedLength22 = 20 + 2;
                    if (tempCurrentResp.length === expectedLength22) {
                        readRespArray.push(tempCurrentResp);
                        readRespString();
                        tempCurrentResp = "";
                        isCommandStarted = true;
                        //    //logger.info('Found complete command ', tempCurrentResp);

                    } else if (tempCurrentResp.length > expectedLength22) {
                        let parsedCommand = tempCurrentResp.substring(0, expectedLength22);
                        readRespArray.push(parsedCommand);
                        //readRespString();
                        let extraString = tempCurrentResp.substring(expectedLength22, tempCurrentResp.length);
                        if (extraString.length < 2) {
                            //     logger.warn("Invalid start of command ", extraString);

                            tempCurrentResp = "";
                            isCommandStarted = true;
                        } else {
                            let deepFlag2 = extraString.charAt(0) + extraString.charAt(1);
                            if (deepFlag2 !== "DF") {
                                isCommandStarted = true;
                                tempCurrentResp = "";
                                //            logger.error("DF Invalid start of command ", deepFlag2);

                            } else {
                                isCommandStarted = false;
                                tempCurrentResp = extraString;
                            }
                        }
                    } else {
                        isCommandStarted = false;
                    }
                } else {
                    let expectedLength2 = (20 + 2 + (length * 2));
                    let expectedLength3 = 34;
                    if (tempCurrentResp.length === expectedLength2) {
                        readRespArray.push(tempCurrentResp);
                        readRespString();
                        isCommandStarted = true;
                        tempCurrentResp = "";
                        //                           //logger.info('Found complete command 1', tempCurrentResp);

                    } else if (tempCurrentResp.length === expectedLength3) {
                        readRespArray.push(tempCurrentResp);
                        readRespString();
                        isCommandStarted = true;
                        tempCurrentResp = "";
                        //logger.info('Found complete command 1', tempCurrentResp);
                    } else if (tempCurrentResp.length > expectedLength2) {
                        let parsedCommand2 = tempCurrentResp.substring(0, expectedLength2);
                        readRespArray.push(parsedCommand2);

                        //    logger.warn("Extra found complete command 1", tempCurrentResp);

                        let extraString2 = tempCurrentResp.substring(expectedLength2, tempCurrentResp.length);
                        if (extraString2.length < 2) {
                            logger.error("Invalid start of command 1 ", extraString2);

                            tempCurrentResp = "";
                            isCommandStarted = true;
                        } else {
                            let deepFlag3 = extraString2.charAt(0) + extraString2.charAt(1);
                            if (deepFlag3 !== "DF") {
                                isCommandStarted = true;
                                tempCurrentResp = "";
                                //          logger.error("DF Invalid start of command 1 ", deepFlag3);
                            } else {
                                isCommandStarted = false;
                                tempCurrentResp = extraString2;
                            }
                        }
                    } else {
                        isCommandStarted = false;
                    }
                }
            } else {
                //     logger.warn('Command is not length of 22');
                isCommandStarted = false;
            }
        }
    } else {
        // //logger.info('Checking for other half part of command');
        if (tempCurrentResp.length >= 22) {
            let length2 = convertHexStringToInteger(tempCurrentResp.substring(20, 22));
            if (length2 === 0) {
                let expectedLength3 = 20 + 2;
                if (tempCurrentResp.length === expectedLength3) {
                    readRespArray.push(tempCurrentResp);
                    readRespString();
                    //    //logger.info("Found complete command 2 ", tempCurrentResp);
                    isCommandStarted = true;
                    tempCurrentResp = "";

                } else if (tempCurrentResp.length > expectedLength3) {
                    let parsedCommand3 = tempCurrentResp.substring(0, expectedLength3);
                    readRespArray.push(parsedCommand3);

                    let extraString4 = tempCurrentResp.substring(expectedLength3, tempCurrentResp.length);
                    if (extraString4.length < 2) {
                        //     logger.error("Invalid start of command 2 ", extraString4);

                        tempCurrentResp = "";
                        isCommandStarted = true;
                    } else {
                        let deepFlag4 = extraString4.charAt(0) + extraString4.charAt(1);
                        if (deepFlag4 !== "DF") {
                            isCommandStarted = true;
                            tempCurrentResp = "";
                            //      logger.error("DF Invalid start of command 2 ", deepFlag4);
                        } else {
                            isCommandStarted = false;
                            tempCurrentResp = extraString4;
                        }
                    }
                } else {
                    isCommandStarted = false;
                }
            } else {
                let expectedLength4 = (20 + 2 + (length2 * 2));
                if (tempCurrentResp.length === expectedLength4) {
                    readRespArray.push(tempCurrentResp);
                    readRespString();
                    //     //logger.info("Found complete command 3 ", tempCurrentResp);
                    isCommandStarted = true;
                    tempCurrentResp = "";

                } else if (tempCurrentResp.length > expectedLength4) {
                    let parsedCommand5 = tempCurrentResp.substring(0, expectedLength4);
                    readRespArray.push(parsedCommand5);
                    //        logger.warn("Extra found complete command 3 ", tempCurrentResp);

                    let extraString6 = tempCurrentResp.substring(expectedLength4, tempCurrentResp.length);
                    if (extraString6.length < 2) {
                        //        logger.error("Invalid start of command 3 ", extraString6);

                        isCommandStarted = true;
                        tempCurrentResp = "";
                    } else {
                        let deepFlag7 = extraString6.charAt(0) + extraString6.charAt(1);
                        if (deepFlag7 !== "DF") {
                            tempCurrentResp = "";
                            isCommandStarted = true;
                            //     logger.error("DF Invalid start of command 3 ", deepFlag7);

                        } else {
                            isCommandStarted = false;
                            tempCurrentResp = extraString6;
                        }
                    }
                } else {
                    isCommandStarted = false;
                }
            }
        } else {
            //logger.info('Command is not yet 22 length');
            isCommandStarted = false;
        }
    }
    isCommandProcessing = false;
};

// setInterval(function () {
//     // //logger.info('- In commandDataArray');
//     if (isCommandProcessing) {
//         return;
//     }
//     if (commandDataArray.length === 0) {
//         if (tempCurrentResp.length !== 0) {
//             proocessCommand(tempCurrentResp);
//         }
//     } else {
//         var cmd = commandDataArray.shift();
//         tempCurrentResp = tempCurrentResp + cmd;
//         proocessCommand(tempCurrentResp);
//     }
// }, 100);

let readRespString = function() {
    setTimeout(function() {
        readRespString();

        if (readRespArray.length === 0) {
            return;
        }

        let processRespData = readRespArray.shift();

        // format the data received from the serial port and then pass it to listener
        let response = exports.createResponse(processRespData);
        if (response === 100 || response === -1) {
            return;
        } else {
            processResponse(response);
        }
        
    }, 70);
};
//readRespString();

let writeArray = [];
// Data writing starts
exports.writePacket = function(bufferData, cb) {

    if (isDis) {
        logger.warn('========== Discarding packet TCP server is down !!! ==========');
        if (cb) {
            cb("Socket not connected", null);
        }
        return;
    } else {
        if (cb) {
            cb(null, true);
        }
    }
    writeArray.push(bufferData);
};

let isDataWriteWait = false;
let processData = function() {
    ////logger.info('---- In processData');
    // setTimeout(function() {

        // logger.info('PROCESS TIMEOUT CALLED');
      
        if (writeArray.length === 0) {
            // processData();
            // //logger.info('----- Return as writeArray length 0');            
            return;
        }
        if (isDataWriteWait) {
            // processData();
            //logger.warn('Signal is missing');
            return;
        }
        if (isDis) {
            // processData();
            logger.warn('isDis ', isDis);
            return;
        }
        currentBufferData = writeArray.shift();
        exports.sendData(currentBufferData);
        // processData();
    // }, 150);
};
setInterval(function(){
    processData();
},70);

exports.sendData = function(currentBufferData) {

    logger.info('DATA SENT - [SERIAL PORT IO] - ', currentBufferData);
    isDataWriteWait = true;

    serialPort.write(currentBufferData, function(err) {
        // if (err) {
        //     logger.error('Error on write: ', err.message);
        // }
        // logger.info('DATA SUCC - [SERIAL PORT IO] - ', currentBufferData);
        isDataWriteWait = false;
    });
};

// Decoding the valid request received on serial port and then returning it as a object that can be human percievable
exports.createResponse = function(data, command) {

    let i = 0;

    let deepFlag = data.charAt(i++) + data.charAt(i++);
    let moduleId = data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++) +
        data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++) +
        data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++) +
        data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++);
    moduleId = moduleId.toUpperCase();

    //logger.info('Serial Data Received : ', data);
    let receivedCommandValue = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));

    //logger.info("deepFlag ", deepFlag, " moduleId ", moduleId, " receivedCommandValue ", receivedCommandValue);
    let response, length, deviceId, dataLength, deviceStatusDetails, j, status, event;

    if (cc2531Constants.command.SET.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.YALE_LOCK_UNLOCK.code === receivedCommandValue) {
        // //logger.info("Discarded set command >>>>>>>>>>>>>>>>>>> ", data + " \n");

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];
        response = {};
        response.moduleId = moduleId;
        response.receivedCommandValue = receivedCommandValue;
        response.lockCommandType = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        const subByte = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response.physicalActivity = 59 == subByte ? true : false;

        if (response.physicalActivity == false) {
            i--;
            i--;
        }

        if (dataLength > 1 && [0, 1, 9, 11, 2].includes(response.lockCommandType)) {
            response.subCommandType = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        }

        otherdata = data.substring(i);
        response.otherdata = otherdata;

        //logger.info("SET >>>>>>>>>>>>>>>>>>> ", response);
        return response;

    } else if (cc2531Constants.command.GET.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };
        // //logger.info("Device ACK >>>>>>>>>>>>>>>>>>> ", response);
        return response;

    } else if (cc2531Constants.command.SET_ALL.code === receivedCommandValue) {
        ////logger.info("Discarded set all command >>>>>>>>>>>>>>>>>>> ", data + " \n");

    } else if (cc2531Constants.command.LOCK_DEVICE.code === receivedCommandValue) {
        ////logger.info("Discarded LOCK_DEVICE command >>>>>>>>>>>>>>>>>>> ", data+ " \n");

    } else if (cc2531Constants.command.GET_ALL.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.PHY_STATUS.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.BOOT_STATUS.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.ACK.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.CURTAIN_ACK.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];

        for (j = 0; j < dataLength; j++) {
            deviceStatusDetails.push({
                deviceId: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                status: convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)),
                moduleId: moduleId
            });
        }

        response = {
            moduleId: moduleId,
            length: length,
            deviceStatusDetails: deviceStatusDetails,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.DOOR_SENSOR_ACK.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            moduleId: moduleId,
            length: length,
            receivedCommandValue: receivedCommandValue
        };

        return response;

    } else if (cc2531Constants.command.STB_STATUS.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            moduleId: moduleId,
            length: length,
            receivedCommandValue: receivedCommandValue
        };
        ////logger.info("STB_STATUS >>>>>>>>>>>>>>>>>>> ", response);        
        return response;

    } else if (cc2531Constants.command.TEMP_SENSOR_STATUS.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        status = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        status_type = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++)); //0 - negative || 1 - positive
        humidity = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            moduleId: moduleId,
            length: length,
            status: status,
            status_type: status_type,
            humidity: humidity,
            receivedCommandValue: receivedCommandValue
        };
        return response;

    } else if (cc2531Constants.command.TEMP_BATTERY_VOLTAGE.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        status = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        sensor_type = "temp",
            response = {
                moduleId: moduleId,
                length: length,
                status: status,
                sensor_type: sensor_type,
                receivedCommandValue: receivedCommandValue
            };
        return response;

    } else if (cc2531Constants.command.TEMP_BATTERY_VOLTAGE.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        status = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        sensor_type = "temp",
            response = {
                moduleId: moduleId,
                length: length,
                status: status,
                sensor_type: sensor_type,
                receivedCommandValue: receivedCommandValue
            };
        ////logger.info("TEMP_BATTERY_VOLTAGE >>>>>>>>>>>>>>>>>>> ", response);        
        return response;

    } else if (cc2531Constants.command.DOOR_SENSOR_STATUS.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        id = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        event = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            sensorId: moduleId,
            moduleId: moduleId,
            length: length,
            receivedCommandValue: receivedCommandValue,
            event: event
        };
        ////logger.info("DOOR_SENSOR_STATUS >>>>>>>>>>>>>>>>>>> ", response);        
        return response;

    } else if (cc2531Constants.command.WATER_SENSOR_STATUS.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        id = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        event = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            sensorId: moduleId,
            moduleId: moduleId,
            length: length,
            receivedCommandValue: receivedCommandValue,
            event: event
        };
        //logger.info("WATER_SENSOR_STATUS >>>>>>>>>>>>>>>>>>> ", response);
        return response;

    } else if (cc2531Constants.command.DEVICE_VOLTAGE.code === receivedCommandValue) {

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        status = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));

        response = {
            moduleId: moduleId,
            length: length,
            status: status,
            receivedCommandValue: receivedCommandValue
        };
        ////logger.info("DEVICE_VOLTAGE >>>>>>>>>>>>>>>>>>> ", response);        
        return response;

    } else if (cc2531Constants.command.ROUTER_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        module_type = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        deviceId = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            length: length,
            moduleId: moduleId,
            module_type: module_type,
            deviceId: deviceId.toString(),
            roomName: moduleId,
            receivedCommandValue: receivedCommandValue
        };
        return response;

    } else if (cc2531Constants.command.IR_SENSOR_COMMAND.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        sensor_type = "IR Blaster";
        response = {
            length: length,
            moduleId: moduleId,
            sensor_type: sensor_type,
            receivedCommandValue: receivedCommandValue
        };
        // //logger.info("IR_SENSOR_COMMAND >>>>>>>>>>>>>>>>>>> ", response);
        return response;
    } else if (cc2531Constants.command.SMART_REMOTE_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        module_type = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        event = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            length: length,
            moduleId: moduleId,
            remote_number: module_type,
            status: event,
            receivedCommandValue: receivedCommandValue
        };
        return response;
    } else if (cc2531Constants.command.GAS_SENSOR_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        is_gas_detect = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        gas_current_value = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        threshold_value = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            module_id: moduleId,
            moduleId: moduleId,
            length: length,
            is_gas_detect: is_gas_detect,
            gas_current_value: gas_current_value,
            threshold_value: threshold_value,
            receivedCommandValue: receivedCommandValue
        };
        return response;
    } else if (cc2531Constants.command.CO2_SENSOR_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        is_co2_detect = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        co2_current_value = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        threshold_value = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            module_id: moduleId,
            moduleId: moduleId,
            length: length,
            is_co2_detect: is_co2_detect,
            co2_current_value: co2_current_value,
            threshold_value: threshold_value,
            receivedCommandValue: receivedCommandValue
        };
        return response;
    } else if (cc2531Constants.command.HEAVY_LOAD_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        deviceId = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        status = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        load_voltage = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        load_current = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        real_power = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));
        total_energy = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++) + data.charAt(i++) + data.charAt(i++));

        response = {
            length: length,
            deviceId: deviceId,
            status: status,
            module_id: moduleId,
            moduleId: moduleId,
            load_voltage: load_voltage,
            load_current: load_current,
            real_power: real_power,
            total_energy: total_energy,
            roomName: moduleId,
            receivedCommandValue: receivedCommandValue
        };
        return response;
    } else if (cc2531Constants.command.REPEATOR_DATA.code === receivedCommandValue) {
        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response = {
            moduleId: moduleId,
            module_id: moduleId,
            length: length,
            receivedCommandValue: receivedCommandValue
        };
        return response;
    } else if (cc2531Constants.command.BROADCAST_STB_ID.code === receivedCommandValue) {
        ////logger.info("Discarded BROADCAST_STB_ID command");

    } else if (cc2531Constants.command.CHANGE_PIR_DEVICE.code === receivedCommandValue) {
        // //logger.info("Discarded set command >>>>>>>>>>>>>>>>>>> ", data + " \n");

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];
        response = {};
        response.moduleId = moduleId;
        response.receivedCommandValue = receivedCommandValue;
        response.commandType = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        response.subCommandType = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
      
        return response;

    }else if (cc2531Constants.command.CHANGE_PIR_DETECTOR.code === receivedCommandValue) {
        
        // //logger.info("Discarded set command >>>>>>>>>>>>>>>>>>> ", data + " \n");

        length = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
        dataLength = length / 2;
        deviceStatusDetails = [];
        response = {};
        response.moduleId = moduleId;
        response.receivedCommandValue = receivedCommandValue;
        response.commandType = convertHexStringToInteger(data.charAt(i++) + data.charAt(i++));
      
        return response;

    } else {
        logger.error("Response does not match with any system command >>>>>>>>>>>>>>>>>>> ", receivedCommandValue);
        return -1;
    }
    return 100;
};

let processResponse = function(response) {
    // Parth: Commented By Parth For Formatting the correct response
    // if (cc2531Constants.command.GET_ALL.code === response.receivedCommandValue) {
    //     callCommandListener(response.deviceStatusDetails, cc2531Constants.command.GET_ALL);
    // } else if (cc2531Constants.command.PHY_STATUS.code === response.receivedCommandValue) {
    //     callCommandListener(response.deviceStatusDetails, cc2531Constants.command.PHY_STATUS);
    if (cc2531Constants.command.GET_ALL.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.GET_ALL);
    } else if (cc2531Constants.command.PHY_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.PHY_STATUS);
    } else if (cc2531Constants.command.ACK.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.ACK);
    } else if (cc2531Constants.command.DOOR_SENSOR_ACK.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.DOOR_SENSOR_ACK);
    } else if (cc2531Constants.command.STB_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.STB_STATUS);
    } else if (cc2531Constants.command.DOOR_SENSOR_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.DOOR_SENSOR_STATUS);
    } else if (cc2531Constants.command.WATER_SENSOR_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.WATER_SENSOR_STATUS);
    } else if (cc2531Constants.command.MOTION_SENSOR_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.MOTION_SENSOR_STATUS);
        // Parth: Commented By Parth For Formatting the correct response
        // } else if (cc2531Constants.command.BOOT_STATUS.code === response.receivedCommandValue) {
        //     callCommandListener(response.deviceStatusDetails, cc2531Constants.command.BOOT_STATUS);
    } else if (cc2531Constants.command.BOOT_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.BOOT_STATUS);
    } else if (cc2531Constants.command.GET_STB_ID.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.GET_STB_ID);
    } else if (cc2531Constants.command.TEMP_SENSOR_STATUS.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.TEMP_SENSOR_STATUS);
    } else if (cc2531Constants.command.TEMP_BATTERY_VOLTAGE.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.TEMP_BATTERY_VOLTAGE);
    } else if (cc2531Constants.command.DEVICE_VOLTAGE.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.DEVICE_VOLTAGE);
    } else if (cc2531Constants.command.ROUTER_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.ROUTER_DATA);
    } else if (cc2531Constants.command.SET.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.SET);
    } else if (cc2531Constants.command.IR_SENSOR_COMMAND.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.IR_SENSOR_COMMAND);
    } else if (cc2531Constants.command.GET.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.GET);
    } else if (cc2531Constants.command.SMART_REMOTE_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.SMART_REMOTE_DATA);
    } else if (cc2531Constants.command.GAS_SENSOR_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.GAS_SENSOR_DATA);
    } else if (cc2531Constants.command.CO2_SENSOR_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.CO2_SENSOR_DATA);
    } else if (cc2531Constants.command.HEAVY_LOAD_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.HEAVY_LOAD_DATA);
    } else if (cc2531Constants.command.REPEATOR_DATA.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.REPEATOR_DATA);
    } else if (cc2531Constants.command.CURTAIN_ACK.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.CURTAIN_ACK);
    } else if (cc2531Constants.command.YALE_LOCK_UNLOCK.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.YALE_LOCK_UNLOCK);
    } else if (cc2531Constants.command.CHANGE_PIR_DEVICE.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.CHANGE_PIR_DEVICE);
    } else if (cc2531Constants.command.CHANGE_PIR_DETECTOR.code === response.receivedCommandValue) {
        callCommandListener(response, cc2531Constants.command.CHANGE_PIR_DETECTOR);
    } else {

        logger.error("FATAL Error response does not match with any system command ", response);
    }
};

//Register Listeners on bootup
let listeners = {};
exports.registerListener = function(listener, command) {
    //  //logger.info("listener registered for command : " + JSON.stringify(command));
    if (!listener) {
        logger.error("registerListener FATAL Error: Invalid listener: ", listener);
        return;
    }
    if (!command) {
        logger.error("registerListener FATAL Error: Invalid command: ", command);
        return;
    }

    if (listeners[command.value]) {
        listeners[command.value].push(listener);
    } else {
        listeners[command.value] = [];
        listeners[command.value].push(listener);
    }
};


//Check Serial Port Status
exports.isSerialPortReady = function() {
    ////logger.info("is SerialPort Ready " + !isDis);
    return !isDis;
};

let callCommandListener = function(resp, command) {
    let tempListener = listeners[command.value];
    if (tempListener) {
        if (tempListener.length > 0) {
            for (let i = 0; i < tempListener.length; i++) {
                tempListener[i](resp);
            }
        } else {
            //logger.error("FATAL Error no listener found for command: ", command);
        }
    } else {
        //   logger.error("FATAL Error no listener found for command: ", command);
    }
};

let convertHexStringToInteger = function(hexString) {
    return parseInt("0x" + hexString);
};