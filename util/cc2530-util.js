const cc2531Constants = require('./cc2531Constants');
const serialPortIO = require('./serialPortIOTCPClient');
//var mqttIO = require('./mqtt-util');
const dataUtil = require('./data-util');
const packetUtil = require('./packet-util');

exports.turnOnOffDevice = function(moduleId, deviceId, deviceStatusTemp, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {

        let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
        let packet = packetUtil.createPackat(cc2531Constants.command.SET, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        //logger.info('bufferData is  ', bufferData);

        serialPortIO.writePacket(bufferData);
    })


};

exports.turnOnOffDeviceWifi = function(moduleId, deviceId, deviceStatusTemp, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    return packetUtil.createPackat(cc2531Constants.command.SET, moduleId, commandDataArray);

};

exports.lockOpenClose = function(moduleId, deviceId, deviceStatusTemp, cb) {
    logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
        let packet = packetUtil.createPackat(cc2531Constants.command.YALE_LOCK_UNLOCK, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        //logger.info('bufferData is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })


};

exports.yaleLockPincodeDelete = function(moduleId, commandType, subCommandType, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = dataUtil.creatSetCommandData(commandType, subCommandType);

        let packet = packetUtil.createPackat(cc2531Constants.command.YALE_LOCK_UNLOCK, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        //logger.info('bufferData is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })


};

exports.changePirDeviceMode = function(moduleId, commandType, subCommandType, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = [commandType, subCommandType];

        let packet = packetUtil.createPackat(cc2531Constants.command.CHANGE_PIR_DEVICE, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        // logger.info('Cireg is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })

};


exports.changePirDeviceinterval = function(moduleId, commandType, subCommandType, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = [commandType, subCommandType];

        let packet = packetUtil.createPackat(cc2531Constants.command.CHANGE_PIR_DEVICE, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        // logger.info('Cireg is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })

};
exports.changePirDeviceStatus = function(moduleId, commandType, subCommandType, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = [commandType, subCommandType];

        let packet = packetUtil.createPackat(cc2531Constants.command.CHANGE_PIR_DEVICE, moduleId, commandDataArray);
        let bufferData = Buffer.from(packet, "hex");
        // logger.info('Cireg is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })

};

exports.yaleLockPincodeSet = function(moduleId, commandType, subCommandType, pinCode, cb) {
    ////logger.info('turnOnOffDevice ', moduleId, deviceId, deviceStatusTemp);

    return new Promise((resolve, reject) => {
        let commandDataArray = dataUtil.creatSetCommandData(commandType, subCommandType);

        let packet = packetUtil.createPacketForYaleLockPinChange(cc2531Constants.command.YALE_LOCK_UNLOCK, moduleId, commandDataArray, pinCode);
        let bufferData = Buffer.from(packet, "hex");
        //logger.info('bufferData is  ', bufferData);
        serialPortIO.writePacket(bufferData);
    })


};

//function getDeviceData(){
exports.getPhysicalDeviceData = function(device, cb) {
    serialPortIO.getPhysicalDeviceData(function(error, result) {
        if (error) {
            logger.error("GEt Physical Status Error " + error);
        } else {
            //logger.info("Get Physical device status result " + JSON.stringify(result));
        }
    });
};

exports.getModuleStatus = function(moduleId) {
    let commandDataArray = dataUtil.creatGetAllCommandData();
    let packet = packetUtil.createPackat(cc2531Constants.command.GET_ALL, moduleId, commandDataArray);
    ////logger.info("getModuleStatus packet: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData);
};

exports.getSwitchModuleStatus = function(switchModuleID) {
    let commandDataArray = dataUtil.creatACKCommandData();
    let packet = packetUtil.createPackat(cc2531Constants.command.ACK, switchModuleID, commandDataArray);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData);
};

exports.configuredevice = function(cb) {
    let deviceId = '00';
    let deviceStatusTemp = '00';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configuredevice packet: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData);
};



exports.configuredevicemanually = function(module_id) {
    let deviceId = '00';
    let deviceStatusTemp = '00';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = module_id;
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configuredevice packet: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData);
};

exports.configureRequest = function(params, cb) {
    const deviceId = params.deviceId;
    const deviceStatusTemp = params.deviceStatusTemp;
    const commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    const moduleId = '0000000000000000';
    const packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for device: ", packet);
    const bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData);
}

exports.configureTempSensor = function(cb) {
    let deviceId = '10';
    let deviceStatusTemp = '01';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for temperature sensor: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.configureCurtainRequest = function(cb) {
    let deviceId = '00';
    let deviceStatusTemp = '00';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configuredevice packet: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
}

exports.configureDoorsensor = function(cb) {
    //logger.info("configureDoorsensor cc2530 util");
    let deviceId = '10';
    let deviceStatusTemp = '02';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for door sensor: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

// exports.configureIRBlaster = function (cb) {
//     //logger.info("into configureIRBlaster");
//     var deviceId = '10';
//     var deviceStatusTemp = '03';
//     var commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
//     var moduleId = '0000000000000000';
//     var packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
//     //logger.info("configure device packet for IR Blaster: ", packet);
//     var bufferData = Buffer.from(packet,"hex");
//     serialPortIO.writePacket(bufferData, function (error, result) {
//         if (error) {
//             logger.error("Serial Write Error : " + error);
//             cb(error, null);
//         } else {
//             cb(null, result);
//         }
//     });
// };

exports.configuresmartRemoteRequest = function(cb) {
    //logger.info("into configuresmartRemoteRequest");
    let deviceId = '10';
    let deviceStatusTemp = '03';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for Smart Remote: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.configureGasSensor = function(cb) {
    let deviceId = '10';
    let deviceStatusTemp = '04';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for Gas sensor: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.configureCo2Sensor = function(cb) {
    let deviceId = '10';
    let deviceStatusTemp = '10';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for Gas sensor: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};



exports.configureGasSensorThresholdValue = function(cb) {
    let deviceId = '10';
    let deviceStatusTemp = '05';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '2098131A004B1200';
    let packet = packetUtil.createThresholdPackat(moduleId, commandDataArray);
    //logger.info("configure device packet for multi sensor Threshold Value: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.configureRepeatorRequest = function(cb) {
    //logger.info("configureRepeatorRequest cc2530 util");
    let deviceId = '10';
    let deviceStatusTemp = '07';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = '0000000000000000';
    let packet = packetUtil.createPackat(cc2531Constants.command.ROUTER_DATA, moduleId, commandDataArray);
    //logger.info("configure device packet for repeator: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.sendSamePacket = function(data) {
    serialPortIO.writePacket(data, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
        } else {
            
        }
    });
};

//update it later as per IR code and DB
exports.IRCommand = function(moduleId, command, cb) {
    //logger.info("IRCOMMAND GENERATE ", moduleId, command);
    let commandDataArray = dataUtil.createIRCommandData(command);
    let packet = packetUtil.createPackat1(cc2531Constants.command.IR_SENSOR_COMMAND, moduleId, commandDataArray);
    //logger.info("packet", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            return cb(error, null);
        } else {
            return cb(null, result);
        }
    });
};

exports.getHeavyLoadValues = function(data, cb) {
    //logger.info("getHeavyLoadValues", data);
    let deviceId = '00';
    let deviceStatusTemp = '00';
    let device_id = data.device_id;
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = data.module_id;
    let packet = packetUtil.createHeavyLoadPackat(moduleId, commandDataArray, device_id);
    //logger.info("configure get heavy load Value: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};

exports.getGasValue = function(data, cb) {
    //logger.info("getGasValue cc2530 util", data);
    let deviceId = '10';
    let deviceStatusTemp = '05';
    let commandDataArray = dataUtil.creatSetCommandData(deviceId, deviceStatusTemp);
    let moduleId = data.module_id;
    // var moduleId = "B04A1D1A004B1200";
    let packet = packetUtil.createGasValuePackat(moduleId, commandDataArray);
    //logger.info("configure device packet for gas value: ", packet);
    let bufferData = Buffer.from(packet, "hex");
    serialPortIO.writePacket(bufferData, function(error, result) {
        if (error) {
            logger.error("Serial Write Error : " + error);
            cb(error, null);
        } else {
            cb(null, result);
        }
    });
};


exports.registerDeviceListenerCallBack = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.SET);
};

exports.registerPhysicalDeviceListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.PHY_STATUS);
};

exports.registerSwitchModuleBootDeviceListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.BOOT_STATUS);
};

exports.registerDeviceStatusACKListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.GET);
};

exports.registerStartupDeviceStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.GET_ALL);
};

//From switchModuleStatusChecker
exports.registerSwitchModuleStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ACK);
};

exports.registerRouterSetupListener = function(listener) {
    ////logger.info("cc2530 router");
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

exports.isSerialPortReady = function() {
    return serialPortIO.isSerialPortReady();
};

//Temperature Sensor
exports.registerTempSensorSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

exports.registerTempSensorStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.TEMP_SENSOR_STATUS);
};

exports.registerTempSensorVoltageListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.TEMP_BATTERY_VOLTAGE);
};



//Door Sensor
exports.registerDoorSensorSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

exports.registerDoorSensorStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.DOOR_SENSOR_STATUS);
};

exports.registerDoorSensorVoltageListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.DOOR_BATTERY_VOLTAGE);
};

// Water Detector
exports.registerWaterDetectorStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.WATER_SENSOR_STATUS);
};

exports.registerWaterDetectorVoltageListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.WATER_SENSOR_BATTERY_VOLTAGE);
};


//Mains Voltage
exports.registerMainsVoltageListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.DEVICE_VOLTAGE);
};


// //General Setup
exports.registerDeviceSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

// //IR Blster setup
exports.registerIRBlasterSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
    //   mqttIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

//Smart Remote setup
exports.registerSmartRemoteSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

//Smart Remote Number
exports.registerSmartRemoteNumberListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.SMART_REMOTE_DATA);
};

//Gas Sensor Configure
exports.registerGasSensorSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

exports.registerGasSensorValueListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.GAS_SENSOR_DATA);
};

//Co2 Sensor Configuration
exports.registerCo2SensorSetupListner= function(listenr) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
}

exports.registerCo2SensorValueListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.CO2_SENSOR_DATA);
};

//Heavy Load Values
exports.registerheavyLoadValueListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.HEAVY_LOAD_DATA);
};

//Repeator Configure
exports.registerRepeatorSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
};

//Repeator Data
exports.registerRepeatorStatusListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.REPEATOR_DATA);
};

// Curtain Setup Listener
exports.registerCurtainSetupListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.ROUTER_DATA);
}

// Curtain Acknowlegment Listener
exports.registerCurtainAcknowledgement = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.CURTAIN_ACK);
}

// Yale Lock Value Setter
exports.registerYaleLockListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.YALE_LOCK_UNLOCK);
}

// Pir Device Listener
exports.registerPirDeviceListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.CHANGE_PIR_DEVICE);
}


// Pir Device Listener
exports.registerPirDetectorListener = function(listener) {
    serialPortIO.registerListener(listener, cc2531Constants.command.CHANGE_PIR_DETECTOR);
}

