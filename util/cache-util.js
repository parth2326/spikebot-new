let cacheManager = {};

cacheManager.deviceIdentifierModuleIdentiferCache = {};
cacheManager.actualDeviceStatusList = {};

module.exports = cacheManager;