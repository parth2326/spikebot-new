const cc2530Util = require('./../util/cc2530-util');
const deviceModel = require('./../models/device')
const eventsEmitter = require('./../util/event-manager');
const cacheManager = require('./../util/cache-util');
const deviceStatusModel = require('./../models/device_status');
const mqtt = require('mqtt');
var _util = {};
let mqttclient = mqtt.connect('mqtt://127.0.0.1:1883');
_util.commandQueue = {};
_util.deviceData = {};

/**
 * @param command_type
 * @param counter
 */

const sleep = function (ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

_util.addToQueue = async function (command) {
    /**
     * command_type
     * counter
     * device_id
     * device_status (optional if the command is on off type)
     */

    // Initializing To the Queue

    // logger.info('IN ADD TO QUEUE : ', command);

    if (Array.isArray(command)) {

        command = command[0];

        command.command_type = command.command_type ? command.command_type : "on-off";
        command.counter = command.counter ? command.counter : 4;

        if (_util.commandQueue[command.device_id] == null) {
            _util.commandQueue[command.device_id] = [];
        }

        // Check if there is a command in quque with same device_id and command_type
        let previousCommand = _util.commandQueue[command.device_id].find(function (item) {
            return item.command_type = command.command_type;
        });

        if (!previousCommand) {
            _util.commandQueue[command.device_id].push(command);
        } else {
            // Replace command if previous command exists
            previousCommand = command;
        }

        // saving the device data in cache 
        // so that we dont need to query in return response received from serial port in general event listener



        await _util.sendCommand({ ...command });

    } else {

        command.command_type = command.command_type ? command.command_type : "on-off";
        command.counter = command.counter ? command.counter : 4;

        // logger.info('COMMAND COUNTER',command);

        _util.commandQueue[command.device_id] = command;

        if (command.command_type == 'on-off' || command.command_type == 'on-off-wifi') {
            _util.deviceData[command.module_identifier + "-" + command.device_identifier] = command.device;
        }

        await _util.sendCommand({ ...command });
    }

}

/**
 * Remove from Queue
 * @param device_id
 * @param device_status (optional)
 * @param device_identifier (optional : needed for curtain)
 * @param pir_mode (optional - need for pir_device only)
 * @param pir_timer (optional - needed for pir_device only)
 * @param command_type (required if array of commands)
 */
_util.removeFromQueue = function (params) {

    try {

        let command = _util.commandQueue[params.device_id];

        //logger.info('Queue Manager | Acknowledgement Received', command);
        if (!command) {
            return;
        }

        if (command.command_type == "on-off" && command.device_status == params.device_status) {
            delete _util.commandQueue[params.device_id];
            //logger.info('Queue Manager | Deleted From the Queue', command);
        }else if (command.command_type == "on-off-wifi" && command.device_status == params.device_status) {
            delete _util.commandQueue[params.device_id];
            //logger.info('Queue Manager | Deleted From the Queue', command);
        }else if (command.command_type == "lock-open-close" && command.device_status == params.device_status) {
            delete _util.commandQueue[params.device_id];
            //logger.info('Queue Manager | Deleted From the Queue', command);
        } else if (command.command_type == "configuration") {
            delete _util.commandQueue[params.device_id];
        } else if (command.command_type == "curtain-open-close" && command.device_status == params.device_status && command.device_identifier == params.device_identifier) {
            delete _util.commandQueue[params.device_id];
        } else if (params.command_type == "change-pir-device-mode") {

            const existingCommand = _util.commandQueue[params.device_id].find(function (item) {
                return item.command_type == params.command_type;
            });

            if (existingCommand && existingCommand.pir_mode == params.pir_mode) {
                _util.commandQueue[params.device_id] = util.commandQueue[params.device_id].filter(function (item) {
                    return item.command_type != params.command_type;
                });
            }

        } else if (params.command_type == "change-pir-device-status") {



            const existingCommand = _util.commandQueue[params.device_id].find(function (item) {
                return item.command_type == params.command_type;
            });

            if (existingCommand && existingCommand.device_status == params.device_status) {
                _util.commandQueue[params.device_id] = _util.commandQueue[params.device_id].filter(function (item) {
                    return item.command_type != params.command_type;
                });
            }

            // delete _util.commandQueue[params.device_id];

        } else if (params.command_type == "change-pir-device-interval") {

            // delete _util.commandQueue[params.device_id];

            const existingCommand = _util.commandQueue[params.device_id].find(function (item) {
                return item.command_type == params.command_type;
            });

            if (existingCommand && existingCommand.pir_timer == params.pir_timer) {
                _util.commandQueue[params.device_id] = _util.commandQueue[params.device_id].filter(function (item) {
                    return item.command_type != params.command_type;
                });
            }

        }



    } catch (ex) {
        logger.error('Queue Manager | Failed to delete from queue', ex);
    }

}

// /**
//  * @param module_identifier
//  * @param device_identifier
//  */
// _util.getQueueCommandFromModuleIdAndDeviceId = async function(params){

//     const queueCommands = 

// }

/**
 * Clear Entire Quese
 */
_util.clearQueue = function () {

    Object.keys(_util.commandQueue).forEach(function (key) {

        if (Array.isArray(_util.commandQueue[key])) {

            const removedCommandList = _util.commandQueue[key].filter(function (item) {
                return item.counter <= 0;
            });

            for (let removedCommand of removedCommandList) {
                _util.handleReverse({ ...removedCommand });
            }

            _util.commandQueue[key] = _util.commandQueue[key].filter(function (item) {
                return item.counter <= 0;
            });

        } else {

            if (_util.commandQueue[key].counter <= 0) {
                _util.handleReverse({ ..._util.commandQueue[key] });
                delete _util.commandQueue[key]
            }
        }

        // if (_util.commandQueue[key].counter == 0) {

        //     if (_util.commandQueue[key].command_type == "on-off") {
        //         deviceController.reverseDeviceStatus(_util.commandQueue[key], function (err, response) {

        //         });

        //         logger.error('Queue Manager | Failed To Process Device On Off Request:', _util.commandQueue[key]);
        //         delete _util.commandQueue[key]
        //     } else if (_util.commandQueue[key].command_type == "lock-open-close") {
        //         deviceController.reverseDeviceStatus(_util.commandQueue[key], function (err, response) { });
        //         logger.error('Queue Manager | Failed To Process Lock Device Update Request:', _util.commandQueue[key]);
        //         delete _util.commandQueue[key]
        //     } else if (_util.commandQueue[key].command_type == "configuration") {
        //         logger.error('Queue Manager | Failed To Get Configuration :', _util.commandQueue[key]);
        //         delete _util.commandQueue[key];
        //     } else if (_util.commandQueue[key].command_type == "curtain-open-close") {
        //         logger.error('Queue Manager | Failed To Process Curtain open close command :', _util.commandQueue[key]);
        //         delete _util.commandQueue[key];
        //     } else if (_util.commandQueue[key].command_type == "change-pir-device-mode") {
        //         logger.error('Queue Manager | Failed To Process Change Pir Device Mode :', _util.commandQueue[key]);
        //         delete _util.commandQueue[key];
        //     } else if (_util.commandQueue[key].command_type == "change-pir-device-status") {
        //         logger.error('Queue Manager | Failed To Process Change Pir Device Status :', _util.commandQueue[key]);
        //         delete _util.commandQueue[key];
        //     }
        // }
    });
}

_util.handleReverse = function (command) {
    if (command.command_type == "on-off" || command.command_type=='on-off-wifi') {
        _util.reverseDeviceStatus(command);
        logger.error('Queue Manager | Failed To Process Device On Off Request:', command);
    } else if (command.command_type == "lock-open-close") {
        _util.reverseDeviceStatus(command);
        logger.error('Queue Manager | Failed To Process Lock Device Update Request:', command);
    } else if (command.command_type == "configuration") {
        logger.error('Queue Manager | Failed To Get Configuration :', command);
    } else if (command.command_type == "curtain-open-close") {
        logger.error('Queue Manager | Failed To Process Curtain open close command :', command);
    } else if (command.command_type == "change-pir-device-mode") {
        logger.error('Queue Manager | Failed To Process Change Pir Device Mode :', command);
    } else if (command.command_type == "change-pir-device-status") {
        logger.error('Queue Manager | Failed To Process Change Pir Device Status :', command);
    } else if (command.command_type == "change-pir-device-interval") {
        logger.error('Queue Manager | Failed To Process Change Pir Device Interval :', command);
        _util.reverseDeviceStatus(command);
    }
}

_util.reverseDeviceStatus = async function (params) {

    // If Command Type is On Off Type
    try {
        if (params.command_type == "on-off" || params.command_type == "on-off-wifi") {

            let device = await deviceModel.get(params.device_id);


            const cacheDataForDevice = cacheManager.actualDeviceStatusList[device.device_id];

            logger.info('Queue Manager Util : ', cacheDataForDevice);
            let new_device_status = cacheDataForDevice == null ? params.device_old_status : cacheDataForDevice.device_status;
            let new_device_sub_status = (cacheDataForDevice == null) ? params.device_old_sub_status : cacheDataForDevice.device_sub_status;
            if (new_device_sub_status === null || new_device_sub_status === undefined) {
                new_device_sub_status = 0;
            }

            if (device) {

                // Return the Status of Old Status 
                await deviceStatusModel.updateMany({
                    device_id: params.device_id,
                    device_status: new_device_status,
                    device_sub_status: new_device_sub_status
                }, {
                    emitRoomPanelStatus: true
                });

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: device.device_type,
                        device_status: new_device_status,
                        device_sub_status: new_device_sub_status
                    }
                });

                // socketio.emit('changeDeviceStatus', {
                //     device_id: params.device_id,
                //     device_type: device.device_type,
                //     device_status: params.device_old_status,
                //     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
                // });

            }

        } else if (params.command_type == "lock-open-close") {

            let device = await deviceModel.get(params.device_id);

            if (device) {

                // Return the Status of Old Status 
                await deviceStatusModel.updateMany({
                    device_id: params.device_id,
                    device_status: params.device_old_status,
                    device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
                }, {
                    emitRoomPanelStatus: false
                });

                eventsEmitter.emit('emitSocket', {
                    topic: 'changeDeviceStatus',
                    data: {
                        device_id: params.device_id,
                        device_type: device.device_type,
                        device_status: params.device_old_status,
                        device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
                    }
                });

                // socketio.emit('changeDeviceStatus', {
                //     device_id: params.device_id,
                //     device_type: device.device_type,
                //     device_status: params.device_old_status,
                //     device_sub_status: params.device_old_sub_status ? params.device_old_sub_status : 0
                // });

            }

        } else if (params.command_type == "change-pir-device-interval") {
            await generalMetaModel.update(device.device_id, 'device', 'pir_timer', params.old_pir_timer);
        }
    } catch (error) {
        logger.error('Queue Manager Util | Reverse Device Status Error', error);
        // return cb(null, appUtil.createErrorResponse(constants.responseCode.INTERNAL_SERVER_ERROR));
    }
}


/**
 * Resend Queue Command
 */
_util.resendQueueCommands = async function () {

    for (let key of Object.keys(_util.commandQueue)) {

        try {
            if (Array.isArray(_util.commandQueue[key])) {
                for (let command of _util.commandQueue[key]) {
                    await _util.sendCommand({ ...command });
                    command.counter--;
                }
            } else {
                await _util.sendCommand({ ..._util.commandQueue[key] });
                _util.commandQueue[key].counter--;

            }


        } catch (error) {

        }

    }

}

/**
 * Send Command
 */
_util.sendCommand = async function (command) {

    // logger.info('Queue Manager - [RESEND COMMAND] - ', command);
    await sleep(50);
    if (command && command.command_type == "on-off") {
        logger.info('COMMAND SENT ', command.device_identifier.toString(), command.device_status);
        cc2530Util.turnOnOffDevice(command.module_identifier, command.device_identifier.toString(), command.device_status);
    } else if (command && command.command_type == "on-off-wifi") {
        let commandvalue = cc2530Util.turnOnOffDeviceWifi(command.module_identifier, command.device_identifier.toString(), command.device_status);

        logger.info('COMMAND FIRED FOR 5F-WIFI: ', commandvalue);
        eventsEmitter.emit('emitMQTT', {
            topic: '/ESP32/'+command.module_identifier, 
            data: commandvalue
        });
        // mqttclient.publish('/ESP32/'+command.module_identifier, commandvalue, function () { });

    } else if (command && command.command_type == "curtain-open-close") {
        cc2530Util.turnOnOffDevice(command.module_identifier, command.device_identifier.toString(), command.device_status);
    } else if (command && command.command_type == "lock-open-close") {
        cc2530Util.lockOpenClose(command.module_identifier, command.device_status, 0);
    } else if (command && command.command_type == "configuration") {
        cc2530Util.configureRequest({
            deviceId: command.device_identifier,
            deviceStatusTemp: command.device_status
        }, (error, result) => {
            if (error) {
                logger.error("New Device configure result ", error);
            } else {

            }
        });

    } else if (command && command.command_type == "change-pir-device-mode") {
        cc2530Util.changePirDeviceMode(command.module_identifier, 0, command.pir_mode);
    } else if (command && command.command_type == "change-pir-device-status") {
        cc2530Util.changePirDeviceStatus(command.module_identifier, 1, command.device_status);
    } else if (command && command.command_type == "change-pir-device-interval") {
        cc2530Util.changePirDeviceinterval(command.module_identifier, 2, command.pir_timer);
    }

    else {
        //logger.info('Queue Manager : Invalid Command Type', command);
    }
}


setInterval(async function () {

    logger.info('QUEUE MANAGER - [RESET QUEUE]');

    // process.nextTick(function(){
    _util.clearQueue();
    await _util.resendQueueCommands();
    // })

}, 3000);


module.exports = _util;