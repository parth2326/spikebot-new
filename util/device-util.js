const cc2530Util = require('./cc2530-util');

exports.setParams = function (homeAutomationModule, appContext, shared) {

	let constants = homeAutomationModule.constants;
	let sqliteDB = shared.db.sqliteDB;
	exports.turnOnOffDevice = function (moduleId, deviceId, deviceStatus, deviceSpecificValue, cb) {
		let deviceStatusTemp = deviceStatus;
		let originalRequest = 1;
		if (deviceSpecificValue !== -1 && deviceStatusTemp !== 0) {
			deviceStatusTemp = deviceSpecificValue;
		}
		exports.sendCommand(moduleId, deviceId, deviceStatusTemp, originalRequest, function (error, result) {
			if (error) {
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};


	exports.sendCommand = function (moduleId, deviceId, deviceStatusTemp, originalRequest, cb) {
		cc2530Util.turnOnOffDevice(moduleId, deviceId, deviceStatusTemp, function (error, result) {
			if (error) {
				logger.error("Device util Change Device Error : " + error);
				cb(error, null);
			} else {
				if (originalRequest === 1) {
					constants.deviceStatusCache[moduleId + '' + deviceId] = {
						moduleId: moduleId,
						deviceId: deviceId,
						deviceStatus: deviceStatusTemp,
						counter: 0
					};
				}
				cb(null, result);
			}
		});
	};


	//update it later as per IR code and DB
	exports.IRCommand = function (moduleId, command, cb) {
		cc2530Util.IRCommand(moduleId, command, function (error, result) {
			if (error) {
				logger.error("Device util Change Device Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure New Device
	exports.configuredevice = function (cb) {
		cc2530Util.configuredevice(function (error, result) {
			if (error) {
				logger.error("configuredevice Device Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Temp Sensor
	exports.configureTempSensor = function (cb) {
		cc2530Util.configureTempSensor(function (error, result) {
			if (error) {
				logger.error("configure temperature Device Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Door Sensor
	exports.configureDoorsensor = function (cb) {
		//logger.info("configure door device util");
		cc2530Util.configureDoorsensor(function (error, result) {
			if (error) {
				logger.error("configure Door Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Gas Sensor
	exports.configureGasSensor = function (cb) {
		cc2530Util.configureGasSensor(function (error, result) {
			if (error) {
				logger.error("configure Door Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Co2 Sensor
	exports.configureCo2Sensor = function (cb) {
		cc2530Util.configureCo2Sensor(function (error, result) {
			if (error) {
				logger.error("configure Co2 Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Curtain Request
	exports.configureCurtainRequest = function (cb) {
		cc2530Util.configureCurtainRequest(function (error, result) {
			if (error) {
				logger.error("configure Door Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	}

	//Configure Smart Remote
	exports.configuresmartRemoteRequest = function (cb) {
		cc2530Util.configuresmartRemoteRequest(function (error, result) {
			if (error) {
				logger.error("configure Smart Remote Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};


	//Configure Multi Sensor Threshold Value
	exports.configureGasSensorThresholdValue = function (cb) {
		cc2530Util.configureGasSensorThresholdValue(function (error, result) {
			if (error) {
				logger.error("configure Gas Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};

	//Configure Multi Sensor Threshold Value
	exports.configureCo2SensorThresholdValue = function (cb) {
		cc2530Util.configureCo2SensorThresholdValue(function (error, result) {
			if (error) {
				logger.error("configure Co2 Sensor Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};


	//Configure New Device
	exports.configureRepeatorRequest = function (cb) {
		cc2530Util.configureRepeatorRequest(function (error, result) {
			if (error) {
				logger.error("configuredevice Device Error : " + error);
				cb(error, null);
			} else {
				cb(null, result);
			}
		});
	};


	//Configure Device Request
	exports.configureDeviceRequest = function (identifier, cb) {
		/**
		 * identifier: curtain | doorSensor | gasSensor | multiSensor | smartRemote | tempSensor | 
		 */

		let configureRequest = undefined;

		switch (identifier) {
			case 'curtain':
				configureRequest = cc2530Util.configureCurtainRequest;
				break;
			default:
				break;
		}
		if (configureRequest) return cb("Configure Request is undefined", null);
		configureRequest((error, result) => {
			if (error) {
				logger.error("configure Door Sensor Error : " + error);
				return cb(error, null);
			}
			cb(null, result);
		});
	}


	//From switchModuleStatus Checker
	exports.getSwitchModuleStatus = function (switchModuleID) {
		cc2530Util.getSwitchModuleStatus(switchModuleID);
	};

	exports.getSwitchModuleDevicesStatusDetails = function (switchModuleID, cb) {
		cc2530Util.getModuleStatus(switchModuleID);
	};

	exports.isSerialPortReady = function () {
		return cc2530Util.isSerialPortReady();
	};


	// Listener Added ----------------------------

	//Register For Individual Device ACK Command
	exports.registerDeviceStatusACKListener = function (deviceStatusListener) {
		cc2530Util.registerDeviceStatusACKListener(deviceStatusListener);
	};

	//Register For Get Service Statrt Device Status -- util/startUpEventListener
	exports.registerStartupDeviceStatusListener = function (physicalDeviceStatusListener) {
		cc2530Util.registerStartupDeviceStatusListener(physicalDeviceStatusListener);
	};

	//Register For Check Switch Module Status -- scanner/switchModuleStatusChecker
	exports.registerSwitchModuleStatusListener = function (switchModuleStatusListener) {
		cc2530Util.registerSwitchModuleStatusListener(switchModuleStatusListener);
	};

	//Register For Get Physical Device Status -- listener/deviceEventListener
	exports.registerPhysicalDeviceStatusListener = function (physicalDeviceStatusListener) {
		cc2530Util.registerPhysicalDeviceListener(physicalDeviceStatusListener);
	};

	//Register For Get Device Status - Callback -- listener/deviceEventListener

	exports.registerDeviceListenerCallBack = function (deviceStatusCallBackListener) {
		cc2530Util.registerDeviceListenerCallBack(deviceStatusCallBackListener);
	};

	//Register For Boot Up Switch Module Device Status -- listener/deviceEventListener
	exports.registerSwitchModuleBootDeviceListener = function (switchModuleBootDeviceListener) {
		cc2530Util.registerSwitchModuleBootDeviceListener(switchModuleBootDeviceListener);
	};

	//Register For Configure Device -- listener/deviceEventListener 
	exports.registerRouterSetupListener = function (routerSetupListener) {
		cc2530Util.registerRouterSetupListener(routerSetupListener);
	};

	//registerDeviceSetupListener
	exports.registerDeviceSetupListener = function(deviceSetuplistener){
		cc2530Util.registerDeviceSetupListener(deviceSetuplistener);
	}

	//---- Temperature Sensor
	//Register For Configure Temperature Device -- listener/deviceEventListener 
	exports.registerTempSensorSetupListener = function (TempDeviceSetupListener) {
		cc2530Util.registerTempSensorSetupListener(TempDeviceSetupListener);
	};

	//Register For Get Temp Device Status -- listener/tempEventListener
	exports.registerTempSensorStatusListener = function (tempSensorStatusListener) {
		cc2530Util.registerTempSensorStatusListener(tempSensorStatusListener);
	};

	//Register For Get Temp Voltage Status -- listener/tempEventListener
	exports.registerTempSensorVoltageListener = function (tempSensorVoltageListener) {
		cc2530Util.registerTempSensorVoltageListener(tempSensorVoltageListener);
	};


	//Register For Get Mains Voltage -- listener/tempEventListener
	exports.registerMainsVoltageListener = function (deviceVoltageListener) {
		cc2530Util.registerMainsVoltageListener(deviceVoltageListener);
	};


	//Register For Configure Door Sensor -- listener/deviceEventListener 
	exports.registerDoorSensorSetupListener = function (doorSensorSetupListener) {
		cc2530Util.registerDoorSensorSetupListener(doorSensorSetupListener);
	};

	//Register For Get Door Sensor Status -- listener/tempEventListener
	exports.registerDoorSensorStatusListener = function (deviceVoltageListener) {
		cc2530Util.registerDoorSensorStatusListener(deviceVoltageListener);
	};

	//Register For Get Door Sensor Voltage -- listener/deviceEventListener 
	exports.registerDoorSensorVoltageListener = function (doorSensorVoltageListener) {
		cc2530Util.registerDoorSensorVoltageListener(doorSensorVoltageListener);
	};

	//Register For IR Blaster -- listener/deviceEventListener 
	exports.registerIRBlasterSetupListener = function (IRBlasterSetupListener) {
		cc2530Util.registerIRBlasterSetupListener(IRBlasterSetupListener);
	};


	//Register For Smart Remote -- listener/deviceEventListener 
	exports.registerSmartRemoteSetupListener = function (smartRemoteSetupListener) {
		cc2530Util.registerSmartRemoteSetupListener(smartRemoteSetupListener);
	};

	//Register For Smart Remote -- listener/deviceEventListener 
	exports.registerSmartRemoteNumberListener = function (smartRemoteNumberListener) {
		cc2530Util.registerSmartRemoteNumberListener(smartRemoteNumberListener);
	};

	//Register For Configure Gas Sensor -- listener/deviceEventListener 
	exports.registerGasSensorSetupListener = function (gasSensorSetupListener) {
		cc2530Util.registerGasSensorSetupListener(gasSensorSetupListener);
	};

	//Register For Get Gas Sensor Status -- listener/gasSensorEventListener
	exports.registerGasSensorValueListener = function (gasSensorValueListener) {
		cc2530Util.registerGasSensorValueListener(gasSensorValueListener);
	};

	//Register For Get Heavy Load Values -- listener/heavyLoadEventListener
	exports.registerheavyLoadValueListener = function (heavyLoadValueListener) {
		cc2530Util.registerheavyLoadValueListener(heavyLoadValueListener);
	};

	//Register For Configure Repeator -- listener 
	exports.registerRepeatorSetupListener = function (repeatorSetupListener) {
		cc2530Util.registerRepeatorSetupListener(repeatorSetupListener);
	};

	//Register For Get Repeator Status -- listener
	exports.registerRepeatorStatusListener = function (repeatorStatusListener) {
		cc2530Util.registerRepeatorStatusListener(repeatorStatusListener);
	};

	//Register For Get Repeator Status -- listener
	exports.registerCurtainSetupListener = function (repeatorStatusListener) {
		cc2530Util.registerCurtainSetupListener(repeatorStatusListener);
	};
};