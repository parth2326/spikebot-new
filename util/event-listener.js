const eventsEmitter = require("./event-manager");
const deviceModel = require('./../models/device');
const roomModel = require('./../models/room');
const panelModel = require('./../models/panel');
const moodModel = require('./../models/mood');
const deviceUtil = require('./../util/device-util');
const userCounterModel = require('./../models/user_device_counter');
const util = require('./general-util').shared.util;
const socketio = util.socket_io;

// module.exports = function () {

// On Block User 
eventsEmitter.on('emitRoomPanelStatus', async function (params) {

    logger.info('[EVENT LISTENER] - EMIT ROOM PANEL STATUS', params);

    const device_id = params.device_id;
    const device_status = params.device_status;

    let panelList = await deviceModel.getPanelListWithMoodPanels(device_id);
    const roomList = [...new Set(panelList.map(item => item.room_id))];


    if (device_status == 1) {

        for (let panel of panelList) {
            // logger.info('[EVENT LISTENER] - EMIT PANEL STATUS', {
            //     panel_id: panel.panel_id,
            //     panel_status: 1
            // });

            socketio.emit('changePanelStatus', {
                panel_id: panel.panel_id,
                panel_status: 1
            });
        }

        for (let room of roomList) {
            // logger.info('[EVENT LISTENER] - EMIT ROOM STATUS', {
            //     room_id: room,
            //     room_status: 1
            // });
            socketio.emit('changeRoomStatus', {
                room_id: room,
                room_status: 1
            });
        }

    } else {
        let statusList = [];
        statusList.push(...roomList.map(p => async function () {
            return {
                "status": await roomModel.getRoomStatus(p),
                "type": "room_status",
                "id": p
            }
        }));
        statusList.push(...panelList.map(p => async function () {
            return {
                "status": await panelModel.getPanelStatus(p.panel_id),
                "type": "panel_status",
                "id": p.panel_id
            }
        }));

        const allStatusList = await Promise.all(statusList.map(p => p()));

        for (let status of allStatusList) {
            if (status.type == 'room_status') {

                // logger.info('[EVENT LISTENER] - EMIT ROOM STATUS', {
                //     room_id: status.id,
                //     room_status: status.status
                // });

                socketio.emit('changeRoomStatus', {
                    room_id: status.id,
                    room_status: status.status
                });
            } else {

                //  logger.info('[EVENT LISTENER] - EMIT PANEL STATUS', {
                //     panel_id: status.id,
                //     panel_status: status.status
                // });

                socketio.emit('changePanelStatus', {
                    panel_id: status.id,
                    panel_status: status.status
                });
            }
        }

    }



    // for (var i = 0; i < panelList.length; i++) {
    //     var panelPostStatus = {
    //         panel_id: panelList[i].panel_id,
    //         panel_status: device_status==1?1:await panelModel.getPanelStatus(panelList[i].panel_id)
    //     };
    //     socketio.emit('changePanelStatus', panelPostStatus);
    // }

    // for (var i = 0; i < roomList.length; i++) {
    //     socketio.emit('changeRoomStatus', {
    //         room_id: roomList[i],
    //         room_status: device_status==1?1:await roomModel.getRoomStatus(roomList[i])
    //     });
    // }

    let moodListOfDevice = await moodModel.getMoodListofDevice(device_id);
    
    const moodList = [...new Set(moodListOfDevice.map(item => item.mood_id))];
    if (device_status == 1) {
        for (let mood of moodList) {
            socketio.emit('changeMoodStatus', {
                mood_id: mood,
                mood_status: 1
            });
        }

    } else {

        let statusList = [];
        statusList.push(...moodList.map(p => async function () {
            return {
                "status": await moodModel.getMoodStatus(p),
                "id": p
            }
        }));

        const allStatusList = await Promise.all(statusList.map(p => p()));

        for (let mood of allStatusList) {
            socketio.emit('changeMoodStatus', {
                mood_id: mood.id,
                mood_status: mood.status
            });
        }

        // for (let mood of moodStatusList) {
        //     socketio.emit('changeMoodStatus', {
        //         mood_id: mood.mood_id,
        //         mood_status: mood.total_on_devices > 0 ? 1 : 0
        //     });
        // }
    }

    // var moodStatusList = await moodModel.getMoodsOfDeviceWithStatus(device_id);

    // for (let mood of moodStatusList) {
    //     socketio.emit('changeMoodStatus', {
    //         mood_id: mood.mood_id,
    //         mood_status: mood.total_on_devices > 0 ? 1 : 0
    //     });
    // }

});

/**
 * Emit Socket
 * @param device_id
 * @param user_id
 */
eventsEmitter.on('emitDeviceAndGeneralCounter', async function (params) {

    // Send individual device counter
    let deviceCounter = await userCounterModel.increaseCounter(params.user_id, params.device_id,params.log_sub_type);
    logger.info('DEVICE COUNTER : ', deviceCounter)
    if (params.log_sub_type && ["humidity_alert", 'temp_alert'].includes(params.log_sub_type)) {

        // update to total sum
        params.counter = deviceCounter;

        deviceCounter = await userCounterModel.getTotalUserDeviceCounter(params.user_id,params.device_id);   
        logger.info('SUB COUNTER SENT',params);
        socketio.to(params.user_id).emit('updateDeviceSubBadgeCounter', params);

        // let humidityCounter = 0;
        // let tempCounter = 0;

        // if (params.log_sub_type == 'humidity_alert') {
        //     humidityCounter = await userCounterModel.increaseCounter(params.user_id, params.device_id, params.log_sub_type);

        //     const humidityParams = { ...params };
        //     humidityParams.counter = humidityCounter;
        //     socketio.to(params.user_id).emit('updateDeviceSubBadgeCounter', humidityParams);
        // } else {

        //     tempCounter = await userCounterModel.increaseCounter(params.user_id, params.device_id, params.log_sub_type);

        //     const tempParams = { ...params };
        //     tempParams.counter = tempCounter;
        //     socketio.to(params.user_id).emit('updateDeviceSubBadgeCounter', tempParams);

        // }

    }

    params.counter = await userCounterModel.getTotalUserDeviceCounter(params.user_id,params.device_id);
    logger.info('FINAL COUNTER SENT',params);
    socketio.to(params.user_id).emit('updateDeviceBadgeCounter', params);

    // Total counter of all devices
    const totalCounter = await userCounterModel.getTotalCounterOfUser(params.user_id);
    logger.error('TOTAL COUNTER : ', totalCounter);
    socketio.to(params.user_id).emit('generalNotificationCounter', {counter:totalCounter ? totalCounter : 0,user_id:params.user_id});

});


/**
 * Emit Socket
 * @param topic
 * @param data
 */
eventsEmitter.on('emitSocket', function (params) {
    logger.info('[EVENT LISTENER] - SOCKET EMITTED', params.topic, params.data);
    socketio.emit(params.topic, params.data);
});

/**
 * Emit Socket
 * @param to
 * @param topic
 * @param data
 */
eventsEmitter.on('emitSocketTo', async function (params) {
    // logger.info('[EVENT LISTENER] - SOCKET EMITTED TO', params);
    socketio.to(params.to).emit(params.topic, params.data);
});

eventsEmitter.on('getRoomOnOffDetails', async function (allDeviceList) {

    const uniqIdentifier = [...new Set(allDeviceList.filter(function (item) {
        return ['fan', 'switch', 'heavyload'].includes(item.device_type) && item.module_type!='5f-wifi';
    }
    ).map(item => item.module_identifier))
    ];

    for (let mI of uniqIdentifier) {
        deviceUtil.getSwitchModuleStatus(mI);
    }

    const uniqIdentifierWifi = [...new Set(allDeviceList.filter(function (item) {
        return ['fan', 'switch', 'heavyload'].includes(item.device_type) && item.module_type=='5f-wifi';
    }
    ).map(item => item.module_identifier))
    ];

    for (let mI of uniqIdentifierWifi) {
        eventsEmitter.emit('emitMQTT',{
            topic:"/ESP32/"+mI,
            data:"DF"+mI+"02"
        })
    }

});

// }