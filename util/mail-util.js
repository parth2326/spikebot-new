const nodemailer = require("nodemailer");
const constants = require("./constants.js");
let adminEmail = constants.adminEmail;
let adminPassword  = constants.adminPassword;
let timerId;

module.exports.sendEmail = function (data) {
    transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: adminEmail,
            pass: adminPassword
        }
    });

    if (timerId) return;
    timerId = setTimeout(function () {
        clearTimeout(timerId);
        timerId = null;
    }, 1000);

    let errormailOptions = {
        from: adminEmail,
        to: adminEmail,
        subject: "Error in SpikeBot service: " + data.email,
        text: 'Error Found in User ' + data.email + " - " + data.home_controller_device_id + " at " + (new Date()) + "\n\n" +
            data.err_msg + "\n\n" +
            data.err_stack
    };

    let adminMailOptions = {
        from: adminEmail,
        to: adminEmail,
        subject: data.mood_name + " Mood Added by " + data.email,
        text: "Mood: " + data.mood_name + "\nName: " + data.first_name + data.last_name + "\nEmail: " + data.email + "\nTime: " + (new Date())
    };

    let userMailOptions = {
        from: adminEmail,
        to: data.userEmail,
        subject: "New Custom Mood: " + data.mood_name + " added",
        text: "Hi " + data.first_name + ",\n\nThank you for the recently added Custom mood <b>" + data.mood_name + "</b>. " + "We will publish your custom mood shortly in Alexa and Google Skill store.\n\nIf your custom mood does not meet acceptance criteria, or we have a question about your custom mood during the review process, we will notify you by using the email address associated with your account.\n\nThanks,\nSpikeBot Team"
    };

    let error_array = [errormailOptions];
    let mood_array = [adminMailOptions, userMailOptions];
   
    let params = data.type == constants.mail_type.error_in_code ? error_array : mood_array;

    for (let i = 0; i < params.length; i++) {
        transporter.sendMail(params[i], function (error, info) {
            if (error) {
                logger.error('Mail Sent Error: ', error);
            } else {
                //logger.info('Mail Sent Successfully');
            }
        });
    }
};



