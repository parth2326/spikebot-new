const unixUtil = require('./unix-util.js');
const network = require('network');
const exec = require('child_process').exec;

exports.getDHCPInfo = function (cb) {
	network.get_active_interface(function (error, ip) {
		if (error) {
			logger.error("Error while fetching DHCP info", error);
			cb(error, null);
		} else {
			exports.getIfaceIpAddresses(function (ipAddressArray) {
				let dhcpInfo = prepareDHCPInfo(ip, ipAddressArray);
				cb(null, dhcpInfo);
			});

		}
	});
};

exports.getIfaceIpAddresses = function (cb) {
	let totalIpFound = 0;
	let totalIfaces = 0;
	let ipAddressArray = [];

	let callCallback = function () {
		if (totalIfaces === totalIpFound) {
			//logger.error("ipAddressArray  ", ipAddressArray);			
			cb(ipAddressArray);
		}
	};

	let ethIndex = 0;
	let wlanindex = 0;
	unixUtil.listInerfaces(function (error, result) {
		////logger.info("listInerfaces error" , error, ". result " , result);	    
		if (result) {
			let ethIfaces = result.wired;
			let wlanIfaces = result.wlan;
			totalIfaces = ethIfaces.length + wlanIfaces.length;

			if (ethIfaces.length > 0) {
				for (let i = 0; i < ethIfaces.length; i++) {
					exports.getIfaceIpAddress(ethIfaces[i], function (error, ip) {
						if (error) {
							logger.error("Error while fetching eth IpAddress", error);
							totalIpFound = totalIpFound + 1;
							callCallback();
						} else {
							var property = "eth" + ethIndex;
							var obj = {};
							obj["iface"] = property;
							obj["ip"] = ip;
							ipAddressArray.push(obj);
							////logger.info("eth IpAddress ", totalIpFound, ip);							    
							totalIpFound = totalIpFound + 1;
							callCallback();
						}

						ethIndex = ethIndex + 1;
						if (totalIpFound === ethIfaces.length) {
							for (let j = 0; j < wlanIfaces.length; j++) {
								let tempWlanIface = wlanIfaces[j];
								exports.getIfaceIpAddress(tempWlanIface, function (error, ip) {
									if (error) {
										logger.error("Error while fetching wlan IpAddress", error);
										totalIpFound = totalIpFound + 1;
										callCallback();
									} else {
										var property = "wlan" + wlanindex;
										var obj = {};
										obj["iface"] = property;
										obj["ip"] = ip;
										ipAddressArray.push(obj);
										////logger.info("wlan IpAddress ", totalIpFound, ip);										    
										totalIpFound = totalIpFound + 1;
										callCallback();
									}
									wlanindex = wlanindex + 1;
								});
							}
						}
					});
				}

			} else if (wlanIfaces.length > 0) {
				for (let j = 0; j < wlanIfaces.length; j++) {
					let tempWlanIface = wlanIfaces[j];
					exports.getIfaceIpAddress(tempWlanIface, function (error, ip) {
						if (error) {
							logger.error("Error while fetching wlan IpAddress", error);
							totalIpFound = totalIpFound + 1;
							callCallback();
						} else {
							var property = "wlan" + wlanindex;
							var obj = {};
							obj["iface"] = property;
							obj["ip"] = ip;
							ipAddressArray.push(obj);
							// //logger.info("wlan IpAddress only", totalIpFound, ip);						    
							totalIpFound = totalIpFound + 1;
							callCallback();
						}
						wlanindex = wlanindex + 1;
					});
				}
			}
		}
	});
};


exports.getIfaceIpAddress = function (iface, cb) {
	let command = "ip addr show " + iface;
	let ipAddress = "192.168.0.11";
	exec(command, function (error, stdout, stderr) {
		////logger.info('getIfaceIpAddress stdout stderr error: ', iface, stdout, stderr, error);        
		cb(null, extractIpAddress(stdout));
	});
};


let extractIpAddress = function (str) {
	if (!str) {
		return "";
	}
	let inetIndex = str.indexOf("inet ");
	if (inetIndex === -1) {
		return "";
	}
	let str = str.substring(inetIndex + 5, str.length);
	let broadCastIndex = str.indexOf(" brd ");
	let ip = str.substring(0, broadCastIndex - 3);
	return ip;
};


let prepareDHCPInfo = function (ip, ipAddressArray) {
	return {
		ipAddressArray: ipAddressArray,
		net_mask: ip.netmask,
		gateway_address: ip.gateway_ip
	};
};