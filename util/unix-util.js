const exec = require('child_process').exec;
const fs = require('fs');

exports.listInerfaces = function (cb) {
    exec('sudo ifconfig', function (error, stdout, stderr) {
        ////logger.info('listInerfaces - error' , error , ' stdout ' , stdout, ' stderr ' , stderr);
        ////logger.info('createLisoOfIface(stdout) ' , createLisoOfIface(stdout));
        cb(null, createLisoOfIface(stdout));
    });
};

let createLisoOfIface = function (string) {
    return {
        wired: getAllWiredConnection(string),
        wlan: getAllWlanConnection(string),
        ra: getAllRaConnection(string)
    };
};

let getAllWiredConnection = function (string) {
    let ethConn = [];
    for (let i = 0; i < 5; i++) {
        if (string.indexOf("eth" + i) != -1) {
            ethConn.push("eth" + i);
        } else {
            break;
        }
    }
    return ethConn;
};

let getAllWlanConnection = function (string) {
    let wlanConn = [];
    for (let i = 0; i < 5; i++) {
        if (string.indexOf("wlan" + i) != -1) {
            wlanConn.push("wlan" + i);
        } else {
            break;
        }
    }
    return wlanConn;
};

var getAllRaConnection = function (string) {
    let raConn = [];
    /*for (var i = 0; i < 5; i++) {
        if (string.indexOf("ra" + i) != -1) {
            raConn.push("ra" + i);
        } else {
            break;
        }
    }*/
    return raConn;
};

exports.configureWiredIface = function (ipAddress, netMask, gatewayAddress, cb) {
    //logger.info("configureWiredIface ipAddress netMask gatewayAddress: ", ipAddress, netMask, gatewayAddress);

    let content = "auto lo" + "\n" +
        "iface lo inet loopback" + "\n\n" +
        "auto eth0" + "\n" +
        "iface eth0 inet static" + "\n" +
        "address " + ipAddress + "\n" +
        "netmask " + netMask + "\n" +
        "gateway " + gatewayAddress + "\n\n" +

        "auto wlan0" + "\n" +
        "allow-hotplug wlan0" + "\n" +
        "iface wlan0 inet manual" + "\n" +
        "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" + "\n" +
        "iface default inet dhcp";

    fs.writeFile("/etc/network/interfaces", content, function (error) {
        if (error) {
            logger.error("FATAL Error interface file write Error: ", error);
            cb(error, null);
        } else {
            ////logger.info("interface Contents written: ", content);            
            cb(null, true);
            exec("sudo /etc/init.d/networking restart", function (error, stdout, stderr) {
                //logger.info('configureWiredIface stdout: ', stdout);
                //logger.info('configureWiredIface stderr: ', stderr);
                //logger.info('configureWiredIface exec error: ', error);
            });
            setTimeout(function () {
                exec("sudo /etc/init.d/networking reload", function (error, stdout, stderr) {
                    //logger.info('configureWiredIface stdout: ', stdout);
                    //logger.info('configureWiredIface stderr: ', stderr);
                    //logger.info('configureWiredIface exec error: ', error);
                });
                setTimeout(function () {
                    exec("sudo reboot", function (error, stdout, stderr) {
                        //logger.info('reboot stdout: ', stdout);
                        //logger.info('reboot stderr: ', stderr);
                        //logger.info('reboot exec error: ', error);
                    });
                }, 10000);
            }, 30000);
        }
    });
};

exports.restartCameraService = function () {
    //logger.info('Restart Camera SRS Service');
    exec("sudo /etc/init.d/srs restart", function (error, stdout, stderr) {
        //logger.info('Camera Restarted!');
    });
};