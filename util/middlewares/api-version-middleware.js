const generalUtil = require('./../general-util');

exports.apiVersionMiddleware = async function (req, res, next) {
    try {
        let api_version = req.header('api_version');
        
        const allMobileVersion = generalUtil.minimum_mobile_version.map((x) => {
            return parseFloat(x, 10)
        })
        
        if (api_version == null || api_version == '') {
            next();
            return;        
        } else if (api_version && allMobileVersion.includes(parseFloat(api_version))) {
            next();
            return;   
        } else {
            logger.error('API VERSION MIDDLEWARE : API VERSION NOT SUPPORTED');
            return res.json({
                code: 504,
                message: "Please update Mobile application."
            });
        }
    } catch(error) {
        logger.error('API VERSION MIDDLEWARE : ', error);

        return res.json({
            code: 401,
            message: "Unauthorized Request"
        });
    }
}