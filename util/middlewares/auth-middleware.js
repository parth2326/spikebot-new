const loginModel = require('./../../models/login');
var validAuthKeys = [];
var authKeyData = {};

exports.authMiddleware = async function (req, res, next) {

    try {
        let authKey = req.header('Authorization');
        if (!authKey) {
            logger.error('AUTH MIDDLEWARE : AUTH KEY NOT PASSED ' + req.protocol + '://' + req.get('host') + req.originalUrl);
            return res.json({
                code: 401,
                message: "Unauthorized Request"
            });
        }

        authKey = authKey.split(" ")[1];

        if (validAuthKeys.includes(authKey)) {
            req.body.authenticatedUser = authKeyData[authKey];
            next();
            return;
        }

        // // const loginKey = await loginModel.getByAuthKey(req.body.user_id,req.body.phone_id);
        const loginKey = await loginModel.getByAuthKey(authKey);

        // logger.info('LOGIN KEY IN AUTH MIDDLEWARE',loginKey.authKey);

        if (!loginKey) {
            return res.json({
                code: 401,
                message: "Invalid Request"
            });
        }

        if (loginKey.status == '1') {

            validAuthKeys.push(authKey);
            authKeyData[authKey] = {
                user_id: loginKey.user_id,
                auth_key: authKey,
                admin: loginKey.admin.toString(),
                phone_id: loginKey.access_identifier,
                phone_type: loginKey.access_type
            };

            req.body.authenticatedUser = {
                user_id: loginKey.user_id,
                auth_key: authKey,
                admin: loginKey.admin.toString(),
                phone_id: loginKey.access_identifier,
                phone_type: loginKey.access_type
            };

            next();

        } else {
            return res.json({
                code: 401,
                message: "Unauthorized Request"
            });
        }

        // next();

    } catch (error) {

        logger.error('AUTH MIDDLEWARE : ', error);

        return res.json({
            code: 401,
            message: "Unauthorized Request"
        });

    }

};



exports.inactiveAuthKey = function (auth_key) {
    validAuthKeys = validAuthKeys.filter(function (item) {
        return item != auth_key;
    })
};


exports.inactiveAllUserKeys = function (user_id,exceptAuthKey=null) {

    const authKeyArray = Object.values(authKeyData);

    const authKeysOfUserId = authKeyArray.filter(function(item){
        return item.user_id == user_id
    }).map(item => item.auth_key);

    validAuthKeys = validAuthKeys.filter(function(item){
        if(exceptAuthKey){
            return item.includes(authKeysOfUserId) && item!=exceptAuthKey;
        }else{
            return item.includes(authKeysOfUserId)
        }
    });

};