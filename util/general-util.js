const dbManager = require('./db-manager');

let homeAutomationModule;
let appContext;
let shared;
let clientSocketObj;

exports.homeAutomationModule = homeAutomationModule;
exports.appContext = appContext;
exports.shared = shared;
exports.clientSocketObj = clientSocketObj;

exports.initiate = function(homeAutomationModule, appContext, shared, clientSocketObj) {

    // General Util : Defined to Free controllers from their dependencies
    this.homeAutomationModule = homeAutomationModule;
    this.appContext = appContext;
    this.shared = shared;
    this.dbManager = dbManager;
    this.clientSocketObj = clientSocketObj;
};

exports.getDeviceIdAndStatus = function(deviceType) {
    let params = undefined;
    switch (deviceType) {
        case 'curtain':
            params = {
                deviceId: '00',
                deviceStatusTemp: '00'
            };
            break;
        case 'temp_sensor':
            params = {
                deviceId: '10',
                deviceStatusTemp: '01'
            };
            break;
        case 'door_sensor':
            params = {
                deviceId: '10',
                deviceStatusTemp: '02'
            };
            break;
        case 'smart_remote':
            params = {
                deviceId: '10',
                deviceStatusTemp: '03'
            };
            break;
        case 'gas_sensor':
            params = {
                deviceId: '10',
                deviceStatusTemp: '04'
            };
            break;
        case 'water_detector':
            params = {
                deviceId: '10',
                deviceStatusTemp: '05'
            };
            break;
        case 'repeater':
            params = {
                deviceId: '10',
                deviceStatusTemp: '07'
            };
            break;
        case 'pir_device':
            params = {
                deviceId: '10',
                deviceStatusTemp: '08'
            };
            break;
        case 'pir_detector':
            params = {
                deviceId: '10',
                deviceStatusTemp: '09'
            }
            break;
        case 'co2_sensor':
            params = {
                deviceId: '10',
                deviceStatusTemp: '10'
            }
            break;

        default:
            params = {
                deviceId: '00',
                deviceStatusTemp: '00'
            };
            break;
    }
    return params;
}

exports.excludeFromInactiveStatus = function() {
    return ['ttlock', 'ir_blaster', 'smart_remote', 'remote', 'tt_lock','smart_bulb', 'energy_meter','5f-wifi'];
}

exports.excludeFromRemote =[ "turn_on", "turn_off", "channel_up", "channel_down", "volume_up", "volume_down", "arrow_left", "arrow_right", "arrow_top", "arrow_bottom", "mute", "ok", "av/tv"];

exports.sensorAlerts = function() {
    return ['ttlock', 'ir_blaster', 'smart_remote', 'remote', 'tt_lock', 'energy_meter'];
}

exports.alertTypes = {
    temp_sensor: {
        temperature: "temperature",
        humidity: "humidity"
    },
    door_sensor: {
        door_open_close: "door_open_close"
    },
    water_detector: {
        water_detected: "water_detected"
    },
    lock: {
        door_lock_unlock: "door_lock_unlock"
    }
}

exports.deviceTypes = {
    'pir_detector': {
        device_type: "pir_detector",
        device_sub_types: null,
        device_icon: "pir",
        device_default_sub_type: 'pir',
        device_name: "Motion Detector",
        meta: {}
    },
    'pir_device': {
        device_type: "pir_device",
        device_sub_types: ["pir", "normal"],
        device_icon: "pir",
        device_default_sub_type: 'pir',
        device_name: "Motion Device",
        meta: {}
    },
    'repeater': {
        device_type: "repeater",
        device_icon: "repeater",
        device_name: "Repeater",
        device_default_sub_type: 'repeater',
        meta: {},
    },
    'fan': {
        device_type: "fan",
        device_sub_types: ["dimmer", "normal"],
        device_icon: "fan",
        device_default_sub_type: 'dimmer',
        device_name: "Fan",
        meta: {}
    },
    'switch': {
        device_type: "switch",
        device_icon: "bulb",
        device_name: "Switch",
        device_default_sub_type: 'switch',
        meta: {}
    },
    'heavyload': {
        device_type: "heavyload",
        device_icon: "heavyload",
        device_name: "Heavy Load",
        device_default_sub_type: 'heavyload',
        meta: {}
    },
    'door_sensor': {
        device_type: "door_sensor",
        device_icon: "door_sensor",
        device_name: "Door Sensor",
        device_default_sub_type: 'door_sensor',
        meta: {}
    },
    'water_detector': {
        device_type: "water_detector",
        device_icon: "water_detector",
        device_name: "Water Detector",
        device_default_sub_type: 'water_detector',
        meta: {}
    },
    "temp_sensor": {
        device_type: "temp_sensor",
        device_icon: "temp_sensor",
        device_name: "Temp Sensor",
        device_default_sub_type: 'temp_sensor',
        meta: {
            "unit": "C"
        }
    },
    "gas_sensor": {
        device_type: "gas_sensor",
        device_icon: "gas_sensor",
        device_name: "Gas Sensor",
        device_default_sub_type: 'gas_sensor',
        meta: {}
    },
    "co2_sensor": {
        device_type: "co2_sensor",
        device_icon: "co2_sensor",
        device_name: "Co2 Sensor",
        device_default_sub_type: 'co2_sensor',
        meta: {}
    },
    "curtain": {
        device_type: "curtain",
        device_icon: "curtain",
        device_name: "Curtain",
        device_default_sub_type: 'curtain',
        meta: {}
    },
    "smart_remote": {
        device_type: "smart_remote",
        device_icon: "smart_remote",
        device_name: "Smart Remote",
        device_default_sub_type: 'smart_remote',
        meta: {}
    },
    "remote": {
        device_type: "remote",
        device_icon: "remote",
        device_name: "Remote",
        device_default_sub_type: 'remote',
        meta: {}
    },
    "jetson": {
        device_type: "jetson",
        device_icon: "jetson",
        device_name: "Smart Cam Device",
        device_default_sub_type: 'jetson',
        meta: {}
    },
    "ir_blaster": {
        device_type: "ir_blaster",
        device_icon: "ir_blaster",
        device_name: "IR Blaster",
        device_default_sub_type: 'ir_blaster',
        meta: {}
    },
    "energy_meter": {
        device_type: "energy_meter",
        device_icon: "energy_meter",
        device_name: "Energy Meter",
        device_default_sub_type: 'energy_meter',
        meta: {}
    },
    "beacon_scanner": {
        device_type: "beacon_scanner",
        device_icon: "beacon_scanner",
        device_name: "Beamer",
        device_default_sub_type: 'beacon_scanner',
        meta: {}
    },
    "yale_lock": {
        device_type: "lock",
        device_sub_types: ['yale_lock'],
        device_icon: "lock",
        device_name: "Lock",
        device_default_sub_type: 'yale_lock',
        meta: {}
    },
    "tt_lock": {
        device_type: "lock",
        device_sub_types: ['tt_lock'],
        device_icon: "lock",
        device_name: "Lock",
        device_default_sub_type: 'tt_lock',
        meta: {}
    },
    "tt_lock_bridge": {
        device_type: "tt_lock_bridge",
        device_sub_types: ['tt_lock_bridge'],
        device_icon: "tt_lock_bridge",
        device_name: "TT Lock Bridge",
        device_default_sub_type: 'tt_lock_bridge',
        meta: {}
    },
    "smart_bulb": {
        device_type: "smart_bulb",
        device_icon: "smart_bulb",
        device_sub_types: ['ifttt_philips_hue_light','ifttt_tp_link_kasa_light','ifttt_yee_light'],
        device_name: "Smart Bulb",
        device_default_sub_type: 'ifttt_philips_hue_light'
    },
    "smart_plug": {
        device_type: "smart_plug",
        device_icon: "smart_plug",
        device_sub_types: ['ifttt_tp_link_kasa_plug'],
        device_name: "Smart Plug",
        device_default_sub_type: 'ifttt_tp_link_kasa_plug'
    },
    "camera": {
        device_type: "camera",
        device_icon: "camera",
        device_sub_types: ["camera"],
        device_name: "Camera",
        device_default_sub_type: 'camera'
    },
    "beacon": {
        device_type: "beacon",
        device_icon: "beacon",
        device_sub_types: ["beacon"],
        device_name: "Radar",
        device_default_sub_type: 'beacon'
    }
}

exports.deviceMetaList = ['unit', 'device_default_status', 'device_brand', 'device_model', 'device_codeset_id', 'ir_blaster_id', 'battery_level'];
exports.deviceStatusList = ['device_status', 'device_sub_status'];

exports.getMetaQueries  = function (otherMetas = []) {
    let metaQueries = [];
    exports.deviceMetaList.push(...otherMetas);
    for (let i = 0; i < exports.deviceMetaList.length; i++) {
        metaQueries.push(`(SELECT meta_value from spike_general_meta where table_name="device" and table_id=d.device_id and meta_name="${exports.deviceMetaList[i]}") as meta_${exports.deviceMetaList[i]}`)
    }
    return metaQueries;
}

exports.getStatusQueries  = function (otherStatusList = []) {
    let statusQueries = [];
    if (exports.deviceStatusList.length > 0) {
        for (let i = 0; i < exports.deviceStatusList.concat(otherStatusList).length; i++) {
            statusQueries.push(`(SELECT status_value from spike_device_status where status_type="${exports.deviceStatusList[i]}" and device_id=d.device_id) as ${exports.deviceStatusList[i]}`)
        }
    }
    return statusQueries;
}

exports.getDeviceType = (moduleType, deviceId) => {

    logger.info('Genral Util Module Type', moduleType);
    logger.info('Genral Util Device ID', deviceId);

    if (moduleType == 3 && deviceId == 2) {
        //logger.info('Curtain');
        return "curtain";
    } else if (moduleType == 4 && deviceId == 2) {
        //logger.info('Yale Lock');
        return "yale_lock";
    } else if (moduleType == 10 && deviceId == "4") {
        // Gas Sensor
        //logger.info('Gas');
        return "gas_sensor";
    } else if (moduleType == 10 && deviceId == "10") {
        // Gas Sensor
        //logger.info('Gas');
        return "co2_sensor";
    } else if (moduleType == 10 && deviceId == 1) {
        // Temperature Sensor
        //logger.info('Temperature');
        return "temp_sensor";
    } else if (moduleType == 10 && deviceId == 3) {
        // Smart Remote 
        //logger.info('Smart Remote');
        return "smart_remote";
    } else if (moduleType == 10 && deviceId == 2) {
        // Door Sensor 
        //logger.info('Door');
        return "door_sensor";
    } else if (moduleType == 10 && deviceId == 5) {
        // Flood Sensor 
        //logger.info('Water detector');
        return "water_detector";
    } else if (moduleType == 10 && deviceId == "7") {
        // Repeater
        //logger.info('Repeater');
        return "repeater";
    } else if (moduleType == 0 && deviceId == "5") {
        // 5 module
        //logger.info('5');
        return "5";
    } else if (moduleType == 1 && deviceId == "5") {
        // 5 module with fan
        //logger.info('5F');
        return "5f";
    } else if (moduleType == 0 && deviceId == "1") {
        // heavy load with one device
        //logger.info('1HL');
        return "heavy_load"
    } else if (moduleType == 0 && deviceId == "2") {
        // heavy load with two device
        //logger.info('2HL');
        return "double_heavy_load";
    } else if (moduleType == 10 && deviceId == "8") {
        // PIR_device
        //logger.info('pir_device');
        return "pir_device";
    } else if (moduleType == 10 && deviceId == "9") {
        // PIR_detector
        logger.info('Get Device Type pir_detector');
        return "pir_detector";
    }
    return "";
}

exports.panelOrderComparator = function(panel1, panel2) {

    // -1 no chnage
    // 1 - change 
    if (panel1.panel_type == 'curtain' && panel2.panel_type == "sensor") {
        return 1;
    }

    if (panel1.panel_type == 'general' && panel2.panel_type == "sensor") {
        return -1;
    }

    if (panel1.panel_type == 'general' && panel2.panel_type == "curtain") {
        return -1;
    }

    if (panel1.panel_type == 'sensor' && panel2.panel_type == "general") {
        return 1;
    }

    if (panel1.panel_type == 'curtain' && panel2.panel_type == "general") {
        return 1;
    }

    if (panel1.panel_type == 'sensor' && panel2.panel_type == "curtain") {
        return -1;
    }

    return 0;
}

exports.panelOrderComparatorForHome = function(panel1, panel2) {

    // -1 no chnage
    // 1 - change 
    if (panel1.panel_type == 'general' && panel2.panel_type == "sensor") {
        return -1;
    }

    return 0;
}

// Devices That can be turn off and on
exports.onOffDeviceTypes = [
    this.deviceTypes.fan.device_type,
    this.deviceTypes.switch.device_type,
    this.deviceTypes.heavyload.device_type,
    this.deviceTypes.remote.device_type,
    this.deviceTypes.curtain.device_type,
    this.deviceTypes.smart_bulb.device_type,
    this.deviceTypes.smart_plug.device_type,
];

exports.getPanelType = (module_type) => {
    // curtain | gas_sensor | temp_sensor | smart_remote | door_sensor | repeater | 5 | 5f | heavy_load | double_heavy_load

    // TODO -> should be moved to constants
    const panelsTypes = {
        "5": "general",
        "5f": "general",
        "heavy_load": "general",
        "double_heavy_load": "general",
        "gas_sensor": "sensor",
        "co2_sensor": "sensor",
        "temp_sensor": "sensor",
        "smart_remote": "sensor",
        "door_sensor": "sensor",
        "water_detector": "sensor",
        "remote": "sensor",
        "lock": "sensor",
        "tt_lock": "sensor",
        "yale_lock": "sensor",
        "ir_blaster": "none",
        "energy_meter": "none",
        "repeater": "none", // => TODO new type
        "curtain": "curtain"
    }
    return panelsTypes[module_type];
}

exports.userMetaList = {
    home_controller_notification_enable: 'home_controller_notification_enable',
    temperature_sensor_notification_enable: 'temperature_sensor_notification_enable',
    door_sensor_notification_enable: 'door_sensor_notification_enable',
    gas_sensor_notification_enable: 'gas_sensor_notification_enable',
    co2_sensor_notification_enable: 'co2_sensor_notification_enable',
    camera_notification_enable: 'camera_notification_enable',
    water_detector_notification_enable: 'water_detector_notification_enable',
    lock_notification_enable: 'lock_notification_enable',
}

exports.deviceStatusMappingTypes = {
    temp_sensor: {
        temp_value: "temp_value",
        temp_battery: "temp_battery",
        temp_humidity: "temp_humidity"
    },
    water_detector: {
        water_detector_battery: "water_detector_battery",
        water_detected: "water_detected"
    },
    gas_sensor: {
        gas_detected: "gas_detected"
    },
    co2_sensor: {
        co2_detected: "co2_detected"
    },
    door_sensor: {
        door_open_close: "door_open_close",
        door_battery: "door_battery"
    },
    heavy_load: {
        heavy_load_voltage: "heavy_load_voltage",
        total_energy: "total_energy",
        real_power: "real_power",
        load_current: "load_current",
        load_voltage: "load_voltage"
    }
}

// exports.allGeneralNotificationsType = ['door_open','door_close','temp_alert','gas_detected','water_detected','door_lock','door_unlock', 'home_controller_active', 'home_controller_inactive', 'camera_active', 'camera_inactive'];
exports.allGeneralNotificationsType = ['alert_active', 'home_controller_active', 'home_controller_inactive', 'camera_active', 'camera_inactive'];


exports.minimum_mobile_version = ["1.0"];
exports.supported_module = ["5", "5f", "repeater", "door_sensor", "temp_sensor", "gas_sensor", "co2_sensor", "ir_blaster", "smart_remote", "water_detector", "double_heavy_load", "yale_lock", "pir_detector", "pir_device", "curtain", "beacon_scanner", "beacon", "remote", "smart_bulb"];
exports.pi_current_version = ["1.0"];

// return exports;