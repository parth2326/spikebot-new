const cc2531Constants = require('./cc2531Constants');
const math = require('mathjs');

exports.createPackat = function(command, moduleId, dataArray) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(command.value);
    let dataLength = exports.getHexValue(exports.getLength(dataArray));
    let packetDataHex = exports.getHexString(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex;
    return packet;
};

//update it later as per IR code and DB
exports.createPackat1 = function(command, moduleId, dataArray) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(command.value);
    let dataLength = exports.getHexValue(exports.getLength(dataArray));
    let packetDataHex = exports.getHexString1(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex;
    return packet;
};

exports.createPacketForYaleLockPinChange = function(command, moduleId, dataArray, pinCode) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(command.value);
    let dataLength = exports.getHexValue(exports.getLength(dataArray) + pinCode.match(/.{1,2}/g).length);
    let packetDataHex = exports.getHexString(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex + pinCode;
    return packet;
};


exports.getLength = function(stringArray) {
    if (isArray(stringArray)) {
        return stringArray.length;
    }
    return 0;
};

let isArray = function(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
};

exports.getHexString = function(stringArray) {
    // //logger.info('getHexString ', stringArray);
    let hexValueArray = exports.getHexArray(stringArray);
    let hexString = "";
    // //logger.info('hexValueArray.length ', hexValueArray.length);
    for (let i = 0; i < hexValueArray.length; i++) {
        hexString = hexString + hexValueArray[i];
    }
    return hexString;
};

exports.getHexString1 = function(stringArray) {
    //   //logger.info('getHexString ', stringArray);
    //var hexValueArray = exports.getHexArray(stringArray);
    let hexString = "";
    ////logger.info('hexValueArray.length ', stringArray.length);
    for (let i = 0; i < stringArray.length; i++) {
        hexString = hexString + stringArray[i];
    }
    return hexString;
};

exports.getHexArray = function(stringArray) {
    let hexValueArray = [];
    //  //logger.info('isArray ' , isArray);
    if (!isArray(stringArray)) {
        //logger.info('Not Array');
        return hexValueArray;
    }
    for (let i = 0; i < stringArray.length; i++) {
        hexValueArray.push(exports.getHexValue(stringArray[i]));
    }
    return hexValueArray;
};

exports.getHexValue = function(stringValue) {
    let hexValue = Number(stringValue).toString(16);
    // //logger.info('hexValue ', hexValue);
    if (hexValue === "NaN") {
        hexValue = "";
    }
    hexValue = hexValue.length === 1 ? "0" + hexValue : hexValue;
    hexValue = hexValue.toUpperCase();
    return hexValue;
};

exports.createThresholdPackat = function(moduleId, dataArray) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(54);
    let dataLength = '20'; //threshold value
    let packetDataHex = exports.getHexString(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex;
    return packet;
};

exports.createHeavyLoadPackat = function(moduleId, dataArray, device_id) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(56);
    //var dataLength = exports.getHexValue(exports.getLength(dataArray)); //threshold value
    let dataLength = device_id;
    let packetDataHex = exports.getHexString(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex;
    return packet;
};

exports.createGasValuePackat = function(moduleId, dataArray) {
    let packetFlag = exports.getHexValue(cc2531Constants.deepFlag);
    let packetModuleId = moduleId;
    let packetCommand = exports.getHexValue(64);
    let dataLength = exports.getHexValue(exports.getLength(dataArray)); //threshold value
    // var dataLength = device_id;
    let packetDataHex = exports.getHexString(dataArray);
    let packet = packetFlag + packetModuleId + packetCommand + dataLength + packetDataHex;
    return packet;
};