const cloudSocketClient = require('socket.io-client');
const logModel = require('./../models/logs');
const generalMetaModel = require('./../models/general_meta');
const userDeviceCounterModel = require('./../models/user_device_counter');
const userModel = require('./../models/user');
const firebaseUtil = require('./../util/firebase-util');
const moment = require('moment');


let socket = cloudSocketClient(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT, {
    reconnection: true,
    reconnectionDelay: 10000,
    reconnectionDelayMax: 15000,
    reconnectionAttempts: 99999
});

const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;

let disconnectTimer;
let firstConnect = true;
let disconnectionTime;
let is_connected = false;

socket.on('connect', async function () {

    console.log('connect to cloud');

    if (is_connected != true) {
        socket.emit('register-pi', {
            home_controller_device_id: home_controller_device_id
        });

        is_connected = true;

        if (firstConnect == true) {

            firstConnect = false;
            const userList = await userModel.list();
    
            for (let user of userList) {
    
                // check if the notification is enabled for that user
                const home_controller_notification_enable = await generalMetaModel.getMetaValue(user.user_id, 'home_controller_notification_enable', '1');
    
                // if the user notification is enabled for that users
                if (home_controller_notification_enable == '1') {
    
                    let message = user.first_name + ' ' + user.last_name + ', your SpikeBot Gateway is Connected!';
    
                    firebaseUtil.sendMessageNotification({
                        message: message,
                        user_id: user.user_id,
                        data: {}
                    });
    
    
                    logModel.add({
                        log_object_id: home_controller_device_id,
                        log_type: 'home_controller_active',
                        description: 'Home Controller'
                    }, {
                        user_id: user.user_id,
                        phone_id: "0000",
                        phone_type: "system"
                    });
    
    
                    userDeviceCounterModel.increaseCounter(user.user_id, home_controller_device_id);
    
                }
            }
    
        } else {
    
            clearTimeout(disconnectTimer);
    
            // if disconnection time is set and its before 15 minutes and s
            if (disconnectionTime && disconnectionTime.valueOf() < moment().subtract('15', 'minutes').toDate().valueOf()) {
    
                disconnectionTime = null;
    
                const userList = await userModel.list();
    
                for (let user of userList) {
    
                    // check if the notification is enabled for that user
                    const home_controller_notification_enable = await generalMetaModel.getMetaValue(user.user_id, 'home_controller_notification_enable', '1');
    
                    // if the user notification is enabled for that users
                    if (home_controller_notification_enable == '1') {
    
                        let message = user.first_name + ' ' + user.last_name + ', your SpikeBot Gateway is Connected!';
    
                        firebaseUtil.sendMessageNotification({
                            message: message,
                            user_id: user.user_id,
                            data: {}
                        });
    
    
                        logModel.add({
                            log_object_id: home_controller_device_id,
                            log_type: 'home_controller_active',
                            description: 'Home Controller'
                        }, {
                            user_id: user.user_id,
                            phone_id: "0000",
                            phone_type: "system"
                        });
    
    
                        userDeviceCounterModel.increaseCounter(user.user_id, home_controller_device_id);
    
                    }
                }
    
            } else {
                disconnectionTime = null;
            }
    
        }
    }

   





    // if (is_connected == false) {
    //     clearTimeout(disconnectTimer);
    //     clearTimeout(connectTimer);

    //     is_connected = true;
    //     connectTimer = setTimeout(async function () {

    //         // it means that system remained disconnected for less than 15 minutes
    //         if (disconnectionTime && ((new Date().valueOf()) - disconnectionTime) < 900000) {
    //             return;
    //         }

    //         // if(disconnectionTime && ((new Date().valueOf()) - disconnectionTime) < 1000){
    //         //     return;
    //         // }

    //         disconnectionTime = null;



    //     }, 900000);

    // }


    // // Register pi to connect to cloud
    // logger.error('BEFORE EVENT EMITTER');

    // logger.error('AFTER EVENT EMITTER');

    // socket.emit('register-pi', {
    //     home_controller_device_id: home_controller_device_id
    // });

    // When pi is di
    socket.on('disconnect', function () {

        console.log('disconnect to cloud');

        if (is_connected == true) {

            is_connected = false;

            clearTimeout(disconnectTimer);
            
            disconnectionTime = new Date();

            disconnectTimer = setTimeout(async function () {
                const userList = await userModel.list();

                for (let user of userList) {
                    // check if the notification is enabled for that user
                    const home_controller_notification_enable = await generalMetaModel.getMetaValue(user.user_id, 'home_controller_notification_enable', '1');

                    // if the user notification is enabled for that users
                    if (home_controller_notification_enable == '1') {
                        logModel.add({
                            log_object_id: home_controller_device_id,
                            log_type: 'home_controller_inactive',
                            description: 'Home Controller'
                        }, {
                            user_id: user.user_id,
                            phone_id: "0000",
                            phone_type: "system"
                        });

                        userDeviceCounterModel.increaseCounter(user.user_id, home_controller_device_id);


                    }
                }

            }, 900000);
            // 900000
            return;
        }




    });

});


module.exports = socket;