const schedule = require('node-schedule');
const moment = require('moment');
const moduleModel = require('./../models/module');
const deviceModel = require('./../models/device');
const deviceStatusModel = require('./../models/device_status');
const logModel = require('./../models/logs');
const scheduleModel = require('./../models/schedule');
const generalUtil = require('./../util/general-util');
const deviceController = require('./../controller/deviceStatusController');
const home_controller_device_id = require('./general-util').appContext.viewerDetails.deviceID;
const dbBackup = require('../util/db-backup');
const eventsEmitter = require("./../util/event-manager");

let scheduledJobs = [];

let j = schedule.scheduleJob('*/15 * * * *', async function (fireDate) {


    //logger.info('Schedule Manager | InActive Scheduler | Run At ', new Date());

    // Parth Code
    // let currentTimeStamp = moment().subtract(60, 'minutes').toDate().getTime();
    // let inActiveModules = await moduleModel.getModuleListByLastResponseTime(currentTimeStamp, generalUtil.excludeFromInactiveStatus());

    // for (let i = 0; i < inActiveModules.length; i++) {

    //     //logger.info('Schedule Manager | InActive Scheduler | Module is Marked Inactive', inActiveModules[i].module_id);
    //     await moduleModel.update(inActiveModules[i].module_id, { is_active: 'n' });

    //     const deviceList = await deviceModel.listByModuleId(inActiveModules[i].module_id);
    //     for(let device of deviceList){
    //         if(generalUtil.onOffDeviceTypes.includes(device.device_type)){
    //             deviceStatusModel.updateMany({
    //                 device_id:device.device_id,
    //                 device_status:"0"
    //             });
    //         }
    //     }

    //     // Emit Real Time Socket To Change Module Status
    //     eventsEmitter.emit('emitSocket', {
    //         topic: 'changeModuleStatus',
    //         data: {
    //             module_id: inActiveModules[i].module_id,
    //             is_active: 'n'
    //         }
    //     });

    // }

    let currentTimeStampForDoor = moment().subtract(20, 'minutes').toDate().getTime();
    let currentTimeStampForAllExceptDoor = moment().subtract(60, 'minutes').toDate().getTime();
    // let inActiveModules = await moduleModel.listByConfiguration();
    let inActiveModules = await moduleModel.getModuleListByLastResponseTime(new Date().valueOf(), generalUtil.excludeFromInactiveStatus());
    for (let i = 0; i < inActiveModules.length; i++) {
        if (inActiveModules[i].module_type === "door_sensor" && inActiveModules[i].last_response_time < currentTimeStampForDoor) {
            await moduleModel.update(inActiveModules[i].module_id, { is_active: 'n' });

            const deviceList = await deviceModel.listByModuleId(inActiveModules[i].module_id);
            for (let device of deviceList) {
                if (generalUtil.onOffDeviceTypes.includes(device.device_type)) {
                    deviceStatusModel.updateMany({
                        device_id: device.device_id,
                        device_status: "0"
                    });
                }
            }

            // Emit Real Time Socket To Change Module Status
            eventsEmitter.emit('emitSocket', {
                topic: 'changeModuleStatus',
                data: {
                    module_id: inActiveModules[i].module_id,
                    is_active: 'n'
                }
            });
        } else if (inActiveModules[i].last_response_time < currentTimeStampForAllExceptDoor) {
            await moduleModel.update(inActiveModules[i].module_id, { is_active: 'n' });

            const deviceList = await deviceModel.listByModuleId(inActiveModules[i].module_id);
            for (let device of deviceList) {
                if (generalUtil.onOffDeviceTypes.includes(device.device_type)) {
                    deviceStatusModel.updateMany({
                        device_id: device.device_id,
                        device_status: "0"
                    });
                }
            }

            // Emit Real Time Socket To Change Module Status
            eventsEmitter.emit('emitSocket', {
                topic: 'changeModuleStatus',
                data: {
                    module_id: inActiveModules[i].module_id,
                    is_active: 'n'
                }
            });
        }
    }

});

// Take DB backup at 12:30
let dbBackUpScheduler = schedule.scheduleJob('30 0 * * *', async function (fireDate) {
    dbBackup.uploadDbBackup(home_controller_device_id);
});

// Restart PI at 3:30
let restartPI = schedule.scheduleJob('30 3 * * *', async function (fireDate) {
    let exec = require('child_process').exec;
    exec("sudo reboot", function (error, stdout, stderr) {
        //logger.info('pi reboot exec stdout: ', stdout);
        //logger.info('pi reboot exec stderr: ', stderr);
        //logger.info('pi reboot exec error: ', error);
    });
});

scheduledJobs.push({
    schedule_id: "inactive-scheduler",
    schedule_type: "inactive_scheduler",
    schedule: j
});

// Initialize Schedules That Are in Database 
exports.init = async function () {

    let scheduleList = await scheduleModel.all();

    for (let i = 0; i < scheduleList.length; i++) {

        const currentSchedule = scheduleList[i];

        exports.addOrUpdateScheduledJob({
            schedule_id: currentSchedule.schedule_id,
            days: currentSchedule.schedule_days.split(","),
            on_time: currentSchedule.on_time,
            off_time: currentSchedule.off_time,
            schedule_type: currentSchedule.schedule_type
        });

    }

}

exports.init();

/**
 * To Add or Update Schedule Job To The Scheduler
 * @param schedule_id (Schedule Id of Database)
 * @param days (Days in array [0,1,2])
 * @param on_time (24 hr format 24:60)
 * @param off_time (24 hr format 24:60)
 * @param schedule_type (schedule / timer)
 */
exports.addOrUpdateScheduledJob = async function (params) {

    //logger.info('Schedule Manager | Add or Update Schedule ', params);

    if (params.schedule_type == null || params.schedule_type == 'schedule') {
        exports.addSchedule(params);
    } else if (params.schedule_type == 'timer') {
        exports.addTimer(params);
    }

}

/**
 * Privately Called By addOrUpdateScheduledJob
 */
exports.addSchedule = async function (params) {

    // Cancel the previous schedules
    await exports.checkIfScheduleExistsandDeactivate(params.schedule_id);

    // Create On Rule
    if (params.on_time != null) {

        let onRule = new schedule.RecurrenceRule();
        onRule.hour = params.on_time.split(":")[0];
        onRule.minute = params.on_time.split(":")[1];
        onRule.dayOfWeek = params.days;

        let onSchedule = schedule.scheduleJob(onRule, async function (schedule) {

            //logger.info('Schedule Manager |Executing On Schedule', schedule.schedule_id);
            exports.executeSchedule(schedule);

        }.bind(null, { schedule_id: params.schedule_id, schedule_type: "on" }));


        scheduledJobs.push({
            schedule_id: params.schedule_id,
            schedule_type: "on",
            schedule: onSchedule
        });

    }


    // Create Off Rule
    if (params.off_time != null) {

        let offRule = new schedule.RecurrenceRule();
        offRule.hour = params.off_time.split(":")[0];
        offRule.minute = params.off_time.split(":")[1];
        offRule.dayOfWeek = params.days;

        let offSchedule = schedule.scheduleJob(offRule, async function (schedule) {

            //logger.info('Schedule Manager |Executing Off Schedule', schedule.schedule_id);
            exports.executeSchedule(schedule);

        }.bind(null, { schedule_id: params.schedule_id, schedule_type: "off" }));

        scheduledJobs.push({
            schedule_id: params.schedule_id,
            schedule_type: "off",
            schedule: offSchedule
        });

    }

}

/**
 * Privately Called By addOrUpdateScheduledJob
 * @param schedule_id
 * @param on_time
 * @param off_time
 */
exports.addTimer = async function (params) {


    // Cancel the previous schedules
    await exports.checkIfScheduleExistsandDeactivate(params.schedule_id);

    // Create On Rule
    if (params.on_time) {

        let onRule = moment(params.on_time, 'YYYY-MM-DD H:mm:ss').toDate();
        //logger.info('Schedule Manager | New (ON) Timer Added On Date', onRule);

        let onSchedule = schedule.scheduleJob(onRule, async function (schedule) {
            //logger.info('Schedule Manager | Executing On Schedule', schedule.schedule_id);
            exports.executeSchedule(schedule);
        }.bind(null, { schedule_id: params.schedule_id, schedule_type: "on" }));

        scheduledJobs.push({
            schedule_id: params.schedule_id,
            schedule_type: "on",
            schedule: onSchedule
        });

    }

    // Create Off Rule
    if (params.off_time) {

        let offRule = moment(params.off_time, 'YYYY-MM-DD H:mm:ss').toDate();
        //logger.info('Schedule Manager | New (OFF) Timer Added On Date', onRule);

        let offSchedule = schedule.scheduleJob(offRule, async function (schedule) {

            //logger.info('Schedule Manager | Executing Off Schedule', schedule.schedule_id);
            exports.executeSchedule(schedule);

        }.bind(null, { schedule_id: params.schedule_id, schedule_type: "off" }));

        scheduledJobs.push({
            schedule_id: params.schedule_id,
            schedule_type: "off",
            schedule: offSchedule
        });

    }

}

exports.executeSchedule = async function (params) {

    let schedule_id = params.schedule_id;

    let currentSchedule = await scheduleModel.get(params.schedule_id);
    if (!currentSchedule) {
        return;
    }

    if (currentSchedule.is_active == 'n') {
        //logger.info('Schedule Manager | Schedule is inactive ', params.schedule_id);
        return;
    }


    if (params.schedule_type == "on") {

        let deviceList = await scheduleModel.getDeviceIdsByScheduleId(schedule_id);
        for (let i = 0; i < deviceList.length; i++) {
            deviceController.changeDeviceStatus({
                device_id: deviceList[i].device_id,
                device_status: 1,
                broadcast_room_panel_status: false,
                log: true,
                authenticatedUser: {
                    user_id: `${currentSchedule.schedule_name} - ${currentSchedule.schedule_type}`,
                    phone_id: '0000',
                    phone_type: 'spikebot'
                }
            }, function (err, response) {

            });
        }


        logModel.add({
            log_type: "schedule_on",
            log_object_id: params.schedule_id
        });


    } else if (params.schedule_type == "off") {

        let deviceList = await scheduleModel.getDeviceIdsByScheduleId(schedule_id);
        for (let i = 0; i < deviceList.length; i++) {
            deviceController.changeDeviceStatus({
                device_id: deviceList[i].device_id,
                device_status: 0,
                broadcast_room_panel_status: false,
                log: true,
                authenticatedUser: {
                    user_id: `${currentSchedule.schedule_name} - ${currentSchedule.schedule_type}`,
                    phone_id: '0000',
                    phone_type: 'spikebot'
                }
            }, function (err, response) {

            });
        }


        logModel.add({
            log_type: "schedule_off",
            log_object_id: params.schedule_id
        });


    }

    // Check if the schedule is timer. when everything of that time is completed.
    // we will have to disable that timer
    if (currentSchedule.schedule_type == 'timer') {
        let scheduleIndex = scheduledJobs.findIndex((item) => {
            return item.schedule_id == currentSchedule.schedule_id && item.schedule_type == params.schedule_type;
        });
        if (scheduleIndex >= 0) scheduledJobs.splice(scheduleIndex, 1);

        // Check if other type of schedule exists
        let otherScheduleOfSameId = scheduledJobs.findIndex((item) => {
            return item.schedule_id == currentSchedule.schedule_id;
        });

        if (otherScheduleOfSameId < 0) {
            await scheduleModel.update(currentSchedule.schedule_id, { is_active: 'n' });
        }

    }



}

/**
 * Check if schedule exists and then cancel the schedule
 * @param schedule_id
 */
exports.checkIfScheduleExistsandDeactivate = async function (schedule_id) {

    let scheduleList = scheduledJobs.filter((item) => item.schedule_id == schedule_id);
    for (let i = 0; i < scheduleList.length; i++) {
        try {
            if (scheduleList[i].schedule) {
                scheduleList[i].schedule.cancel();
            }
        } catch (error) {
            logger.info('Schedule Manager - [Cancel Schedule]', error);
        }
    }

}