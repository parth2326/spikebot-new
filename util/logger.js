    const path = require('path'),
        fs = require('fs'),
        winston = require('winston'),
        moment = require('moment');
    require('winston-daily-rotate-file');

    let datetime =  function () {
        return moment().format("DD-MM-YYYY hh:mm:ss:SSS A");
    };

    let logDirectory = path.join(__dirname, '../logs');
    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

    let logFormatter = function (options) {
        return '[' + datetime() + '] - [' + options.level.toUpperCase() + ']' +
            ' - ' + (options.message ? options.message : '') + (options.meta && Object.keys(options.meta).length ? '\t' + JSON.stringify(options.meta) : '');
    };

    let transport1 = new winston.transports.DailyRotateFile({
        filename: logDirectory + '/log',
        timestamp: datetime,
        formatter: logFormatter,       
        prepend: true,
        level: process.env.ENV === 'development' ? 'error' : 'error',
        json: false,
        localTime: true,
        zippedArchive: true
    });

    let transport2 = new winston.transports.Console({
        colorize: true,
        timestamp: datetime,
        formatter: logFormatter,        
        localTime: true,
        level: process.env.ENV === 'development' ? 'debug' : 'error',
    });

    let logger = new(winston.Logger)({
        transports: [
            transport1, transport2
        ]
    });

    module.exports = logger;