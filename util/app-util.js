const moment = require('moment');
const dns = require('dns');
const shortid = require('shortid');
const exec = require('child_process').exec;
let constants, sqliteDB, viewerDetails;
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const encryptKey = '123456';
const input_encoding = "utf8";
const output_encoding = "hex";

//Generate Token For Camera
exports.generateCameraToken = function (camera_id, cb) {
    let time = exports.currentDateTimeinSeconds();
    let secret_key = constants.mysecretkeylive;

    let command = "echo -n \"" + secret_key + "/livestream" + camera_id + "" + time + "\" | openssl dgst -md5 -binary | openssl enc -base64 | tr \'+/\' \'-_\' | tr -d \'=\'";

    exec(command, function (error, stdout, stderr) {
        if (stdout) {
            let append_token = "?e=" + time + "&st=" + stdout.trim();
            data = {
                camera_id: camera_id,
                append_token: append_token
            };
            let response = exports.createSuccessResponse(constants.responseCode.SUCCESS, data);
            return cb(null, response);
        } else {
            //logger.info("error in getting token");
            let response = exports.createErrorResponse(constants.responseCode.NO_MSG);
            return cb(null, response);
        }
    });
};

exports.setParams = function (homeAutomationModule, appContext, shared) {
    constants = homeAutomationModule.constants;
    sqliteDB = shared.db.sqliteDB;
    viewerDetails = appContext.viewerDetails;
};



exports.getTodayDay = function () {
    return moment().format('dddd');
};

exports.timeFormat = function () {
    return 'YYYY-MM-DD HH:mm:ss';
};

exports.generateTokenId = function () {
    return shortid.generate() + '' + shortid.generate();
};

exports.generateRoomId = function () {
    return shortid.generate();
};

exports.generateRandomId = function (prefix = '') {
    return prefix ?  prefix + "-"+new Date().getTime() + '_' + shortid.generate():new Date().getTime() + '_' + shortid.generate();
};

exports.currentDateTime = function () {
    return moment().format("YYYY-MM-DD HH:mm:ss");
};

exports.currentDateTimeinSeconds = function () {
    return new Date().getTime();
};

exports.timestamp = function () {
    return Date.now();
};

exports.getTimeDiffSeconds = function (time1, time2) {
    let momentTime1 = moment(time1),
        momentTime2 = moment(time2);
    return momentTime2.subtract(momentTime1).seconds();
};

exports.getDeviceOffTime = function (lastTurnOnTime, auto_off_timer) {
    let deviceAutoOffTime = getDeviceAutoOffTime(auto_off_timer);
    let hour = deviceAutoOffTime.hour;
    let minute = deviceAutoOffTime.minute;
    let second = deviceAutoOffTime.second;
    let timeToOffDevice = lastTurnOnTime.add(hour, 'hours').add(minute, 'minutes').add(second, 'seconds');
    return timeToOffDevice;
};

let getDeviceAutoOffTime = function (auto_off_timer) {
    let timeArray = auto_off_timer.split(":");
    let hour = parseInt(timeArray[0].toString());
    let minute = parseInt(timeArray[1].toString());
    let second = "00";
    if (timeArray[2]) {
        second = parseInt(timeArray[2].toString());
    }
    return {
        hour: hour,
        minute: minute,
        second: second
    };
};

exports.createResponse = function (code, message, data) {
    return {
        code: code,
        message: message,
        data: data
    };
};

exports.createErrorResponse = function (responseCode, data) {
    return exports.createResponse(responseCode.code, responseCode.message, data);
};

exports.createSuccessResponse = function (responseCode, data) {
    return exports.createResponse(responseCode.code, responseCode.message, data);
};

exports.isCloudSyncEnabled = function () {
    return constants.isCloudSyncEnabled === constants.cloudSync.enabled;
};

exports.isLocalTurn = function () {
    return constants.currentTurn === constants.cloudSyncTurn.local;
};

exports.checkInternet = function (cb) {
    dns.lookup("8.8.8.8", function (error) {
        if (error) {
            logger.error("Error while checking internet connection. Error: ", error);
            //logger.info("Checking internet connection again");
            dns.lookup("8.8.8.8", function (error) {
                if (error) {
                    logger.error("Error while checking internet connection again. Error: ", error);
                    cb(false);
                } else {
                    cb(true);
                }
            });
        } else {
            cb(true);
        }
    });
};


exports.encryptPassword = function (password) {
    let cipher = crypto.createCipher(algorithm, encryptKey);
    let crypted = cipher.update(password, input_encoding, output_encoding);
    crypted = crypted + cipher.final(output_encoding);
    return crypted;
};

exports.decryptPassword = function (password) {
    let decipher = crypto.createDecipher(algorithm, encryptKey);
    let dec = decipher.update(password, output_encoding, input_encoding);
    dec = dec + decipher.final(input_encoding);
    return dec;
};

exports.checkPushNotificationPlateform = function (device_arn) {
    if (device_arn.includes("GCM")) {
        return constants.anroid_live;
    } else if (device_arn.includes("APNS_SANDBOX")) {
        return constants.ios_dev;
    } else if (device_arn.includes("APNS")) {
        return constants.ios_live;
    } else {
        //logger.info('Not Matched');
        return 1;
    }
};

exports.getUserName = function (user_id, cb) {
    return new Promise((resolve, reject) => {
        let findusername = sqliteDB.prepare(constants.GET_USER_NAME_BY_ID);
        findusername.all(user_id, function (error, result) {
            findusername.finalize();
            if (error) {
                logger.error("User Name Not Found!");
                return cb ? cb(0) : resolve(0);
            } else {
                userName = result.length ? result[0].user_name : 0;
                return cb ?  cb(userName) : resolve(userName);
            }
        });
    })
};


exports.getTimeInMinutes = function (time) {
    //logger.info("time", time);
    let timearray = time.split(":"),
        inMinutes = timearray[0] * 60 + timearray[1];

    return inMinutes;
};