const FCM = require('fcm-node');
let fcm = new FCM(process.env.FCM_SERVERKEY);
let prefix = process.env.FCM_TOPIC_PREFIX;

let _firebaseNotificationService = {};
_firebaseNotificationService.sendNotificationToTopic = async function(topic, notification_type, data) {

    let message = {};

    if (notification_type == "message") {
        message = data;
        message.to = '/topics/' + prefix + "-" + topic;
    }

    message.priority = 'high';

    fcm.send(message, function(err, response) {
        
    });

}

/**
 * Send Notification
 * @param title
 * @param message
 * @param data
 * @param badge_count
 * @param user_id
 */
_firebaseNotificationService.sendMessageNotification = function(params) {
    logger.info('Notification Data : ', params);
    let notificationData = {
        notification: {
            body: params.message,
            data: params.data ? params.data : null,
            sound: "default",
            badge: params.badge_count ? params.badge_count : 1
        }
    };

    _firebaseNotificationService.sendNotificationToTopic(params.user_id, "message", notificationData);

}


_firebaseNotificationService.subscribeToTopic = function(token, user_id) {
    fcm.subscribeToTopic([token], process.env.FCM_TOPIC_PREFIX + "-" + user_id, (err, res) => {});
}

_firebaseNotificationService.unsubscribeToTopic = function(token, user_id) {
    fcm.unsubscribeToTopic([token], process.env.FCM_TOPIC_PREFIX + "-" + user_id, (err, res) => {
        console.log(res);
    });
}

module.exports = _firebaseNotificationService;