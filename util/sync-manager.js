const Client = require('node-rest-client').Client;
const restClient = new Client();
const dbManager = require('./db-manager');
const userModel = require('./../models/user');
const logger = require('./logger');
const home_controller_device_id = require('./general-util').appContext.viewerDetails.deviceID;
let syncManager = {};
syncManager.excludedTables = ['mst_icons', 'spike_log_categories', 'spike_migrations'];
syncManager.excludedTablesForDelete = ['spike_logs', 'spike_device_status_mapping', 'mst_home_controller_list'];
// syncManager.reverseSyncTables = ['spike_login'];

/**
 * Send Primary Request To Server
 */
syncManager.sendRequestToServer = async (data, url) => {

    return new Promise((resolve, reject) => {

        let args = {
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        };

        try {
            // logger.info('SYNC MANAGER REQ TO SERVER 1', process.env.CLOUD_URL, process.env.CLOUD_PORT);
            restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/" + url, args, async function (data, response) {
                if (data.code == 200) {
                    let updateList = data.data.data;
                    let updatedIds = await updateList.map(item => item[syncManager.tablePrimaryKeys[data.data.table_name]]);
                    
                    // if(data.data.table_name=='spike_logs'){
                    //     await dbManager.executeNonQuery(`DELETE FROM ${data.data.table_name} WHERE is_sync=2 AND ${syncManager.tablePrimaryKeys[data.data.table_name]} IN ('${updatedIds.join("','")}') AND log_type NOT IN ('alert_active','home_controller_active','home_controller_inactive')`, []);
                    // } else {
                    //     await dbManager.executeNonQuery(`UPDATE ${data.data.table_name} SET is_sync=1 WHERE is_sync=2 AND ${syncManager.tablePrimaryKeys[data.data.table_name]} IN ('${updatedIds.join("','")}')`, []);
                    // }
                    await dbManager.executeNonQuery(`UPDATE ${data.data.table_name} SET is_sync=1 WHERE is_sync=2 AND ${syncManager.tablePrimaryKeys[data.data.table_name]} IN ('${updatedIds.join("','")}')`, []);
                    
                    resolve(true);
                }
            });


        } catch (error) {
            resolve(false);
        }

    })

}


syncManager.sendDeleteRequestToServer = async (data) => {

    return new Promise((resolve,reject) => {

        let args = {
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        };
    
        logger.info('SYNC MANAGER DEL TO SERVER 1');

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/sync/delete", args, async function (data, response) {
            if (data.code == 200) {
                let updateList = data.data;
                updatedIds = await updateList.map(item => item.table_id);
                await dbManager.executeNonQuery(`UPDATE spike_table_delete_history SET is_sync=1 WHERE table_id IN ('${updatedIds.join("','")}')`, []);
                return resolve(true);
            }
            resolve(false);
        });

        logger.info('SYNC MANAGER DEL TO SERVER 2');

    });

    
}


/**

 * Sync User Data From Local To Live
 * Is Sync 0 Means there is some change and is not synced with live
 * Is Sync 1 Means Opposite of the above statement
 */
syncManager.syncUser = async () => {

    const userList = await dbManager.all('select * from mst_user where is_sync=?', [0]);

    for (let user of userList) {

        let args = {
            data: user,
            headers: {
                "Content-Type": "application/json"
            }
        };

        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/checkuser", args, async function (data, response) {
            if (data.code == 200) {
                userModel.update(data.data.user_id, {
                    is_sync: 1
                });
            }
        });

    }
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Update other tables
 */
syncManager.updateAllTables = async () => {

    // process.nextTick(async function () {

    let tableList = Object.keys(syncManager.tablePrimaryKeys);

    for (table of tableList) {

        await timeout(60000);

        let requestData = {};
        requestData.home_controller_device_id = home_controller_device_id; // TOGO :  get from util
        requestData.table_name = table;
        requestData.table_key = syncManager.tablePrimaryKeys[table];

        // logger.info('SYNC MANAGER - [GETTING TABLE DATA] - ', table);
        requestData.data = await syncManager.getSyncDataOfTable(table);

        // Do not send request to server if not needed
        if (requestData.data.length == 0) {
            continue;
        }

        await syncManager.sendRequestToServer(requestData, 'sync/update');

    }

    // Send Delete Request As Well
    let deleteRequestData = {};
    deleteRequestData.home_controller_device_id = home_controller_device_id; // TOGO :  get from util
    deleteRequestData.data = await syncManager.getDeletedEntries(table);

    // Do not send request to server if not needed
    if (deleteRequestData.data.length > 0) {
        await syncManager.sendDeleteRequestToServer(deleteRequestData);
    }

    syncManager.updateAllTables();

    // });

}

syncManager.updateCameraTables = async () => {

    // process.nextTick(async function () {

    let tableList = Object.keys(syncManager.tablePrimaryKeys);

    for (table of tableList) {

        if (!(table.includes('camera') || table.includes('jetson'))) {
            continue;
        }

        let requestData = {};
        requestData.home_controller_device_id = home_controller_device_id; // TOGO :  get from util
        requestData.table_name = table;
        requestData.table_key = syncManager.tablePrimaryKeys[table];

        logger.info('SYNC MANAGER - [GETTING TABLE DATA] - ', table);
        requestData.data = await syncManager.getSyncDataOfTable(table);

        // Do not send request to server if not needed
        if (requestData.data.length == 0) {
            continue;
        }

        await syncManager.sendRequestToServer(requestData, 'sync/update');

    }

    // Send Delete Request As Well
    let deleteRequestData = {};
    deleteRequestData.home_controller_device_id = home_controller_device_id; // TOGO :  get from util
    deleteRequestData.data = await syncManager.getDeletedEntries(table);

    // Do not send request to server if not needed
    if (deleteRequestData.data.length > 0) {
        await syncManager.sendDeleteRequestToServer(deleteRequestData);
    }

    // syncManager.updateAllTables();

    // });

}


/**
 * Update only one table
 * @param home_controller_device_id
 * @param table_name
 * @param table_key
 */
syncManager.updateTable = async (table) => {

    let requestData = {};
    requestData.home_controller_device_id = home_controller_device_id; // TOGO :  get from util
    requestData.table_name = table;
    requestData.table_key = syncManager.tablePrimaryKeys[table];
    requestData.data = await syncManager.getSyncDataOfTable(table);

    // For Delete sync
    // if (!syncManager.excludedTablesForDelete.includes(table)) {
    //     requestData.all_ids = await syncManager.getAllIds(table, syncManager.tablePrimaryKeys[table]);
    // }

    await syncManager.sendRequestToServer(requestData, 'sync/update');
}

/**
 * Get Sync Data of Table
 * @param table_name
 */
syncManager.getSyncDataOfTable = async (table_name) => {

    let data = await dbManager.all(`SELECT * FROM ${table_name} WHERE is_sync IN ('0','2')`, []);
    // mark the sync data as table sync in process
    dbManager.executeNonQuery(`UPDATE ${table_name} SET is_sync='2' WHERE is_sync='0'`, []);
    return data;
}

/**
 * Get Deleted Entries From the List
 */
syncManager.getDeletedEntries = async () => {
    return await dbManager.all(`SELECT * FROM spike_table_delete_history WHERE is_sync IN ('0','2') and table_name NOT IN ('spike_logs')`, []);
}

/**
 * Get Sync Data of Table
 * @param table_name
 */
syncManager.getAllIds = async (table_name, primary_key) => {
    let data = await dbManager.all(`SELECT ${primary_key} FROM ${table_name}`, []);
    return data.map(item => item[primary_key]);
}


/**
 * Get Sync Data of Table
 * @param table_name
 */
syncManager.getTotalCount = async (table_name) => {
    let data = await dbManager.get(`SELECT count(0) as total_entries FROM ${table_name}`, []);
    return data.total_entries;
}

syncManager.resetAllSyncs = async () => {
    let tableList = Object.keys(syncManager.tablePrimaryKeys);
    for (let table of tableList) {
        if (table != 'spike_logs') {
            await dbManager.executeNonQuery(`UPDATE ${table} set is_sync=0`, []);
        }

    }
}


syncManager.tablePrimaryKeys = {
    "camera_notification_mapping": 'mapping_id',
    "mst_camera_child_priviledge": 'priviledge_id',
    'mst_camera_devices': 'camera_id',
    'mst_camera_notification': 'camera_notification_id',
    'mst_home_controller_list': 'home_controller_device_id',
    'mst_mood_list': 'mood_id',
    'mst_user': 'user_id',
    'spike_alert_config': 'alert_config_id',
    'spike_login': 'login_id',
    'spike_alerts': 'alert_id',
    'spike_device_status': 'device_status_id',
    'spike_device_status_mapping': 'mapping_id',
    'spike_devices': 'device_id',
    'spike_general_meta': 'meta_id',
    'spike_jetson': 'jetson_id',
    'spike_logs': 'log_id',
    'spike_modules': 'module_id',
    'spike_mood_devices': 'mood_device_mapping_id',
    'spike_panel_devices': 'panel_device_id',
    'spike_panels': 'panel_id',
    'spike_rooms': 'room_id',
    'spike_schedule_devices': 'schedule_device_mapping_id',
    'spike_schedules': 'schedule_id'
}

module.exports = syncManager;