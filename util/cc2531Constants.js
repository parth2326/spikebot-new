//configure : 34 (52)
/* 
    Temp : 0A (10)  : 01
    Door : 0A (10)  : 02
*/

// Zigbee sending and receiving constants details.

let constants = {
    deepFlag: "223", // Hex DF
    command: {


        ROUTER_DATA: {
            name: "Register Router details",
            value: "52",
            code: 52
        },

        RESPONSE_ROUTER_DATA: {
            name: "Response Router details",
            value: "53",
            code: 53
        },

        SET: {
            name: "On Off Status CallBack",
            value: "01",
            code: 1
        },

        GET_ALL: {
            name: "Get Module Status",
            value: "02",
            code: 2
        },

        GET: {
            name: "Get Device Status",
            value: "92",
            code: 92
        },

        SET_ALL: {
            name: "Set Module Status",
            value: "03",
            code: 3
        },

        RESET_STB: {
            name: "Reset PI module",
            value: "04",
            code: 4
        },

        ACK: {
            name: "Check module status",
            value: "05",
            code: 5
        },

        PHY_STATUS: {
            name: "Received switch status",
            value: "06",
            code: 6
        },

        STB_STATUS: {
            name: "Get Pi status", // PI
            value: "07",
            code: 7
        },

        DOOR_SENSOR_STATUS: {
            name: "Received Door sensor status",
            value: "08",
            code: 8
        },

        WATER_SENSOR_STATUS: {
            name: "Received Water sensor status",
            value: "161",
            code: 161
        },

        MOTION_SENSOR_STATUS: {
            name: "Received motion sensor status",
            value: "09",
            code: 9
        },

        LOCK_DEVICE: {
            name: "Lock device",
            value: "10",
            code: 10
        },

        BOOT_STATUS: {
            name: "Received switch module boot",
            value: "11",
            code: 11
        },

        CONFIG: {
            name: "Check Module",
            value: "80",
            code: 80
        },

        GET_STB_ID: {
            name: "Get Smart Box ID",
            value: "12",
            code: 12
        },

        BROADCAST_STB_ID: {
            name: "Broadcast Smart Box ID",
            value: "13",
            code: 13
        },

        DOOR_SENSOR_ACK: {
            name: "Check door sensor status",
            value: "14",
            code: 14
        },

        CURTAIN_ACK: {
            name: "Curtain ack status",
            value: "15",
            code: 15
        },

        IR_SENSOR_COMMAND: {
            name: "Check IR sensor status",
            value: "20",
            code: 20
        },

        TEMP_SENSOR_STATUS: {
            name: "Check temp sensor status",
            value: "22",
            code: 22
        },

        TEMP_BATTERY_VOLTAGE: {
            name: "Check temp sensor voltage",
            value: "23",
            code: 23
        },

        WATER_SENSOR_BATTERY_VOLTAGE: {
            name: "Check water sensor voltage",
            value: "23",
            code: 23
        },

        DOOR_BATTERY_VOLTAGE: {
            name: "Check temp sensor voltage",
            value: "23",
            code: 23
        },

        DEVICE_VOLTAGE: {
            name: "Check device voltage",
            value: "25",
            code: 25
        },

        SENSOR_CONFIGURE: {
            name: "Configure Sensor",
            value: "52",
            code: 52
        },

        SMART_REMOTE_DATA: {
            name: "Smart Remote Data",
            value: "51",
            code: 51
        },

        GAS_SENSOR_DATA: {
            name: "Gas Sensor details",
            value: "55",
            code: 55
        },

        HEAVY_LOAD_DATA: {
            name: "Heavy Load details",
            value: "56",
            code: 56
        },

        REPEATOR_DATA: {
            name: "Heavy Load details",
            value: "57",
            code: 57
        },

        YALE_LOCK_UNLOCK: {
            name: "Yale Lock Unlock",
            value: "58",
            code: 58
        },

        CHANGE_PIR_DEVICE: {
            name: "Change PIR Device Mode/Status",
            value: "59",
            code: 59
        },

        CHANGE_PIR_DETECTOR: {
            name: "Change PIR Detector",
            value: "60",
            code: 60
        },

        CO2_SENSOR_DATA: {
            name: "Co2 Sensor details",
            value: "61",
            code: 61
        },
        
    }
};
module.exports = constants;