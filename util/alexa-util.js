const mailUtil = require('./mail-util.js');
const fs_extra = require('fs-extra');
const constants = require("./constants.js");
let fs = shared.util.fs;

const exec = require('child_process').exec;

const skill_id = 'amzn1.ask.skill.48fb8350-737c-4309-b26d-9ebdc1e608d5';
const locale = ['en-IN', 'en-US', 'en-CA', 'en-AU', 'en-GB'];


module.exports.addtoAlexaSkill = function (params) {
    //logger.info("into addtoAlexaSkill", params);
    let mood_name = params.mood_name;    

    exec("sudo ask api get-model -s " + skill_id + " --stage development --locale en-IN > alexaskill.json", function (error, stdout, stderr) {
        //logger.info('Get Interaction model stdout: ', stdout);
        //logger.info('Get Interaction model stderr: ', stderr);
        //logger.info('Get Interaction model error: ', error);
    });
    
    setTimeout(() => {

        let original_format = "            }\n" +
            "          ],\n" +
            "          \"name\": \"rooms\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}";

        let replacement_format = "            },\n" +
            "            {\n" +
            "              \"name\": {\n" +
            "                \"value\": \"" + mood_name + "\",\n" +
            "                \"synonyms\": [\n" +
            "                  \"" + mood_name + "\"\n" +
            "                ]\n" +
            "              }\n" +
            "            }\n" +
            "          ],\n" +
            "          \"name\": \"rooms\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}";

        fs.readFile('/home/pi/node/homeController/alexaskill.json', 'utf8', function (err, data) {
            if (err) {
                logger.error("FATAL Error file read Error: ", error);
                cb(error, null);
            } else {
                //logger.info("file read");
                let replacement = data.replace(original_format, replacement_format);
                fs.writeFile('/home/pi/node/homeController/alexaskill.json', replacement, function (error) {
                    if (error) {
                        logger.error("FATAL Error Updating file write Error: ", error);
                        cb(error, null);
                    } else {
                        //logger.info("file written");
                        mail_data = {
                            mood_name: mood_name,
                            email: params.email,
                            first_name: params.first_name,
                            last_name: params.last_name,
                            userEmail : params.userEmail,
                            type: constants.mail_type.custom_mood_add
                        };
                         mailUtil.sendEmail(mail_data);
                    }
                });
            }
        });

        for (let i = 0; i < locale.length; i++) {
            // var update_interaction_model = "ask api update-model --skill-id '" + skill_id + "' --file /node/homeController/alexaskill.json --locale '" + locale[i] + "' --stage development --debug";
                 exec("ask api update-model --skill-id " + skill_id + " --file /home/pi/node/homeController/alexaskill.json --locale " + locale[i] + " --stage development", function (error, stdout, stderr) {
                //logger.info('update interaction model stdout: ', stdout);
                //logger.info('update interaction model stderr: ', stderr);
                //logger.info('update interaction model error: ', error);
            });
        }
    }, 20000);




};