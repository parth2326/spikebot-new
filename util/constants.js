let constants = {

	profile_vpn_port: '',
	camera_vpn_port: '',
	home_directory_path: '/home/pi/node/homeController',


	// GET_NEW_DEVICES_LIST: "select mr.room_id, mr.room_name, mr.room_status, mr.room_order, mr.home_controller_device_id, mp.panel_id, mp.panel_name, mp.panel_order, mp.panel_status, " +
	// 	"rdm.room_device_id, rdm.module_id, rdm.device_id, rdm.device_name, rdm.device_icon, " + "rdm.device_status, rdm.device_specific_value, rdm.device_type, rdm.is_active, rdm.is_alive " + "from mst_room as mr inner join mst_panel as mp ON mr.room_id = mp.room_id " +
	// 	" INNER JOIN room_device_mapping as rdm ON mp.panel_id = rdm.panel_id" +
	// 	" WHERE mr.is_active = '1' and mr.home_controller_device_id = ?" +
	// 	" ORDER BY mr.id ASC, mp.id ASC, rdm.device_id ASC",

	//=========== For Alexa and google ============
	ACCESS_TOKEN_LIFETIME: 3600,
	REFRESH_TOKEN_LIFETIME: 3600 * 24 * 30,
	USER_DETILS: "SELECT user_id, user_email FROM mst_user WHERE user_id=?",


	//============ mst_login ============
	INSERT_LOGIN_DETAILS: "INSERT INTO mst_login (token_id, device_id, device_type, user_id, created_by, created_date) VALUES (?,?,?,?,?,?)",
	INSERT_MST_LOGIN_FROM_CLOUD: "INSERT INTO mst_login (device_push_token, device_arn, uuid, phone_id, phone_type, user_id, created_date, created_by, is_sync) VALUES (?,?,?,?,?,?,?,?,?)",
	INSERT_LOGIN_DETAILS_ON_SIGNUP: "INSERT INTO mst_login(user_id, device_push_token, device_arn, phone_id, phone_type, created_by, created_date) VALUES (?,?,?,?,?,?,?)",
	ADD_MST_LOGIN_DB: "INSERT INTO mst_login(device_push_token, device_arn, phone_id, phone_type, is_sync, user_id) VALUES (?,?,?,?,?,?)",
	INSERT_ARN_DETAILS: "INSERT INTO mst_login(device_arn, device_push_token, phone_id, phone_type, is_sync, user_id, created_date) VALUES (?,?,?,?,?,?,?)",

	UPDATE_LOGIN_USER_DETAILS: "UPDATE mst_login SET device_arn=?, uuid=?, is_sync=? WHERE device_push_token=? and phone_id=? and phone_type=? and user_id=?",
	UPDATE_LOGIN_DETAILS: "UPDATE mst_login SET token_id=?, user_id=?, modified_by=?, modified_date=? WHERE device_id=? AND device_type=?",
	UPDATE_MST_LOGIN_DB: "UPDATE mst_login SET device_arn=?, is_sync=? where device_push_token=? and user_id=? and phone_id=? and phone_type=?",

	CHECK_EXIST_LOGIN_USER_DETAILS: "SELECT * FROM mst_login where device_push_token=? and phone_id=? and phone_type=? and user_id=?",
	CHECK_TOKEN_AND_USERID: "SELECT * FROM mst_login WHERE user_id=? AND token_id=?",
	CHECK_DEVICE_ID_ADN_TYPE: "SELECT * FROM mst_login WHERE device_id=? AND device_type=?",
	GET_USERS_DEVICE_ARN: "select group_concat(device_arn) as device_arn from mst_login group by user_id",
	GET_DOOR_USERS_TOPIC_ARN: "Select ifNull(group_concat(DISTINCT c.topic_arn),0) as topic_arns from mst_door_sensor a left join door_sensor_notification b on a.door_sensor_id=b.door_sensor_id left join mst_user c on b.user_id=c.user_id where a.door_sensor_module_id=?",
	SCANNING_LOGIN_QUERY: "SELECT * FROM mst_login WHERE is_sync=?",
	IS_MST_LOGIN_RECORD_EXIST_QUERY: "Select * FROM mst_login where user_id=? and device_push_token=? and phone_id=? and phone_type=?",
	CHECK_ADMIN_EXISTS: "Select * from mst_user where admin=?",

	DELETE_DEVICE_ARN: "DELETE FROM mst_login WHERE device_arn=?",
	DELETE_LOGOUT_DETAILS_LOCALLY: "DELETE FROM mst_login where device_push_token=? and phone_id=? and phone_type=? and user_id=?",

	//============ mst_user ==================		
	SIGNUP_USER: "INSERT INTO mst_user(user_id, admin_user_id, user_email, user_password ,first_name, last_name, user_name, topic_arn, user_phone, vpn_port, home_controller_device_id, home_controller_name, admin, last_push_time, created_by, created_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
	INSERT_SHARE_PROFILE_DATA: "INSERT INTO mst_user(user_email, user_password, first_name, last_name, user_name, home_controller_device_id, home_controller_name, created_by, created_date) VALUES (?,?,?,?,?,?,?,?,?)",
	ADD_USER_PROFILE_STATUS: "INSERT INTO mst_user(user_id, home_controller_name, user_email, user_password, first_name, last_name, user_name, user_phone, home_controller_device_id, vpn_port, is_homecontroller_enable, is_tempsensor_enable, is_doorsensor_enable, is_active, is_sync, admin, created_by, created_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
	ADD_CHILD_USER: "INSERT INTO mst_user(user_id, admin_user_id, user_email, user_password ,first_name, last_name, user_name, topic_arn, user_phone, vpn_port, home_controller_device_id, home_controller_name, admin, created_by, created_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",


	UPDATE_PASSWORD: "UPDATE mst_user SET user_password=?, is_sync=?, modified_by=?, modified_date=? WHERE user_name=?",
	UPDATE_USER_PROFILE_STATUS: 'UPDATE mst_user SET home_controller_name=?, user_password=?, first_name=?, last_name=?, user_name=?, user_phone=?, vpn_port=?, is_homecontroller_enable=?, is_tempsensor_enable=?, is_doorsensor_enable=?, is_active=?, is_sync=? , admin=?, modified_by=?, modified_date=? WHERE user_id=?',
	UPDATE_USER_PROFILE_DATA: "UPDATE mst_user SET first_name=?, last_name=?, user_name=?, user_phone=?, is_sync=?, modified_by =?, modified_date=? WHERE user_id=?",
	UPDATE_USER_PROFILE_DATA_PASSWORD: "UPDATE mst_user SET first_name=?, last_name=?, user_name=?, user_password=?, user_phone=?, is_sync=?, modified_by =?, modified_date=? WHERE user_id=?",
	SAVE_NOTIFICATION_LIST: "Update mst_user SET is_homecontroller_enable=?, is_tempsensor_enable=?, is_doorsensor_enable=?, is_gassensor_enable=?, is_camera_enable=? where user_id=? AND home_controller_device_id=? ",
	UPDATE_CAMERA_KEY_STATUS: "Update mst_user SET camera_key=? where home_controller_device_id=?",
	UPDATE_BADGE_COUNT: "Update mst_user SET badge_count=? where topic_arn=?",


	USER_LOGIN: "SELECT * FROM mst_user WHERE user_name=? and user_password=?",
	//WIFI_USER_LOGIN: "SELECT * FROM mst_user WHERE wifi_user_name=? and wifi_user_password=?",
	RESET_PASSWORD: "SELECT * FROM mst_user WHERE user_email=?",
	GET_USER_INFO: "SELECT user_email, first_name, last_name, user_name, user_phone FROM mst_user WHERE home_controller_device_id=?",
	GET_USER_DATA: "SELECT a.user_id, a.user_email, a.first_name, a.last_name, a.vpn_port, a.user_name, a.user_password, a.camera_key, a.admin, a.home_controller_device_id as mac_address, a.is_active, b.home_controller_ip as gateway_ip FROM mst_user a left join mst_home_controller_list b on b.home_controller_device_id=b.home_controller_device_id WHERE a.user_id=? ORDER BY a.id DESC",
	GET_USER_DATA_BY_CONTROLLER_ID: "SELECT a.user_id, a.user_email, a.first_name, a.last_name, a.vpn_port, a.user_name, a.user_password, a.camera_key, a.admin, a.home_controller_device_id as mac_address, a.is_active, b.home_controller_ip as gateway_ip FROM mst_user a left join mst_home_controller_list b on b.home_controller_device_id=b.home_controller_device_id WHERE a.home_controller_device_id=? ORDER BY a.id DESC",
	GET_USER_DETAILS: "SELECT user_id, user_email, user_name, first_name FROM mst_user WHERE admin=1 ORDER BY id DESC",
	GET_USER_NAME_DETAILS: "SELECT user_id, first_name, last_name, user_email FROM mst_user WHERE home_controller_device_id=? ORDER BY id DESC",
	SCANNIG_USER_PROFILE_QUERY: "SELECT * FROM mst_user WHERE is_sync=?",
	GET_DEVICE_DATA: "SELECT a.*, b.* FROM mst_user a, mst_home_controller_list b WHERE home_controller_ip=?",
	IS_USER_PROFILE_RECORD_EXIST_QUERY: "SELECT * FROM mst_user WHERE home_controller_device_id=? and user_id=?",
	CHECK_USER_EXIST: "SELECT * FROM mst_user WHERE user_name=? and is_active=1",
	CHECK_VPN_PORT: "SELECT vpn_port where user_id=?",
	CHECK_NOTIFICATION_STATUS: "Select is_homecontroller_enable, is_doorsensor_enable, is_tempsensor_enable, is_camera_enable, is_gassensor_enable from mst_user where user_id=?",
	GET_USER_DETAILS_FOR_S3: "SELECT user_id, user_email FROM mst_user WHERE home_controller_device_id=? and admin=1 ORDER BY id DESC",
	//GET_PI_DETAILS: "SELECT wifi_user_name, wifi_user_password, home_controller_device_id as mac_address from mst_user where home_controller_device_id=?",
	GET_CAMERA_ENABLE_FOR_FACE_DETECTION: "Select is_camera_enable from mst_user where home_controller_device_id=?",
	GET_BADGE_COUNT_OF_USER: "Select badge_count from mst_user where user_id=?",
	GET_ADMIN_DETAILS: "Select * from mst_user where admin_user_id=? and admin=1",
	GET_CHILD_DETAILS: "Select * from mst_user where user_id=? and admin=0",
	GET_LOCAL_USER_LIST: "SELECT * FROM mst_user",
	GET_USER_NAME_BY_ID: "SELECT user_name from mst_user where user_id=? and is_active=1",
	CHECK_SAME_USER_EXISTS: "Select * from mst_user where first_name=? and last_name=? and user_name=? and user_phone=? and user_id=?",
	GET_TOPIC_ARN: "Select topic_arn from mst_user where user_id=?",
	GET_USER_DETAILS_FROM_TOPIC_ARN: "Select * from mst_user where topic_arn=?",
	GET_UERS_DEVICES: "Select  DISTINCT a.room_device_id, a.device_name, a.device_status,b.panel_id, b.panel_name, c.room_id, c.room_name from room_device_mapping a left join mst_panel b on a.panel_id=b.panel_id left join mst_room c on b.room_id=c.room_id where a.user_id=? and a.is_original=1 and a.is_active=1",


	//============ mst_home_controller_list ================	
	INSERT_HOME_CONTROLLER_DATA: "INSERT INTO mst_home_controller_list(home_controller_ip, net_mask, gateway_address, home_controller_id, home_controller_device_id, created_by, created_date) VALUES (?,?,?,?,?,?,?)",
	ADD_HOME_CONTROLLER_DATA: "INSERT INTO mst_home_controller_list(home_controller_id, home_controller_device_id, home_controller_ip, home_controller_status, home_controller_description, is_sync, net_mask=?, gateway_address=?, is_static=?, is_dhcp=?, is_active, created_by, created_date) VALUES (?,?,?,?,?,?,?,?,?)",

	UPDATE_HOME_CONTROLLER_STATUS: "UPDATE mst_home_controller_list SET home_controller_ip=?, home_controller_status=?, home_controller_description=?, is_sync=?, net_mask=?, gateway_address=?, is_static=?, is_dhcp=?, is_active=?, modified_by=?, modified_date=? WHERE home_controller_device_id=?",
	UPDATE_HOME_CONTROLLER_DEVICE_ID: "UPDATE mst_home_controller_list SET home_controller_ip=?, is_static=?, is_dhcp=?, is_sync=?, modified_by=?, modified_date=?",

	FIND_CONTROLLER_IP: "SELECT * FROM mst_home_controller_list WHERE home_controller_device_id=?",
	CHECK_HOME_CONTROLLER_STATUS: "SELECT * FROM mst_home_controller_list WHERE home_controller_device_id=? and is_active='1'",
	SCANNING_HOME_CONTROLLER_QUERY: "SELECT * FROM mst_home_controller_list WHERE is_sync=?",
	IS_HOME_CONTROLLER_RECORD_EXIST_QUERY: "SELECT * FROM mst_home_controller_list WHERE home_controller_device_id=?",
	SELECT_SYSTEM_INFO: "SELECT home_controller_ip, net_mask, gateway_address, is_static, is_dhcp FROM mst_home_controller_list",

	// //============ mst_system_info ================

	// UPDATE_SYSTEM_INFO_TO_SYNC: "UPDATE mst_system_info SET is_sync=?, modified_by=?, modified_date=? WHERE system_id=?",

	// SELECT_SYSTEM_DEVICES_TO_SYNC: "SELECT * FROM mst_system_info WHERE is_sync=0",




	//============= mst_camera_devices ==================
	ADD_CAMERA_DETAILS: "INSERT INTO mst_camera_devices(camera_id, user_id, home_controller_device_id, camera_name, camera_ip, camera_videopath, camera_icon, camera_vpn_port, user_name, password, camera_url, created_date, created_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",

	UPDATE_CAMERA_LIST_SYNC: "UPDATE mst_camera_devices SET is_sync=? WHERE camera_id=?",
	UPDATE_CAMERA_STATUS: "UPDATE mst_camera_devices SET is_active=? WHERE camera_id=?",
	UPDATE_CAMERA: "UPDATE mst_camera_devices SET camera_name=?, camera_ip=?, camera_videopath=?, user_name=?, password=?, is_sync=? WHERE camera_id=?",
	UPDATE_CAMERA_LOGS: "UPDATE mst_notification SET is_unread=0 WHERE activity_type='Camera' ",
	UPDATE_CAMERA_LOGS_BY_ID: "UPDATE mst_notification SET is_unread=0 WHERE activity_type='Camera' and module_id=?",

	/* Will update in v4 */
	//GET_CAMERA_LIST: "SELECT id, camera_id, camera_name, camera_icon FROM mst_camera_devices WHERE home_controller_device_id=?",
	GET_CAMERA_LIST: " SELECT  jetson_device_id, camera_id, camera_name, camera_url, camera_ip, camera_videopath, camera_icon, camera_vpn_port, user_name, password , is_active from mst_camera_devices where user_id=?",
	GET_ADMIN_CAMERA_LIST: "SELECT  camera_id, camera_name, camera_url, camera_ip, camera_videopath, camera_icon, camera_vpn_port, user_name, password , is_active from mst_camera_devices where home_controller_device_id=?",
	GET_CAMERA_INFO: "SELECT * FROM mst_camera_devices WHERE camera_id=? ",
	GET_CAMERA_LIST_TO_SYNC: "SELECT * FROM mst_camera_devices WHERE is_sync=?",
	GET_CAMERA_ID_NAMES: "SELECT camera_id, camera_name from mst_camera_devices",
	GET_CHILD_CAMERA_ID_NAMES: "Select ",
	// GET_CAMERA_DETAILS_FOR_FACE_DETECTION: "SELECT b.camera_name, b.camera_id, b.is_active, b.is_push_enable, c.*, (Select is_camera_enable from mst_user a left join mst_camera_devices b on a.user_id=b.user_id) as is_camera_enable from mst_camera_devices b left join camera_notification c on b.camera_id=c.camera_id where b.camera_ip=? and b.camera_videopath=?",
	// GET_CAMERA_DETAILS_FOR_FACE_DETECTION: "SELECT camera_name, camera_id, is_active from mst_camera_devices where camera_ip=? and camera_videopath=?",
	GET_CAMERA_DETAILS_BY_ID: "Select * from mst_camera_devices where camera_id=?",
	CHECK_CAMERA_LIST: "SELECT a.camera_id, a.camera_name, a.camera_url, a.is_active, a.camera_ip, a.camera_videopath, b.home_controller_ip FROM mst_camera_devices a left join mst_home_controller_list b on a.home_controller_device_id = b.home_controller_device_id WHERE a.home_controller_device_id=?",
	GET_HOME_CONTROLLER_USER_DETAILS: "Select a.user_id, a.home_controller_device_id, a.first_name, a.last_name, a.is_homecontroller_enable, a.is_camera_enable, a.topic_arn	FROM mst_user a left join mst_home_controller_list b on (a.home_controller_device_id = b.home_controller_device_id) AND a.home_controller_device_id=?",
	GET_CAMERA_IDS: "SELECT camera_id FROM mst_camera_devices group by camera_id",
	CHECK_DUPLICATE_CAMERA_NAME: "Select camera_name from mst_camera_devices where camera_name=? COLLATE NOCASE",
	CHECK_CAMERA_EXISTS: "Select * from mst_camera_devices where camera_id=?",
	CHECK_SAME_CAMERA_NAME: "SELECT camera_name FROM mst_camera_devices WHERE camera_name=? AND NOT camera_id=?",
	CHECK_DUPLICATE_CAMERA_NOTIFICATION: "select DISTINCT (SELECT COUNT(*) FROM camera_notification_mapping b WHERE a.camera_notification_id = b.camera_notification_id) as notification_cameras, a.camera_notification_id, GROUP_CONCAT(a.camera_id) as camera_id from camera_notification_mapping a left join mst_camera_notification b on a.camera_notification_id=b.camera_notification_id WHERE notification_cameras=? group by a.camera_notification_id",
	CHECK_DUPLICATE_CAMERA_NOTIFICATION_BY_ID: "select DISTINCT (SELECT COUNT(*) FROM camera_notification_mapping b WHERE a.camera_notification_id = b.camera_notification_id) as notification_cameras, a.camera_notification_id, GROUP_CONCAT(a.camera_id) as camera_id from camera_notification_mapping a left join mst_camera_notification b on a.camera_notification_id=b.camera_notification_id WHERE notification_cameras=? and not a.camera_notification_id=? group by a.camera_notification_id",
	GET_CAMERA_DETAILS: "SELECT id, camera_id, camera_name, camera_url, camera_ip, camera_videopath, camera_icon, camera_vpn_port, user_name, password , is_active, ifnull((select count(is_unread) from mst_notification where activity_type='Camera' and is_unread=1),0) as is_unread from mst_camera_devices where camera_id=?",
	GET_ALL_CAMERA_IDS: "Select camera_id from mst_camera_devices",
	DELETE_CAMERA: "DELETE FROM mst_camera_devices WHERE camera_id=?",



	//=============mst_camera_notification===========

	ADD_CAMERA_NOTIFICATION: "INSERT INTO mst_camera_notification(camera_notification_id, start_time, end_time, alert_interval, user_id, created_by, created_date) VALUES (?,?,?,?,?,?,?)",
	INSERT_CAMERA_NOTIFICATION_MAPPING: "INSERT INTO camera_notification_mapping(mapping_id,camera_notification_id, camera_id) VALUES (?,?,?)",


	UPDATE_CAMERA_NOTIFICATION_ALERT: "UPDATE mst_camera_notification SET start_time=?, end_time=?, alert_interval=?, modified_by=?, modified_date=?,is_sync=0 where camera_notification_id=? ",
	UPDATE_CAMERA_ALERT_STATUS: " UPDATE mst_camera_notification SET is_active=?,is_sync=0 where camera_notification_id=? ",

	CHECK_ORIGINAL_CAMERA_ALERT_DATA_FOR_UPDATE: "SELECT GROUP_CONCAT(b.camera_id) as camera_id, a.* FROM camera_notification_mapping b left join mst_camera_notification a on a.camera_notification_id = b.camera_notification_id WHERE a.camera_notification_id=?",
	GET_CAMERA_ALERT_DETAILS: "SELECT GROUP_CONCAT(c.camera_id) as camera_ids, a.* FROM mst_camera_notification a left join camera_notification_mapping c on a.camera_notification_id = c.camera_notification_id  GROUP BY a.camera_notification_id order by a.created_date DESC",
	GET_CHILD_CAMERA_ALERT_DETAILS: "Select group_concat(mcm.camera_id) as camera_ids, mcn.* from camera_notification_mapping mcm left join mst_camera_notification mcn on mcm.camera_notification_id = mcn.camera_notification_id where mcm.camera_notification_id IN (SELECT a.camera_notification_id FROM mst_camera_notification a left join camera_notification_mapping c on a.camera_notification_id = c.camera_notification_id  where c.camera_id = ? ) GROUP BY mcm.camera_notification_id",
	GET_CAMERA_PUSH_LOGS: "Select a.id, a.activity_action, a.activity_type, a.activity_description, a.activity_time, a.is_unread, a.image_url from mst_notification a where a.activity_type='Camera' and activity_action='Person Detected' and a.is_unread=1 ORDER BY a.notification_id DESC ",
	GET_CHILD_CAMERA_PUSH_LOGS: "Select a.id, a.activity_action, a.activity_type, a.activity_description, a.activity_time, a.is_unread, a.image_url from mst_notification a where a.module_id=? and a.activity_type='Camera' and activity_action='Person Detected' and a.is_unread=1 ORDER BY a.notification_id DESC ",
	//GET_CAMERA_NOTIFICATION: "SELECT id, activity_action, activity_type, activity_description, activity_time, image_url  FROM mst_notification where activity_type='Camera' ORDER BY notification_id DESC LIMIT ?,25",
	GET_CAMERA_DETAILS_FOR_FACE_DETECTION: "Select start_time ,end_time from mst_camera_notification where camera_notification_id=?",
	GET_CAMERA_DETAILS_FOR_PYTHON: "Select a.start_time, a.end_time, a.alert_interval, a.camera_notification_id, a.is_active, a.user_id, GROUP_CONCAT(b.camera_id) as camera_ids , GROUP_CONCAT(c.camera_name) as camera_names, GROUP_CONCAT(c.camera_videopath) as camera_videopaths, GROUP_CONCAT(c.camera_ip) as camera_ips, GROUP_CONCAT(c.is_active) as camera_active, (Select is_camera_enable from mst_user) as camera_enable from mst_camera_notification a left join camera_notification_mapping b on a.camera_notification_id = b.camera_notification_id left join mst_camera_devices c on b.camera_id=c.camera_id where a.is_active=1 group by b.camera_notification_id",
	GET_CAMERA_IDS_TOPIC_ARN: "Select  ifNull(b.topic_arn,'') as topic_arn from mst_camera_devices a left join mst_user b on a.user_id=b.user_id where a.camera_id=? UNION ALL Select ifNull(b.topic_arn,'') from mst_camera_child_priviledge a left join mst_user b on a.user_id=b.user_id where a.camera_id=?",
	GET_ALL_CAMERA_DETAILS: "Select * from mst_camera_devices",

	DELETE_CAMERA_NOTIFICATION_MAPPING: "DELETE FROM camera_notification_mapping WHERE camera_id=? AND camera_notification_id=?",
	DELETE_CAMERA_NOTIFICATION_ALERT: "DELETE FROM mst_camera_notification where camera_notification_id=?",
	DELETE_CAMERA_MAPPING_RECORDS: "DELETE FROM camera_notification_mapping WHERE camera_notification_id=?",
	DELETE_CAMERA_ALERT: "DELETE FROM camera_notification_mapping WHERE camera_id=?",




	//===============notification list=========
	GET_NOTIFICATION_LIST: " Select ifNull(is_homecontroller_enable,'0') as is_homecontroller_enable, ifNull(is_doorsensor_enable, '0')as is_doorsensor_enable, ifNull(is_tempsensor_enable, '0') as is_tempsensor_enable, ifNull(is_camera_enable, '0') as is_camera_enable, ifNull(is_gassensor_enable, '0') as is_gassensor_enable from mst_user where home_controller_device_id=? and user_id=?",

	SET_NOTIFICATION_STATUS_UNREAD: "Update mst_notification SET is_unread=? where module_id=?",


	//============ admin page ===============
	GET_TABLE_LIST: "SELECT name FROM sqlite_master WHERE type='table'",





	//==============mst_child_room_priviledge================

	ADD_CAMERA_CHILD_USER: "INSERT INTO mst_camera_child_priviledge(priviledge_id,user_id, camera_id, created_by, created_date) VALUES (?,?,?,?,?)",

	UPDATE_CHILD_USER_PASSWORD: "UPDATE mst_user SET user_password=?, is_sync=? WHERE user_id=?",

	GET_ADMIN_ROOM_LIST: "SELECT DISTINCT room_id, room_name FROM mst_room WHERE is_active=1 and room_type=? and home_controller_device_id=? ",
	GET_CHILD_USER_ROOMS: "Select DISTINCT room_id, room_type, room_name from mst_room where user_id=? and room_type=0",
	GET_CHILD_USER_ROOM_IDS: "Select DISTINCT room_id from mst_room where user_id=? and room_type=0",
	GET_CHILD_USER_MOODS: "Select DISTINCT room_id, room_type, room_name  from mst_room where original_room_list LIKE ?",
	GET_CHILD_USER_CAMERAS: "Select DISTINCT a.camera_id,b.camera_name from mst_camera_child_priviledge a left join mst_camera_devices b on a.camera_id=b.camera_id where a.user_id=?",
	GET_CHILD_USERS: "Select user_id, user_name, user_password, admin from mst_user where (admin=0 or admin=-1) and is_active=1",
	GET_CHILD_CAMERA_LIST: "Select  b.camera_id, b.camera_name,b.jetson_device_id, b.camera_url, b.camera_ip, b.camera_videopath, b.camera_icon, b.camera_vpn_port, b.user_name, b.password, b.is_active, from mst_camera_child_priviledge a left join mst_camera_devices b on a.camera_id=b.camera_id where a.user_id= ?",
	// FIND_CHILD_SCHEDULE_LIST_ON_ROOM: " SELECT DISTINCT ifnull(b.room_name,'') as room_name, GROUP_CONCAT(c.room_device_id) as room_device_id, a.* FROM 	mst_schedule a left join mst_room b on a.room_id=b.room_id left join schedule_device_mapping c on a.schedule_id = c.schedule_id WHERE a.room_id LIKE ? GROUP BY a.schedule_id order by a.created_date DESC",
	FIND_CHILD_SCHEDULE_LIST_ON_ROOM: " SELECT DISTINCT ifnull(b.room_name,'') as room_name, GROUP_CONCAT(c.room_device_id) as room_device_id, a.* FROM mst_schedule a left join mst_room b on a.room_id=b.room_id left join schedule_device_mapping c on a.schedule_id = c.schedule_id WHERE a.room_id LIKE ? and schedule_type=? GROUP BY a.schedule_id order by a.created_date DESC",
	FIND_CHILD_EMPTY_SCHEDULE_MAPPING: "select DISTINCT schedule_id from schedule_device_mapping where schedule_id NOT IN (select schedule_id from mst_schedule)",
	GET_CHILD_SCHEDULES: "Select schedule_id from mst_schedule where user_id=?",
	GET_CHILD_ROOM_DETAILS_PANEL_MODULE_ID: "Select * from mst_panel where room_id=?",
	FIND_CHILD_EMPTY_PANEL: "select DISTINCT panel_id from mst_panel where panel_id NOT IN (select panel_id from room_device_mapping)",
	FIND_CHILD_EMPTY_CAMERA_NOTIFICAITON: "	select DISTINCT camera_notification_id from camera_notification_mapping where camera_notification_id NOT IN (select camera_notification_id from mst_camera_notification )",
	GET_CHILD_MOOD_LIST: "Select room_id as mood_id from mst_room where original_room_list LIKE ? ",
	CHECK_USER_PASSWORD: "Select * from mst_user where user_password=? and user_id=?",

	DEACTIVATE_CHILD_USER: "UPDATE mst_user SET is_active=0 where user_id=?",
	DELETE_CHILD_ROOM_MOOD: "DELETE FROM mst_room where user_id=?",
	DELETE_CHILD_PANEL: "DELETE FROM mst_panel where user_id=?",
	DELETE_CHILD_ROOM_MAPPING: "DELETE FROM room_device_mapping where user_id=?",
	DELETE_CHILD_SCHEDULE: "DELETE FROM mst_schedule where user_id=?",
	DELETE_CHILD_LOGS: "DELETE FROM mst_notification where user_id=?",
	DELETE_CHILD_CAMERA: "DELETE FROM mst_camera_child_priviledge where user_id=?",
	DELETE_CHILD_CAMERA_NOTIFICATION_BY_USER_ID: "Delete from mst_camera_notification where user_id=?",
	DELETE_CHILD_CAMERA_NOTIFICATION: "DELETE FROM mst_camera_notification where camera_notification_id IN (Select camera_notification_id from camera_notification_mapping where camera_id=? and user_id=?)",
	DELETE_CHILD_SCHEDULE_DEVICE_MAPPING: "DELETE FROM schedule_device_mapping where schedule_id=?",
	DELETE_CHILD_ROOM_BY_ID: "DELETE FROM mst_room where room_id=? and user_id=?",
	DELETE_CHILD_MOOD_RELATED_ROOM: "DELETE FROM mst_room where room_id=? and user_id=? ",
	DELETE_CHILD_SCHEDULE_RELATED_ROOM: "DELETE FROM mst_schedule where room_id LIKE ? and user_id=? ",
	DELETE_ROOM_MAPPING_BY_MODULE_ID: "DELETE FROM room_device_mapping where module_id=? and user_id=?",
	DELETE_SCHEDULE_MAPPING_BY_MODULE_ID: "DELETE FROM schedule_device_mapping where module_id=? and user_id=?",
	DELETE_CHILD_MOOD_PANEL: "DELETE FROM mst_panel where panel_id=? and panel_type=-1",
	DELETE_CHILD_CAMERA_BY_ID: "DELETE FROM mst_camera_child_priviledge where camera_id=? and user_id=?",
	DELETE_EMPTY_CHILD_CAMERA_NOTIFICATION: "Delete from camera_notification_mapping where camera_notification_id=?",
	DELETE_CHILD_SCHEDULE_MAPPING_ROOM: "Delete from schedule_device_mapping where module_id=? and user_id=?",
	DELETE_CHILD_SCHEDULE_ON_MOOD: "DELETE FROM mst_schedule where room_id=? and user_id=? ",


	responseCode: {
		CUSTOM_MESSAGE: (code, message) => ({
			code,
			message
		}),


		SUCCESS: {
			code: 200,
			message: 'Success'
		},
		SIGNUP_SUCCESS: {
			code: 200,
			message: 'Succesfully! Your account has been created'
		},

		CLOUD_CONNECTION_NOT_AVAILABLE: {
			code: 201,
			message: 'Colud connection not available'
		},
		INTERNAL_SERVER_ERROR: {
			code: 501,
			message: 'Something went wrong. Try Again!'
		},
		NOT_FOUND: {
			code: 400,
			message: 'Not found'
		},
		INTERNET_CONNECTION_NOT_AVAILABLE: {
			code: 403,
			message: 'Something went wrong. Try Again!'
		},
		SUCCESS: {
			code: 200,
			message: 'Success'
		},
		DUPLUICATE_DATA: {
			code: 300,
			message: 'Already Exist'
		},
		DUPLUICATE_ENTITY: {
			code: 301,
			message: 'Duplicate value not allowed'
		},
		DUPLUICATE_DELETED_ENTITY: {
			code: 302,
			message: 'Duplicate value not allowed for deleted entity'
		},
		NO_MSG: {
			code: 200,
			message: ''
		},
		INVALID_SENSOR_TYPE: {
			code: 301,
			message: 'Sensor type is Invalid. Try Again!'
		},
		NO_UNASSIGNED_SENSOR: {
			code: 200,
			message: 'No Unassigned Sensor Found'
		},
		NOT_DONE: {
			code: 301,
			message: 'Synch from cloud not done.!!!'
		},
		INVALID_KEY: {
			code: 400,
			message: 'Invalid Key!!!'
		},
		ADMIN_ALREADY_EXISTS: {
			code: 301,
			message: 'User is already Regsitered. Please Login!'
		},

		// Module
		MODULE_NOT_FOUND: {
			code: 200,
			message: 'Module Not Found.'
		},

		//Room
		ADD_ROOM_NAME: {
			code: 301,
			message: 'Add Room Name'
		},
		DUPLICATE_MODULE_FOUND: {
			code: 301,
			message: 'Module is already configured and added. Add Module Devices from - "Add from Exisiting"'
		},
		DUPLICATE_ROOM_NAME: {
			code: 301,
			message: 'Duplicate Room Name is not allowed'
		},
		ROOM_ADD: {
			code: 200,
			message: 'Room added'
		},
		ROOM_UPDATE: {
			code: 200,
			message: 'Room details updated'
		},
		ROOM_DELETE: {
			code: 200,
			message: 'Room deleted'
		},
		ROOM_NOT_FOUND: {
			code: 200,
			message: 'Room is not deleted. Try Again!'
		},
		ROOM_NAME_BLANK: {
			code: 301,
			message: 'Blank Panel Name is not allowed'
		},
		NO_ROOM_FOUND_FOR_SENSOR: {
			code: 200,
			message: 'No Room Found, Create a Room to add Sensor'
		},
		NO_UNASSIGNED_DEVICES: {
			code: 301,
			message: 'No Unassigned Devices Available'
		},
		NO_ROOMS: {
			code: 200,
			message: 'No Room Found'
		},

		//Panel
		DUPLICATE_PANEL_NAME: {
			code: 301,
			message: 'Duplicate Panel Name is not allowed'
		},
		DUPLICATE_DEVICE_FOUND_IN_PANEL: {
			code: 301,
			message: 'Duplicate Devices are not allowed in same panel'
		},
		PANEL_ADD: {
			code: 200,
			message: 'Panel added'
		},
		PANEL_UPDATE: {
			code: 200,
			message: 'Panel updated'
		},
		PANEL_DELETE: {
			code: 200,
			message: 'Panel deleted'
		},
		PANEL_NOT_FOUND: {
			code: 200,
			message: 'Panel is not deleted. Try Again!'
		},
		PANEL_NAME_BLANK: {
			code: 301,
			message: 'Blank Panel Name is not allowed'
		},

		//Device		
		DUPLICATE_SWITCH_NAME: {
			code: 301,
			message: 'Duplicate Switch Name is not allowed'
		},
		DEVICE_ADD: {
			code: 200,
			message: 'Device added'
		},
		DEVICE_UPDATE: {
			code: 200,
			message: 'Device updated'
		},
		DEVICE_DELETE: {
			code: 200,
			message: 'Devices are deleted!'
		},
		DEVICE_NOT_FOUND: {
			code: 400,
			message: 'Device Not found'
		},
		BUTTON_NOT_FOUND: {
			code: 400,
			message: 'Button Not found'
		},

		//Mood
		DUPLUICATE_MOOD_NAME: {
			code: 301,
			message: 'Duplicate Mood Name is not allowed'
		},
		DUPLUICATE_MOOD_DEVICE_ENTITY: {
			code: 301,
			message: 'Duplicate Mood Devices are not allowed'
		},
		MOOD_ADD: {
			code: 200,
			message: 'Mood added'
		},
		MOOD_UPDATE: {
			code: 200,
			message: 'Mood updated'
		},
		MOOD_DELETE: {
			code: 200,
			message: 'Mood deleted'
		},
		NO_MOOD_UPDATE: {
			code: 200,
			message: 'No Mood Details updated'
		},
		MOOD_EXISTS_IN_LIST: {
			code: 301,
			message: 'Mood with same name already exists'
		},

		//Schedule		
		DUPLUICATE_SCHEDULE_NAME: {
			code: 301,
			message: 'Same Schedule Name Already Exists'
		},
		DUPLICATE_SCHEDULE_DETAILS: {
			code: 301,
			message: 'Same Schedule Details Already Exists'
		},
		DUPLUICATE_SCHEDULE_DEVICE_ENTITY: {
			code: 301,
			message: 'Duplicate Schedule Devices are not allowed'
		},
		DUPLUICATE_SCHEDULE_TIME: {
			code: 200,
			message: 'Schedule On/Off time is conflicting with same devices. Are you sure want to save ?'
		},
		DUPLUICATE_TIMER_TIME: {
			code: 200,
			message: 'Timer On/Off time is conflicting with same devices. Are you sure want to save ?'
		},
		SCHEDULE_EXIST: {
			code: 602,
			message: "Schedule is added on this Device. Are you sure want to delete ?"
		},
		SCHEDULE_ADD: {
			code: 200,
			message: 'Schedule added'
		},
		SCHEDULE_UPDATE: {
			code: 200,
			message: 'Schedule updated'
		},
		SCHEDULE_DELETE: {
			code: 200,
			message: 'Schedule deleted'
		},

		TIMER_ADD: {
			code: 200,
			message: 'Timer added'
		},
		TIMER_DELETE: {
			code: 200,
			message: 'Timer deleted'
		},
		TIMER_UPDATE: {
			code: 200,
			message: 'Timer updated'
		},

		//Camera
		CAMERA_UPDATE: {
			code: 200,
			message: 'Camera Updated'
		},
		CAMERA_DELETE: {
			code: 200,
			message: 'Camera Deleted'
		},
		CAMERA_NOT_FOUND: {
			code: 200,
			message: 'Camera Not Found!'
		},
		SAME_CAMERA_NAME: {
			code: 301,
			message: 'Camera With Same Name Already Exists'
		},
		CAMERA_KEY_SUCCESS: {
			code: 200,
			message: 'Camera Key Matched Successfully. You can add cameras now.'
		},
		CAMERA_ALERT_ADDED: {
			code: 200,
			message: 'Notification Alert Added Successfully!'
		},
		CAMERA_ALERT_UPDATED: {
			code: 200,
			message: 'Notification Alert Updated Successfully!'
		},
		CAMERA_NOTIFICATION_DELETED: {
			code: 200,
			message: 'Notification Alert Deleted Successfully!'
		},
		FALSE_IMAGE_UPLOAD_SUCCESS: {
			code: 200,
			message: 'False Image Reported'
		},

		//User
		USER_NOT_FOUND: {
			code: 102,
			message: 'New User, No Data Found!'
		},
		LOGIN_FAILURE: {
			code: 502,
			message: 'Incorrect username or password'
		},
		ALL_DETAILS_REQUIRED: {
			code: 400,
			message: "All fields required"
		},
		PROFILE_UPDATE: {
			code: 200,
			message: 'Profile updated'
		},

		//Temp Sensor
		TEMP_SENSOR_ADD: {
			code: 200,
			message: 'Temperature Sensor Added'
		},
		NO_TEMP_DEVICES: {
			code: 400,
			message: 'Temperature Sensor Not Found'
		},
		NO_UNASSIGNED_TEMP_DEVICES: {
			code: 200,
			message: 'No Unassigned Temperature Sensor Found'
		},
		DUPLICATE_TEMP_DEVICE_NAME: {
			code: 301,
			message: 'Temperature Sensor With Same Name Already Exists'
		},
		TEMPERATURE_DEVICE_UPDATE: {
			code: 200,
			message: 'Temperature Sensor Updated'
		},
		TEMP_SENSOR_DELETE: {
			code: 200,
			message: 'Temperature Sensor Deleted'
		},
		SAME_TEMP_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		TEMP_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Temperature Sensor Notification Added Successfully'
		},
		TEMP_NOTIFICATION_UPDATED: {
			code: 200,
			message: 'Temperature Sensor Notification Updated Successfully'
		},
		TEMP_NOTIFICATION_DELETED: {
			code: 200,
			message: 'Temperature Sensor Notification Deleted Successfully'
		},
		ALREADY_SAME_TEMP_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		SAME_TEMP_SENSOR_NAME: {
			code: 301,
			message: 'Temperature Sensor With Same Name Already Exists'
		},
		SAME_UNASSIGNED_TEMP_SENSOR_NAME: {
			code: 301,
			message: 'Temperature name already exists in unassigned Temperature Sensor List'
		},
		TEMP_SENSOR_TEMP_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Notification for Temperature Added Successfully'
		},
		TEMP_SENSOR_HUMIDITY_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Notification for Humidity Added Successfully'
		},
		SAME_TEMP_SENSOR_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		TEMP_SENSOR_HUMIDITY_NOTIFICATION_UPDATED: {
			code: 200,
			message: 'Notification for Humidity Updated Successfully'
		},
		TEMP_SENSOR_TEMP_NOTIFICATION_UPDATED: {
			code: 200,
			message: 'Notification for Temperature Updated Successfully'
		},

		//Door Sensor
		DUPLICATE_DOORSENSOR_FOUND: {
			code: 301,
			message: 'Door Sensor is Already Configured'
		},
		SAME_DOOR_SENSOR_NAME: {
			code: 301,
			message: 'Door Sensor With Same Name Already Exists'
		},

		//Alexa Google
		LIGHTS_OFF: {
			code: 200,
			message: 'Lights are Off'
		},
		LIGHTS_ON: {
			code: 200,
			message: 'Lights are On'
		},

		ALL_PARAMETERS_REQUIRED: {
			code: 400,
			message: 'All Paramters are required!'
		},

		//Door Sensor
		DOOR_SENSOR_CONFIGURE_SUCCESS: {
			code: 200,
			message: 'Door Sensor Configured'
		},

		DOOR_SENSOR_ADDED: {
			code: 200,
			message: 'Door Sensor Added Successfully'
		},

		DOOR_SENSOR_DELETE_SUCCESS: {
			code: 200,
			message: 'Door Sensor Deleted Successfully'
		},

		DOOR_SENSOR_UPDATE: {
			code: 200,
			message: 'Door Sensor Details Updated Successfully'
		},

		NO_UNASSIGNED_DOOR_SENSOR: {
			code: 200,
			message: 'No Unassigned Door Sensor Found'
		},

		NO_DOOR_SENSOR: {
			code: 301,
			message: 'No Door Sensor Found'
		},

		DUPLICATE_DOOR_SENSOR_NAME: {
			code: 301,
			message: 'Door Sensor With Same Name Already Exists'
		},
		SAME_UNASSIGNED_DOOR_SENSOR_NAME: {
			code: 301,
			message: 'Door Sensor name already exists in unassigned Door Sensor List'
		},

		NOTIFICATION_LIST_SAVED: {
			code: 200,
			message: 'Notification List Updated'
		},

		SAME_DOOR_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		DOOR_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Door Sensor Notification Added Successfully'
		},
		ALREADY_SAME_DOOR_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		DOOR_NOTIFICATION_UPDATED: {
			code: 200,
			message: 'Door Sensor Notification Updated Successfully'
		},
		DOOR_NOTIFICATION_DELETED: {
			code: 200,
			message: 'Door Sensor Notification Deleted Successfully'
		},
		SAME_MIN_MAX_VALUE: {
			code: 300,
			message: '"Min and Max value should not be same'
		},
		MIN_GREATER_THAN_MAX: {
			code: 300,
			message: 'Min Value Should be Lesser than Max Value.'
		},
		SAME_START_END_DATE: {
			code: 300,
			message: 'Enter different start and end date'
		},
		IR_COMMAND_SUCCESS: {
			code: 200,
			message: 'IR Sensor Command Send Successfully'
		},
		FILE_APPEND_FAILED: {
			code: 300,
			message: 'File cannot be appended'
		},
		FILE_WRITE_FAILED: {
			code: 300,
			message: 'File cannot be updated'
		},
		FILE_READ_FAILED: {
			code: 300,
			message: 'File cannot be read'
		},

		//IR Blaster
		SAME_IR_DEVICE_NAME: {
			code: 301,
			message: 'IR Blaster With Same Name Already Exists'
		},
		IR_BLASTER_ADDED: {
			code: 200,
			message: 'IR Blaster Added'
		},
		REMOTE_ADDED: {
			code: 200,
			message: 'Remote Added'
		},
		DUPLICATE_REMOTE_NAME: {
			code: 301,
			message: 'Remote With Same Name Already Exists'
		},
		REMOTE_UPDATE: {
			code: 200,
			message: 'Remote Details Updated Successfully'
		},
		NO_REMOTE_FOUND: {
			code: 301,
			message: 'No Remote Found'
		},
		REMOTE_DELETE: {
			code: 200,
			message: 'Remote Deleted Successfully'
		},
		REMOTE_SCHEDULE_ADDED: {
			code: 200,
			message: 'Remote Schedule Added Successfully'
		},
		SAME_IR_SCHEDULE_DETAILS: {
			code: 301,
			message: 'Same schedule details Already Exists'
		},
		REMOTE_SCHEDULE_NOT_FOUND: {
			code: 200,
			message: 'Schedule Details not found!'
		},
		REMOTE_SCHEDULE_DELETE: {
			code: 200,
			message: 'Remote Schedule Deleted Successfully'
		},
		NO_REMOTE_DETAILS_FOUND: {
			code: 301,
			message: 'No remote details found'
		},
		NO_IR_BLASTER_DETAILS_FOUND: {
			code: 301,
			message: 'No IR blaster details found'
		},
		IR_BLASTER_DELETED: {
			code: 200,
			message: 'IR Blaster Deleted Successfully'
		},
		IR_BLASTER_UPDATED: {
			code: 200,
			message: 'IR Blaster Updated Successfully'
		},
		INVALID_TEMPERATURE_RANGE: {
			code: 301,
			message: 'Temperature should be between 17 and 30'
		},
		NO_IR_BLASTER_ADDED: {
			code: 301,
			message: 'No IR Blaster Added!'
		},
		IR_BLASTER_DOESNOT_EXISTS: {
			code: 200,
			message: 'IR Blaster doesnt exist!'
		},
		SAME_IR_MODULE_EXISTS: {
			code: 200,
			message: 'IR Blaster already configured! Add from Unconfigured Sensor devices'
		},


		WIFIN_LOGIN_FAILURE: {
			code: 400,
			message: 'Invalid Credentials. Try Again'
		},

		//child user
		CHILD_USER_ADDED: {
			code: 200,
			message: 'Child User Added Successfully'
		},
		SAME_CHILD_USER_EXIST: {
			code: 301,
			message: 'User with same username already Exist! Try Another Username.'
		},
		CHILD_USER_DELETED: {
			code: 200,
			message: 'Child User Deleted Successfully'
		},
		CHILD_USER_UPDATED: {
			code: 200,
			message: 'Child User Updated Successfully'
		},
		NO_CHILD_EXISTS: {
			code: 200,
			message: 'No Child User Exists'
		},

		//Smart Remote
		SAME_SMART_REMOTE_NAME: {
			code: 301,
			message: 'Smart Remote With Same Name Already Exists'
		},
		SMART_REMOTE_ADDED: {
			code: 200,
			message: 'Smart Remote Added Successfully'
		},
		SMART_REMOTE_UPDATED: {
			code: 200,
			message: 'Smart Remote Updated Successfully'
		},
		SMART_REMOTE_DELETE_SUCCESS: {
			code: 200,
			message: 'Smart Remote Deleted Successfully'
		},
		SAME_SMART_REMOTE_NUMBER: {
			code: 301,
			message: 'Same Number Already Assigned. Try Another!'
		},

		//Multi Sensor
		SAME_MULTI_SENSOR_NAME: {
			code: 301,
			message: 'Multi Sensor With Same Name Already Exists'
		},
		MULTI_SENSOR_ADD: {
			code: 200,
			message: 'Multiple Sensor Added'
		},
		DUPLICATE_MULTI_SENSOR_NAME: {
			code: 301,
			message: 'Multi Sensor With Same Name Already Exists'
		},
		SAME_UNASSIGNED_MULTI_SENSOR_NAME: {
			code: 301,
			message: 'Multi Sensor name already exists in unassigned Multi Sensor List'
		},
		MULTI_SENSOR_UPDATE: {
			code: 200,
			message: 'Multi Sensor Updated'
		},
		NO_MULTI_SENSOR: {
			code: 400,
			message: 'Multi Sensor Not Found'
		},
		MULTI_SENSOR_DELETE: {
			code: 200,
			message: 'Multi Sensor Deleted'
		},
		SAME_MULTI_SENSOR_SCHEDULE_DETAILS: {
			code: 300,
			message: 'Schedule With Same Details Already Exist'
		},
		MULTI_SENSOR_TEMP_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Multi Sensor Notification for Temperature Added Successfully'
		},
		MULTI_SENSOR_HUMIDITY_NOTIFICATION_ADDED: {
			code: 200,
			message: 'Multi Sensor Notification for Humidity Added Successfully'
		},
		MULTI_SENSOR_TEMP_NOTIFICATION_UPDATED: {
			code: 200,
			message: 'Multi Sensor Notification for Temperature Updated Successfully'
		},
		// MULTI_SENSOR_HUMIDITY_NOTIFICATION_UPDATED: {
		// 	code: 200,
		// 	message: 'Multi Sensor Notification for Humidity Updated Successfully'
		// },
		MULTI_SENSOR_NOTIFICATION_DELETED: {
			code: 200,
			message: 'Multi Sensor Notification Deleted Successfully'
		},

		//Philips Hue
		PHILIPS_BRIDGE_ADDED: {
			code: 200,
			message: 'Philips Bridge Added to SpikeBot'
		},
		NO_HUE_BRIDGE_FOUND: {
			code: 501,
			message: 'Philips Bridge link button not pressed'
		},
		SMART_DEVICE_ADDED: {
			code: 200,
			message: 'Smart Device Added Successfully'
		},
		NO_PHILIPS_SMART_BULB_FOUND: {
			code: 501,
			message: 'No Philips Hue Bulb Found!'
		},
		SAME_BRIDGE_ALREADY_EXISTS: {
			code: 301,
			message: 'Philips Hue already Exists'
		},


		//Smart Lock
		SAME_LOCK_BRIDGE_ALREADY_EXISTS: {
			code: 301,
			message: 'Same Bridge already Exists'
		},
		SAME_LOCK_BRIDGE_NAME: {
			code: 301,
			message: 'TTlock Bridge With Same Name Already Exists'
		},
		SMART_LOCK_BRIDGE_ADDED: {
			code: 200,
			message: 'TTlock Bridge Added Successfully'
		},
		LOCK_DELETE: {
			code: 200,
			message: 'Lock Deleted'
		},
		SAME_LOCK_EXISTS: {
			code: 301,
			message: 'Lock Already Exists'
		},

		//Gas Sensor
		SAME_GAS_SENSOR_NAME: {
			code: 301,
			message: 'Gas Sensor With Same Name Already Exists'
		},
		GAS_SENSOR_ADD: {
			code: 200,
			message: 'Gas Sensor Added'
		},
		SAME_UNASSIGNED_GAS_SENSOR_NAME: {
			code: 301,
			message: 'Gas Sensor name already exists in unassigned Gas Sensor List'
		},
		GAS_SENSOR_NAME_UPDATE: {
			code: 200,
			message: 'Gas Sensor Name Updated'
		},
		GAS_SENSOR_DELETE: {
			code: 200,
			message: 'Gas Sensor Deleted'
		},

		//Co2 Sensor
		SAME_CO2_SENSOR_NAME: {
			code: 301,
			message: 'Co2 Sensor With Same Name Already Exists'
		},
		CO2_SENSOR_ADD: {
			code: 200,
			message: 'Co2 Sensor Added'
		},
		SAME_UNASSIGNED_CO2_SENSOR_NAME: {
			code: 301,
			message: 'Co2 Sensor name already exists in unassigned Co2 Sensor List'
		},
		CO2_SENSOR_NAME_UPDATE: {
			code: 200,
			message: 'Co2 Sensor Name Updated'
		},
		CO2_SENSOR_DELETE: {
			code: 200,
			message: 'Co2 Sensor Deleted'
		},

		//Repeator
		SAME_REPEATOR_NAME: {
			code: 301,
			message: 'Repeator With Same Name Already Exists'
		},
		REPEATOR_ADDED: {
			code: 200,
			message: 'Repeator Added'
		},
		REPEATOR_DELETED: {
			code: 200,
			message: 'Repeator Deleted'
		},
		REPEATOR_UPDATED: {
			code: 200,
			message: 'Repeator Updated'
		}

	},


	defaultDeviceSpecificValue: -1,

	NOT_FOUND: 400,
	SUCCESS: 200,
	ERROR: 404,
	CLOUD_CONNECTION_NOT_AVAILABLE: 201,

	isCloudSyncEnabled: 1,

	cloudSync: {
		enabled: 1,
		disabled: 0
	},

	cloudSyncTurn: {
		local: 1,
		cloud: 2
	},

	currentTurn: null,
	isCloudConnected: false,
	isClient: true,

	moduleStatusCache: {

	},

	deviceStatusCache: {},
	deviceStatusCacheAckLeft: {},

	mysecretkeylive: 123,

	camera_tokens: {

	},

	camera_image_names: {

	},

	deviceActiveStatus: {
		active: 1,
		inActive: 0,
		deleted: -1
	},

	moduleActiveStatus: {
		active: 1,
		inActive: 0,
		deleted: -1
	},

	scheduleDeviceStatus: {
		on: {
			value: "on",
			code: 1
		},

		off: {
			value: "off",
			code: 0
		}
	},

	deviceAutoOffStatus: {
		active: 1,
		inActive: 0
	},

	userId: null,
	userEmail: null,
	firstName: null,
	lastName: null,
	userName: null,

	disabledDevicesForAutoTurnOff: {},

	daysValue: {
		Sunday: 0,
		Monday: 1,
		Tuesday: 2,
		Wednesday: 3,
		Thursday: 4,
		Friday: 5,
		Saturday: 6
	},

	//Android Development + Production
	android_platform_arn: 'arn:aws:sns:us-east-1:554906049655:app/GCM/spikebot_anroid_v3_live',

	anroid_live: -1,

	//IOS Development
	ios_platform_arn_sandbox: 'arn:aws:sns:us-east-1:554906049655:app/APNS_SANDBOX/spikebot_ios_v3_sandbox',
	ios_dev: 0,

	//IOS Production
	ios_platform_arn_live: 'arn:aws:sns:us-east-1:554906049655:app/APNS/spikebot_ios_v3_live',
	ios_live: 1,

	panel_type: {
		regular_panel: 1,
		sensor_panel: 2,
		curtain_panel: 3
	},

	sensor_type: {
		no_sensor: -1,
		door_sensor: 0,
		temp_sensor: 1,
		ir_blaster: 2,
		remote: 3,
		camera: -1,
		smart_remote: 4,
		//multi_sensor: 5
		gas_sensor: 5,
		curtain: "curtain",
	},


	default_ir_data: {
		ac: {
			speed: 'AUTO',
			power: 'OFF',
			temperature: '24'
		}
	},

	adminEmail: 'spikebot.tech@spikebot.io',
	adminPassword: 'spikeTECH2389',

	mail_type: {
		error_in_code: 1,
		custom_mood_add: 2
	},

	SENSOR_DATA_TIME: 180,
	REPEATOR_DATA_TIME: 180,
	disable_sensor_icon: "unavailable"

};

module.exports = constants;