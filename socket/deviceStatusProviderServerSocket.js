const moduleModel = require('./../models/module');
const deviceModel = require('./../models/device');
const loginModel = require('./../models/login');
const deviceStatusModel = require('../models/device_status');
let cc2530Util = require('./../util/cc2530-util');
// const { changeDeviceStatus } = require('../controller/deviceStatusController');
// const { changePanelStatus,changeRoomStatus,changeMoodStatus } = require('../controller/roomController');
const mqtt = require('mqtt');

const initiateConnect = async function (socket) {

    logger.info('LOGIN KEY : ', socket.handshake.query.token);
    const loginKey = await loginModel.getByAuthKey(socket.handshake.query.token);
    logger.info('INITIAL CONNECT : ', loginKey);
    if (loginKey) {

        logger.info('IN LOGIN KEY');
        socket.authenticatedUser = {
            user_id: loginKey.user_id,
            auth_key: socket.handshake.query.token,
            admin: loginKey.admin.toString(),
            phone_id: loginKey.access_identifier,
            phone_type: loginKey.access_type
        };

        logger.info('[SOCKET IO] - USER CONNECTED - ' + loginKey.access_type);

        socket.join(loginKey.user_id);
    }

}

module.exports = function (homeAutomationModule, appContext, shared) {

   
    let io = shared.util.socket_io;

    // MQTT 
    let mqttClient = mqtt.connect('mqtt://127.0.0.1:1883');

    mqttClient.on('connect', () => {


        mqttClient.subscribe('ESP/Temperature');

        mqttClient.on('message', async function (topic, message, packet) {

            if (topic == 'beacon-scan') {

            } else if (topic == 'beacon-data') {

            } else if (topic == 'ESP/Temperature') {

                //logger.info('MQTT Temperature Received',message.toString());
                message = JSON.parse(message.toString());
                const device = await deviceModel.getByDeviceIdentifierAndModuleIdentifier(message.module_identifier, message.module_identifier);

                if (device) {

                    moduleModel.updateLastResponseTime(message.module_identifier);
                    deviceStatusModel.updateMany({
                        device_id: device.device_id,
                        device_status: message.temp
                    });

                    io.emit('changeIrBlasterTemperature', {
                        ir_blaster_id: device.device_id,
                        temperature: message.temp
                    });

                }

            } else {
                io.emit('ir-command', {
                    topic: topic,
                    message: message
                });
            }

        });
    });

    // io.use((socket, next) => {
    //     let token = socket.handshake.query.token;
    //     // if (isValid(token)) {
    //     return next();
    // });

   

    io.sockets.on('connection', function (socket) {

        // logger.info("Socket Connected", socket.id);
        // let token = socket.handshake.query.token;

        initiateConnect(socket);

        // socket.on('user:changeDeviceStatus', function (params) {
        //     params.authenticatedUser = { ...socket.authenticatedUser };
        //     changeDeviceStatus(params);
        // });

        // socket.on('user:changePanelStatus', function (params) {
        //     param.authenticatedUser = { ...socket.authenticatedUser };
        //     changePanelStatus(params);
        // });

        // socket.on('user:changeRoomStatus', function (params) {
        //     param.authenticatedUser = { ...socket.authenticatedUser };
        //     changeRoomStatus(params);
        // });

        // socket.on('user:changeMoodStatus', function (params) {
        //     param.authenticatedUser = { ...socket.authenticatedUser };
        //     changeMoodStatus(params);
        // });

        socket.on('disconnect', function () {
            logger.info("Socket Disconnected ", socket.authenticatedUser.phone_type);
        });

        socket.on('socketHeavyLoadValues', async function (data) {
            logger.info("HEAVY LOAD SOCKET");
            let moduleData = await moduleModel.get(data.module_id);
            data.module_id = moduleData.module_identifier;
            data.device_id = 0;

            cc2530Util.getHeavyLoadValues(data, function (error, result) {
                if (error) {
                    logger.error("Socket change device error ", error);
                }
            });

        });


        socket.on('socketGasSensorValues', function (data) {
            cc2530Util.getGasValue(data, function (error, result) {
                if (error) {
                    logger.error("Socket change device error ", error);
                }
            });
        });

        /**
         * Subscribe to ir blaster id
         * @param device_id
         */
        socket.on('subscribeToIRBlaster', async function (params) {

            //logger.info('IR BLASTER SUBSCRIPTION', params);

            const device = await deviceModel.get(params.device_id);
            mqttClient.publish('PI/' + device.device_identifier, '45678');
            mqttClient.subscribe('ESP/' + device.device_identifier);

        });

        /**
         * Send Command To IR Blaster
         * @param device_id
         * @param command
         */
        socket.on('sendToIRBlaster', async function (params) {

            //logger.info('IR BLASTER TEST', params);
            const device = await deviceModel.get(params.device_id);
            mqttClient.publish('PI/' + device.device_identifier, params.command);

        });

    });
}