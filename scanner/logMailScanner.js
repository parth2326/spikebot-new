//Cron Job for backup of logs in GoogleDrive every Month

const exec = require('child_process').exec;
const mailUtil = require('../util/mail-util.js');

const moment = require('moment');

module.exports = function (homeAutomationModule, appContext, shared) {
    let CronJobManager = shared.util.CronJobManager,
        manager = new CronJobManager();

    let logsScheduleId = "spikeBotLogsId";
    let timerPattern = "10 0 1 * *"; //At 00:10 on day-of-month 1

    let logMonthlyZip = function () {

        let date = new Date(),
            y = date.getFullYear(),
            m = date.getMonth();
        let firstDay = new Date(y, m, 1);
        let lastDay = new Date(y, m + 1, 0);

        firstDay = moment(firstDay).format('YYYY-MM-DD');
        lastDay = moment(lastDay).format('YYYY-MM-DD');
        month = moment(date).format('MMMM');

        //logger.info("into empty file mode");
        exec("sudo touch " + month + ".txt", function (error1, stdout, stderr) {
            if (error1 !== null) {
                console.log('exec error: ' + error1);
            } else {
                //logger.info("into permission mode");
                exec("sudo chmod 777 " + month + ".txt", function (error2, stdout, stderr) {
                    if (error2 !== null) {
                        console.log('exec error: ' + error2);
                    } else {
                        //logger.info("into append mode");
                        exec("find . -type f -newermt " + firstDay + " ! -newermt " + lastDay + " -name '*.gz' > " + month + ".txt", function (error3, stdout, stderr) {
                            if (error3 !== null) {
                                console.log('exec error: ' + error3);
                            } else {
                                //logger.info("into zip mode");
                                exec("cat " + month + ".txt | sudo zip -r  /home/pi/node/homeController/logs/" + month + ".zip -@", function (error4, stdout, stderr) {
                                    if (error4 !== null) {
                                        console.log('exec error: ' + error3);
                                    } else {
                                        //logger.info('zip format stdout: ', stdout);
                                        exec("sudo chmod 777 /home/pi/node/homeController/logs/" + month + ".zip | sudo rm -rf " + month + ".txt", function (error5, stdout, stderr) {
                                            if (error5 !== null) {
                                                console.log('exec error: ' + error3);
                                            } else {
                                                //logger.info('File deleted');
                                                data = {
                                                    month: month
                                                };
                                                mailUtil.logEmail(data);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    };


    manager.add(logsScheduleId, timerPattern, function () {
        logMonthlyZip();
    });

    manager.start(logsScheduleId);

};