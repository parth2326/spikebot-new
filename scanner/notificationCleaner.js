const logsModel = require('./../models/logs');

_notficationCleaner = {};
_notficationCleaner.cleanNotification = async () => {

    //logger.info('Clear Logs Before 1 Month - Executed');
    await logsModel.deleteLogsBeforeDays(30);

}

setTimeout(function () {
    _notficationCleaner.cleanNotification();
}, 2000);

module.exports = _notficationCleaner;