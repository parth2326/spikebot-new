// //Cron Job for backup of logs in GoogleDrive every Month

const exec = require('child_process').exec;
//var mailUtil = require('../util/mail-util.js');

const moment = require('moment');

module.exports = function (homeAutomationModule, appContext, shared) {
    let CronJobManager = shared.util.CronJobManager,
        manager = new CronJobManager(),
        constants = homeAutomationModule.constants,
        viewerDetails = appContext.viewerDetails;

    let logsScheduleId = "spikeBotLogsId";
    let dbScheduleId = "spikeBotDbId";
    let piRebootID = "SpikeBotRebootId";
    let piTemperatureID = "SpikeBotTempId";
    let piCPUID = "SpikeBotCPUId";

    let timerPatternlog = "10 0 1 * *"; //At 00:10 on day-of-month 1
    let timerPatterndb = "30 0 * * *"; //everyday at 12:30am everyday
    let timerPatternreboot = "30 3 * * *";
    let timerPatternTemp = "*/5 * * * *";
    let timerPatternCPU = "*/6 * * * *";


    let home_directory_path = constants.home_directory_path;

    //   function for extracting zip file and move it to s3 bucket
    let logMonthlyZip = function () {

        let date = new Date(),
            y = date.getFullYear(),
            m = date.getMonth();
        let startDate = new Date(y, m - 2, 1);
        let endDate = new Date(y, m + 1, 0);

        startDate = moment(startDate).format('YYYY-MM-DD');
        endDate = moment(endDate).format('YYYY-MM-DD');
        month = moment(date).subtract(1, "month").format('MMMM');

        ////make empty file
        exec("sudo touch " + month + ".txt", function (error1, stdout, stderr) {
            if (error1 !== null) {
                logger.error('exec error: ' + error1);
            } else {
                //give 777 permission to the empty file
                exec("sudo chmod 777 " + month + ".txt", function (error2, stdout, stderr) {
                    if (error2 !== null) {
                        logger.error('exec error: ' + error2);
                    } else {
                        //  find the .gz files from 1st date of the month till last date of the month and append it to empty file
                        exec("find . -type f -newermt " + startDate + " ! -newermt " + endDate + " -name '*.gz' > " + month + ".txt", function (error3, stdout, stderr) {
                            if (error3 !== null) {
                                logger.error('exec error: ' + error3);
                            } else {
                                // generate the zip file with month as name and save it in /logs
                                exec("cat " + month + ".txt | sudo zip -r  " + home_directory_path + "/logs/" + month + ".zip -@", function (error4, stdout, stderr) {
                                    if (error4 !== null) {
                                        logger.error('exec error: ' + error3);
                                    } else {
                                        // give 777 permission to the zip file and delete the created empty file
                                        exec("sudo chmod 777  " + home_directory_path + "/logs/" + month + ".zip | sudo rm -rf " + month + ".txt", function (error5, stdout, stderr) {
                                            if (error5 !== null) {
                                                clogger.error('exec error: ' + error3);
                                            } else {
                                                data = {
                                                    month: month
                                                };
                                                //path of the created zip file
                                                let filePath = home_directory_path + "/logs/" + month + ".zip";

                                                let finduserdata = sqliteDB.prepare(constants.GET_USER_DETAILS_FOR_S3);
                                                finduserdata.all(viewerDetails.deviceID, function (error, result) {
                                                    if (error) {
                                                        logger.error("User Details Not Found!");
                                                        return 0;
                                                    } else {
                                                        data = {
                                                            month: month,
                                                            email: result[0].user_email,
                                                            user_id: result[0].user_id,
                                                            filePath: filePath,
                                                            type: 'logs'
                                                        };

                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    };


    // let dailyDB = function () {
   
    // };


    let rebootpi = function () {
        exec("sudo reboot", function (error, stdout, stderr) {
            //logger.info('pi reboot exec stdout: ', stdout);
            //logger.info('pi reboot exec stderr: ', stderr);
            //logger.info('pi reboot exec error: ', error);
        });
    };

    let piTemp = function () {
        exec("sudo vcgencmd measure_temp", function (error, stdout, stderr) {
            ////logger.info('pi temperature exec stdout: ', stdout);
            //   //logger.info('pi temperature exec stderr: ', stderr);
            //  //logger.info('pi temperature exec error: ', error);
        });
    };


    let piCPUusage = function () {
        exec("sudo node /home/pi/node/homeController/cpu_usage.js", function (error, stdout, stderr) {
            // //logger.info('pi temperature exec stdout: ', stdout);
            //    //logger.info('pi temperature exec stderr: ', stderr);
            //   //logger.info('pi temperature exec error: ', error);
        });
    };


    //manager schedule for logs
    manager.add(logsScheduleId, timerPatternlog, function () {
        logMonthlyZip();
    });

    //manager schedule for db
    manager.add(dbScheduleId, timerPatterndb, function () {
        // dailyDB();
    });


    // manager schedule for Pi reboot
    manager.add(piRebootID, timerPatternreboot, function () {
        rebootpi();
    });


    manager.add(piTemperatureID, timerPatternTemp, function () {
        piTemp();
    });

    manager.add(piCPUID, timerPatternCPU, function () {
        piCPUusage();
    });

    manager.start(logsScheduleId);
    manager.start(dbScheduleId);
    manager.start(piRebootID);
    manager.start(piTemperatureID);
    manager.start(piCPUID);

};



