/**
 * Scanner created to delete old videos in case of free space in pi is very less
 * Added By : Parth Patel
 */

const nodeDiskInfo = require('node-disk-info');
const fs = require('fs');
let fsPromises = fs.promises;
let _scanner = {};
const recordingDirectory = process.env.PROJECT_PATH + '/static/storage/volume/pi/';

_scanner.init = async function () {


    try {

        // while (15 > await _scanner.getFreeSpacePercentage()) {

        // Get list of files in recording directory
        // and order them based on modified date
        let files = await fsPromises.readdir(recordingDirectory);
        files = files.map(function (fileName) {
            const stats = fs.statSync(recordingDirectory + '/' + fileName);
            return {
                name: fileName,
                time: stats.mtime.getTime(),
                size: stats.size
            };
        })
            .sort(function (a, b) {
                return a.time - b.time;
            })
            .map(function (v) {
                return v.name;
            });


        // Delete fifteen videos in one go to empty space
        for (let videoFile of files.slice(0, 15)) {
            fs.unlinkSync(recordingDirectory + "/" + videoFile);
        }

        // }

    } catch (e) {
        console.error(e);
    }

}

_scanner.getFreeSpacePercentage = async function () {
    try {

        // Get All The Disks
        const disks = nodeDiskInfo.getDiskInfoSync();

        // Find the disk of our particular choice
        const rootDisk = disks.find(function (item) {
            return item.filesystem == '/dev/root';
        });

        // Calculate free space percentage 
        if (rootDisk) {
            const freeSpacePercentage = (rootDisk.available / rootDisk.blocks) * 100;
            logger.warn('SYSTEM - FREE SPACE REMAINING IS ', freeSpacePercentage);
            return freeSpacePercentage;
        } else {
            return 100;
        }

    } catch (error) {
        return 100;
    }
}


// Clean space every 15 mins
setTimeout(function () {
    _scanner.init();
}, 900000);

module.exports = _scanner;