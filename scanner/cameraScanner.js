const {
    homeAutomationModule,
    appContext,
    shared,
    // clientSocketObj
} = require('./../util/general-util');

// module.exports = function (homeAutomationModule, appContext, shared, clientSocketObj) {
const appUtil = homeAutomationModule.appUtil;

// module.exports = function (homeAutomationModule, appContext,sqliteDB) {

// var appUtil = homeAutomationModule.appUtil,
constants = homeAutomationModule.constants,
    cameraController = homeAutomationModule.cameraController;
sqliteDB = shared.db.sqliteDB;

let cameraTokens = function () {

    cameraController.getCameraIds(function (error, result) {
        if (error !== null) {
            logger.error("getCameraIds Error while selecting camera ids. Error: ", error);
        } else {
            //  //logger.info("result of camera Ids", result);
            if (result.code === constants.responseCode.SUCCESS.code) {
                let cameraIdList = result.data.cameraIdList;

                for (let i = 0; i < cameraIdList.length; i++) {
                    let cameraId = cameraIdList[i].camera_id;

                    appUtil.generateCameraToken(cameraId, function (error, result) {
                        if (error) {
                            //logger.info("Error in generating camera token");
                        } else {
                            let token = result.data.append_token,
                                camera_id = result.data.camera_id;

                            constants.camera_tokens[camera_id] = {
                                camera_id: camera_id,
                                token: token
                            };

                        }
                    });
                }
            }
        }
    });
};


// let getAdminUserDetails = function () {
//     let userId = '',
//         userEmail = '',
//         userName = '';
//     // //logger.info("Request - constants.userId " , constants.userId , " constants.userEmail " , constants.userEmail );
//     if (constants.userId == null || constants.userId == "" || constants.userEmail == "" || constants.userEmail == null) {
//         let finduserdata = sqliteDB.prepare(constants.GET_USER_DETAILS);
//         finduserdata.all(function (error, result) {
//             if (error) {
//                 logger.error("User Details Not Found!");
//                 return 0;
//             } else {
//                 if (result.length > 0) {
//                     userId = result[0].user_id;
//                     userEmail = result[0].user_email;
//                     userName = result[0].first_name;
//                     constants.userEmail = userEmail;
//                     constants.userId = userId;
//                     constants.userName = userName;
//                 }
//             }
//         });
//     }
// };


// let cameraVideoDownload = function () {
//     exec("sudo sh /camera/rcd/rcd.sh", function (error, stdout, stderr) {
//         //logger.info('stdout: ', stdout);
//         //logger.info('stderr: ', stderr);
//         //logger.info('exec error: ', error);
//     });
// };

// setInterval(() => {
//     cameraVideoDownload();
// }, 900000);


setInterval(() => {
    cameraTokens();
}, 7200000); //2 hours

setTimeout(function () {
    cameraTokens();
}, 30000); //30 seconds

// setTimeout(function () {
//     getAdminUserDetails();
// }, 100);

