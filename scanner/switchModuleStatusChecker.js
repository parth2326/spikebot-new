const moduleModel = require('./../models/module');

module.exports = function (homeAutomationModule, appContext) {
        deviceUtil = homeAutomationModule.deviceUtil
    
    let sleep = async function (ms){
        return new Promise(resolve=>{
            setTimeout(resolve,ms)
        })
    }

    let switchModuleStatusChecker = async function (params) {
     
        let moduleList = await moduleModel.listByConfiguration();
        for(let deviceModule of moduleList)
        {
            if(['heavy_load','double_heavy_load','5','5f, 5f-wifi'].includes(deviceModule.module_type.toString())){

                //logger.info(" Switch Module Status Checker ",deviceModule.module_identifier);

                if (params === false) {
                    await sleep(30000);
                }
                deviceUtil.getSwitchModuleStatus(deviceModule.module_identifier);
            }
        }

    };
 
    
    // Check Module Status When Server is Restarted
    setTimeout(function () {
        switchModuleStatusChecker(true);
    }, 2000);

    // Check Module Status Every 30 Minutes
    setInterval(function(){
        switchModuleStatusChecker(false);
    },1800000);

};