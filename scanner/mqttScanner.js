const mqtt = require('mqtt');
const logger = require('../util/logger');
const eventEmitter= require('./../util/event-manager');
let mqttClient = mqtt.connect('mqtt://127.0.0.1:1883');

mqttClient.on('connect', () => {

    // mqttClient.subscribe('/ESP32/5f');
    logger.info('MQTTT SUBSCRIBE')
    mqttClient.subscribe('/ESP32/5fACK');

    mqttClient.on('message', async function (topic, message, packet) {
        eventEmitter.emit(topic,message.toString());
    });

});

/** Emit Data From MQTT */
eventEmitter.on('emitMQTT', async function (data) {

    logger.info('[MQTT EMIT]',data.topic);

    mqttClient.publish(data.topic,data.data,function(){

    });
});

module.exports = mqttClient;