const Client = require('node-rest-client').Client;
const restClient = new Client();

let moduleName;
let constants;
let cloudURL;
let appUtil;
let viewerDetails;
let homeControllerDeviceId;
let homeControllerIP;

let headers = {
    "Content-Type": "application/json"
};

const powerEventListener = function (eventType, cb) {
    cb();
};

exports.setParams = function (homeAutomationModule, appContext, shared) {
    moduleName = homeAutomationModule.moduleName;
    constants = homeAutomationModule.constants;
    cloudURL = homeAutomationModule.cloudURL;
    appUtil = homeAutomationModule.appUtil;
    viewerDetails = appContext.viewerDetails;
    homeControllerDeviceId = viewerDetails.deviceID;
    homeControllerIP = viewerDetails.deviceIP;
};

/* Profile sync is always enabled*/
exports.callRestUserProfiles = function (userList, cb) {
    //logger.info("callRestUserProfiles", userList.length);
    if (!constants.isCloudConnected) {
        logger.warn("callRestUserProfiles Home controller is not connected to cloud cloudURL: ", cloudURL);
        return;
    }
    let args = {
        data: {
            userList: userList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateUserProfiles", args, function (data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestUserProfiles FATAL Error while parsing data received from updateUserProfiles. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
};

// Update Home Controller Status
exports.callRestForHomeController = function (homeControllerList, cb) {
    //logger.info("callRestForHomeController", homeControllerList.length);
    if (!constants.isCloudConnected) {
        logger.warn("callRestForHomeController Home controller is not connected to cloud cloudURL: ", cloudURL);
        return;
    }
    let args = {
        data: {
            homeControllerList: homeControllerList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateHomeController", args, function (data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestForHomeController FATAL Error while parsing data received from callRestForHomeController. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
};

/* Master Login sync is always enabled*/
exports.callMstLoginDetails = function (LoginList, cb) {

    //logger.info("INTO callMstLoginDetails", LoginList.length);
    if (!constants.isCloudConnected) {
        logger.warn("callMstLoginDetails Home controller is not connected to cloud cloudURL: ", cloudURL);
        return;
    }
    let args = {
        data: {
            LoginList: LoginList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateMstLogin", args, function (data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callMstLoginDetails FATAL Error while parsing data received from getMstLoginDetails. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
};

/* For Creating ARN*/
exports.createARNOnCloud = function (ARNDetails, cb) {

    //logger.info("INTO createARNOnCloud", ARNDetails);
    if (!constants.isCloudConnected) {
        logger.warn("createARNOnCloud Home controller is not connected to cloud cloudURL: ", cloudURL);
        return cb(null, false);
    }
    let args = {
        data: {
            ARNDetails: ARNDetails,
        },
        headers: headers
    };
    restClient.post(cloudURL + "/createARNOnCloud", args, function (data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, data);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("createARNOnCloud FATAL Error while parsing data received from createARNOnCloud. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
};

/* Recent activity sync is always enabled*/
/*exports.callRestForNotification = function (notificationList, cb) {
    if (!constants.isCloudConnected) {
        logger.warn('callRestForRecentActivity Home controller is not connected to cloud cloudURL: ', cloudURL);
        return;
    }
    var args = {
        data: {
            notificationList: notificationList,
            homeControllerDeviceId: homeControllerDeviceId,
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateNotification", args, function (data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestForNotification Errorr while parsing data received from updateNotification. Error: ", error);
                return cb(null, false);
            }
        }
    });
};*/


//Update System Devices List
/* exports.callRestForSystemDevices = function(systemDeviceList, cb){
    if (appUtil.isCloudSyncEnabled() === false) {
        logger.warn('callRestForMasterDevices Cloud sync is disabled');
        return;
    }
    if (!constants.isCloudConnected) {
        logger.warn('callRestForSystemDevices Home controller is not connected to cloud cloudURL: ', cloudURL);
        return;
    }
    var args = {
        data: {
            systemDeviceList: systemDeviceList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateSystemDevices", args, function(data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestForSystemDevices FATAL Error while parsing data received from callRestForSystemDevices. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
}; */


//Update Master Devices List
/* exports.callRestForMasterDevices = function(mstDeviceList, cb) {
    if (appUtil.isCloudSyncEnabled() === false) {
        logger.warn('callRestForMasterDevices Cloud sync is disabled');
        return;
    }
    if (!constants.isCloudConnected) {
        logger.warn('callRestForMasterDevices Home controller is not connected to cloud cloudURL: ', cloudURL);
        return;
    }
    var args = {
        data: {
            mstDeviceList: mstDeviceList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateMasterDeviceList", args, function(data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestForMasterDevices FATAL Error while parsing data received from updateMasterDeviceList. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
}; */

//Update Room Device Mapping Devices
/* exports.callRestUpdateBatchTrans = function(deviceList, homeControllerDeviceId, cb) {
    if (appUtil.isCloudSyncEnabled() === false) {
        logger.warn("callRestUpdateBatchTrans Cloud sync is disabled");
        return;
    }
    if (!constants.isCloudConnected) {
        logger.warn("callRestUpdateBatchTrans Home controller is not connected to cloud cloudURL: ", cloudURL);
        return;
    }
    var args = {
        data: {
            deviceList: deviceList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateBatchTrans", args, function(data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestUpdateBatchTrans FATAL Error while parsing data received from updateBatchTrans. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
}; */



/* Camera List sync */
/* exports.callRestCameralist = function(cameraList, cb){
    if (!constants.isCloudConnected) {
        logger.warn('callRestCameralist Home controller is not connected to cloud cloudURL: ', cloudURL);
        return;
    }
    var args = {
        data: {
            cameraList: cameraList,
            homeControllerDeviceId: homeControllerDeviceId,
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateCameraList", args, function(data, response) {
        if (cb) {
            try {
               if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestCameralist Errorr while parsing data received from updateCameraList. Error: ", error);
                return cb(null, false);
            }
        }
    });
}; */

/* Temp Sensor sync */
/* exports.callRestTempSensorList = function(tempSensorList, cb){
    if (!constants.isCloudConnected) {
        logger.warn('callRestTempSensorList Home controller is not connected to cloud cloudURL: ', cloudURL);
        return;
    }
    var args = {
        data: {
            tempSensorList: tempSensorList,
            homeControllerDeviceId: homeControllerDeviceId,
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateTempSensorList", args, function(data, response) {
        if (cb) {
            try {
               if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestTempSensorList Errorr while parsing data received from updateTempList. Error: ", error);
                return cb(null, false);
            }
        }
    });
}; */


//========================== scheduleDeviceScanner =======================================
/* exports.callRestForDeviceTimers = function(scheduledDeviceList, autoOffDeviceList, homeControllerDeviceId, cb) {
    if (appUtil.isCloudSyncEnabled() === false) {
        logger.warn("callRestForDeviceTimers Cloud sync is disabled");
        return;
    }
    if (!constants.isCloudConnected) {
        logger.warn("callRestForDeviceTimers Home controller is not connected to cloud cloudURL: ", cloudURL);
        return;
    }
    var args = {
        data: {
            scheduledDeviceList: scheduledDeviceList,
            autoOffDeviceList: autoOffDeviceList,
            homeControllerDeviceId: homeControllerDeviceId
        },
        headers: headers
    };
    restClient.post(cloudURL + "/updateDeviceTimerSchedule", args, function(data, response) {
        if (cb) {
            try {
                if (data.code === constants.responseCode.SUCCESS.code) {
                    return cb(null, true);
                } else {
                    return cb(null, false);
                }
            } catch (error) {
                logger.error("callRestForDeviceTimers FATAL Error while parsing data received from updateDeviceTimers. Error: ", error, data);
                return cb(null, false);
            }
        }
    });
}; */