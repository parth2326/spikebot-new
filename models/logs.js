const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');
const moment = require('moment');
const generalUtil = require('./../util/general-util');
let homeControllerDeviceId = generalUtil.appContext.viewerDetails.deviceID;
const eventsEmitter = require("./../util/event-manager");

const log = function () { };


/**
 * Add Log to the database
 * @param log_type 
 * @param device_id 
 * @param module_id
 * @param room_id
 * @param panel_id
 * @param alert_id
 * @param camera_id
 * @param schedule_id
 * @param user
 */
log.prototype.add = async function (params, user = {
    phone_id: "0000",
    phone_type: "SpikeBot",
    user_id: null
}, meta = {}) {

    const logModel = this;

    process.nextTick(async function () {


        try {  




            // Set Default Parameters if the parameters are not found
            params.log_id = appUtil.generateRandomId('LOG');

            // if(user===null){
            //     user = {
            //         phone_id: "0000",
            //         phone_type: "SpikeBot",
            //         user_id: null
            //     };
            // }

            params.phone_id = user.phone_id;
            params.user_id = user.user_id;
            params.phone_type = user.phone_type;
            params.show = params.show === 0 ? 0 : 1;
            params.created_at = new Date().valueOf().toString();

            let columns = Object.keys(params);
            let columnSeperated = columns.join(",");

            let questions = [];
            let questionSeperated = "";

            for (let i = 0; i < columns.length; i++) {
                questions.push("?");
            }

            questionSeperated = questions.join(",")

            const query = "INSERT INTO spike_logs (" + columnSeperated + ") VALUES (" + questionSeperated + ")";

            await dbManager.executeNonQuery(query, Object.values(params));

            /**
             * For Mood And Room Logs
             */
            if (['room_add', 'room_update', 'room_off', 'room_on'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    'UPDATE spike_logs SET description=(SELECT room_name from spike_rooms where room_id=?) where log_id=?', [params.log_object_id, params.log_id]
                );
            } else if (['room_delete'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    'UPDATE spike_logs SET description=? where log_id=?', [(JSON.parse(params.deleted_data)).room_name, params.log_id]
                );
                return;
            }

            if (['mood_add', 'mood_update', 'mood_off', 'mood_on'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    'UPDATE spike_logs SET description=(SELECT mood_name from mst_mood_list where mood_id=?) where log_id=?', [params.log_object_id, params.log_id]
                );
            } else if (['mood_delete'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    'UPDATE spike_logs SET description=? where log_id=?', [(JSON.parse(params.deleted_data)).mood_name, params.log_id]
                );
                return;
            }


            /**
             * For Panel
             */
            if (['panel_add', 'panel_update', 'panel_on', 'panel_off'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    `UPDATE spike_logs SET description=(SELECT spike_panels.panel_name || "|" || spike_rooms.room_name 
                                                        from spike_panels inner join spike_rooms on spike_rooms.room_id=spike_panels.room_id
                                                        where spike_panels.panel_id=? 
                                              ) where log_id=?`, [params.log_object_id, params.log_id]
                );
            } else if (['panel_delete'].includes(params.log_type)) {
                await dbManager.executeNonQuery(
                    'UPDATE spike_logs SET description=(? || "|" || (SELECT room_name from spike_rooms where room_id=?)) where log_id=?', [(JSON.parse(params.deleted_data)).panel_name, params.log_object_id, params.log_id]
                );
                return;
            }

            /**
             * For Alert Logs
             */
            if (['alert_add', 'alert_update', 'alert_delete', 'alert_enable', 'alert_disable', 'alert_active'].includes(params.log_type)) {

                if (params.log_type == 'alert_delete') {

                    await dbManager.executeNonQuery(
                        'UPDATE spike_logs SET description=(SELECT device_name from spike_devices where device_id=?) where log_id=?', [(JSON.parse(params.deleted_data)).device_id, params.log_id]
                    );

                } else if (params.log_type == 'alert_active') {

                    await dbManager.executeNonQuery(
                        `UPDATE spike_logs SET description=(SELECT spike_devices.device_name || '|' || spike_rooms.room_name from spike_devices 
                        left join spike_panel_devices on spike_panel_devices.device_id=spike_devices.device_id
                        left join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id
                        left join spike_rooms on spike_rooms.room_id=spike_panels.room_id 
                        where spike_devices.device_id=? limit 1) 
                        where log_id=?`, [params.log_object_id, params.log_id]
                    );
 
                    eventsEmitter.emit('emitDeviceAndGeneralCounter', {
                        user_id:params.user_id,
                        device_id:params.log_object_id,
                        log_sub_type: params.log_sub_type
                    });


                    // if (meta.sub_description) {

                        // const socketEmitter = require('./../util/general-util').shared.util.socket_io;

                        // const generalMetaModel = require('./../models/general_meta');
                        // generalMetaModel.add({
                        //     meta_id: appUtil.generateRandomId("META"),
                        //     table_id: params.log_id,
                        //     table_name: "log",
                        //     meta_name: "sub_description",
                        //     meta_value: meta.sub_description
                        // });

                        // const deviceCounter = await logModel.getUnseenDeviceAlertsCount(params.user_id, params.log_object_id);
                        // eventsEmitter.emit('emitSocketTo', {
                        //     topic: 'updateDeviceBadgeCounter',
                        //     to: params.user_id,
                        //     data: {
                        //         user_id: params.user_id,
                        //         device_id: params.log_object_id,
                        //         counter: deviceCounter
                        //         }
                        // });

                        // let count = await logModel.getAllUnseenAlertsCounter({
                        //     user_id: params.user_id
                        // });


                        // eventsEmitter.emit('emitSocketTo', {
                        //     topic: 'generalNotificationCounter',
                        //     to: params.user_id,
                        //     data: count
                        // }); 
                        
                
                        // socketEmitter.to(params.user_id).emit('generalNotificationCounter', count);

                    // }

                } else {
                    await dbManager.executeNonQuery(
                        'UPDATE spike_logs SET description=(SELECT device_name from spike_devices where device_id=(SELECT device_id from spike_alerts where alert_id=?)) where log_id=?', [params.log_object_id, params.log_id]
                    );
                }
                return;
            }

            /**
             * For Schedule Logs
             */
            if (['schedule_add', 'schedule_update', 'schedule_delete', 'schedule_enable', 'schedule_disable', 'schedule_on', 'schedule_off'].includes(params.log_type)) {

                if (params.log_type == 'schedule_delete') {

                    await dbManager.executeNonQuery(
                        'UPDATE spike_logs SET description=? where log_id=?', [(JSON.parse(params.deleted_data)).schedule_name, params.log_id]
                    );

                } else {

                    await dbManager.executeNonQuery(
                        'UPDATE spike_logs SET description= (SELECT schedule_name from spike_schedules where schedule_id=?) where log_id=?', [params.log_object_id, params.log_id]
                    );

                }
                return;

            }

            /**
             * For Device Logs
             */
            if (params.description == null && ['device_on', 'device_off', 'device_add', 'device_update', 'device_delete', 'gas_detected', 'temp_alert', 'door_open', 'door_close', 'water_detected', 'door_lock', 'door_unlock','password_change'].includes(params.log_type)) {
                
                if (params.log_type == 'device_delete') {

                    await dbManager.executeNonQuery(
                        'UPDATE spike_logs SET description=? where log_id=?', [(JSON.parse(params.deleted_data)).device_name, params.log_id]
                    );

                } else {

                    await dbManager.executeNonQuery(
                        `UPDATE spike_logs SET description=(SELECT spike_devices.device_name || '|' || spike_panels.panel_name || '|' || spike_rooms.room_name from spike_devices 
                        left join spike_panel_devices on spike_panel_devices.device_id=spike_devices.device_id
                        left join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id
                        left join spike_rooms on spike_rooms.room_id=spike_panels.room_id 
                        where spike_devices.device_id=? limit 1) 
                        where log_id=?`, [params.log_object_id, params.log_id]
                    );

                }

                return;

            }


            return true;

        } catch (error) {
            logger.error('[LOG INSERT]', error);
        }

    });


}

log.prototype.categories = async function () {
    const query = 'select * from spike_log_categories';
    return await dbManager.all(query);
}


/**
 * Get All Logs
 */
log.prototype.all = async function () {
    const query = "SELECT * FROM spike_logs";
    return await dbManager.all(query);
}

log.prototype.getUnseenLogsCounter = async function () {

}

/**
 * Get Unseen Room Logs Counter
 * @param user_id
 * @param room_id
 */
log.prototype.getUnseenRoomLogsCounter = async function (user_id, room_id) {

    const query = `SELECT count(0) as total_unseen FROM spike_logs WHERE (log_object_id = ? 
        OR 
        log_object_id IN (SELECT panel_id from spike_panels where room_id=?)
        OR log_object_id IN (
                            SELECT device_id from spike_panel_devices WHERE
                                panel_id IN (
                                    SELECT panel_id from spike_panels where room_id=?    
                                )
                        ))
        AND (seen_by NOT like ? OR seen_by IS NULL )`;

        let counter = await dbManager.get(query, [room_id, room_id, room_id, `%${user_id}%`]);
    return counter.total_unseen;
}

/**
 * Get Unseen Camera Logs Counter
 * @param user_id
 * @param camera_id
 */
log.prototype.getUnseenCameraLogsCounter = async function (user_id, camera_id) {

    const query = `SELECT count(0) as total_unseen FROM spike_logs WHERE camera_id=?
        AND (seen_by NOT like ? OR seen_by IS NULL )`;

        let counter = await dbManager.get(query, [camera_id, `%${user_id}%`]);
    return counter.total_unseen;
}

/**
 * Delete logs before days
 * @param days
 */
log.prototype.deleteLogsBeforeDays = async function (days) {

    let date = moment().subtract(days, 'days').toDate().valueOf().toString();
    const query = `DELETE from spike_logs where is_sync=1 AND CAST(created_at as REAL) < ? `;
    await dbManager.executeNonQuery(query, [date]);
    return true;
}

/**
 * Get Unseen Room Alerts (Notifications) Counter
 * @param user_id
 * @param room_id
 */
log.prototype.getUnseenRoomAlertsCounter = async function (user_id, room_id) {

    const query = `SELECT count(0) as total_unseen FROM spike_logs WHERE
    log_object_id IN (
                        SELECT device_id from spike_panel_devices WHERE
                            panel_id IN (
                                SELECT panel_id from spike_panels where room_id=?    
                            )
                    )
    AND log_type IN ('door_open','door_close','temp_alert','gas_detected','water_detected','door_lock','door_unlock')
    AND (show=1 OR show IS NULL) 
    AND (seen_by NOT LIKE ? OR seen_by IS NULL ) and description NOT NULL`;
    let counter = await dbManager.get(query, [room_id, `%${user_id}%`]);
    return counter.total_unseen;
}

/**
 * Get All Unseen Alerts  (Notifications) Counter Of User
 * @param authenticatedUser Object(user_id,admin)
 */
log.prototype.getAllUnseenAlertsCounter = async function (authenticatedUser) {

    let applicableLogTypes = generalUtil.allGeneralNotificationsType;
    // query += "  ('" + applicableLogTypes.join("','") + "')";

    let query = `SELECT count(0) as total_unseen FROM spike_logs WHERE
    log_type IN (${"'" + applicableLogTypes.join("','") + "'"})
    AND (seen_by NOT LIKE ? OR seen_by IS NULL ) AND (show=1 OR show IS NULL) and description NOT NULL `;

    let queryParams = [`%${authenticatedUser.user_id}%`];

    // Show the logs based on user priviledge
    // if (authenticatedUser.admin == 1) {

    // query += ` AND (log_object_id IN (
    //                         SELECT device_id from spike_panel_devices where panel_id IN (
    //                             SELECT panel_id from spike_panels where room_id IN (
    //                                 SELECT room_id from spike_rooms where room_users LIKE ?
    //                             )
    //                         )
    //                     ) 
    //             OR log_object_id IN ( SELECT camera_id from mst_camera_devices) 
    //             OR log_object_id = '${homeControllerDeviceId}'
    //             )`;

    //             queryParams.push(`%${authenticatedUser.user_id}%`);

    query += ` AND ((log_object_id IN (
                    SELECT device_id from spike_panel_devices where panel_id IN (
                        SELECT panel_id from spike_panels where room_id IN (
                            SELECT room_id from spike_rooms where room_users LIKE ?
                        )
                    ) 
                ) AND user_id=? AND log_type='alert_active' )
      
        OR (log_object_id = '${homeControllerDeviceId}' AND user_id=?)
        )`;


    queryParams.push(`%${authenticatedUser.user_id}%`);
    queryParams.push(authenticatedUser.user_id);
    queryParams.push(authenticatedUser.user_id);

    // } else {

    //     query += ` AND (
    //                             (log_object_id IN (
    //                                 SELECT device_id from spike_panel_devices where panel_id IN (
    //                                     SELECT panel_id from spike_panels where room_id IN (
    //                                         SELECT room_id from spike_rooms where room_users LIKE ?
    //                                     )
    //                                 ) 
    //                             ) AND user_id=? AND log_type='alert_active' )

    //                             OR log_object_id = '${homeControllerDeviceId}'

    //                                 )`;

    //     queryParams.push(`%${authenticatedUser.user_id}%`);
    //     queryParams.push(authenticatedUser.user_id);

    // }


    let counter = await dbManager.get(query, queryParams);
    return counter.total_unseen;
}

/**
 * Get Unseen Room Alert Counter for all users
 */
log.prototype.getUnseenRoomAlertCounterForAllUsers = async function () {

    const query = 'select sr.room_id, mu.user_id from spike_rooms sr left outer join mst_user mu';
    const userRoomList = await dbManager.all(query);

    let counterList = [];
    for (let i = 0; i < userRoomList.length; i++) {
        userRoom = userRoomList[i];
        counterList.push({
            room_id: userRoom.room_id,
            user_id: userRoom.user_id,
            unseen_alert_counter: await this.getUnseenRoomAlertsCounter(userRoom.user_id, userRoom.room_id)
        });
    }


    return counterList;

}

/**
 * Get Unseen Room Alert Counter For All Users of specific device
 * @param device_id
 */
log.prototype.getUnseenRoomAlertCounterForAllUsersForSpecificDevice = async function (device_id) {

    const query = `Select * from 
                                (select sr.room_id, mu.user_id from spike_rooms sr left outer join mst_user mu) tbl 
                            where tbl.room_id IN (
                                                    select sp.room_id from spike_panel_devices spd
                                                    inner join spike_panels sp on sp.panel_id=spd.panel_id
                                                    where spd.device_id=?
                                                )`;
    const userRoomList = await dbManager.all(query, [device_id]);

    let counterList = [];
    for (let i = 0; i < userRoomList.length; i++) {
        userRoom = userRoomList[i];
        counterList.push({
            room_id: userRoom.room_id,
            user_id: userRoom.user_id,
            unseen_alert_counter: await this.getUnseenRoomAlertsCounter(userRoom.user_id, userRoom.room_id)
        });
    }


    return counterList;

}

/**
 * Get Unseen Device Alerts Counter
 * @param user_id
 * @param device_id 
 */
log.prototype.getUnseenDeviceAlerts = async function (user_id, device_id) {

    const query = `SELECT   l.user_id,
                            l.log_id as id,
                            l.log_type,
                            l.seen_by,
                            slc.log_action as activity_action,
                            slc.log_parent_type as activity_type,
                            l.description as activity_description,
                            sgm.meta_value as sub_description,
                            l.created_at,
                            mu.user_name
                    from spike_logs l
                    inner join spike_log_categories slc on l.log_type = slc.log_type 
                    left join mst_user mu on mu.user_id=l.user_id
                    left join spike_general_meta sgm on sgm.table_id=l.log_id 
                    where l.log_object_id = ?
                            AND l.log_type IN ('alert_active')
                            AND l.user_id=?
                            AND (l.seen_by NOT LIKE ? OR l.seen_by IS NULL ) and l.description NOT NULL order by CAST(l.created_at as REAL) desc`;

    return await dbManager.all(query, [device_id, user_id, `%${user_id}%`]);

}

/**
 * Get Unseen Camera Alerts (Notifications) Of specific camera
 * @param user_id
 * @param camera_id
 */
log.prototype.getUnseenCameraAlerts = async function (user_id, camera_id) {

    const query = `SELECT   l.log_id as id, l.user_id,
                            l.log_type,
                            l.seen_by,
                            slc.log_action as activity_action,
                            slc.log_parent_type as activity_type,
                            l.description as activity_description,
                            l.created_at,
                            l.image_url
                    from spike_logs l
                    inner join spike_log_categories slc on l.log_type = slc.log_type 
                    where l.log_object_id = ?
                            AND l.log_type IN ('camera_persor_detected')
                            AND (l.seen_by NOT LIKE ? OR l.seen_by IS NULL ) `;

    return await dbManager.all(query, [camera_id, `%${user_id}%`]);

}

/**
 * Get All Unseen Camera Alerts
 * @param user_id
 */
log.prototype.getAllUnseenCameraAlerts = async function (user_id) {

    const query = `SELECT   l.log_id as id, l.user_id,
                            l.log_type,
                            l.seen_by,
                            slc.log_action as activity_action,
                            slc.log_parent_type as activity_type,
                            l.description as activity_description,
                            l.image_url,
                            l.created_at
                    from spike_logs l
                    inner join spike_log_categories slc on l.log_type = slc.log_type 
                    where  l.log_type IN ('camera_persor_detected')
                            AND (l.seen_by NOT LIKE ? OR l.seen_by IS NULL ) `;

    return await dbManager.all(query, [`%${user_id}%`]);

}

/**
 * Get Unseen Device Alerts  Count Of specific device
 * @param user_id
 * @param device_id
 */
log.prototype.getUnseenDeviceAlertsCount = async function (user_id, device_id) {

    const query = `SELECT count(0) as total_unseen_count
                    from spike_logs l
                    where l.log_object_id = ?
                            AND l.user_id = ?
                            AND l.log_type  = 'alert_active'
                            AND (l.seen_by NOT LIKE ? OR l.seen_by IS NULL ) and l.description NOT NULL`;

                            let unseenCounter = await dbManager.get(query, [device_id, user_id, `%${user_id}%`]);
    return unseenCounter.total_unseen_count;
}


/**
 * Get Unseen Device Alert count for all users
 * @param device_id
 */
log.prototype.getUnseenDeviceAlertsCountofAllUsers = async function (device_id) {

    const query = `SELECT (SELECT count(0) 
                    from spike_logs l
                    where l.log_object_id = ?
                            AND l.log_type = 'alert_active'
                            AND l.description IS NOT NULL
                            AND l.user_id = mu.user_id
                            AND (l.seen_by NOT LIKE '%' || mu.user_id || '%' OR l.seen_by IS NULL )) as total_unseen_count, user_id from mst_user mu  where mu.is_active='1'`;

                            let unseenCounter = await dbManager.all(query, [device_id]);
    return unseenCounter;
}


/**
 * Mark Seen for specific user
 */
log.prototype.markAllLogsSeenForUser = async function (user_id) {

    const query = `UPDATE spike_logs SET seen_by=IFNULL(seen_by,'') || '|' || ? WHERE 
             ( 
                    seen_by NOT like ?
                    OR 
                    seen_by IS NULL 
                 )`;

    await dbManager.executeNonQuery(query, [user_id, `%${user_id}%`]);

}

/**
 * Mark Seen for specific user
 */
log.prototype.markSeen = async function (user_id, params) {

    if (params.log_type == 'room' && params.room_id) {

        const query = `UPDATE spike_logs SET seen_by=ifnull(seen_by,'') || '|' || ? WHERE 
            (   
                log_object_id = ? 
                OR 
                log_object_id IN (
                                SELECT panel_id from spike_panels where room_id=?
                            )
                OR log_object_id IN (
                                    SELECT device_id from spike_panel_devices WHERE
                                        panel_id IN (
                                            SELECT panel_id from spike_panels where room_id=?    
                                        )
                                )
            )
            AND (
                    seen_by NOT like ?
                    OR 
                    seen_by IS NULL 
                )`;

        await dbManager.executeNonQuery(query, [user_id, params.room_id, params.room_id, params.room_id, `%${user_id}%`]);

    } else if (params.log_type == 'device' && params.device_id) {
        // Edited by Brijesh Parmar
        const query = `UPDATE spike_logs SET seen_by=IFNULL(seen_by,'') || '|' || ? WHERE 
            log_object_id = ?
             OR log_object_id IN (
                                SELECT alert_id from spike_alerts where device_id=?
                )
             AND ( 
                    seen_by NOT like ?
                    OR 
                    seen_by IS NULL 
                 )`;

        //logger.info('Log Model | Mark Seen Query', query);

        await dbManager.executeNonQuery(query, [user_id, params.device_id, params.device_id, `%${user_id}%`]);

    } else if (params.log_type == 'camera' && params.camera_id) {
        // Edited by Brijesh Parmar
        const query = `UPDATE spike_logs SET seen_by=IFNULL(seen_by,'') || '|' || ? WHERE 
            log_object_id = ?
            `;

        //logger.info('Log Model | Mark Seen Query', query);

        await dbManager.executeNonQuery(query, [user_id, params.camera_id]);
    }

}

/***
 * Check if the device can be turned on / off by beacon
 * @param device_id
 */
log.prototype.canBeaconTurnOnDevice = async function (device_id) {

    const query = 'select * from spike_logs where log_object_id=? order by CAST(created_at as REAL) desc LIMIT 1';
    const lastLogData = await dbManager.get(query, [device_id]);
    if (lastLogData && lastLogData.log_type == 'device_on' && lastLogData.user_id != 'Beacon Activity') {
        return false;
    } else {
        return true;
    }

}

module.exports = new log();