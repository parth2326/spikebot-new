const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const user_device_counter = function() {};


/**
 * Clear by user_id and device_id
 * @param user_id
 * @param device_id
 * @param counter_type
 */
user_device_counter.prototype.clearDeviceCounter = async function(user_id, device_id, counter_type="alert_counter") {
    const query = `UPDATE spike_user_device_counter SET counter = 0, is_sync=0 WHERE user_id = ? AND device_id = ? AND counter_type = ?`;
    return await dbManager.executeNonQuery(query, [user_id, device_id, counter_type]);
}


/**
 * Clear by user_id and device_id
 * @param user_id
 * @param device_id
 */
user_device_counter.prototype.clearAllDeviceCounterForUser = async function(user_id, device_id) {
    const query = `UPDATE spike_user_device_counter SET counter = 0, is_sync=0 WHERE user_id = ? AND device_id = ?`;
    return await dbManager.executeNonQuery(query, [user_id, device_id]);
}

/**
 * Clear all device counter for specific user
 * @param {*} user_id 
 */
user_device_counter.prototype.clearAllDeviceCounter = async function(user_id){
    const query = `UPDATE spike_user_device_counter SET counter = 0, is_sync=0 WHERE user_id = ?`;
    return await dbManager.executeNonQuery(query, [user_id]);
}

/**
 * Increase counter of user 
 * @param {*} user_id 
 * @param {*} device_id 
 */
user_device_counter.prototype.increaseCounter = async function(user_id, device_id, counter_type="alert_counter") {

    let counterData = await dbManager.get('select counter from spike_user_device_counter where user_id=? and device_id=? and counter_type=? limit 1',[user_id,device_id, counter_type]);
   
    if (!counterData) {
        
        await dbManager.executeInsert('spike_user_device_counter', {
            spike_user_device_counter_id: appUtil.generateRandomId('META'),
            user_id: user_id,
            device_id: device_id,
            counter_type: counter_type
        });

        return 1;

    } else {
        let query = "UPDATE spike_user_device_counter SET counter = counter+1, is_sync=0 WHERE user_id = ? AND device_id = ? AND counter_type = ?";
        await dbManager.executeNonQuery(query, [user_id, device_id, counter_type]);
        return counterData.counter+1;
    }

}

/**
 * Increase counter of user 
 * @param {*} user_id 
 * @param {*} device_id 
 */
user_device_counter.prototype.decreaseCounter = async function(user_id, device_id, counter_type="alert_counter",decreaseQty=1) {

    let counterData = await dbManager.get('select counter from spike_user_device_counter where user_id=? and device_id=? and counter_type=? limit 1',[user_id,device_id, counter_type]);
   
    if (!counterData) {
        
        await dbManager.executeInsert('spike_user_device_counter', {
            spike_user_device_counter_id: appUtil.generateRandomId('META'),
            user_id: user_id,
            device_id: device_id,
            counter:0,
            counter_type: counter_type
        });

    } else {

        let query = `UPDATE spike_user_device_counter SET counter = counter-${decreaseQty}, is_sync=0 WHERE user_id = ? AND device_id = ? AND counter_type = ?`;
        await dbManager.executeNonQuery(query, [user_id, device_id, counter_type]);
        
    }


}

/**
 * Get Counter of device of specific user
 * @param {*} user_id 
 * @param {*} device_id 
 */
user_device_counter.prototype.getUserDeviceCounter = async function(user_id, device_id, counter_type='alert_counter') {

    let counterData = await dbManager.get('select counter from spike_user_device_counter where user_id=? and device_id=? and counter_type=? limit 1',[user_id,device_id, counter_type]);
    if (!counterData) {
        return 0;
    } else {
        return counterData.counter;
    }
}

/**
 * Get Counter of device of specific user
 * @param {*} user_id 
 * @param {*} device_id 
 */
user_device_counter.prototype.getTotalUserDeviceCounter = async function(user_id, device_id) {

    let counterData = await dbManager.get('select sum(counter) as counter from spike_user_device_counter where user_id=? and device_id=? ',[user_id,device_id]);
    if (!counterData) {
        return 0;
    } else {
        return counterData.counter ? counterData.counter:0;
    }
}

/**
 * Get Total Counter of all devices of specific user
 * @param {*} user_id 
 */
user_device_counter.prototype.getTotalCounterOfUser = async function(user_id) {

    let counterData = await dbManager.get('select SUM(counter) as total_counter from spike_user_device_counter where user_id=? ',[user_id]);
    if (!counterData) {
        return 0;
    } else {
        return counterData.total_counter;
    }
}

module.exports = new user_device_counter();