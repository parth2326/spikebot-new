const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');
const home_controller_device_id = require('./../util/general-util').appContext.viewerDetails.deviceID;
const crypto = require('crypto');
const user = function () {

};

/**
 * Add Child User
 * @param user_id
 * @param user_name
 * @param password (in encrypted form only)
 * @param first_name
 * @param topic_arn
 */
user.prototype.addChildUser = async function (params) {

    let adminDetails = await this.getAdminUser();
    params.created_by = adminDetails.user_email;
    params.user_email = adminDetails.user_email;
    params.last_name = adminDetails.last_name;
    params.admin = 0;
    params.admin_user_id = adminDetails.user_id;
    params.home_controller_device_id = adminDetails.home_controller_device_id;
    params.home_controller_name = adminDetails.home_controller_name;
    params.vpn_port = adminDetails.vpn_port;
    params.user_phone = adminDetails.user_phone;
    params.created_date = appUtil.currentDateTime();
    params.modified_date = appUtil.currentDateTime();
    params.modified_by = null;

    // Formation Of Query
    let columns = Object.keys(params);
    let questions = [];
    let questionSeperated = "";
    for (let i = 0; i < columns.length; i++) {
        questions.push("?");
    }
    questionSeperated = questions.join(",")
    let query = 'INSERT into mst_user (' + columns.join(",") + ') VALUES (' + questionSeperated + ')';

    // Execute Query
    await dbManager.executeNonQuery(query, Object.values(params));

    return params;

}



/**
 * Check If User Phone is available
 * @param  user_phone 
 */
user.prototype.checkIfUserPhoneIsAvailable =  async function(user_phone) {

    let Client = require('node-rest-client').Client;
    let restClient = new Client();

    let args = {
        data: {
            user_phone
        },
        headers: {
            "Content-Type": "application/json"
        }
    };
    
   return new Promise((resolve, reject) => {
        
        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/auth/user-phone/validate" , args, async function(data, response) {

            console.log('USERNAME CHECK IN ONLINE DB',data);
    
            if (data.code == 200) {
               return resolve(true);
            }
            
            return resolve(false);
        });

    }) 

}


/**
 * Check If User Name is available
 * @param  user_name 
 */
user.prototype.checkIfUserNameIsAvailable =  async function(user_name) {

    let Client = require('node-rest-client').Client;
    let restClient = new Client();

    let args = {
        data: {
            user_name
        },
        headers: {
            "Content-Type": "application/json"
        }
    };
    
   return new Promise((resolve, reject) => {
        
        restClient.post(process.env.CLOUD_URL + ":" + process.env.CLOUD_PORT + "/auth/user-name/validate" , args, async function(data, response) {

            console.log('USERNAME CHECK IN ONLINE DB',data);
    
            if (data.code == 200) {
               return resolve(true);
            }
            
            return resolve(false);
        });

      }) 

}

/**
 * Login User
 * @param user_name
 * @param user_password (unencrypted)
 * @param fcm_token
 * @param phone_id
 * @param phone_type
 */
user.prototype.login = async function (params) {

    params.user_password = appUtil.encryptPassword(params.user_password);

    const loginQuery = `SELECT * from mst_user WHERE user_name=? and user_password=? and is_active=1 `;
    const userData = await dbManager.get(loginQuery, [params.user_name, params.user_password]);
    if (!userData) {
        return false;
    }

    logger.info('User Data ', userData);
    const loginModel = require('./login');
    const hash = crypto.createHmac('sha256', 'spikebot#123456')
                   .update(new Date().valueOf().toString()+"-"+userData.user_id)
                   .digest('hex');

    const loginData = await loginModel.add({
        user_id: userData.user_id,
        auth_key:hash,
        // auth_key: Buffer.from(JSON.stringify({
        //                 timestamp: new Date().valueOf().toString(),
        //                 "home_controller_device_id": home_controller_device_id,
        //                 payload: appUtil.encryptPassword(userData.user_id)
        //             })).toString('base64'),
        access_identifier: params.phone_id,
        access_type: params.phone_type,
        fcm_token: params.fcm_token
    });

    loginData.user = userData;
    return loginData;
}

/**
 * List of all active users
 */
user.prototype.list = async function () {
    const query = `SELECT * from mst_user WHERE is_active=1`;
    return await dbManager.all(query, []);
}

/** 
 * Get User By User Id
 * @param user_id
 */
user.prototype.get = async function (user_id) {
    const query = `SELECT * from mst_user WHERE user_id = ? LIMIT 1`;
    const params = [user_id];
    return await dbManager.get(query, params);
}

/**
 * Check if the username exists
 * @param user_name
 */
user.prototype.checkIfUserNameExists = async function (user_name) {
    const query = 'SELECT * from mst_user where user_name=?';
    let data = await dbManager.all(query, [user_name]);
    if (data.length > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Get All Child Users
 */
user.prototype.getChildUsers = async function () {
    const query = `SELECT * from mst_user WHERE (admin=0 or admin=-1) and is_active=1`;
    return await dbManager.all(query, []);
}

/**
 * Get Admin User
 */
user.prototype.getAdminUser = async function () {
    const query = 'SELECT * from mst_user where admin=1';
    return await dbManager.get(query, []);
}

/**
 * Get Admin User using Phone Number
 */
user.prototype.getAdminUserUsingPhoneNumber = async function(user_phone) {
    const query = 'SELECT * from mst_user where admin=1 and user_phone = ?';
    return await dbManager.get(query, [user_phone]);
}

/**
 * Update User Details
 * @param user_id
 * @param updatableData
 */
user.prototype.update = async function (user_id, updatableData) {

    updatableData.is_sync = 0;

    return await dbManager.executeUpdate('mst_user',updatableData, {'user_id' : user_id} );
    
    // let query = `UPDATE mst_user SET `;

    // query += dbManager.generateUpdateQuery(updatableData) + ' ';
    // query += "WHERE user_id = ?";

    // return await dbManager.executeNonQuery(query, [user_id]);
}

/**
 * Increase Badge count of user
 * @param user_id
 */
user.prototype.increaseBadgeCountByUserId = async function (user_id) {
    const query = 'UPDATE mst_user set badge_count=badge_count+1,is_sync=0 where user_id=?';
    return await dbManager.executeNonQuery(query, [user_id]);
}

/**
 * Clear badge count of user
 * @param user_id
 */
user.prototype.clearBadgeCount = async function (user_id) {
    const query = 'UPDATE mst_user set badge_count=0,is_sync=0 where user_id=?';
    return await dbManager.executeNonQuery(query, [user_id]);
}

module.exports = new user();