const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const panel = function() {
    //tableName : spike_panels
    /**
     * // Columns : 
     * panel_id
     * room_id
     * panel_name
     * panel_type -> general | sensor | curtain
     * panel_status
     * 
     * module_id
     */
};

/**
 * List All the panels of all the rooms
 */
panel.prototype.list = async function() {
    const query = "SELECT * FROM spike_panels";
    return await dbManager.all(query, []);
}

/**
 * List Panel By Room Id
 * @param room_id
 */
panel.prototype.lisByRoomId = async function(room_id) {
    const query = "SELECT * FROM spike_panels WHERE room_id=?";
    return await dbManager.all(query, [room_id]);
}

/**
 * Get panel for list of panel ids
 * @param panel_device_ids
 * NOTE:SQLINJECTIONCHECK
 */
// panel.prototype.listByPanelDeviceIds = async function(panel_device_ids) {
//     const query = `SELECT * FROM spike_panels WHERE panel_id IN (SELECT panel_id from spike_panel_devices WHERE panel_device_id IN ('${panel_device_ids.join("','")}'))`;
//     return await dbManager.all(query, []);
// }

/**
 * Get Mood Panels
 * @param mood_id
 */
panel.prototype.getMoodPanels = async(mood_id) => {

    
    const query = `SELECT p.*,r.room_name FROM spike_panels p INNER JOIN spike_rooms r ON r.room_id=p.room_id WHERE p.panel_id IN 
    (SELECT panel_id from spike_panel_devices WHERE panel_device_id IN (
        SELECT panel_device_id from spike_mood_devices where mood_id=?
    ))`;
    return await dbManager.all(query, [mood_id]);
}

/** List panel by room Id and panel Type
 * @param room_id
 * @param panel_type
 */
panel.prototype.lisByRoomIdAndType = async function(room_id, panel_type) {
    const query = "SELECT * FROM spike_panels WHERE room_id = ? AND panel_type = ?";
    return await dbManager.all(query, [room_id, panel_type]);
}

/**
 * Get Panel by Panel Id
 * @param panel_id
 */
panel.prototype.get = async function(panel_id) {
    const query = "SELECT * FROM spike_panels WHERE panel_id=?";
    return await dbManager.get(query, [panel_id]);
}

/**
 * Get Panel with Room Details
 * @param panel_id
 */
panel.prototype.getPanelWithRoomDetails = async function(panel_id) {
    const query = "SELECT spike_panels.*,spike_rooms.room_name FROM spike_panels inner join spike_rooms on spike_rooms.room_id=spike_panels.room_id WHERE spike_panels.panel_id=? ";
    return await dbManager.get(query, [panel_id]);
}

/** Get One Panel By Room Id And Panel Type
 * @param room_id
 * @param panel_type
 */
panel.prototype.getByRoomIdAndType = async function(room_id, panel_type) {
    const query = `SELECT * FROM spike_panels WHERE room_id = ? AND panel_type = ?`;
    return await dbManager.get(query, [room_id, panel_type]);
}

/**
 * Get Sensor Panel of Room Id
 * @param room_id
 */
panel.prototype.getSensorPanelByRoomId = async function(room_id) {

    // Check if Sensor Panel Exists In That Room
    const existingPanel = await this.getByRoomIdAndType(room_id, 'sensor');
    if (existingPanel) return existingPanel;

    // If Sensor Panel Doesnt Exist Create Sensor Panel And Return
    const newPanel = {
        panel_id: appUtil.generateRandomId(),
        room_id: room_id,
        panel_name: 'Sensor Panel',
        panel_type: 'sensor',
        module_id: null
    }

    await this.add(newPanel);
    return newPanel;

}

/**
 * Get PIR Panel of Room Id
 * @param room_id
 */
panel.prototype.getPirPanelByRoomId = async function(room_id) {

    // logger.info('Get PIR Panel By Room Id');
    // Check if PIR Panel Exists In That Room
    const existingPanel = await this.getByRoomIdAndType(room_id,'pir');
    if (existingPanel) return existingPanel;

    // If PIR Panel Doesnt Exist Create PIR Panel And Return
    const newPanel = {
        panel_id: appUtil.generateRandomId(),
        room_id: room_id,
        panel_name: 'Motion Panel',
        panel_type: 'pir',
        module_id: null
    }

    await this.add(newPanel);
    return newPanel;
}

/**
 * Get Smart Device Panel of Room Id
 * @param room_id
 */
panel.prototype.getSmartDevicesPanelByRoomId = async function(room_id) {

    // logger.info('Get Smart Device Panel By Room Id');
    // Check if PIR Panel Exists In That Room
    const existingPanel = await this.getByRoomIdAndType(room_id,'smart_device');
    if (existingPanel) return existingPanel;

    // If PIR Panel Doesnt Exist Create PIR Panel And Return
    const newPanel = {
        panel_id: appUtil.generateRandomId(),
        room_id: room_id,
        panel_name: 'Smart Device Panel',
        panel_type: 'smart_device',
        module_id: null
    }

    await this.add(newPanel);
    return newPanel;
}

panel.prototype.getPirDetectorByMoodId = async function(mood_id) {

    // logger.info('Get PIR Detector By Mood Id');
    // Check if PIR Panel Exists In That Room
    const existingPanel = await this.getMoodPanels(mood_id);
    if (existingPanel) return existingPanel;

    // If PIR Panel Doesnt Exist Create PIR Detector And Return
    const newPanel = {
        panel_id: appUtil.generateRandomId(),
        room_id: room_id,
        panel_name: 'PIR Detector',
        panel_type: 'pir_detector',
        module_id: null
    }

    await this.add(newPanel);
    return newPanel;
}

/** Add Panel
 * @param panel_id
 * @param room_id
 * @param panel_name
 * @param panel_type (general / sensor / curtain)
 */
panel.prototype.add = async function(panelData) {

    /**
     * panel_id
     * room_id
     * panel_name
     * panel_type
     */
    const query = "INSERT INTO spike_panels (panel_id,room_id,panel_name,panel_type,module_id) VALUES (?,?,?,?,?)";
    const params = [panelData.panel_id, panelData.room_id, panelData.panel_name, panelData.panel_type, panelData.module_id ? panelData.module_id : null];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Update Panel Details
 * @param panel_id
 * @param panelData 
 */
panel.prototype.update = async function(panel_id, panelData) {

    panelData.is_sync = 0;

    // Edited By Brijesh Parmar
    return await dbManager.executeUpdate('spike_panels', panelData, {'panel_id' : panel_id});
    // let query = "UPDATE spike_panels SET ";
    // query += dbManager.generateUpdateQuery(panelData) + ' ';
    // query += "WHERE panel_id = ?";
    // return await dbManager.executeNonQuery(query, [panel_id]);
}

/**
 * Delete Panel 
 * @param panel_id
 */
panel.prototype.delete = async function(panel_id) {
    const query = "DELETE FROM spike_panels WHERE panel_id = ?";
    await dbManager.executeNonQuery(query, [panel_id]);
}

/**
 * Delete Panel By Room Id
 * @param room_id
 */
panel.prototype.deleteByRoomId = async function(room_id) {
    const query = "DELETE FROM spike_panels WHERE room_id = ?";
    return await dbManager.executeNonQuery(query, [room_id]);
}

/** 
 * Get Panel Status
 * @param panel_id
 */
panel.prototype.getPanelStatus = async function(panel_id) {

    const query = `select pd.device_id from spike_panel_devices pd 
                    inner join spike_device_status sds on sds.device_id=pd.device_id
                    inner join spike_devices d on d.device_id=pd.device_id
                    inner join spike_panels p on p.panel_id=pd.panel_id
                    where d.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') 
                    and  sds.status_type='device_status' and sds.status_value='1'
                    and p.panel_id=? limit 1
                    `;

    // const query = "select count(0) as total_on_devices from spike_panel_devices AS pd inner join spike_devices AS d ON d.device_id=pd.device_id where pd.panel_id=? and (select status_value from spike_device_status where device_id=d.device_id and status_type='device_status')='1'";
    let status = await dbManager.get(query, [panel_id]);
    if(status){
        return 1;
    }else{
        return 0;
    }
    // return status.total_on_devices == 0 ? 0 : 1
}

module.exports = new panel();