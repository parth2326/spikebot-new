const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const general_meta = function() {};

/**
 * Get Meta By Table Id
 * @param table_id 
 * @param meta_name
 */
general_meta.prototype.getByTableId = async function(table_id, meta_name) {
    const query = `SELECT * FROM spike_general_meta WHERE table_id = ? AND meta_name=?`;
    return await dbManager.get(query, [table_id, meta_name]);
}

/**
 * Get Meta By Table Id Array and Table Name
 * @param table_id array
 * @param table_name
 */
general_meta.prototype.getByTableIdArrayAndTablename = async function(table_id_array, table_name) {
    const query = `SELECT * FROM spike_general_meta WHERE table_id IN ('`+table_id_array.join("','")+`') AND table_name=?`;
    return await dbManager.all(query, [table_name]);
}

/**
 * Get Meta By Table Id and Table Name
 * @param table_id
 * @param table_name
 */
general_meta.prototype.getByTableIdAndTablename = async function(table_id, table_name) {
    const query = `SELECT * FROM spike_general_meta WHERE table_id = ? AND table_name = ?`;
    return await dbManager.all(query, [table_id, table_name]);
}

/**
 * Get All With  Meta Name, Meta Value, Table name
 * @param meta_value
 * @param meta_name
 * @param table_name
 */
general_meta.prototype.getByTableAndMeta = async function(meta_value, meta_name, table_name) {
    const query = `SELECT * FROM spike_general_meta WHERE meta_value = ? AND meta_name = ? and table_name = ?`;
    return await dbManager.all(query, [meta_value, meta_name, table_name]);
}


/**
 * Get Meta Value
 * @param table_id
 * @param meta_value
 * @param default_value (optional) this will replace value if null
 */
general_meta.prototype.getMetaValue = async function(table_id, meta_name, default_value = null) {
    const query = `SELECT meta_value FROM spike_general_meta WHERE table_id = ? AND meta_name = ?`;
    let tempMeta = await dbManager.get(query, [table_id, meta_name]);
    if (tempMeta) {
        return tempMeta.meta_value;
    } else {
        return default_value;
    }
}



/**
 * Get Meta By Value
 * @param table_name
 * @param meta_type
 * @param meta_value
 */
general_meta.prototype.getMetaByValue = async function(table_name, meta_name, meta_value) {
    const query = `SELECT * FROM spike_general_meta WHERE table_name = ? AND meta_name = ? AND meta_value = ?`;
    return await dbManager.get(query, [table_name, meta_name, meta_value]);
}

/**
 * Get Meta By Value
 * @param table_name
 * @param meta_type
 * @param meta_value
 */
general_meta.prototype.listMetaByValue = async function(table_name, meta_name, meta_value) {
    const query = `SELECT * FROM spike_general_meta WHERE table_name = ? AND meta_name = ? AND meta_value = ?`;
    return await dbManager.all(query, [table_name, meta_name, meta_value]);
}

/**
 * Add Meta
 * @param table_id
 * @param table_name
 * @param meta_name
 * @param meta_value
 */
general_meta.prototype.add = async function(params) {
    return await dbManager.executeInsert('spike_general_meta', params);
}

/**
 * Delete By Table Id and Table Name
 * @param table_id
 * @param table_name
 */
general_meta.prototype.deleteByTableIdAndTableName = async function(table_id, table_name) {
    return await dbManager.executeNonQuery('delete from spike_general_meta where table_id=? and table_name=?', [table_id, table_name]);
}

/**
 * Delete By Table Id, Table Name and Meta Name
 * @param table_id
 * @param table_name
 * @param meta_name
 */
general_meta.prototype.deleteByTableIdAndTableNameAndMetaName = async function(table_id, table_name, meta_name) {
    return await dbManager.executeNonQuery('delete from spike_general_meta where table_id=? and table_name=? and meta_name=?', [table_id, table_name, meta_name]);
}

/**
 * Update Meta / Insert Meta
 * @param table_id
 * @param meta_name
 * @param meta_value
 */
general_meta.prototype.update = async function(table_id, table_name, meta_name, meta_value) {

    let meta = await this.getByTableId(table_id, meta_name);
   
    if (!meta) {
        
        return await dbManager.executeInsert('spike_general_meta', {
            meta_id: appUtil.generateRandomId('META'),
            table_id: table_id,
            table_name: table_name,
            meta_name: meta_name,
            meta_value: meta_value
        });

    } else {
        let query = "UPDATE spike_general_meta SET meta_value = ?, is_sync=0 WHERE table_id = ? AND meta_name = ?";
        return await dbManager.executeNonQuery(query, [meta_value, table_id, meta_name]);
    }

}

/**
 * Delete By Meta Name
 * @param table_id
 * @param table_name
 */
general_meta.prototype.deleteByMetaName = async function(meta_name) {
    return await dbManager.executeNonQuery('delete from spike_general_meta where meta_name=?', [meta_name]);
}



module.exports = new general_meta();