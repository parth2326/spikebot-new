const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const dbManager = require('../util/db-manager');
// const deviceStatusModel = require('./../models/device_status');
const generalMetaModel = require('./../models/general_meta');
// const deviceMetaList = ['unit', 'device_default_status', 'device_brand', 'device_model', 'device_codeset_id', 'ir_blaster_id', 'battery_level']
// const deviceStatusList = ['device_status', 'device_sub_status'];

const device = function () {

    //tableName : spike_devices
    /**
     * device_id
     * module_id
     * device_identifier
     * device_status => 0 | 1
     * device_sub_status
     * device_type
     * device_sub_type
     * device_name
     * device_icon
     * meta
     */

};

/**
 * Other Meta To be added
 * @param {*} otherMetas 
 */
// device.prototype.getMetaQueries = function (otherMetas = []) {
//     metaQueries = [];
//     deviceMetaList.push(...otherMetas);
//     for (var i = 0; i < deviceMetaList.length; i++) {
//         metaQueries.push(`(SELECT meta_value from spike_general_meta where table_name="device" and table_id=d.device_id and meta_name="${deviceMetaList[i]}") as meta_${deviceMetaList[i]}`)
//     }

//     return metaQueries;
// }

// /**
//  * Get status queriess
//  * @param {*} otherStatusList
//  */
// device.prototype.getStatusQueries = function (otherStatusList = []) {

//     statusQueries = [];
//     if (deviceStatusList.length > 0) {
//         for (var i = 0; i < deviceStatusList.concat(otherStatusList).length; i++) {
//             statusQueries.push(`(SELECT status_value from spike_device_status where status_type="${deviceStatusList[i]}" and device_id=d.device_id) as ${deviceStatusList[i]}`)
//         }
//     }

//     return statusQueries;
// }

device.prototype.listActiveDevices = async function () {
    // const query = `SELECT sd.device_id, sd.device_name, sp.panel_id, sp.panel_name, sr.room_id, sr.room_name, sd.device_type, sd. device_sub_type, sm.is_configured, sm.is_active, 
    const query = `SELECT sd.device_id, sd.device_name, 
    sr.room_name || '-' || sp.panel_name || '-' || sd.device_name AS room_panel_name
    FROM spike_devices sd
    INNER JOIN spike_panel_devices spd ON spd.device_id = sd.device_id
    INNER JOIN spike_panels sp ON spd.panel_id = sp.panel_id
    INNER JOIN spike_rooms sr ON sp.room_id = sr.room_id
    INNER JOIN spike_modules sm ON sd.module_id = sm.module_id
        and sm.is_configured = "y" and sm.is_active = "y" 
        and device_type IN ("switch", "fan", "heavyload"); `;
    return await dbManager.all(query, []);
}

/**
 * Get List of devices of specific module
 * @param {*} module_id 
 */
device.prototype.listByModuleId = async function (module_id) {
    const query = `SELECT * FROM spike_devices WHERE module_id = ?`;
    return await dbManager.all(query, [module_id]);
}

/**
 * Get Device By Id
 * @param {*} device_id 
 */
device.prototype.get = async function (device_id) {
    const query = `SELECT * FROM spike_devices WHERE device_id = ?`;
    return await dbManager.get(query, [device_id]);
}

/**
 * Get Only One / First Devices of module
 * @param module_id 
 */
device.prototype.getByModuleId = async function (module_id) {
    const query = `SELECT * FROM spike_devices WHERE module_id = ?`;
    return await dbManager.get(query, [module_id]);
}

/**
 * Get Details of specific Device
 * @param {*} device_id 
 */
device.prototype.getDetails = async function (device_id) {

    const query = `
        SELECT 
            d.device_id,
            d.module_id, 
            d.device_name,
            d.device_icon,
            ${generalUtil.getStatusQueries().join()},
            ${generalUtil.getMetaQueries().join()},
            d.device_type,
            d.device_sub_type,
            d.device_identifier,
            m.is_active,
            m.module_type, 
            m.module_identifier
        FROM 
            spike_devices AS d
            LEFT JOIN spike_modules AS m ON m.module_id = d.module_id
        WHERE 
            d.device_id = ?
    `;

    return await dbManager.get(query, [device_id]);
}

/**
 * Get Device by device identifier and module identifier
 * @param {*} device_identifier 
 * @param {*} module_identifier 
 * @param {*} device_status 
 */
device.prototype.getByDeviceIdentifierAndModuleIdentifier = async function (device_identifier, module_identifier, device_status = ['device_status', 'device_sub_status'], includeMeta = true) {

    let statusQueries = [];
    if (device_status.length > 0) {
        for (let i = 0; i < device_status.length; i++) {
            statusQueries.push(`(SELECT status_value from spike_device_status where status_type="${device_status[i]}") as ${device_status[i]}`)
        }
    }

    const query = `
        SELECT 
            d.module_id, 
            d.device_id, 
            d.device_name,
            d.device_icon,
            ${generalUtil.getStatusQueries().join()},
            ${includeMeta == true ? generalUtil.getMetaQueries().join() : 'count(0) as total'},
            d.device_type,
            d.device_sub_type,
            d.device_identifier,
            m.is_active,
            m.module_type, 
            m.module_identifier,
            spd.panel_device_id
        FROM 
            spike_devices AS d
            LEFT JOIN spike_modules AS m ON m.module_id = d.module_id
            LEFT JOIN spike_panel_devices AS spd ON spd.device_id=d.device_id
        WHERE 
            d.device_identifier = ? and m.module_identifier = ?
    `;

    return await dbManager.get(query, [device_identifier, module_identifier]);

}

/**
 * Add Device
 * @param device_id
 * @param module_id
 * @param device_identifier
 * @param device_type
 * @param device_icon 
 */
device.prototype.add = async function (deviceData) {
    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_icon) VALUES (?,?,?,?,?,?,?)";
    const params = [deviceData.device_id, deviceData.module_id, deviceData.device_identifier, deviceData.device_type, deviceData.device_icon];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Add 5 Panel
 * @param deviceData.module_id 
 */
device.prototype.add5Device = async function (deviceData) {

    // const devices = ()
    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";
    for (let i = 1; i <= 5; i += 1) {
        //NOTE : ORDER matters;
        const newDevice = {
            device_id: appUtil.generateRandomId(generalUtil.deviceTypes.switch.device_type.toUpperCase()),
            module_id: deviceData.module_id,
            device_identifier: i,
            device_type: "switch",
            device_name: "bulb" + i,
            device_icon: "bulb"
        }
        await dbManager.executeNonQuery(query, Object.values(newDevice));

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_status',
            status_value: 0
        });

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_sub_status',
            status_value: 0
        });

    }
}

/**
 * Add 5f Device
 * @param deviceData.module_id
 */
device.prototype.add5fDevice = async function (deviceData) {

    // const devices = ()
    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon,device_sub_type) VALUES (?,?,?,?,?,?,?)";

    for (let i = 1; i <= 5; i += 1) {
        //NOTE : ORDER matters;

        // First device is always fan in 5f Module Type
        const deviceType = i === 1 ? generalUtil.deviceTypes.fan.device_type : generalUtil.deviceTypes.switch.device_type;

        const newDevice = {
            device_id: appUtil.generateRandomId(generalUtil.deviceTypes[deviceType].device_type.toUpperCase()),
            module_id: deviceData.module_id,
            device_identifier: i,
            device_type: generalUtil.deviceTypes[deviceType].device_type,
            device_name: generalUtil.deviceTypes[deviceType].device_name + " " + i,
            device_icon: generalUtil.deviceTypes[deviceType].device_icon,
            device_sub_type: generalUtil.deviceTypes[deviceType].device_default_sub_type ? generalUtil.deviceTypes[deviceType].device_default_sub_type : null
        }

        await dbManager.executeNonQuery(query, Object.values(newDevice));

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_status',
            status_value: 0
        });

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_sub_status',
            status_value: 0
        });
    }
}


/**
 * Add Heavy Load
 * @param deviceData.module_id (required)
 * @param qnt (quantity of heavy load devices to add) (1 - 1HL, 2-2HL)
 */
device.prototype.addHeavyLoad = async function (deviceData, qnt) {

    // const devices = ()
    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";
    for (let i = 1; i <= qnt; i += 1) {
        const newDevice = {
            device_id: appUtil.generateRandomId(generalUtil.deviceTypes.heavyload.device_type.toUpperCase()),
            module_id: deviceData.module_id,
            device_identifier: i,
            device_type: generalUtil.deviceTypes.heavyload.device_type,
            device_name: generalUtil.deviceTypes.heavyload.device_name + " " + i,
            device_icon: generalUtil.deviceTypes.heavyload.device_icon
        }
        await dbManager.executeNonQuery(query, Object.values(newDevice));

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_status',
            status_value: 0
        });

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_sub_status',
            status_value: 0
        });
    }
}

/**
 * Add IR Blaster
 * @param module_id
 * @param module_identifier
 * @param meta (object: key value pair) 
 */
device.prototype.addIRBlaster = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.ir_blaster.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.ir_blaster.device_type,
        device_name: generalUtil.deviceTypes.ir_blaster.device_name,
        device_icon: generalUtil.deviceTypes.ir_blaster.device_icon
    }

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: 0
    });

    return newDevice;

}

/**
 * Add IR Blaster
 * @param module_id
 * @param module_identifier
 * @param meta (object: key value pair) 
 */
device.prototype.addEnergyMeter = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.energy_meter.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.energy_meter.device_type,
        device_name: generalUtil.deviceTypes.energy_meter.device_name,
        device_icon: generalUtil.deviceTypes.energy_meter.device_icon
    }

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: 0
    });

    return newDevice;

}

/**
 * Add Smart Bulb
 * @param module_id
 * @param module_identifier (unique device identifier received from iftt)
 * @param device_sub_type
 * @param meta (object: key value pair) 
 */
device.prototype.addSmartBulb = async function (moduleData) {

    logger.info('Module Data Smart_bulb', moduleData);
    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_sub_type,device_name,device_icon) VALUES (?,?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.smart_bulb.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.smart_bulb.device_type,
        device_sub_type: moduleData.device_sub_type,
        device_name: generalUtil.deviceTypes.smart_bulb.device_name,
        device_icon: generalUtil.deviceTypes.smart_bulb.device_icon
    }

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: '#ff0000'
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_brightness',
        status_value: 50
    });

    return newDevice;

}


/**
 * Add Smart Plug
 * @param module_id
 * @param module_identifier (unique device identifier received from iftt)
 * @param device_sub_type
 * @param meta (object: key value pair) 
 */
device.prototype.addSmartPlug = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_sub_type,device_name,device_icon) VALUES (?,?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.smart_plug.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.smart_plug.device_type,
        device_sub_type: moduleData.device_sub_type,
        device_name: generalUtil.deviceTypes.smart_plug.device_name,
        device_icon: generalUtil.deviceTypes.smart_plug.device_icon
    }

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: '#ff0000'
    });

    return newDevice;

}


/**
 * Add Beacon Scanner Device
 * @param module_id
 * @param module_identifier 
 * @param meta (object: key value pair)
 */
device.prototype.addBeaconScanner = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.beacon_scanner.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.beacon_scanner.device_type,
        device_name: generalUtil.deviceTypes.beacon_scanner.device_name,
        device_icon: generalUtil.deviceTypes.beacon_scanner.device_icon
    }

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    return await dbManager.executeNonQuery(query, Object.values(newDevice));

}

/**
 * Adding Remote of AC / TV etc that are controlled by IR Blaster
 * @param module_id
 * @param module_identifier
 * @param meta (object:key value pair)
 */
device.prototype.addRemote = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_sub_type,device_name,device_icon) VALUES (?,?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.remote.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.remote.device_type,
        device_sub_type: moduleData.device_sub_type,
        device_name: generalUtil.deviceTypes.remote.device_name,
        device_icon: generalUtil.deviceTypes.remote.device_icon
    }


    await dbManager.executeNonQuery(query, Object.values(newDevice));

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: moduleData.meta.device_default_status
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_swing_status',
        status_value: 0
    });

    return newDevice;

}


/**
 * Adding TT Lock Bridge
 * @param module_id
 * @param module_identifier
 */
device.prototype.addTTLockBridge = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.tt_lock_bridge.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.tt_lock_bridge.device_type,
        device_name: generalUtil.deviceTypes.tt_lock_bridge.device_name,
        device_icon: generalUtil.deviceTypes.tt_lock_bridge.device_icon
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: moduleData.meta.device_default_status
    });

    return newDevice;

}


/**
 * Adding TT Lock
 * @param module_id
 * @param module_identifier
 * @param meta (object: key value pair)
 */
device.prototype.addTTLock = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.tt_lock.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.tt_lock.device_type,
        device_sub_type: generalUtil.deviceTypes.tt_lock.device_sub_types[0],
        device_name: generalUtil.deviceTypes.tt_lock.device_name,
        device_icon: generalUtil.deviceTypes.tt_lock.device_icon
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    // Add Meta To The List
    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: moduleData.meta.device_default_status
    });

    return newDevice;

}

/**
 * Adding Beacon
 * @param module_id
 * @param module_identifier
 * @param meta : (key value pair)
 */
device.prototype.addBeacon = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.beacon.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.beacon.device_type,
        device_name: generalUtil.deviceTypes.beacon.device_name,
        device_icon: generalUtil.deviceTypes.beacon.device_icon
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    if (moduleData.meta != null) {
        for (let metaKey of Object.keys(moduleData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, moduleData.meta[metaKey]);
        }
    }

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: 0
    });

    return newDevice;

}

/**
 * Adding Jetson For Camera Controlling (not used at the moment)
 * @param module_id
 * @param module_identifier
 */
device.prototype.addJetson = async function (moduleData) {

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";

    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes.jetson.device_type.toUpperCase()),
        module_id: moduleData.module_id,
        device_identifier: moduleData.module_identifier,
        device_type: generalUtil.deviceTypes.jetson.device_type,
        device_name: moduleData.device_name ? moduleData.device_name : generalUtil.deviceTypes.jetson.device_name,
        device_icon: generalUtil.deviceTypes.jetson.device_icon,
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    return newDevice;
}


/**
 * Only for sensors do not use it for other devices
 * @param module_id
 * @param module_identifier
 * @param module_type
 * @param meta (key value pair)
 */
device.prototype.addDevice = async function (deviceData, qnt) {

    let meta = generalUtil.deviceTypes[deviceData.module_type].meta ? generalUtil.deviceTypes[deviceData.module_type].meta : {};

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";
    for (let i = 1; i <= qnt; i += 1) {
        const newDevice = {
            device_id: appUtil.generateRandomId(generalUtil.deviceTypes[deviceData.module_type].device_type.toUpperCase()),
            module_id: deviceData.module_id,
            device_identifier: deviceData.module_identifier,
            device_type: generalUtil.deviceTypes[deviceData.module_type].device_type,
            device_name: generalUtil.deviceTypes[deviceData.module_type].device_name + " " + i,
            device_icon: generalUtil.deviceTypes[deviceData.module_type].device_icon
        }

        // Insert Default Meta To The System
        if (generalUtil.deviceTypes[deviceData.module_type].meta != null) {
            for (let metaKey of Object.keys(generalUtil.deviceTypes[deviceData.module_type].meta)) {

                await generalMetaModel.add({
                    meta_id: appUtil.generateRandomId('META'),
                    table_name: 'device',
                    table_id: newDevice.device_id,
                    meta_name: metaKey,
                    meta_value: generalUtil.deviceTypes[deviceData.module_type].meta[metaKey]
                });
            }
        }

        if (deviceData.meta != null) {
            for (let metaKey of Object.keys(deviceData.meta)) {
                await generalMetaModel.update(newDevice.device_id, 'device', metaKey, deviceData.meta[metaKey]);
            }
        }

        await dbManager.executeNonQuery(query, Object.values(newDevice));

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_status',
            status_value: 0
        });

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_sub_status',
            status_value: 0
        });

    }
}


// Only for sensors do not use it for other devices
device.prototype.addYaleLock = async function (deviceData, qnt = 1) {
    /*
        module_id
        module_identifier
        module_type
        last_response_time
        device_identifier
    */
    // const devices = ()

    let meta = generalUtil.deviceTypes[deviceData.module_type].meta ? generalUtil.deviceTypes[deviceData.module_type].meta : {};

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_sub_type,device_name,device_icon) VALUES (?,?,?,?,?,?,?)";
    for (let i = 1; i <= qnt; i += 1) {

        const newDevice = {
            device_id: appUtil.generateRandomId(generalUtil.deviceTypes[deviceData.module_type].device_type.toUpperCase()),
            module_id: deviceData.module_id,
            device_identifier: i,
            device_type: generalUtil.deviceTypes.yale_lock.device_type,
            device_sub_type: deviceData.module_type,
            device_name: generalUtil.deviceTypes.yale_lock.device_name + " " + i,
            device_icon: generalUtil.deviceTypes.yale_lock.device_icon
        }

        // Insert Default Meta To The System
        if (generalUtil.deviceTypes[deviceData.module_type].meta != null) {
            for (let metaKey of Object.keys(generalUtil.deviceTypes[deviceData.module_type].meta)) {
                await generalMetaModel.add({
                    meta_id: appUtil.generateRandomId('META'),
                    table_name: 'device',
                    table_id: newDevice.device_id,
                    meta_name: metaKey,
                    meta_value: generalUtil.deviceTypes[deviceData.module_type].meta[metaKey]
                });
            }
        }

        if (deviceData.meta != null) {
            for (let metaKey of Object.keys(deviceData.meta)) {
                await generalMetaModel.update(newDevice.device_id, 'device', metaKey, deviceData.meta[metaKey]);
            }
        }

        await dbManager.executeNonQuery(query, Object.values(newDevice));

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_status',
            status_value: 0
        });

        await dbManager.executeInsert('spike_device_status', {
            device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
            device_id: newDevice.device_id,
            status_type: 'device_sub_status',
            status_value: 0
        });


    }
}

/**
 * Add PIR Device
 * @param module_id
 * @param module_identifier
 * @param module_type
 * @param meta (key value pair)
 */
device.prototype.addPirDevice = async function (deviceData) {

    logger.info('Add PIR Device');
    let meta = generalUtil.deviceTypes[deviceData.module_type].meta ? generalUtil.deviceTypes[deviceData.module_type].meta : {};

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_sub_type,device_name,device_icon) VALUES (?,?,?,?,?,?,?)";
    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes[deviceData.module_type].device_type.toUpperCase()),
        module_id: deviceData.module_id,
        device_identifier: deviceData.module_identifier,
        device_type: generalUtil.deviceTypes[deviceData.module_type].device_type,
        device_sub_type: generalUtil.deviceTypes.pir_device.device_sub_types[0],
        // device_name: generalUtil.deviceTypes[deviceData.module_type].device_name + " " + i,
        device_name: generalUtil.deviceTypes[deviceData.module_type].device_name,
        device_icon: generalUtil.deviceTypes[deviceData.module_type].device_icon
    }

    // Insert Default Meta To The System
    // if (generalUtil.deviceTypes[deviceData.module_type].meta != null) {
    //     for (let metaKey of Object.keys(generalUtil.deviceTypes[deviceData.module_type].meta)) {

    //         await generalMetaModel.add({
    //             meta_id: appUtil.generateRandomId('META'),
    //             table_name: 'device',
    //             table_id: newDevice.device_id,
    //             meta_name: metaKey,
    //             meta_value: generalUtil.deviceTypes[deviceData.module_type].meta[metaKey]
    //         });
    //     }
    // }

    if (deviceData.meta != null) {
        for (let metaKey of Object.keys(deviceData.meta)) {
            await generalMetaModel.update(newDevice.device_id, 'device', metaKey, deviceData.meta[metaKey]);
        }
    }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: 0
    });
}

/**
 * Add PIR Detector
 * @param module_id
 * @param module_identifier
 * @param module_type
 * @param meta (key value pair)
 */
device.prototype.addPirDetector = async function (deviceData) {

    logger.info('Add PIR Dtector');
    let meta = generalUtil.deviceTypes[deviceData.module_type].meta ? generalUtil.deviceTypes[deviceData.module_type].meta : {};

    const query = "INSERT INTO spike_devices (device_id,module_id,device_identifier,device_type,device_name,device_icon) VALUES (?,?,?,?,?,?)";
    const newDevice = {
        device_id: appUtil.generateRandomId(generalUtil.deviceTypes[deviceData.module_type].device_type.toUpperCase()),
        module_id: deviceData.module_id,
        device_identifier: deviceData.module_identifier,
        device_type: generalUtil.deviceTypes[deviceData.module_type].device_type,
        // device_name: generalUtil.deviceTypes[deviceData.module_type].device_name + " " + i,
        device_name: generalUtil.deviceTypes[deviceData.module_type].device_name,
        device_icon: generalUtil.deviceTypes[deviceData.module_type].device_icon
    }

    // Insert Default Meta To The System
    // if (generalUtil.deviceTypes[deviceData.module_type].meta != null) {
    //     for (let metaKey of Object.keys(generalUtil.deviceTypes[deviceData.module_type].meta)) {

    //         await generalMetaModel.add({
    //             meta_id: appUtil.generateRandomId('META'),
    //             table_name: 'device',
    //             table_id: newDevice.device_id,
    //             meta_name: metaKey,
    //             meta_value: generalUtil.deviceTypes[deviceData.module_type].meta[metaKey]
    //         });
    //     }
    // }

    // if (deviceData.meta != null) {
    //     for (let metaKey of Object.keys(deviceData.meta)) {
    //         await generalMetaModel.update(newDevice.device_id, 'device', metaKey, deviceData.meta[metaKey]);
    //     }
    // }

    await dbManager.executeNonQuery(query, Object.values(newDevice));

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_status',
        status_value: 0
    });

    await dbManager.executeInsert('spike_device_status', {
        device_status_id: appUtil.generateRandomId('DEVICE-STATUS'),
        device_id: newDevice.device_id,
        status_type: 'device_sub_status',
        status_value: 0
    });
}

// /**
//  * Update device Status
//  * @param deviceData (object of device)
//  * @param options (emitRoomPanelStatus=> (true/false))
//  */
// device.prototype.updateStatus = async function (deviceData, options = {}) {

//     logger.info('Device Model | Update Status', deviceData);
//     let updateData = {};

//     // Update status if provided
//     if (deviceData.device_status) {
//         updateData.device_status = deviceData.device_status;
//         await deviceStatusModel.update(deviceData.device_id, 'device_status', deviceData.device_status);
//     }

//     // update sub status if provided
//     if (deviceData.device_sub_status!==null && deviceData.device_sub_status!==undefined) {
//         updateData.device_sub_status = deviceData.device_sub_status;
//         await deviceStatusModel.update(deviceData.device_id, 'device_sub_status', deviceData.device_sub_status);
//     }

//     // update swing status if provided
//     if (deviceData.device_swing_status) {
//         updateData.device_swing_status = deviceData.device_swing_status;
//         await deviceStatusModel.update(deviceData.device_id, 'device_swing_status', deviceData.device_swing_status);
//     }

//     // Emit the room panel status when there is an update in device status
//     // if (options.emitRoomPanelStatus && options.emitRoomPanelStatus == true) {
//     //     eventsEmitter.emit('emitRoomPanelStatus',deviceData);
//     // }
// }

/**
 * Update Device details
 * @param device_id
 * @param updateQuery
 */
device.prototype.update = async function (device_id, updateQuery) {
    updateQuery.is_sync = 0;

    // Edited by Brijesh Parmar
    return await dbManager.executeUpdate('spike_devices', updateQuery, { 'device_id': device_id });
    // let query = "UPDATE spike_devices SET ";
    // query += dbManager.generateUpdateQuery(updateQuery) + ' ';
    // query += "WHERE device_id = ?";
    // return await dbManager.executeNonQuery(query, [device_id]);
}

/**
 * Delete device by device_id
 * @param device_id
 */
device.prototype.delete = async function (device_id) {
    const query = `DELETE FROM spike_devices WHERE device_id = ?`;
    await dbManager.executeNonQuery(query, device_id);
}

/**
 * Delete by Module Id
 * @param module_id
 */
device.prototype.deleteByModuleId = async function (module_id) {
    const query = `DELETE FROM spike_devices WHERE module_id = ?`;
    await dbManager.executeNonQuery(query, module_id);
}

/**
 * Get List of device icons
 */
device.prototype.listIcons = async function () {
    let query = `SELECT * FROM mst_icons WHERE icon_type=0 and is_active=1`;
    return await dbManager.all(query, []);
}

/**
 * Get Panels of specific device
 * @param device_id
 */
device.prototype.getPanelList = async function (device_id) {
    const query = `
    SELECT DISTINCT p.panel_id, p.room_id
    from spike_panel_devices pd
    inner join spike_panels p on p.panel_id=pd.panel_id 
    inner join spike_rooms r on r.room_id=p.room_id 
    where pd.device_id=?
    `;
    return await dbManager.all(query, [device_id]);
}

/**
 * Get Panel List with Moods Panels
 * @param device_id
 */
device.prototype.getPanelListWithMoodPanels = async function (device_id) {
    const query = `
    SELECT DISTINCT p.panel_id, p.room_id
    from spike_panel_devices pd
    inner join spike_panels p on p.panel_id=pd.panel_id 
    inner join spike_rooms r on r.room_id=p.room_id 
    where pd.device_id=?
    `;
    return await dbManager.all(query, [device_id]);
}

/**
 * Get device and panel room name
 * @param device_id
 */
device.prototype.getDevicePanelRoomName = async function (device_id) {

    const query = `SELECT spd.device_id,sp.panel_name,sr.room_name from spike_panel_devices spd
                        INNER JOIN spike_panels sp on sp.panel_id=spd.panel_id
                        INNER JOIN spike_rooms sr on sr.room_id=sp.room_id
                        WHERE spd.device_id=? LIMIT 1`;

    return await dbManager.get(query, [device_id]);

}



module.exports = new device();