const dbManager = require('../util/db-manager');
const fs = require('fs');

const migration = function() {

    //tableName : spike_migrations
    /**
     * migration_id
     * timestamp
     */

    this.init();
    //logger.info('migration initialized');
};

/** Miration table to be initialized */
migration.prototype.init = async function() {
    const query = `
    CREATE TABLE IF NOT EXISTS 
        spike_migrations(
            migration_id TEXT PRIMARY KEY,
            timestamp TEXT NOT NULL
        );
    `;
    await dbManager.executeNonQuery(query, []);

    this.execute();
}

/**
 * Execute migration found in migrations folder
 */
migration.prototype.execute = async function() {

    let files = fs.readdirSync(__dirname + "/migrations");

    for (let i = 0; i < files.length; i++) {

        let currentMigration = await dbManager.get('select * from spike_migrations where migration_id=?', [files[i]]);

        if (!currentMigration) {

            logger.info('[DB - MIGRATION] ', files[i]);

            try {
                let migrationModules = require('./migrations/' + files[i]);
                await migrationModules.execute();
            } catch (error) {
                logger.error('[DB - MIGRATION] ', error);
            }

        }

    }
}

module.exports = new migration();