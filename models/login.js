const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const login = function () { };

/**
 * List of All Active Key
 * @param selectors (i.e columns)
 */
login.prototype.listAllActiveKeys = async function (status) {
    const query = `SELECT * FROM spike_login where status=?`;
    return await dbManager.all(query, [status]);
}

/**
 * Inactive All Keys
 * @param user_id 
 * @param phone_id 
 */
login.prototype.inactiveAllKeysExcept = async function (user_id,phone_id) {
    const query = `UPDATE spike_login set status='0',is_sync=0  WHERE user_id<>? AND phone_id<>?`;
    return await dbManager.executeNonQuery(query, [user_id,phone_id]);
}


/**
 * Inactive All Keys Except Specific Auth Key
 * @param auth_key 
 */
login.prototype.inactiveAllKeysExceptAuthKey = async function (auth_key) {
    const query = `UPDATE spike_login set status='0',is_sync=0  WHERE auth_key<>?`;
    return await dbManager.executeNonQuery(query, [auth_key]);
}


/**
 * Inactive All Keys
 * @param auth_key 
 */
login.prototype.inactiveAuthKey = async function (auth_key) {
    const query = `UPDATE spike_login set status='0',is_sync=0 WHERE auth_key=? `;
    return await dbManager.executeNonQuery(query, [auth_key]);
}


/**
 * Inactive All User Keys
 * @param user_id
 */
login.prototype.inactiveAllUserKeys = async function(user_id) {
    const query = `UPDATE spike_login set status='0',is_sync=0  WHERE user_id=?`;
    return await dbManager.executeNonQuery(query, [user_id]);
}

/**
 * Get By User_id And Phone_id
 * @param user_id
 * @param phone_id
 */
login.prototype.get = async function (user_id, phone_id) {
    const query = `SELECT * FROM spike_login WHERE user_id = ? and access_identifier=? and status='1'`;
    return await dbManager.get(query, [user_id,phone_id]);
}


/**
 * Get By Auth Key
 * @param auth_key
 */
login.prototype.getByAuthKey = async function (auth_key) {
    const query = `SELECT spike_login.*,mst_user.admin FROM spike_login INNER JOIN mst_user ON mst_user.user_id=spike_login.user_id WHERE spike_login.auth_key = ? `;
    return await dbManager.get(query, [auth_key]);
}


/** 
 * Add New Room
 * @param user_id
 * @param phone_id
 * @param phone_type
 * @param firebase_token
 */
login.prototype.add = async function (params) {
    params.login_id = appUtil.generateRandomId('LOGIN');
    params.created_at = new Date().valueOf().toString();
    params.status = '1';
    await dbManager.executeInsert('spike_login', params);
    return params;
}

/**
 * Delete By Login_id
 * @param login_id
 */
login.prototype.delete = async function (login_id) {
    const query = "DELETE FROM spike_login WHERE login_id = ?";
    return await dbManager.executeNonQuery(query, [login_id]);
}


module.exports = new login();