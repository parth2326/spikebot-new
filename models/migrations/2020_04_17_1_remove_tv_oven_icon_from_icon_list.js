var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_migration = {};

_migration.execute = async function() {

    await dbManager.executeNonQuery(`DELETE from mst_icons where icon_name IN ('cfl','fridge','tv','oven','heavyload') `, []);
    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_04_17_1_remove_tv_oven_icon_from_icon_list.js', new Date().valueOf().toString()]);

}

module.exports = _migration;