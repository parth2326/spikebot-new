var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};
const syncManager = require('./../../util/sync-manager')
_seeder.execute = async function () {

    try {

        const tables = await dbManager.all(`SELECT name FROM main.sqlite_master WHERE type='table'`);
        

        const tableData = syncManager.tablePrimaryKeys;

        for (let table of Object.keys(tableData)) {
            
            if(!Object.values(tables).includes(table)){
                continue;
            }

            await dbManager.executeNonQuery(`CREATE TRIGGER IF NOT EXISTS ${table}_after_delete_trigger  AFTER DELETE    
            ON ${table} 
            BEGIN  
            INSERT INTO spike_table_delete_history(table_id, table_name) VALUES (OLD.${tableData[table]}, '${table}');  
            END;  `, []);
        }

        await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_04_05_4_create_trigger_for_all_tables.js', new Date().valueOf().toString()]);

    } catch (error) {
        logger.error('[DB-MIGRATION - CREATE TRIGGERS FOR ALL TABLES]',error);
        return;
    }

    return;
}

module.exports = _seeder;