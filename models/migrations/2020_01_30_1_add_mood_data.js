var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    const moodList = [
        'lobby',
        'frontyard',
        'backyard',
        'basement',
        'bathroom',
        'bathroom1',
        'bathroom2',
        'bathroom3',
        'bathroom4',
        'bedroom',
        'bedroom2',
        'bedroom3',
        'bedroom4',
        'diningroom',
        'downstairs',
        'upstairs',
        'driveway',
        'maindoor',
        'evening',
        'kidroom',
        'kidroom1',
        'kidroom2',
        'garage',
        'guestroom',
        'gym',
        'hallway',
        'hallway1',
        'hallway2',
        'hallway3',
        'kitchen',
        'livingroom',
        'livingroom1',
        'livingroom2',
        'masterbedroom',
        'morning',
        'night',
        'office',
        'staircase',
        'bossoffice',
        'bossoffice1',
        'bossoffice2',
        'bossoffice3',
        'office1',
        'office2',
        'office3',
        'office4',
        'conferenceroom',
        'conferenceroom1',
        'conferenceroom2',
        'conferenceroom3',
        'pantry',
        'hallway4',
        'computerroom',
        'computerroom1',
        'computerroom2',
        'computerroom3'
    ];

    for (var i = 0; i < moodList.length; i++) {
        
        try {

            const mood = await dbManager.get('select * from mst_mood_list where mood_name=?',[moodList[i]]);

            if (mood) {
                continue;
            }

            await dbManager.executeInsert('mst_mood_list',{
                mood_id:appUtil.generateRandomId('MOOD'),
                mood_name:moodList[i],
                is_active:1
            });

        } catch (error) {
            //logger.info('ERROR', error);
        }
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_01_30_1_add_mood_data.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;