var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE TRIGGER IF NOT EXISTS spike_rooms_after_delete_trigger  AFTER DELETE    
    ON spike_rooms  
    BEGIN  
    INSERT INTO spike_table_delete_history(table_id, table_name) VALUES (OLD.room_id, 'spike_rooms');  
    END;  `, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_04_05_2_create_delete_trigger_on_room_table.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;