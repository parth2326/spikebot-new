var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`DROP TABLE IF EXISTS spike_login`, []);
    } catch (error){

    }
    
    // Create table for spike login 
    await dbManager.executeNonQuery(`CREATE TABLE IF NOT EXISTS spike_login
    (
        login_id TEXT NOT NULL PRIMARY KEY,
        auth_key TEXT NOT NULL,
        access_identifier TEXT NOT NULL,
        access_type TEXT NOT NULL,
        user_id TEXT NOT NULL,
        status TEXT NOT NULL DEFAULT '1',
        is_sync TEXT NOT NULL DEFAULT '0',
        fcm_token TEXT,
        created_at TEXT NOT NULL 
    )`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_06_04_1_create_spike_login.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;