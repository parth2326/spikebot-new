
var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_modules_module_identifier ON spike_modules (module_identifier);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_devices_module_id ON spike_devices (module_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_devices_device_identifier ON spike_devices (device_identifier);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_device_id ON spike_device_status (device_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_general_meta_table_id ON spike_general_meta (table_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_general_meta_table_name ON spike_general_meta (table_name);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_general_meta_meta_name ON spike_general_meta (meta_name);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_03_13_2_add_indexes_in_tables.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;