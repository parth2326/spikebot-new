var dbManager = require('../../util/db-manager');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`ALTER TABLE spike_schedules
        ADD on_time_difference TEXT ;`, []);    
    } catch(error) {
        logger.info('Migration Error | Add ON_Time Column', error);
    }

    try {
        await dbManager.executeNonQuery(`ALTER TABLE spike_schedules
        ADD off_time_difference TEXT ;`, []);    
    } catch(error) {
        logger.info('Migration Error | Add OFF_Time Column', error);
    }


    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_08_07_1_add_timer_columns_in_schedule.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;