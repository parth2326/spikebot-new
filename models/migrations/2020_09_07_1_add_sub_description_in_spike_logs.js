var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`ALTER TABLE spike_logs
        ADD sub_description TEXT;`, []);    
    } catch(error) {
        logger.info('Migration Error | Add Sub Description Column', error);
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_09_07_1_add_sub_description_in_spike_logs.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;