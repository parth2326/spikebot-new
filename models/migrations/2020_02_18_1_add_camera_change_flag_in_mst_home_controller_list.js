var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`ALTER TABLE mst_home_controller_list
        ADD camera_sync_status TEXT NOT NULL DEFAULT '0' ;`, []);    
    } catch(error) {
        logger.info('Add Camera Changes Error', error);
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_02_18_1_add_camera_change_flag_in_mst_home_controller_list.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;