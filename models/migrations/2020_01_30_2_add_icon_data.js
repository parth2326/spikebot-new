

var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    const iconList = [
        "ac",
        "bulb",
        "cfl",
        "fan",
        "fridge",
        "tv",
        "oven",
        "generic",
        "heavyload"
    ];

    await dbManager.executeNonQuery('delete from mst_icons');

    for (var i = 0; i < iconList.length; i++) {
        
        try {

            await dbManager.executeInsert('mst_icons',{
                icon_name:iconList[i],
                icon_type:0,
                icon_image:'-'
            });

        } catch (error) {
            //logger.info('ERROR', error);
        }
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_01_30_2_add_icon_data.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;