
var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_devices_device_type ON spike_devices (device_type);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_devices_device_sub_type ON spike_devices (device_sub_type);`, []);
    
    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_08_26_1_add_index_in_spike_devices.js', new Date().valueOf().toString()]);
}

module.exports = _seeder;