var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`DROP TABLE IF EXISTS spike_user_device_counter`, []);
    } catch (error){

    }
    
    // Create table for spike login 
    await dbManager.executeNonQuery(`CREATE TABLE IF NOT EXISTS spike_user_device_counter
    (
        spike_user_device_counter_id TEXT NOT NULL PRIMARY KEY,
        user_id TEXT NOT NULL,
        device_id TEXT NOT NULL,
        counter_type TEXT NOT NULL DEFAULT 'alert_counter',
        counter INTEGER NOT NULL DEFAULT 1,
        is_sync TEXT NOT NULL DEFAULT '0'
    )`, []);


    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_user_device_counter_user_id ON spike_user_device_counter (user_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_user_device_counter_device_id ON spike_user_device_counter (device_id);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_09_03_1_add_spike_user_device_counter_table.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;