var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE TRIGGER IF NOT EXISTS spike_panels_after_delete_trigger  AFTER DELETE    
    ON spike_panels 
    BEGIN  
    INSERT INTO spike_table_delete_history(table_id, table_name) VALUES (OLD.panel_id, 'spike_panels');  
    END;  `, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_04_05_3_create_panel_delete_trigger.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;