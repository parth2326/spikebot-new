
var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_login_user_id ON spike_login (user_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_login_auth_key ON spike_login (auth_key);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_login_access_identifier ON spike_login (access_identifier);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_06_19_1_create_index_in_spike_login_table.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;