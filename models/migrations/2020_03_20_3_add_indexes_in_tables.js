
var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_status_type ON spike_device_status (status_type);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_03_13_2_add_indexes_in_tables.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;