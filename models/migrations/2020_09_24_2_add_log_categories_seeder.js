var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function() {

    const logCategoryList = [{
        "log_type": "password_change",
        "log_display_name": "Password Changed",
        "log_parent_type": "device",
        "log_action": "password_change",
        "log_subtype": null,
        "log_devicetype": null
    }];

    // await dbManager.executeNonQuery('delete from spike_log_categories');

    for (var i = 0; i < logCategoryList.length; i++) {

        try {
            await dbManager.executeInsert('spike_log_categories', logCategoryList[i]);
        } catch (error) {
            //logger.info('ERROR', error);
        }
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_09_24_2_add_log_categories_seeder.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;