var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function() {

    const logCategoryList = [{
        "log_type": "mood_add",
        "log_display_name": "Mood Added",
        "log_parent_type": "mood",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "mood_update",
        "log_display_name": "Mood Update",
        "log_parent_type": "mood",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "mood_delete",
        "log_display_name": "Mood Delete",
        "log_parent_type": "mood",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "mood_on",
        "log_display_name": "Mood On",
        "log_parent_type": "mood",
        "log_action": "on",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "mood_off",
        "log_display_name": "Mood Off",
        "log_parent_type": "mood",
        "log_action": "off",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "room_add",
        "log_display_name": "Room Added",
        "log_parent_type": "room",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "room_update",
        "log_display_name": "Room Update",
        "log_parent_type": "room",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "room_delete",
        "log_display_name": "Room Delete",
        "log_parent_type": "room",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "room_on",
        "log_display_name": "Room On",
        "log_parent_type": "room",
        "log_action": "off",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "room_off",
        "log_display_name": "Room Off",
        "log_parent_type": "room",
        "log_action": "on",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_add",
        "log_display_name": "Schedule Added",
        "log_parent_type": "schedule",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_update",
        "log_display_name": "Schedule Update",
        "log_parent_type": "schedule",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_delete",
        "log_display_name": "Schedule Delete",
        "log_parent_type": "schedule",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_on",
        "log_display_name": "Schedule On",
        "log_parent_type": "schedule",
        "log_action": "off",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_off",
        "log_display_name": "Schedule Off",
        "log_parent_type": "schedule",
        "log_action": "on",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_enable",
        "log_display_name": "Schedule Enable",
        "log_parent_type": "schedule",
        "log_action": "enable",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "schedule_disable",
        "log_display_name": "Schedule Disable",
        "log_parent_type": "schedule",
        "log_action": "disable",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_add",
        "log_display_name": "Alert Add",
        "log_parent_type": "alert",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_update",
        "log_display_name": "Alert Update",
        "log_parent_type": "alert",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_delete",
        "log_display_name": "Alert Delete",
        "log_parent_type": "alert",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_enable",
        "log_display_name": "Alert Enable",
        "log_parent_type": "alert",
        "log_action": "enable",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_disable",
        "log_display_name": "Alert Disable",
        "log_parent_type": "alert",
        "log_action": "disable",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "alert_active",
        "log_display_name": "Alert Active",
        "log_parent_type": "alert",
        "log_action": "active",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "device_add",
        "log_display_name": "Device Add",
        "log_parent_type": "device",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "device_update",
        "log_display_name": "Device Update",
        "log_parent_type": "device",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "device_delete",
        "log_display_name": "Device Delete",
        "log_parent_type": "device",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "device_on",
        "log_display_name": "Device On",
        "log_parent_type": "device",
        "log_action": "off",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "device_off",
        "log_display_name": "Device Off",
        "log_parent_type": "device",
        "log_action": "on",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "curtain_open",
        "log_display_name": "Curtain Open",
        "log_parent_type": "device",
        "log_action": "open",
        "log_subtype": 'curtain',
        "log_devicetype": 'curtain'
    }, {
        "log_type": "curtain_close",
        "log_display_name": "Curtain Close",
        "log_parent_type": "device",
        "log_action": "close",
        "log_subtype": 'curtain',
        "log_devicetype": 'curtain'
    }, {
        "log_type": "door_open",
        "log_display_name": "Door Open",
        "log_parent_type": "device",
        "log_action": "door_open",
        "log_subtype": "sensor",
        "log_devicetype": "door_sensor"
    }, {
        "log_type": "door_close",
        "log_display_name": "Door Close",
        "log_parent_type": "device",
        "log_action": "door_close",
        "log_subtype": "sensor",
        "log_devicetype": "door_sensor"
    }, {
        "log_type": "gas_detected",
        "log_display_name": "Gas Detected",
        "log_parent_type": "device",
        "log_action": "gas_detected",
        "log_subtype": "sensor",
        "log_devicetype": "gas_sensor"
    }, {
        "log_type": "temp_alert",
        "log_display_name": "Temperature Alert",
        "log_parent_type": "device",
        "log_action": "temp_alert",
        "log_subtype": "sensor",
        "log_devicetype": "temp_sensor"
    }, {
        "log_type": "panel_add",
        "log_display_name": "Panel Added",
        "log_parent_type": "panel",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "panel_update",
        "log_display_name": "Panel Update",
        "log_parent_type": "panel",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "panel_delete",
        "log_display_name": "Panel Delete",
        "log_parent_type": "panel",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "panel_on",
        "log_display_name": "Panel On",
        "log_parent_type": "panel",
        "log_action": "on",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "panel_off",
        "log_display_name": "Panel Off",
        "log_parent_type": "panel",
        "log_action": "off",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "child_user_add",
        "log_display_name": "Child User Added",
        "log_parent_type": "profile",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "child_user_update",
        "log_display_name": "Child User Update",
        "log_parent_type": "profile",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "child_user_delete",
        "log_display_name": "Child User Delete",
        "log_parent_type": "profile",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "camera_add",
        "log_display_name": "Camera Added",
        "log_parent_type": "camera",
        "log_action": "add",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "camera_update",
        "log_display_name": "Camera Update",
        "log_parent_type": "camera",
        "log_action": "update",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "camera_delete",
        "log_display_name": "Camera Delete",
        "log_parent_type": "camera",
        "log_action": "delete",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "camera_person_detected",
        "log_display_name": "Person Detected",
        "log_parent_type": "camera",
        "log_action": "person_detected",
        "log_subtype": "camera",
        "log_devicetype": "person_detected"
    }, {
        "log_type": "water_detected",
        "log_display_name": "Water Detected",
        "log_parent_type": "device",
        "log_action": "water_detected",
        "log_subtype": "sensor",
        "log_devicetype": "water_detector"
    }, {
        "log_type": "camera_inactive",
        "log_display_name": "Camera Inactive",
        "log_parent_type": "camera",
        "log_action": "inactive",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "camera_active",
        "log_display_name": "Camera Active",
        "log_parent_type": "camera",
        "log_action": "active",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "door_lock",
        "log_display_name": "Door Lock",
        "log_parent_type": "device",
        "log_action": "door_lock",
        "log_subtype": "sensor",
        "log_devicetype": "lock"
    }, {
        "log_type": "door_unlock",
        "log_display_name": "Door Unlock",
        "log_parent_type": "device",
        "log_action": "door_unlock",
        "log_subtype": "sensor",
        "log_devicetype": "lock"
    }, {
        "log_type": "home_controller_active",
        "log_display_name": "Home Controller Connected",
        "log_parent_type": "active",
        "log_action": "home_controller",
        "log_subtype": null,
        "log_devicetype": null
    }, {
        "log_type": "home_controller_inactive",
        "log_display_name": "Home Controller Disconnected",
        "log_parent_type": "inactive",
        "log_action": "inactive",
        "log_subtype": null,
        "log_devicetype": null
    }];

    // await dbManager.executeNonQuery('delete from spike_log_categories');

    for (var i = 0; i < logCategoryList.length; i++) {

        try {
            await dbManager.executeInsert('spike_log_categories', logCategoryList[i]);
        } catch (error) {
            //logger.info('ERROR', error);
        }
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_02_20_1_add_log_categories_seeder.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;