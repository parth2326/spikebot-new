var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    try {
        await dbManager.executeNonQuery(`ALTER TABLE spike_logs
        ADD log_sub_type TEXT;`, []);    
    } catch(error) {
        logger.info('Migration Error | Add Log Sub Type Column', error);
    }

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_09_10_1_add_sub_log_sub_type_in_spike_logs.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;