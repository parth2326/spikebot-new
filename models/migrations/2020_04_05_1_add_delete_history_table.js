var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE TABLE IF NOT EXISTS spike_table_delete_history
    (
        table_id TEXT NOT NULL PRIMARY KEY,
        table_name TEXT NOT NULL,
        is_sync TEXT NOT NULL DEFAULT '0'
    )`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_04_05_1_add_delete_history_table.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;