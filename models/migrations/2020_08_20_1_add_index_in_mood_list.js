
var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS mst_mood_list_mood_id ON mst_mood_list (mood_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_mood_devices_mood_id ON spike_mood_devices (mood_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_mood_devices_panel_device_id ON spike_mood_devices (panel_device_id);`, []);


    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_08_20_1_add_index_in_mood_list.js', new Date().valueOf().toString()]);
}

module.exports = _seeder;