
var dbManager = require('../../util/db-manager');
const appUtil = require('../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_mapping_device_id ON spike_device_status_mapping (device_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_mapping_status_type ON spike_device_status_mapping (status_type);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_mapping_timestamp ON spike_device_status_mapping (timestamp);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_mapping_status_value ON spike_device_status_mapping (status_value);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_device_status_mapping_created_date ON spike_device_status_mapping (created_date);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_08_17_1_add_index_in_spike_device_status_mapping.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;