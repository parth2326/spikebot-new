
var dbManager = require('../../util/db-manager');
const appUtil = require('./../../util/app-util');
_seeder = {};

_seeder.execute = async function () {

    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_logs_user_id ON spike_logs (user_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_logs_seen_by ON spike_logs (seen_by);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_logs_log_object_id ON spike_logs (log_object_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_panels_room_id ON spike_panels (room_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_panel_devices_panel_id ON spike_panel_devices (panel_id);`, []);
    await dbManager.executeNonQuery(`CREATE INDEX IF NOT EXISTS spike_panel_devices_device_id ON spike_panel_devices (device_id);`, []);

    await dbManager.executeNonQuery('INSERT INTO spike_migrations (migration_id,timestamp) VALUES (?,?)', ['2020_03_13_add_indexes_in_tables.js', new Date().valueOf().toString()]);

}

module.exports = _seeder;