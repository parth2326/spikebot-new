const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');
const generalUtil = require('./../util/general-util');
const mood_device = function() {};

/**
 * Add Mood Device
 * @param mood_id
 * @param panel_device_id
 */
mood_device.prototype.add = async function(params) {
    params.mood_device_mapping_id = appUtil.generateRandomId('MOOD-DEVICE');
    return await dbManager.executeInsert('spike_mood_devices', params);
}

/**
 * Delete By Mood Id
 * @param mood_id
 */
mood_device.prototype.deleteByMoodId = async function(mood_id) {
    // Edited by Brijesh Parmar
    const query = `DELETE FROM spike_mood_devices where mood_id=?`;
    // return await dbManager.executeNonQuery('DELETE FROM spike_mood_devices where mood_id=?', mood_id);    
    return await dbManager.executeNonQuery(query, [mood_id]);
}

/**
 * Get Mood By Mood id
 * @param mood_id
 */
mood_device.prototype.getByMoodId = async(mood_id) => {
    const query = `select * from spike_mood_devices where mood_id=?`;
    return await dbManager.all(query, [mood_id]);
}

/**
 * Get Detailed Panel List by Mood Id
 */
mood_device.prototype.getDetailedListByMoodId = async(mood_id) => {

    // const deviceModel = require('./../models/device');

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_type,
        m.module_identifier
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
        pd.panel_device_id IN (SELECT panel_device_id from spike_mood_devices where mood_id=?);    
    `;
    return await dbManager.all(query, [mood_id]);

}


module.exports = new mood_device();