const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const dbManager = require('../util/db-manager');


const jetson = function () {

};

/**
 * Get List of jetson 
 */
jetson.prototype.list = async function () {
    const query = `SELECT * FROM spike_jetson`;
    return await dbManager.all(query, []);
}

/**
 * Get Jetson By Jetson Id
 * @param jetson_id
 */
jetson.prototype.get = async function (jetson_id) {
    const query = `SELECT * FROM spike_jetson WHERE jetson_id = ?`;
    return await dbManager.get(query, [jetson_id]);
}

/**
 * Get Jetson By IP Address
 * @param jetson_ip
 */
jetson.prototype.getByIp = async function (jetson_ip) {
    const query = `SELECT * FROM spike_jetson WHERE jetson_ip = ?`;
    return await dbManager.get(query, [jetson_ip]);
}

/**
 * Check if jetson with same ip already exists or not
 * @param jetson_id
 */
jetson.prototype.checkIfJetsonExists = async function (jetson_ip) {
    const query = `SELECT * FROM spike_jetson WHERE jetson_ip = ?`;
    let jetsonData = await dbManager.get(query, [jetson_ip]);
    if (jetsonData) {
        return true;
    } else {
        return false;
    }
}

/**
 * Add New Jetson
 * @param jetson_ip
 * @param jetson_name
 */
jetson.prototype.add = async function (jetsonData) {
    jetsonData.jetson_id = appUtil.generateRandomId(generalUtil.deviceTypes.jetson.device_type.toUpperCase())
    await dbManager.executeInsert('spike_jetson', jetsonData);
    return jetsonData;
}


/**
 * Update Jetson In DB
 * @param jetson_ip (optional)
 * @param jetson_name (optional)
 */
jetson.prototype.update = async function (jetson_id, updateQuery) {
    updateQuery.is_sync = 0;
    // Edited By Brijesh Parmar
    return await dbManager.executeUpdate('spike_jetson', updateQuery, {'jetson_id' : jetson_id});

    // let query = "UPDATE spike_jetson SET ";
    // query += dbManager.generateUpdateQuery(updateQuery) + ' ';
    // query += "WHERE jetson_id = ?";
    // console.log('update Query', query);
    // return await dbManager.executeNonQuery(query, [jetson_id]);
}

/**
 * Delete Jetson From DB
 * @param jetson_id
 */
jetson.prototype.delete = async function (jetson_id) {
    const query = `DELETE FROM spike_jetson WHERE jetson_id = ?`;
    await dbManager.executeNonQuery(query, jetson_id);
}


module.exports = new jetson();