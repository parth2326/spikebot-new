const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const dbManager = require('../util/db-manager');
const moment = require('moment');
const alertConfigModel = require('./device_alert_config');

const device_alert = function() {};


/**
 * Add Alert
 * @param device_id
 * @param alert_type
 * @param user_id
 * @param days
 * @param start_time (HHmm)
 * @param end_time (HHmm)
 * @param config - configuration parameters of alert
 */
device_alert.prototype.add = async function(params) {

    let alertData = {
        alert_id: appUtil.generateRandomId('ALERT'),
        alert_type: params.alert_type,
        device_id: params.device_id,
        user_id: params.user_id,
        days: params.days.join(","),
        start_time: params.start_time ? params.start_time : null,
        end_time: params.end_time ? params.end_time : null
    }

    const query = "INSERT INTO spike_alerts (alert_id, device_id, alert_type, user_id, days, start_time, end_time) VALUES (?,?,?,?,?,?,?)";
    const queryParams = [alertData.alert_id, alertData.device_id, alertData.alert_type, alertData.user_id, alertData.days, alertData.start_time, alertData.end_time];

    await dbManager.executeNonQuery(query, queryParams)

    // Add Alert Configuration
    for (const [key, value] of Object.entries(params.config)) {
        await alertConfigModel.add({
            alert_id: alertData.alert_id,
            param_name: key,
            param_value: value
        });
    }

    return alertData;
}

/**
 * Mark Alert as Inactive
 * @param alert_id
 */
device_alert.prototype.markInactive = async function(alert_id) {
    await this.update(alert_id, { is_active: 'n' });
}

/**
 * Update Alert
 * @param alert_id
 * @param updatableData
 */
device_alert.prototype.update = async function(alert_id, updatableData) {

    updatableData.is_sync = 0;

    // Edited by Brijesh Parmar
    let config = updatableData.config;
    delete updatableData.config;

    await dbManager.executeUpdate('spike_alerts', updatableData, {'alert_id': alert_id});
    // let query = "UPDATE spike_alerts SET ";


    // // Update the alert information if any
    // query += dbManager.generateUpdateQuery(updatableData) + ' ';
    // query += "WHERE alert_id = ?";
    // await dbManager.executeNonQuery(query, [alert_id]);

    // Delete Old Configuration And Then Add New Configuration
    if (config != null && Object.keys(config).length > 0) {
        await alertConfigModel.deleteByAlertId(alert_id);
        for (const [key, value] of Object.entries(config)) {
            await alertConfigModel.add({
                alert_id: alert_id,
                param_name: key,
                param_value: value
            });
        }

    }

    return true;
}

/**
 * Delete Alert
 * @param alert_id
 */
device_alert.prototype.delete = async function(alert_id) {
    const query = "DELETE FROM spike_alerts WHERE alert_id=?";
    const params = [alert_id];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete by User Id And Device Id
 * @param user_id
 * @param device_id
 */
device_alert.prototype.deleteByUserIdAndDeviceId = async function(user_id, device_id) {
    const query = "DELETE FROM spike_alerts WHERE user_id = ? and device_id = ?";
    const params = [user_id, device_id];
    return await dbManager.executeNonQuery(query, params);
}


/**
 * Delete by  Device Id
 * @param device_id
 */
device_alert.prototype.deleteByDeviceId = async function(device_id) {
    const query = "DELETE FROM spike_alerts WHERE device_id = ?";
    const params = [device_id];
    return await dbManager.executeNonQuery(query, params);
}


/**
 * Get Alerts of that specific device
 * @param device_id
 * @param is_active
 */
device_alert.prototype.getAlertsByDeviceId = async function(device_id, is_active, alert_type) {

    let query = `SELECT * from spike_alerts WHERE device_id = ?`;
    let queryParams = [device_id];

    // Edited by Brijesh Parmar
    if (is_active != null) {
        query += " AND is_active = ? "
        queryParams.push(is_active);
    }

    if (alert_type != null) {
        query += " AND alert_type = ? "
        queryParams.push(alert_type);
    }

    // const params = [device_id, is_active,alert_type];
    let response = await dbManager.all(query, queryParams);

    for (let i = 0; i < response.length; i++) {
        let configurationList = await alertConfigModel.getByAlertId(response[i].alert_id);
        for (let j = 0; j < configurationList.length; j++) {
            response[i][configurationList[j].param_name] = configurationList[j].param_value;
        }
    }

    return response;

}

/**
 * Get All Configs of the alert
 * @param alert_id
 */
device_alert.prototype.config = async function(alert_id) {
    let query = 'SELECT * from spike_alert_config where alert_id=?'
    return await dbManager.all(query, [alert_id]);
}

/**
 * Get Alert by Alert Id
 * @param alert_id
 */
device_alert.prototype.get = async function(alert_id) {
    let query = `SELECT * from spike_alerts WHERE alert_id = ?`;
    const params = [alert_id];
    return await dbManager.get(query, params);
}

/**
 * Get Current Device Alerts of specifc device
 * @param device_id
 */
device_alert.prototype.getCurrentDeviceAlerts = async function(device_id) {

    let query = `SELECT * from spike_alerts 
                    WHERE device_id = ? 
                    AND is_active='y' 
                    AND (days=? OR days like ? OR days like ? OR days like ?)`;

    let d = new Date();
    let day = d.getDay();
    let hours = d.getHours();
    let minutes = d.getMinutes();

    let currenTime = hours.toString() + "" + minutes.toString();

    const params = [device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', currenTime, currenTime];
    return await dbManager.all(query, params);

}

/**
 * get Device alerts by device type
 * @param device
 * @param params (alert_type,status)
 */
device_alert.prototype.getDeviceAlertByDeviceType = async function(device, params) {

    let query = '';
    let queryParams = [];
    if (device.device_type == generalUtil.deviceTypes.temp_sensor.device_type && params.alert_type == generalUtil.alertTypes.temp_sensor.temperature) {

        /**
         * device
         * params.status
         * params.alert_type = 'temperature'
         */

        query = `SELECT a.*, 
                        m.topic_arn, 
                        sp.panel_name,
                        sr.room_name,
                        ac.param_name,
                        ac.param_value,
                        m.badge_count,
                        (SELECT meta_value FROM spike_general_meta WHERE table_id = a.user_id AND meta_name = ?) as is_notification_enable
                    from spike_alert_config ac 
                        inner join spike_alerts a on a.alert_id=ac.alert_id
                        inner join mst_user m on m.user_id=a.user_id
                        left join spike_panel_devices spd on spd.device_id=a.device_id
                        left join spike_panels sp on sp.panel_id=spd.panel_id
                        left join spike_rooms sr on sr.room_id=sp.room_id  
                        WHERE a.device_id = ? 
                        AND a.is_active='y'
                        AND (a.days=? OR a.days like ? OR a.days like ? OR a.days like ?) 
                        AND a.alert_type=?
                        AND ((ac.param_name='max_temp' AND CAST(ac.param_value as REAL) < ?) OR (ac.param_name='min_temp' AND CAST(ac.param_value as REAL) > ?))`;

        let d = new Date();
        let day = d.getDay();
        let hours = d.getHours();
        let minutes = d.getMinutes();

        let currenTime = hours.toString() + "" + minutes.toString();

        queryParams = [generalUtil.userMetaList.temperature_sensor_notification_enable, device.device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', params.alert_type, parseInt(params.status.toString()), parseInt(params.status.toString())];
    }

    if (device.device_type == generalUtil.deviceTypes.temp_sensor.device_type && params.alert_type == generalUtil.alertTypes.temp_sensor.humidity) {

        /**
         * device
         * params.status
         * params.alert_type = 'humidity'
         */

        query = `SELECT a.*,
                        m.topic_arn,
                        sp.panel_name,
                        sr.room_name,
                        ac.param_name,
                        ac.param_value,
                        m.badge_count ,
                        (SELECT meta_value FROM spike_general_meta WHERE table_id = a.user_id AND meta_name = ?) as is_notification_enable
                from spike_alert_config ac 
                    inner join spike_alerts a on a.alert_id=ac.alert_id
                    inner join mst_user m on m.user_id=a.user_id
                    left join spike_panel_devices spd on spd.device_id=a.device_id
                    left join spike_panels sp on sp.panel_id=spd.panel_id
                    left join spike_rooms sr on sr.room_id=sp.room_id  
                    WHERE a.device_id = ? 
                    AND a.is_active='y'
                    AND (a.days=? OR a.days like ? OR a.days like ? OR a.days like ?) 
                    AND a.alert_type=?
                    AND ((ac.param_name='max_humidity' AND CAST(ac.param_value as REAL) < ?) OR (ac.param_name='min_humidity' AND CAST(ac.param_value as REAL) > ?))`;

        let d = new Date();
        let day = d.getDay();
        let hours = d.getHours();
        let minutes = d.getMinutes();

        let currenTime = hours.toString() + "" + minutes.toString();

        queryParams = [generalUtil.userMetaList.temperature_sensor_notification_enable, device.device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', params.alert_type, parseInt(params.status), parseInt(params.status)];

    }

    if (device.device_type == generalUtil.deviceTypes.door_sensor.device_type && params.alert_type == generalUtil.alertTypes.door_sensor.door_open_close) {

        /**
         * device
         * params.status
         * params.alert_type = 'door_open_close'
         */

        query = `SELECT a.alert_id,
                        a.device_id,
                        a.alert_type,
                        sp.panel_name,
                        sr.room_name,
                        m.topic_arn,
                        a.user_id,
                        m.badge_count,
                        (SELECT meta_value FROM spike_general_meta WHERE table_id = a.user_id AND meta_name = ?) as is_notification_enable
                    from spike_alerts a
                            inner join mst_user m on m.user_id=a.user_id
                            left join spike_panel_devices spd on spd.device_id=a.device_id
                            left join spike_panels sp on sp.panel_id=spd.panel_id
                            left join spike_rooms sr on sr.room_id=sp.room_id  
                            WHERE a.device_id = ? 
                            AND a.is_active='y'
                            AND (a.days=? OR a.days like ? OR a.days like ? OR a.days like ?) 
                            AND a.alert_type=?

                            AND (
                                    (
                                        
                                        CAST(a.start_time as REAL) > CAST(a.end_time as REAL)
                                        
                                        AND
                                        (
                                            CAST(a.start_time as REAL) < ? 
                                            OR
                                            CAST(a.end_time as REAL) > ?
                                        )
                                    )
                                    OR 
                                    (
                                        CAST(a.start_time as REAL) < ? AND CAST(a.end_time as REAL) > ? 
                                    )
                                ) 
                             `;

                             let d = new Date();
                             let day = d.getDay();
                             let hours = d.getHours();
                             let minutes = d.getMinutes();

                             let currenTime = moment().format('HHmm');

        queryParams = [generalUtil.userMetaList.door_sensor_notification_enable, device.device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', params.alert_type, parseInt(currenTime), parseInt(currenTime), parseInt(currenTime), parseInt(currenTime)];
    }

    if (device.device_type == generalUtil.deviceTypes.water_detector.device_type && params.alert_type == generalUtil.alertTypes.water_detector.water_detected) {

        /**
         * device
         * params.status
         * params.alert_type = 'water_detected'
         */

        query = `SELECT a.alert_id,
                        a.device_id,
                        a.alert_type,
                        sp.panel_name,
                        sr.room_name,
                        m.topic_arn,
                        a.user_id,
                        m.badge_count,
                        (SELECT meta_value FROM spike_general_meta WHERE table_id = a.user_id AND meta_name = ?) as is_notification_enable
                    from spike_alerts a
                            inner join mst_user m on m.user_id=a.user_id
                            left join spike_panel_devices spd on spd.device_id=a.device_id
                            left join spike_panels sp on sp.panel_id=spd.panel_id
                            left join spike_rooms sr on sr.room_id=sp.room_id  
                            WHERE a.device_id = ? 
                            AND a.is_active='y'
                            AND (a.days=? OR a.days like ? OR a.days like ? OR a.days like ?) 
                            AND a.alert_type=?
                            AND (
                                    (
                                        
                                        CAST(a.start_time as REAL) > CAST(a.end_time as REAL)
                                        
                                        AND
                                        (
                                            CAST(a.start_time as REAL) < ? 
                                            OR
                                            CAST(a.end_time as REAL) > ?
                                        )
                                    )
                                    OR 
                                    (
                                        CAST(a.start_time as REAL) < ? AND CAST(a.end_time as REAL) > ? 
                                    )
                            ) `;

                            let d = new Date();
                            let day = d.getDay();
                            let hours = d.getHours();
                            let minutes = d.getMinutes();

                            let currenTime = moment().format('HHmm');

        queryParams = [generalUtil.userMetaList.water_detector_notification_enable, device.device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', params.alert_type, parseInt(currenTime), parseInt(currenTime), parseInt(currenTime), parseInt(currenTime)];
    }

    if (device.device_type == generalUtil.deviceTypes.yale_lock.device_type && params.alert_type == generalUtil.alertTypes.lock.door_lock_unlock) {

        /**
         * device
         * params.status
         * params.alert_type = 'door_lock_unlock'
         */

        query = `SELECT a.alert_id,
                        a.device_id,
                        a.alert_type,
                        sp.panel_name,
                        sr.room_name,
                        m.topic_arn,
                        a.user_id,
                        m.badge_count,
                        (SELECT meta_value FROM spike_general_meta WHERE table_id = a.user_id AND meta_name = ?) as is_notification_enable
                    from spike_alerts a
                            inner join mst_user m on m.user_id=a.user_id
                            left join spike_panel_devices spd on spd.device_id=a.device_id
                            left join spike_panels sp on sp.panel_id=spd.panel_id
                            left join spike_rooms sr on sr.room_id=sp.room_id  
                            WHERE a.device_id = ? 
                            AND a.is_active='y'
                            AND (a.days=? OR a.days like ? OR a.days like ? OR a.days like ?) 
                            AND a.alert_type=?

                            AND (
                                    (
                                        
                                        CAST(a.start_time as REAL) > CAST(a.end_time as REAL)
                                        
                                        AND
                                        (
                                            CAST(a.start_time as REAL) < ? 
                                            OR
                                            CAST(a.end_time as REAL) > ?
                                        )
                                    )
                                    OR 
                                    (
                                        CAST(a.start_time as REAL) < ? AND CAST(a.end_time as REAL) > ? 
                                    )
                                ) 
                             `;

                             let d = new Date();
                             let day = d.getDay();
                             let hours = d.getHours();
                             let minutes = d.getMinutes();

                             let currenTime = moment().format('HHmm');

        queryParams = [generalUtil.userMetaList.lock_notification_enable,device.device_id, day, '%,' + day, day + ",%", '%,' + day + ',%', params.alert_type, parseInt(currenTime), parseInt(currenTime), parseInt(currenTime), parseInt(currenTime)];
    }

    return await dbManager.all(query, queryParams);

}


module.exports = new device_alert();