
const dbManager = require('../util/db-manager');

const room = function () {
    //tableName : spike_rooms
    /**
     * // Columns : 
     * room_id
     * room_status
     * room_users: usersids
     * meta : {}
     */

};

/**
 * Check if room Name exists
 * @param roomName
 */
room.prototype.isRoomNameExists = async function (roomName) {
    const query = `SELECT count(room_id) AS count FROM spike_rooms WHERE room_name = ?`;
    return await dbManager.get(query, [roomName]);
}

/**
 * List of All Rooms
 * @param selectors (i.e columns)
 */
room.prototype.list = async function (...selectors) {
    const query = `SELECT ${selectors} FROM spike_rooms`;
    return await dbManager.all(query, []);
}

/**
 * List of Room Type
 * @param selectors (i.e columns)
 */
room.prototype.listByType = async function (room_type, selectors) {
    const query = `SELECT ${selectors.join()} FROM spike_rooms`;
    return await dbManager.all(query, []);
}

/**
 * List by User
 * @param user_id
 * @param room_type
 * @param meta
 */
room.prototype.listByUser = async function (user_id, room_type, meta = []) {

    if (meta.length > 0) {

        // metaQueries = [];
        // if (meta.length > 0) {
        //     for (var i = 0; i < meta.length; i++) {
        //         metaQueries.push(`(SELECT meta_value from spike_general_meta where table_id="${r.room_id}" and table_name="room" and meta_name="${meta[i]}") as meta_${meta[i]}`)
        //     }
        // }

    }

    const query = `SELECT r.* FROM spike_rooms r WHERE (room_users like ?)`;

    return await dbManager.all(query, [   '%' + user_id + '%']);
}

/**
 * Get Room By room Id
 * @param room_id
 */
room.prototype.get = async function (room_id, room_type=null) {
    const query = `SELECT * FROM spike_rooms WHERE room_id = ?`;
    return await dbManager.get(query, [room_id]);
}

/** 
 * Add New Room
 * @param room_id
 * @param room_name
 * @param room_users
 * @param created_by
 */
room.prototype.add = async function (roomData) {

    /**
     * room_id
     * room_name
     * room_users
     * created_by
     */

    const query = "INSERT INTO spike_rooms (room_id, room_name, room_users, created_by) VALUES (?,?,?,?)";
    const params = [roomData.room_id, roomData.room_name, roomData.room_users, roomData.created_by];
    return await dbManager.executeNonQuery(query, params);

}

/** 
 * Update Room Details
 * @param room_id
 * @param roomData
 * */
room.prototype.update = async function (room_id, roomData) {

    roomData.is_sync = 0;

    return await dbManager.executeUpdate('spike_rooms', roomData, {'room_id' : room_id});
    // let query = "UPDATE spike_rooms SET ";
    // query += dbManager.generateUpdateQuery(roomData) + ' ';
    // query += "WHERE room_id = ?";
    // return await dbManager.executeNonQuery(query, [room_id]);
}

/**
 * Delete Room By Room Id
 * @param room_id
 */
room.prototype.delete = async function (room_id) {
    const query = "DELETE FROM spike_rooms WHERE room_id = ?";
    return await dbManager.executeNonQuery(query, [room_id]);
}

/** Get Unseen Logs of Room
 * @param user_id
 * @param room_id
 */
room.prototype.getUnseenLogs = async function (user_id, room_id) {
    const query = `SELECT count(0) as total_unseen FROM spike_logs WHERE (room_id = ? 
                    OR 
                    panel_id IN (SELECT panel_id from spike_panels where room_id=?)
                    OR device_id IN (
                                        SELECT device_id from spike_panel_devices WHERE
                                            panel_id IN (
                                                SELECT panel_id from spike_panels where room_id=?    
                                            )
                                    ))
                    AND seen_by NOT like ?`;
    let counter = await dbManager.get(query, [room_id, room_id, room_id,'%'+user_id+'%']);
    return counter.total_unseen;
}

room.prototype.getAllRoomDeviceCount = async function() {
    const query = `SELECT sr.room_id, (SELECT count(0) from spike_panel_devices where panel_id IN (
        SELECT panel_id from spike_panels where room_id=sr.room_id
    )) as total_devices from spike_rooms sr`
    let totalDevice =  await dbManager.all(query);
    return totalDevice;
}

/**
 * Get User Rooms with their device count
 */
room.prototype.getAllChildWithRoomDeviceCount = async function(user_id) {
    const query = `SELECT sr.*, (SELECT count(0) from spike_panel_devices where panel_id IN (
        SELECT panel_id from spike_panels where room_id=sr.room_id
    )) as total_devices from spike_rooms sr where sr.room_users like ?`
    return  await dbManager.all(query,['%'+user_id+'%']);
    // return totalDevice;
}

/**
 * Get UnSeen Alerts Count
 * @param user_id
 * @param room_id
 */
room.prototype.getUnseenAlerts = async function (user_id, room_id) {
    const query = `SELECT count(0) as total_unseen FROM spike_logs WHERE
                    device_id IN (
                                        SELECT device_id from spike_panel_devices WHERE
                                            panel_id IN (
                                                SELECT panel_id from spike_panels where room_id=?    
                                            )
                                    )
                    AND log_type IN ('door_open','door_close','temp_alert','gas_detected','door_lock','door_unlock')
                    AND (seen_by NOT LIKE ? OR seen_by IS NULL ) `;
    let counter = await dbManager.get(query, [room_id,'%' + user_id + '%']);
    return counter.total_unseen;
}

/**
 * When creating Mood the name of mood is fixed because it has to map with alexa cloud app.
 * to get the name `list mst_mood_list` is used.
 */
room.prototype.getRoomStatus = async function (room_id) {

    // const query = `select count(0) as total_on_devices from spike_panel_devices pd 
    //                     inner join spike_devices d on d.device_id=pd.device_id 
    //                     inner join spike_panels p on p.panel_id=pd.panel_id 
    //                     where d.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') and (select status_value from spike_device_status where device_id=d.device_id and status_type='device_status')='1' and p.room_id=?`;

    const query = `select pd.device_id from spike_panel_devices pd 
                    inner join spike_device_status sds on sds.device_id=pd.device_id
                    inner join spike_devices d on d.device_id=pd.device_id
                    inner join spike_panels p on p.panel_id=pd.panel_id
                    where d.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') 
                    and  sds.status_type='device_status' and sds.status_value='1'
                    and p.room_id=? limit 1
                    `;


    let status = await dbManager.get(query, [room_id]);
    if(status){
        return 1;
    }else{
        return 0;
    }
    return status.total_on_devices == 0 ? 0 : 1
}

// room.prototype.getRoomStatus = async function (room_id) {

//     const query = `select count(0) as total_on_devices from spike_panel_devices pd 
//                         inner join spike_devices d on d.device_id=pd.device_id 
//                         inner join spike_panels p on p.panel_id=pd.panel_id 
//                         where d.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') and (select status_value from spike_device_status where device_id=d.device_id and status_type='device_status')='1' and p.room_id=?`;

//     var status = await dbManager.get(query, [room_id]);
//     return status.total_on_devices == 0 ? 0 : 1
// }


/**
 * Get All Panels with their status of specific room
 * @param room_id
 */
room.prototype.getPanelWithStatus = async function (room_id) {
    const query = `select panel_id, 
                    ( select count(0) from spike_panel_devices pd 
                      inner join spike_devices d on d.device_id=pd.device_id 
                      where (select status_value from spike_device_status where device_id=d.device_id and status_type='device_status')=1 and pd.panel_id=p.panel_id 
                    ) as total_on_devices 
                    from spike_panels p  
                    where room_id=? 
                           and panel_type='general';
                 `;
    let status = await dbManager.get(query, [room_id]);
    return status;
}


room.prototype.getMoodName = async function (mood_name_id) {
    const query = "SELECT * FROM mst_mood_list WHERE id = ? AND is_active = 1";
    return await dbManager.get(query, [mood_name_id]);
}

// room.prototype.listAvailibleMoodNames = async function (room_names = []) {
//     let query = `SELECT * FROM mst_mood_list WHERE is_active = 1`;
//     if (room_names.length) {
//         query += "  AND mood_name NOT IN ("
//         for (let value of room_names) {
//             query += typeof value === 'string' ? "'" : "";
//             query += value
//             query += typeof value === 'string' ? "'" : "";
//             query += ",";
//         }
//         query = query.substr(0, query.length - 1);
//         query += ')';
//     }
//     return await dbManager.all(query, []);
// }

/**
 * @param room_list
 * @param user_id
 */
room.prototype.updatePrivilege = async function (params) {

    const roomList = await this.listByType('room', ['room_id', 'room_name', 'room_users', 'created_by']);

    for (let cRoom of roomList) {

        let cUser = cRoom.room_users.split(",");

        // If user has priviledge to the before but now no longer needed
        if (cUser.includes(params.user_id) && !params.room_list.includes(cRoom.room_id)) {

            let newUserList = cUser.filter((item) => {
                return item != params.user_id
            });

            // Remove User From the room and then update the room details
            await this.update(cRoom.room_id, {
                room_users: newUserList.join(",")
            })

            // Delete All Moods That that Devices of that particular room
            // await dbManager.executeNonQuery(`DELETE from spike_panel_devices where 
            //                                     device_id IN (  
            //                                                     SELECT device_id from spike_panel_devices 
            //                                                     inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
            //                                                     and spike_panels.room_id=?
            //                                                  )
            //                                     AND 
            //                                     panel_device_id IN (    
            //                                                             SELECT panel_device_id from spike_panel_devices 
            //                                                             inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
            //                                                             inner join spike_rooms on spike_rooms.room_id=spike_panels.room_id
            //                                                             WHERE created_by=? and spike_rooms.room_type='mood'
            //                                                         )`, [cRoom.room_id, params.user_id]);
            
            await dbManager.executeNonQuery(`DELETE from spike_mood_devices where 
                                                mood_id IN (select mood_id from mst_mood_list where created_by=?)
                                                AND 
                                                panel_device_id IN (
                                                                        SELECT panel_device_id from spike_panel_devices 
                                                                        inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
                                                                        inner join spike_rooms on spike_rooms.room_id=spike_panels.room_id
                                                                        WHERE spike_rooms.room_id=?
                                                                    )`, [params.user_id,cRoom.room_id]);
            // Delete All Alerts Set by User Of that Room
            await dbManager.executeNonQuery(`DELETE FROM spike_alerts 
                                                where user_id=? 
                                                AND 
                                                device_id IN (
                                                                SELECT device_id from spike_panel_devices 
                                                                inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
                                                                and spike_panels.room_id=?
                                                             )`, [params.user_id, cRoom.user_id]);


        } else if ((!cUser.includes(params.user_id)) && params.room_list.includes(cRoom.room_id)) {

            cUser.push(params.user_id);
            await this.update(cRoom.room_id, {
                room_users: cUser.join(",")
            });

        }
    }

    return true;

}

module.exports = new room();