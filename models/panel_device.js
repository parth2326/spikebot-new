const dbManager = require('../util/db-manager');
const generalUtil = require('./../util/general-util');
const panel_device = function() {
    //tableName : spike_panel_devices
    /**
     * // Columns : 
     * panel_device_id
     * device_id
     * panel_id
     * meta : {}
     */
};

/**
 * List by panel Id
 * @param panel_id
 */
panel_device.prototype.listByPanelId = async function(panel_id) {
    const query = 'SELECT * FROM spike_panel_devices WHERE panel_id = ?';
    //logger.info("Query :: ", query, panel_id);
    return await dbManager.all(query, [panel_id]);
}

/**
 * Get Detailed List By Panel Id (includes device details)
 * @param device_id
 */
panel_device.prototype.detailedByDeviceId = async function(device_id) {

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.module_identifier,
        m.is_active,
        m.module_type
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
        pd.device_id = ?;    
    `;
    return await dbManager.get(query, [device_id]);
}

/**
 * Get Detailed List By Panel Id (includes device details)
 * @param panel_id
 */
panel_device.prototype.detailedListByPanelId = async function(panel_id) {

    // const deviceModel = require('./../models/device');

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_type, 
        m.module_identifier
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
        pd.panel_id = ?;    
    `;
    return await dbManager.all(query, [panel_id]);
}

/**
 * Get Detailed List By Room Id (includes device details)
 * @param room_id
 */
panel_device.prototype.detailedListByRoomId = async function(room_id) {

    // const deviceModel = require('./../models/device');

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_type, 
        m.module_identifier,
        pd.panel_id
    FROM 
        spike_panel_devices AS pd
        LEFT JOIN spike_panels as sp ON sp.panel_id = pd.panel_id
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
         sp.room_id=?
    `;
    return await dbManager.all(query, [room_id]);
}

/**
 * Get All Panels Device Ids With Room Ids
 */
panel_device.prototype.getPanelDeviceIdsWithRoomId = async function(panelDeviceIdList) {
    const query = `
    SELECT 
        pd.panel_device_id, 
        sp.room_id
    FROM 
        spike_panel_devices AS pd 
        INNER JOIN spike_panels AS sp ON pd.panel_id = sp.panel_id
        WHERE pd.panel_device_id IN (${dbManager.getQuestionMarks(panelDeviceIdList.length)})
    `;
    return await dbManager.all(query, panelDeviceIdList);
}

/**
 * Get All Panels Device with their details
 */
panel_device.prototype.allDetailedList = async function() {

    // const deviceModel = require('./../models/device');

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        p.panel_name,
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_type,
        pd.panel_id
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
        LEFT JOIN spike_panels AS p ON p.panel_id = pd.panel_id
    `;
    return await dbManager.all(query, []);
}

/**
 * Get Detailed List Panel Devices with their details by panel id and Mood Id
 * @param panel_id
 * @param mood_id
 */
panel_device.prototype.detailedListByPanelIdAndMoodId = async function(panel_id, mood_id) {

    // const deviceModel = require('./../models/device');

    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries().join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_type
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
        pd.panel_id = ? AND 
        pd.panel_device_id IN (select panel_device_id from spike_mood_devices where mood_id=?);    
    `;
    return await dbManager.all(query, [panel_id, mood_id]);
}

/**
 * List from panel ids
 */
panel_device.prototype.listFromPanelIds = async function(panel_device_ids = []) {
    if (!panel_device_ids.length) return Promise.resolve();
    let query = "SELECT * FROM spike_panel_devices";

    if (panel_device_ids.length) {
        query += " WHERE panel_device_id IN ("
        for (let value of panel_device_ids) {
            query += typeof value === 'string' ? "'" : "";
            query += value
            query += typeof value === 'string' ? "'" : "";
            query += ",";
        }
        query = query.substr(0, query.length - 1);
        query += ')';
    }
    //logger.info("query ::: ", query);
    return await dbManager.all(query, []);
}

/**
 * Get Panel Device by device_id
 * @param device_id
 */
panel_device.prototype.get = async function(device_id) {
    const query = `SELECT * FROM spike_panel_devices WHERE device_id = ?`;
    return await dbManager.get(query, [device_id]);
}


/**
 * Get Panel Device by device_id
 * @param panelDeviceIdList
 */
panel_device.prototype.getMultipleWithStatus = async function(panelDeviceIdList) {


    // const deviceModel = require('./../models/device');

    const query = `SELECT  
    pd.panel_device_id, 
    pd.device_id, 
    d.module_id, 
    d.device_name,
    d.device_icon,
    ${generalUtil.getStatusQueries().join()}
    FROM spike_panel_devices  AS pd
    LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
    WHERE pd.panel_device_id IN (${dbManager.getQuestionMarks(panelDeviceIdList.length)})`;

    //logger.info('QUERyYYYYYYYYYYYYYYYY ', query);


    return await dbManager.all(query, panelDeviceIdList);
};

/**
 * Add new Panel Device
 * @param panelDeviceData (panel_device_id,device_id,panel_id)
 */
panel_device.prototype.add = async function(panelDeviceData) {
    /**
     * panel_device_id
     * device_id
     * panel_id
     * meta
     */
    const query = "INSERT INTO spike_panel_devices (panel_device_id,device_id,panel_id) VALUES (?,?,?)";
    const params = [panelDeviceData.panel_device_id, panelDeviceData.device_id, panelDeviceData.panel_id ? panelDeviceData.panel_id : null];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete Panel Device By Panel Device Id
 * @param panel_device_id
 */
panel_device.prototype.delete = async function(panel_device_id) {
    const query = "DELETE FROM spike_panel_devices WHERE panel_device_id = ?";
    return await dbManager.executeNonQuery(query, [panel_device_id]);
}

/**
 * Delete By panel Id and device Id
 * @param panel_id
 * @param device_id
 */
panel_device.prototype.deleteByPanelIdAndDeviceId = async function(panel_id, device_id) {
    const query = "DELETE FROM spike_panel_devices WHERE panel_id = ? AND device_id=?";
    return await dbManager.executeNonQuery(query, [panel_id, device_id]);
}

/**
 * Delete By Device Id
 * @param device_id
 */
panel_device.prototype.deleteByDeviceId = async function(device_id) {
    const query = "DELETE FROM spike_panel_devices WHERE device_id = ?";
    return await dbManager.executeNonQuery(query, [device_id]);
}

/**
 * Delete By Panel Id
 * @param panel_id
 */
panel_device.prototype.deleteByPanelId = async function(panel_id) {
    const query = "DELETE FROM spike_panel_devices WHERE panel_id = ?";
    return await dbManager.executeNonQuery(query, [panel_id]);
}

/**
 * Delete by Room Id
 */
panel_device.prototype.deleteByRoomId = async function(room_id) {
    const query = "DELETE FROM spike_panel_devices WHERE panel_id IN (SELECT panel_id from spike_panels where room_id=?)";
    await dbManager.executeNonQuery(query, [room_id]);
}

/**
 * Find Device By Device Type
 * @param deviceTypes (array of device types)
 */
panel_device.prototype.findByDeviceType = async function(deviceTypes = [],otherMetas=[]) {

    // const deviceModel = require('./../models/device');

    // var inQuery = '';
    // if (deviceTypes.length == 0) {
    //     return null;
    // } else {
    //     inQuery = "'" + deviceTypes.join("','") + "'";
    // }

    // Edited by Brijesh Parmar
    const query = `
    SELECT 
        pd.panel_device_id, 
        pd.device_id, 
        d.module_id, 
        d.device_name,
        d.device_icon,
        ${generalUtil.getStatusQueries().join()},
        ${generalUtil.getMetaQueries(otherMetas).join()},
        d.device_type,
        d.device_sub_type,
        d.device_identifier,
        m.is_active,
        m.module_identifier,
        m.module_type
    FROM 
        spike_panel_devices AS pd 
        LEFT JOIN spike_devices AS d ON pd.device_id = d.device_id
        LEFT JOIN spike_modules AS m ON d.module_id = m.module_id
    WHERE 
        d.device_type IN (${dbManager.getQuestionMarks(deviceTypes.length)})`;


    return await dbManager.all(query, deviceTypes);

}


module.exports = new panel_device();