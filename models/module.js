const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const deviceModule = function() {
    //tableName : spike_modules
    /**
     * // Columns : 
     * module_id
     * module_identifier
     * module_type : curtain | gas_sensor | temp_sensor | smart_remote | door_sensor | repeater | 5 | 5f | heavy_load | double_heavy_load
     * is_active
     * last_response_time
     * is_configured: y | n
     */
};

/**
 * Check if module is already configured
 * @param module_identifier
 */
deviceModule.prototype.isConfigured = async function(module_identifier) {
    const query = "SELECT count(module_id) as count from spike_modules WHERE module_identifier = ? AND is_configured = 'y'";
    const params = [module_identifier];
    return await dbManager.get(query, params);
}

/**
 * Check if module exists by module_identifier
 * @param module_identifier
 */
deviceModule.prototype.isExists = async function(module_identifier) {
    const query = "SELECT count(module_id) as count from spike_modules WHERE module_identifier = ?";
    const params = [module_identifier];
    return await dbManager.get(query, params);
}

/**
 * Update Last Response time of specific module
 * @param module_identifier
 */
deviceModule.prototype.updateLastResponseTime = async function(module_identifier) {
    return await this.updateByModuleIdentifier(module_identifier, {
        last_response_time: new Date().getTime().toString(),
        is_active: 'y'
    });
}

/**
 * List modules by configuration
 * @param is_configured (y/n)
 */
deviceModule.prototype.listByConfiguration = async function(is_configured = 'y') {
    const query = "SELECT * from spike_modules WHERE is_configured = ?";
    return await dbManager.all(query, [is_configured]);
}

/**
 * Get Module By Module Id 
 * @param module_id
 * @param module_type (optional)
 * */
deviceModule.prototype.get = async function(module_id,module_type=null) {

    if (module_type) {
        const query = `SELECT * from spike_modules WHERE module_id = ? AND module_type = ? LIMIT 1`;
        const params = [module_id,module_type];
        return await dbManager.get(query, params);
    } else {
        const query = `SELECT * from spike_modules WHERE module_id = ? LIMIT 1`;
        const params = [module_id];
        return await dbManager.get(query, params);
    }
 
}

/**
 * Get By Identifier
 * @param module_identifier
 */
deviceModule.prototype.getByIdentifier = async function(module_identifier) {
    const query = `SELECT * from spike_modules WHERE module_identifier = ? LIMIT 1`;
    const params = [module_identifier];
    return await dbManager.get(query, params);
}

/**
 * Get by Identifier And Module Type
 * @param module_identifier
 * @param module_type
 */
deviceModule.prototype.getByIdentifierAndModuleType = async function(module_identifier, module_type) {
    const query = `SELECT * from spike_modules WHERE module_identifier = ? and module_type = ?`;
    const params = [module_identifier, module_type];
    return await dbManager.get(query, params);
}

/**
 * Add Module
 * @param moduleData
 */
deviceModule.prototype.add = async function(moduleData) {
    const query = "INSERT INTO spike_modules (module_id, module_identifier, module_type, is_active, last_response_time, is_configured) VALUES (?,?,?,?,?,?)";
    const params = [moduleData.module_id, moduleData.module_identifier, moduleData.module_type, moduleData.is_active, moduleData.last_response_time, moduleData.is_configured];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Update By Module Identifier
 * @param module_identifier
 * @param updatableData
 */
deviceModule.prototype.updateByModuleIdentifier = async function(moduleIdentifier, updatableData) {
    updatableData.is_sync = 0;
    // Edited By Brijesh Parmar
    return await dbManager.executeUpdate('spike_modules', updatableData, {'module_identifier' : moduleIdentifier});
    // let query = "UPDATE spike_modules SET ";
    // query += dbManager.generateUpdateQuery(updatableData) + ' ';
    // query += "WHERE module_identifier = ?";
    // return await dbManager.executeNonQuery(query, [moduleIdentifier]);
}

/**
 * Update device module
 * @param module_id
 * @param updatableData
 */
deviceModule.prototype.update = async function(module_id, updatableData) {

    updatableData.is_sync = 0;

    // Edited BY Brijesh Parmar
    return await dbManager.executeUpdate('spike_modules', updatableData, {'module_id' : module_id});
    // let query = "UPDATE spike_modules SET ";

    // query += dbManager.generateUpdateQuery(updatableData) + ' ';
    // query += "WHERE module_id = ?";

    // return await dbManager.executeNonQuery(query, [module_id]);
}

/**
 * Delete Module By Module Id
 * @param module_id
 */
deviceModule.prototype.delete = async function(module_id) {
    const query = "DELETE FROM spike_modules WHERE module_id=?";
    const params = [module_id];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete By Device Id
 * @param device_id
 */
deviceModule.prototype.deleteByDeviceId = async function(device_id) {
    const query = "DELETE FROM spike_modules WHERE module_id=(SELECT module_id from spike_devices where device_id=?)";
    const params = [device_id];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete By Module Identifier
 * @param module_identifier
 */
deviceModule.prototype.deleteByIdentifier = async function(module_identifier) {
    const query = "DELETE FROM spike_modules WHERE module_identifier=?";
    const params = [module_identifier];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Get Unassigned List
 * @param unAssignedType (default all) 
 */
deviceModule.prototype.unassignedList = async function(unAssignedType = 'all') {

    // Get All the objects that are unassigned
    let query = `SELECT mst.* from spike_modules AS mst where mst.module_id NOT IN (SELECT DISTINCT m.module_id from spike_panel_devices AS pd 
    inner join spike_devices AS d on d.device_id=pd.device_id 
    inner join spike_modules AS m on m.module_id=d.module_id) AND mst.is_configured='y' AND mst.module_type<>'remote'  `;

    // If Unassigned type is not all
    if (unAssignedType == 'all') {
        return await dbManager.all(query);
    } else if (unAssignedType == "panel") {
        query += ' AND mst.module_type IN ("5","5f","heavy_load","double_heavy_load")'
        return await dbManager.all(query);
    } else {
        query += ' AND mst.module_type=?'
        return await dbManager.all(query, [unAssignedType]);
    }

}

/**
 * Get Module List having reponse time less than provided time
 * @param last_response_time
 * @param excludeModuleType (to exclude modules that is not needed in list)
 * Called internally so no need to worry about sqlite
 */
deviceModule.prototype.getModuleListByLastResponseTime = async function(last_response_time, excludeModuleType = []) {

    let query = 'select * from spike_modules where CAST(last_response_time as INTEGER) < ?'

    if (excludeModuleType.length > 0) {
        let excludeQuery = "'" + excludeModuleType.join("','") + "'";
        query += ' AND module_type NOT IN ( ' + excludeQuery + ' ) ';
    }

    return await dbManager.all(query, last_response_time);
}

/**
 * Check if the module is unassgined
 */
deviceModule.prototype.checkIfIsUnassigned = async function(module_id) {

    // Check if the module is unassigned.
    let query = `SELECT mst.* from spike_modules AS mst where mst.module_id NOT IN (SELECT DISTINCT m.module_id from spike_panel_devices AS pd 
    inner join spike_devices AS d on d.device_id=pd.device_id 
    inner join spike_modules AS m on m.module_id=d.module_id) AND mst.is_configured='y'`;

    let unAssignedModuleList = await dbManager.all(query);

    let isFound = unAssignedModuleList.filter(function(item) {
        return item.module_id == module_id;
    });

    return isFound.length > 0 ? true : false;

}

/**
 * Check if is unassined by module_identifier
 * @param module_identifier
 */
deviceModule.prototype.checkIfIsUnassignedByModuleIdentifier = async function(module_identifier) {

    // Check if the module is unassigned.
    let query = `SELECT mst.* from spike_modules AS mst where mst.module_id NOT IN (SELECT DISTINCT m.module_id from spike_panel_devices AS pd 
    inner join spike_devices AS d on d.device_id=pd.device_id 
    inner join spike_modules AS m on m.module_id=d.module_id) AND mst.is_configured='y'`;

    let unAssignedModuleList = await dbManager.all(query);

    let isFound = unAssignedModuleList.filter(function(item) {
        return item.module_identifier == module_identifier;
    });

    return isFound.length > 0 ? true : false;

}

module.exports = new deviceModule();