const dbManager = require('../util/db-manager');
const eventEmitter = require('../util/event-manager');

const device_status = function() {

};

/**
 * Get device statuses of specific device
 * @param device_id
 */
device_status.prototype.get = async function(device_id) {
    let query = `SELECT * FROM spike_devices WHERE device_id = ?`;
    return await dbManager.get(query, [device_id]);
}

device_status.prototype.getStatus = async function(device_id,status_type) {
    let query = `SELECT status_value FROM spike_device_status WHERE device_id = ? and status_type=?`;

    let data =  await dbManager.get(query, [device_id,status_type]);
    if(data){
        return data.status_value;
    }else{
        return 0;
    }
}

/**
 * Update Device Status
 * @param device_id
 * @param status_type
 * @param status_value
 * @param is_sync (default 0)
 */
device_status.prototype.update = async function(device_id, status_type, status_value, is_sync = 0) {
    let query = "UPDATE spike_device_status SET status_value = ?, is_sync = ? WHERE device_id = ? AND status_type = ?";
    return await dbManager.executeNonQuery(query, [status_value, is_sync, device_id, status_type]);
}

device_status.prototype.updateMany = async function (deviceData, options = {}){

    logger.info('Device Status Model | Update Status', deviceData);

    let updateData = {};

    let promiseList = [];

    // Update status if provided
    if (deviceData.device_status !==null && deviceData.device_status!==undefined) {
        updateData.device_status = deviceData.device_status;
        promiseList.push(this.update(deviceData.device_id, 'device_status', deviceData.device_status));
        // await deviceStatusModel.update(deviceData.device_id, 'device_status', deviceData.device_status);
    }

    // update sub status if provided
    if (deviceData.device_sub_status!==null && deviceData.device_sub_status!==undefined) {
        updateData.device_sub_status = deviceData.device_sub_status;
        promiseList.push(this.update(deviceData.device_id, 'device_sub_status', deviceData.device_sub_status));
        // await deviceStatusModel.update(deviceData.device_id, 'device_sub_status', deviceData.device_sub_status);
    }

    // update swing status if provided
    if (deviceData.device_swing_status !==null && deviceData.device_swing_status!==undefined) {
        updateData.device_swing_status = deviceData.device_swing_status;
        promiseList.push(this.update(deviceData.device_id, 'device_swing_status', deviceData.device_swing_status));
        // await deviceStatusModel.update(deviceData.device_id, 'device_swing_status', deviceData.device_swing_status);
    }

    await Promise.all(promiseList);

    if(options.emitRoomPanelStatus===true){
        eventEmitter.emit('emitRoomPanelStatus', {
            device_id: deviceData.device_id,
            device_status: updateData.device_status
        });
    }

    return;

}

module.exports = new device_status();