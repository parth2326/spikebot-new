const dbManager = require('../util/db-manager');
const moodDeviceModel = require('./../models/mood_device');
const appUtil = require('./../util/app-util');

let metaList = ['smart_remote_no'];
const mood = function () { };

/**
 * Get All Mood List
 * @param mood_id
 */
mood.prototype.get = async function (mood_id) {
    const query = `SELECT m.* FROM mst_mood_list m where m.mood_id=?`;
    return await dbManager.get(query, [mood_id]);
}

/**
 * Get All Mood List
 */
mood.prototype.getAllMoodList = async function () {
    const query = `SELECT m.* FROM mst_mood_list m`;
    return await dbManager.get(query, []);
}

mood.prototype.getMetaQueries = function () {
    let metaQueries = [];
    for (let i = 0; i < metaList.length; i++) {
        metaQueries.push(`(SELECT meta_value from spike_general_meta where table_name="mood" and table_id=m.mood_id and meta_name="${metaList[i]}") as meta_${metaList[i]}`)
    }
    return metaQueries;
}


/**
 * Get All Mood List
 */
mood.prototype.getAllAllocatedMoods = async function () {
    const query = `SELECT m.*, ${this.getMetaQueries().join()} FROM mst_mood_list m where m.mood_id IN (select mood_id from spike_mood_devices)`;
    return await dbManager.all(query, []);
}

/**
 * Get All Mood List
 */
mood.prototype.getAllUnallocatedMoods = async function () {
    const query = `SELECT m.*, ${this.getMetaQueries().join()} FROM mst_mood_list m where m.mood_id NOT IN (select mood_id from spike_mood_devices)`;
    return await dbManager.all(query, []);
}

/**
 * Get Mood Status
 * @param mood_id
 */
mood.prototype.getMoodStatus = async function (mood_id) {

    // const query = `select count(0) as total_on_devices from (select d.device_id, (select status_value from spike_device_status where device_id=d.device_id and status_type='device_status') as device_status from spike_devices d where d.device_id IN 
    // (select device_id from spike_panel_devices where panel_device_id IN 
    //         (select panel_device_id from spike_mood_devices where mood_id=?)
    // )) mst where mst.device_status=1`;

    const query = `select pd.device_id from spike_panel_devices pd 
    inner join spike_mood_devices smd on pd.panel_device_id=smd.panel_device_id
    inner join spike_device_status sds on sds.device_id=pd.device_id
    inner join spike_devices d on d.device_id=pd.device_id
    where d.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') 
    and  sds.status_type='device_status' and sds.status_value='1'
    and smd.mood_id=? limit 1`;

    // const query = `select count(0) as total_on_devices from spike_device_status 
    //                     inner join spike_panel_devices on spike_panel_devices.device_id=spike_device_status.device_id
    //                     inner join spike_mood_devices on spike_mood_devices.panel_device_id = spike_panel_devices.panel_device_id
    //                     where spike_mood_devices.mood_id = ? and spike_device_status.status_type='device_status' and spike_device_status.status_value='1'
    //                     `;

    let status = await dbManager.get(query, [mood_id]);
    if(status){
        return 1;
    }else{
        return 0;
    }
    // return status.total_on_devices == 0 ? 0 : 1
}

/**
 * Get Mood List Of Device - Used to get mood of various devices
 */
mood.prototype.getMoodsOfDeviceWithStatus = async function (device_id) {

    const query = `SELECT mood_id, 
    (
          select count(0) from spike_devices inner join spike_device_status 
         on spike_device_status.device_id=spike_devices.device_id 
              and spike_device_status.status_value=1 
              and spike_device_status.status_type="device_status" 
              AND spike_devices.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') 
              and spike_devices.device_id IN ( SELECT device_id from spike_panel_devices WHERE panel_device_id IN (SELECt panel_device_id from spike_mood_devices where mood_id=m.mood_id) )
    ) as total_on_devices
     from mst_mood_list m where m.mood_id IN 
        (select DISTINCT mood_id from spike_mood_devices inner join spike_panel_devices on spike_panel_devices.panel_device_id=spike_mood_devices.panel_device_id where spike_panel_devices.device_id=? 
                
        )`;

    return await dbManager.all(query, [device_id]);

}

/**
 * Get Mood List Of Device - Used to get mood of various devices
 */
mood.prototype.getMoodsOfDeviceWithStatusOptimized = async function (device_id) {

    const query = `SELECT mood_id, 
    (
          select count(0) from spike_devices inner join spike_device_status 
         on spike_device_status.device_id=spike_devices.device_id 
              and spike_device_status.status_value=1 
              and spike_device_status.status_type="device_status" 
              AND spike_devices.device_type IN ('fan','switch','heavyload','remote','curtain','smart_bulb') 
              and spike_devices.device_id IN ( SELECT device_id from spike_panel_devices WHERE panel_device_id IN (SELECt panel_device_id from spike_mood_devices where mood_id=m.mood_id) )
    ) as total_on_devices
     from mst_mood_list m where m.mood_id IN 
        (select DISTINCT mood_id from spike_mood_devices inner join spike_panel_devices on spike_panel_devices.panel_device_id=spike_mood_devices.panel_device_id where spike_panel_devices.device_id=? 
                
        )`;

    return await dbManager.all(query, [device_id]);

}

mood.prototype.getMoodListofDevice = async function(device_id){
    const query = `SELECT mst_mood_list.mood_id from mst_mood_list inner join spike_mood_devices on spike_mood_devices.mood_id=mst_mood_list.mood_id 
                    inner join spike_panel_devices on spike_panel_devices.panel_device_id=spike_mood_devices.panel_device_id where spike_panel_devices.device_id=?`;
    return await dbManager.all(query, [device_id]);
}

/**
 * Delete all mood by user id
 * @param user_id
 */
mood.prototype.deleteAllMoodsByUserId = async (user_id) => {
    const query = `SELECT m.* FROM mst_mood_list m where m.mood_id IN (select mood_id from spike_mood_devices) AND created_by = ?`;
    const moodList = await dbManager.all(query, [user_id]);

    for (let mood of moodList) {

        await this.update(mood_id, {
            created_by: null
        });

        await moodDeviceModel.deleteByMoodId(mood.mood_id);
    }

    return null;
}

/**
 * Get User Moods
 * @param user_id
 */
mood.prototype.getUserMoods = async function (user_id) {
    const query = `SELECT m.*, ${this.getMetaQueries().join()} FROM mst_mood_list m where m.mood_id IN (select mood_id from spike_mood_devices) AND created_by = ?`;
    return await dbManager.all(query, [user_id]);
}

/**
 * Add Mood Data
 * @param mood_name (mood name as key in params)
 */
mood.prototype.createMoodIfNotExists = async function (mood_name) {

    const existingMood = await dbManager.get('select * from mst_mood_list where lower(mood_name)=?', [mood_name.trim().toLowerCase()]);

    if (existingMood) {
        return existingMood;
    } else {
        const moodData = {
            "mood_id": appUtil.generateRandomId("MOOD"),
            "mood_name": mood_name.trim(),
            "is_active": 1
        }
        await dbManager.executeInsert('mst_mood_list', moodData);

        return await dbManager.get('select * from mst_mood_list where mood_id=?', [moodData.mood_id]);
    }

}

/**
 * Add Mood Data
 * @param mood_id (references mood id from mst_mood_list)
 * @param created_by (user_id)
 * @param devices (array of panel_device_ids)
 * @param createdByFlag
 */
mood.prototype.add = async function (params) {

    for (let panel_device_id of params.devices) {
        await moodDeviceModel.add({
            mood_id: params.mood_id,
            panel_device_id: panel_device_id
        });
    }

    logger.info('Created By Flag', params.createdByFlag);
    if (params.createdByFlag == true) {
        return;        
    } else {
        await this.update(params.mood_id, {
            created_by: params.created_by
        });
    }

    return true;
}


/**
 * Update Mood details by mood id
 * @param mood_id
 * @param updateQuery
 */
mood.prototype.update = async function (mood_id, updateQuery) {

    updateQuery.is_sync = 0;

    // Edited by Brijesh Parmar
    return await dbManager.executeUpdate('mst_mood_list', updateQuery, { 'mood_id': mood_id });
    // let query = "UPDATE mst_mood_list SET ";
    // query += dbManager.generateUpdateQuery(updateQuery) + ' ';
    // query += "WHERE mood_id = ?";
    // return await dbManager.executeNonQuery(query, [mood_id]);
}

/**
 * Get Smart Remote No
 * @param {*} mood_id 
 */
mood.prototype.getSmartRemoteNo = async function (mood_id) {

    const query = 'SELECT meta_value from spike_general_meta where table_name="mood" and table_id=? and meta_name=?';
    const data = await dbManager.get(query, [mood_id, 'smart_remote_no']);
    if (data) {
        return data.meta_value;
    }
    return null;
}


module.exports = new mood();