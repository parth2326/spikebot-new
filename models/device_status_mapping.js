const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');
const moment = require('moment');

const device_status_mapping = function () {

    //tableName : spike_device_status_mapping
    /**
     * mapping_id
     * device_id
     * status_type
     * meta1
     * meta2
     * meta3
     * meta4
     * created_at format (Y-m-d)
     * timestamp
     */

};


/**
 * Add Device Status Mapping
 * @param device_id
 * @param status_type
 * @param status_value
 */
device_status_mapping.prototype.add = async function (params) {

    let mappingData = {
        mapping_id: appUtil.generateRandomId('MAP'),
        device_id: params.device_id,
        status_type: params.status_type,
        status_value: params.status_value,
        created_date: moment().format('YYYY-MM-DD'),
        timestamp: moment().utc().valueOf().toString()
    }

    const query = "INSERT INTO spike_device_status_mapping (mapping_id, device_id, status_type, status_value, created_date, timestamp) VALUES (?,?,?,?,?,?)";
    const queryParams = [mappingData.mapping_id, mappingData.device_id, mappingData.status_type, mappingData.status_value, mappingData.created_date, mappingData.timestamp];

    await dbManager.executeNonQuery(query, queryParams)

    return true;
}


/**
 * Add Device Status Mapping
 * @param device_id
 * @param status_type
 * @param status_value
 */
device_status_mapping.prototype.addOrUpdate = async function (params) {

    const previousRecord = await dbManager.get('SELECT mapping_id from spike_device_status_mapping where device_id=? and created_date=? and status_type=?',
        [params.device_id, moment().format('YYYY-MM-DD'), params.status_type]);

    if (previousRecord) {

        await this.update(previousRecord.mapping_id, {
            status_value: params.status_value,
            timestamp: moment().utc().valueOf().toString()
        });

    } else {

        let mappingData = {
            mapping_id: appUtil.generateRandomId('MAP'),
            device_id: params.device_id,
            status_type: params.status_type,
            status_value: params.status_value,
            created_date: moment().format('YYYY-MM-DD'),
            timestamp: moment().utc().valueOf().toString()
        }

        const query = "INSERT INTO spike_device_status_mapping (mapping_id, device_id, status_type, status_value, created_date, timestamp) VALUES (?,?,?,?,?,?)";
        const queryParams = [mappingData.mapping_id, mappingData.device_id, mappingData.status_type, mappingData.status_value, mappingData.created_date, mappingData.timestamp];

        await dbManager.executeNonQuery(query, queryParams)

        return true;
    }

}

/**
 * Delete Mapping By Device Id
 * @param device_id
 */
device_status_mapping.prototype.deleteByDeviceId = async function (device_id) {
    const query = "DELETE FROM spike_device_status_mapping WHERE device_id = ?";
    const params = [device_id];
    return await dbManager.executeNonQuery(query, params);
}


/**
 * Get By Device Id
 * @param device_id
 */
device_status_mapping.prototype.getByDeviceId = async function (device_id) {
    let query = `SELECT * from spike_device_status_mapping WHERE device_id = ?`;
    const params = [device_id];
    return await dbManager.get(query, params);
}

device_status_mapping.prototype.update = async function (mapping_id, updateQuery) {
    updateQuery.is_sync = 0;
    return await dbManager.executeUpdate('spike_device_status_mapping', updateQuery, { 'mapping_id': mapping_id });
}



module.exports = new device_status_mapping();