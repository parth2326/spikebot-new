const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');

const device_alert_config = function () { };

/**
 * Add Alert Config
 * @param alert_id
 * @param param_name
 * @param param_value
 */
device_alert_config.prototype.add = async function (params) {

    let alertData = {
        alert_config_id:appUtil.generateRandomId('ALERT-CONFIG'),
        alert_id: params.alert_id,
        param_name: params.param_name,
        param_value: params.param_value
    }

    const query = "INSERT INTO spike_alert_config (alert_config_id,alert_id, param_name, param_value) VALUES (?,?,?,?)";
    const queryParams = [alertData.alert_config_id,alertData.alert_id, alertData.param_name, alertData.param_value];
    return await dbManager.executeNonQuery(query, queryParams);
}


/**
 * Delete by Alert Id
 * @param alert_id
 */
device_alert_config.prototype.deleteByAlertId = async function (alert_id) {
    const query = "DELETE FROM spike_alert_config WHERE alert_id=?";
    const params = [alert_id];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Get Alert by Alert Id
 * @param alert_id
 */
device_alert_config.prototype.getByAlertId = async function (alert_id) {
    const query = "SELECT * FROM spike_alert_config WHERE alert_id=?";
    const params = [alert_id];
    return await dbManager.all(query, params);
}


module.exports = new device_alert_config();