const appUtil = require('./../util/app-util');
const generalUtil = require('./../util/general-util');
const dbManager = require('../util/db-manager');

const camera = function() {};

/**
 * Add Camera Details To Database
 * @param user_id
 * @param home_controller_device_id
 * @param camera_name
 * @param camera_ip
 * @param camera_videopath
 * @param camera_vpn_port
 * @param user_name
 * @param password
 * @param camera_url
 * @param jetson_device_id (optional)
 */
camera.prototype.add = async(params) => {

    const newCamera = {
        camera_id: params.camera_id,
        user_id: params.user_id,
        home_controller_device_id: params.home_controller_device_id,
        camera_name: params.camera_name,
        camera_ip: params.camera_ip,
        camera_videopath: params.camera_videopath,
        camera_icon: generalUtil.deviceTypes.camera.device_icon,
        camera_vpn_port: params.camera_vpn_port,
        user_name: params.user_name,
        password: params.password,
        camera_url: params.camera_url,
        created_date: appUtil.currentDateTime(),
        created_by: params.user_id,
        jetson_device_id: params.jetson_device_id ? params.jetson_device_id : null
    }

    const query = 'INSERT INTO mst_camera_devices(camera_id, user_id, home_controller_device_id, camera_name, camera_ip, camera_videopath, camera_icon, camera_vpn_port, user_name, password, camera_url, created_date, created_by,jetson_device_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

    await dbManager.executeNonQuery(query, Object.values(newCamera));

    return newCamera;
}

/**
 * Delete All Cameras Of Jetson
 * @param jetson_device_id
 */
camera.prototype.deleteByJetsonDeviceId = async(jetson_device_id) => {

    const deleteChildPriviledgeQuery = 'DELETE from mst_camera_child_priviledge where camera_id IN (SELECT camera_id from mst_camera_devices where jetson_device_id=?)';
    const deleteCameraQuery = 'DELETE from mst_camera_devices where jetson_device_id=?'
    await dbManager.executeNonQuery(deleteChildPriviledgeQuery, [jetson_device_id]);
    await dbManager.executeNonQuery(deleteCameraQuery, [jetson_device_id]);

}

/**
 * Update Camera Details
 * @param camera_id
 * @param updateQuery
 */
camera.prototype.update = async function(camera_id, updateQuery) {
    updateQuery.is_sync = 0;

    // Edited by Brijesh Parmar
    return await dbManager.executeUpdate('mst_camera_devices', updateQuery, {'camera_id' : camera_id});
    // let query = "UPDATE mst_camera_devices SET ";
    // query += dbManager.generateUpdateQuery(updateQuery) + ' ';
    // query += "WHERE camera_id = ?";
    // return await dbManager.executeNonQuery(query, [camera_id]);
}

/**
 * Get Camera by Camera Id
 * @param camera_id
 */
camera.prototype.get = async function(camera_id) {
    let query = `SELECT * from mst_camera_devices WHERE camera_id = ?`;
    const params = [camera_id];
    return await dbManager.get(query, params);
}

/**
 * User Camera for specific user
 * @param user_id
 */
camera.prototype.getUserCameras = async function(user_id) {

    let query = `SELECT * from mst_camera_devices where camera_id IN (SELECT camera_id from mst_camera_child_priviledge where user_id=?)`;
    const params = [user_id];
    return await dbManager.all(query, params);

}

/**
 * Check if user has camera priviledge
 * @param user_id
 */
camera.prototype.checkIfUserHasCameraPriviledge = async function(user_id, camera_id) {

    let query = `SELECT count(0) as camera_priviledge from mst_camera_child_priviledge where user_id=? and camera_id=?`;

    const params = [user_id, camera_id];
    const data = await dbManager.get(query, params);

    if (data.camera_priviledge == 0) {
        return false;
    } else {
        return true;
    }
}


/**
 * Check if camera name exists
 * @param camera_name
 */
camera.prototype.checkIfCameraNameExists = async function(camera_name) {

    let query = `SELECT count(0) as camera_count from mst_camera_devices where camera_name=?`;

    const params = [camera_name];
    const data = await dbManager.get(query, params);

    if (data.camera_count == 0) {
        return false;
    } else {
        return true;
    }
}

/**
 * Get All Cameras of jetson
 * @param jetson_id
 */
camera.prototype.getJetsonCamera = async function(jetson_id) {
    let query = `SELECT m.*,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_day' limit 1) as confidence_score_day,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_night' limit 1) as confidence_score_night
    from mst_camera_devices m where jetson_device_id=?`;
    const params = [jetson_id];
    return await dbManager.all(query, params);
}

/**
 * Get Child User Cameras
 * @param user_id
 */
camera.prototype.getChildUserCameras = async(user_id) => {
    logger.info('Camera User Id : ', user_id);
    let query = `SELECT m.*, (SELECT count(0) as total_unseen FROM spike_logs WHERE log_object_id=m.camera_id
    AND (seen_by NOT like ?  OR seen_by IS NULL ) AND log_type='camera_persor_detected' ) as total_unread,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_day' limit 1) as confidence_score_day,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_night' limit 1) as confidence_score_night
    from mst_camera_devices m where m.camera_id IN (SELECT camera_id from mst_camera_child_priviledge where user_id=?)`;
    const params = ['%' + user_id + '%' , user_id];
    return await dbManager.all(query, params);
}

/**
 * Get Admin Cameras
 * @param user_id
 */
camera.prototype.getAdminUserCameras = async(user_id) => {
    let query = `SELECT m.*, (SELECT count(0) as total_unseen FROM spike_logs WHERE log_object_id=m.camera_id
    AND (seen_by NOT like ?  OR seen_by IS NULL )) as total_unread,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_day' limit 1) as confidence_score_day,
    (select meta_value from spike_general_meta where table_id=m.camera_id and meta_name='confidence_score_night' limit 1) as confidence_score_night
    from mst_camera_devices m`;
    const params = ['%' + user_id + '%'];
    return await dbManager.all(query, params);
}

/**
 * @param jetson_id
 * @param user_id
 */
camera.prototype.getJetsonCameraWithUserPriviledge = async(jetson_id, user_id) => {

    let query = `SELECT * from mst_camera_devices where jetson_device_id=? AND camera_id IN (SELECT camera_id from mst_camera_child_priviledge where user_id=?)`;
    const params = [jetson_id, user_id];
    return await dbManager.all(query, params);

}

/**
 * @param jetson_id
 * @param user_id
 */
camera.prototype.getCameraAlerts = async(jetson_id, user_id) => {

    let query = `SELECT * from mst_camera_devices where jetson_device_id=? AND camera_id IN (SELECT camera_id from mst_camera_child_priviledge where user_id=?)`;
    const params = [jetson_id, user_id];
    return await dbManager.all(query, params);

}

/**
 * @param jetson_id
 * @param user_id
 */
camera.prototype.list = async() => {
    let query = `SELECT * from mst_camera_devices`;
    const params = [];
    return await dbManager.all(query, params);
}

module.exports = new camera();