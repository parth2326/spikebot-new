const appUtil = require('./../util/app-util');
const dbManager = require('../util/db-manager');
const scheduleManager = require('../util/schedule-manager');

const schedule = function () {
    //tableName : spike_schedules
    /**
     * // Columns : 
     * schedule_id
     * schedule_type (schedule/timer)
     * schedule_name
     * schedule_device_type : room | mood 
     * schedule_days
     * on_time 18:00
     * off_time 11:00
     * is_active
     * meta : {}
     * created_by : {}
     */
};


/**
 * @param schedule_days [0,1,2] array
 * @param on_time ex 18:00 24 hr format
 * @param off_time ex 11:00 24 hr format
 * @param on_time_difference 
 * @param off_time_difference
 * @param schedule_type
 * @param room_id (if schedule_device_type is mood)
 * @param schedule_name
 * @param schedule_device_type
 * @param devices
 * @param created_by
 */
schedule.prototype.add = async function (params) {


    const query = "INSERT INTO spike_schedules (schedule_id,schedule_name,schedule_days,on_time,off_time,on_time_difference, off_time_difference,schedule_type,schedule_device_type,room_id,mood_id,created_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    const newSchedule = {
        schedule_id: appUtil.generateRandomId('SCHEDULE'),
        schedule_name: params.schedule_name,
        schedule_days: params.schedule_days.join(","),
        on_time: params.on_time,
        off_time: params.off_time,
        on_time_difference: params.on_time_difference ? params.on_time_difference : null,
        off_time_difference: params.off_time_difference ? params.off_time_difference : null,
        schedule_type: params.schedule_type,
        schedule_device_type: params.schedule_device_type,
        room_id: params.room_id ? params.room_id : null,
        mood_id: params.mood_id ? params.mood_id : null,
        created_by: params.created_by
    }

    await dbManager.executeNonQuery(query, Object.values(newSchedule));

    if (params.devices) {

        // Insert Device To Schedule
        for (let i = 0; i < params.devices.length; i++) {
            await dbManager.executeInsert('spike_schedule_devices', {
                schedule_device_mapping_id: appUtil.generateRandomId('SCHEDULE-DEVICE'),
                schedule_id: newSchedule.schedule_id,
                panel_device_id: params.devices[i]
            });
        }

    }

    // Add schedule To Device Manager
    scheduleManager.addOrUpdateScheduledJob({
        schedule_id: newSchedule.schedule_id,
        on_time: newSchedule.on_time,
        off_time: newSchedule.off_time,
        schedule_type: newSchedule.schedule_type,
        days: params.schedule_days
    });

    return newSchedule;

}


/**
 * Get Schedule By Schedule Id
 * @param schedule_id
 */
schedule.prototype.get = async function (schedule_id) {
    const query = `SELECT * from spike_schedules WHERE schedule_id = ? LIMIT 1`;
    const params = [schedule_id];
    return await dbManager.get(query, params);
}

/**
 * Get Schedule By Schedule Id
 * @param schedule_name
 */
schedule.prototype.checkIfScheduleWithNameExists = async function (schedule_name) {
    const query = `SELECT * from spike_schedules WHERE lower(schedule_name) = ?`;
    const params = [schedule_name.trim().toLowerCase()];
    const data = await dbManager.get(query, params);
    if (data) {
        return true;
    } else {
        return false;
    }
}



/**
 * Find Schedule 
 * @param schedule_device_type (optional)
 * @param room_id (optional)
 * @param user_id (optional)
 */
schedule.prototype.find = async function (params) {

    let queryParams = [];
    let query = `SELECT s.* from spike_schedules s  WHERE schedule_id NOT NULL `;

    if (params.room_id) {
        query += ` AND s.schedule_id IN (
                                            select distinct schedule_id from spike_schedule_devices 
                                            where panel_device_id IN (
                                                                        select panel_device_id from spike_panel_devices 
                                                                        inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
                                                                        where spike_panels.room_id=?
                                                                    ) OR s.mood_id=?
                                        )`;
        queryParams.push(params.room_id);
        queryParams.push(params.room_id);
    }

    if (params.user_id) {
        query += ` AND s.schedule_id IN (
                                            select distinct schedule_id from spike_schedule_devices 
                                            where panel_device_id IN (
                                                                            select panel_device_id from spike_panel_devices 
                                                                            inner join spike_panels on spike_panels.panel_id=spike_panel_devices.panel_id 
                                                                            where spike_panels.room_id IN (SELECT room_id from spike_rooms WHERE room_users LIKE ?)
                                                                     )
                                        )`;
        queryParams.push('%' + params.user_id + '%');
    }

    if (params.schedule_device_type) {
        query += ` AND s.schedule_device_type=?`;
        queryParams.push(params.schedule_device_type);
    }

    return await dbManager.all(query, queryParams);

}

/**
 * Get Device Ids From Schedule id
 * @param schedule_id
 */
schedule.prototype.getDeviceIdsByScheduleId = async function (schedule_id) {
    const query = `SELECT panel_device_id,device_id from spike_panel_devices where panel_device_id IN ( SELECT panel_device_id from spike_schedule_devices where schedule_id=?)`;
    const params = [schedule_id];
    return await dbManager.all(query, schedule_id);
}

/**
 * Update Schedule
 * @param schedule_id
 * @param updatableData
 */
schedule.prototype.update = async function (schedule_id, updatableData) {

    updatableData.is_sync = 0;


    let devices = updatableData.devices;
    delete updatableData.devices;

    await dbManager.executeUpdate('spike_schedules', updatableData, { 'schedule_id': schedule_id })

    // let query = "UPDATE spike_schedules SET ";

    // query += dbManager.generateUpdateQuery(updatableData) + ' ';
    // query += "WHERE schedule_id = ?";

    // await dbManager.executeNonQuery(query, [schedule_id]);

    if (devices) {

        await this.deleteDevices(schedule_id);

        // const insertDeviceQuery = "INSERT INTO spike_schedule_devices (schedule_device_mapping_id,schedule_id,panel_device_id) VALUES (?,?,?)";
        // Insert Device To Schedule
        for (let i = 0; i < devices.length; i++) {
            await dbManager.executeInsert('spike_schedule_devices', {
                schedule_device_mapping_id: appUtil.generateRandomId('SCHEDULE-DEVICE'),
                schedule_id: schedule_id,
                panel_device_id: devices[i]
            });
            // await dbManager.executeNonQuery(insertDeviceQuery, [appUtil.generateRandomId('SCHEDULE-DEVICE'),schedule_id,devices[i]]);
        }
    }

    // Update Schedule in Schedule Manager Respectively
    const currentSchedule = await this.get(schedule_id);
    scheduleManager.addOrUpdateScheduledJob({
        schedule_id: currentSchedule.schedule_id,
        schedule_type: currentSchedule.schedule_type,
        days: currentSchedule.schedule_days.split(","),
        on_time: currentSchedule.on_time,
        off_time: currentSchedule.off_time
    });

    return true;
}

/**
 * Delete Devices of schedule
 * @param schedule_id
 */
schedule.prototype.deleteDevices = async function (schedule_id) {
    const query = 'DELETE FROM spike_schedule_devices where schedule_id=?';
    const params = [schedule_id];
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete Schedule
 * @param schedule_id
 */
schedule.prototype.delete = async function (schedule_id) {
    const query = "DELETE FROM spike_schedules WHERE schedule_id=?";
    const params = [schedule_id];
    scheduleManager.checkIfScheduleExistsandDeactivate(schedule_id);
    return await dbManager.executeNonQuery(query, params);
}

/**
 * Delete Device From Schedule
 * @param {*} schedule_id
 * @param {*} panel_device_id
 */
schedule.prototype.deleteDeviceFromSchedule = async function (schedule_id, panel_device_id) {
    await dbManager.executeNonQuery('delete from spike_schedule_devices where schedule_id=? and panel_device_id=?', [schedule_id, panel_device_id]);
}


/**
 * Delete Empty Schedules
 * @param schedule_id
 */
schedule.prototype.deleteEmptySchedule = async (params, cb) => {
    return await dbManager.executeNonQuery('delete from spike_schedules where (select count(0) from spike_schedule_devices s where s.schedule_id=schedule_id) = 0 ');
}

/**
 * Delete Schedule By Mood Id
 * @param schedule_id
 */
schedule.prototype.deleteByMoodId = async function (schedule_id) {
    const query = "DELETE FROM spike_schedules WHERE mood_id=?";
    const params = [schedule_id];
    scheduleManager.checkIfScheduleExistsandDeactivate(schedule_id);
    return await dbManager.executeNonQuery(query, params);
}


/**
 * Get All Schedules
 */
schedule.prototype.all = async function () {
    const query = "SELECT * FROM spike_schedules";
    return await dbManager.all(query);
}


/**
 * Get By Mood Id
 * @param mood_id
 */
schedule.prototype.getByMoodId = async function (mood_id) {
    const query = "SELECT * FROM spike_schedules where mood_id=?";
    return await dbManager.all(query, [mood_id]);
}


module.exports = new schedule();