const dbManager = require('../util/db-manager');

const home_controller = function () {

};


/**
 * Get Home Controller
 */
home_controller.prototype.get = async function () {
    const query = `SELECT * from mst_home_controller_list`;
    return await dbManager.get(query, []);
}


/**
 * Update Home Controller Data
 * @param home_controller_device_id
 * @param updateQuery 
 */
home_controller.prototype.update = async function (home_controller_device_id, updateQuery) {
    updateQuery.is_sync = 0;

    // Edited by Brijesh Parmar
    return await dbManager.executeUpdate('mst_home_controller_list', updateQuery, {'home_controller_device_id' : home_controller_device_id});

    // let query = "UPDATE mst_home_controller_list SET ";
    // query += dbManager.generateUpdateQuery(updateQuery) + ' ';
    // query += "WHERE home_controller_device_id = ?";
    // return await dbManager.executeNonQuery(query, [home_controller_device_id]);
}

module.exports = new home_controller();