var getmac = require('getmac');
var exec = require('child_process').exec;
module.exports = function () {

	exports = {};
	exports.deviceID = "";
	exports.deviceIP = "";
	exports.isWindows = process.platform.indexOf('win') === 0;
	exports.isLinux = process.platform.indexOf('linux') === 0;

	exports.init = function (cb) {
		if (exports.isWindows) {
			getmac.getMac(function (error, macAddress) {
				if (error) {
					logger.error("Error while getting MAC address of device. Error: ", error);
					return cb(error, null);
				} else {
					macAddress = macAddress.trim();
					logger.info('[MAC-ADDRESS]',macAddress);
					exports.deviceID = macAddress;
					setTimeout(function () {
						return cb(null, macAddress);
					}, 2000);
				}
			});
		} else if (exports.isLinux) {
			exec('sudo cat /sys/class/net/eth0/address', function (error, macAddress, stderr) {
				macAddress = macAddress.trim();
				logger.info('[MAC-ADDRESS]',macAddress);
				exports.deviceID = macAddress;
				cb(null, macAddress);
			});
		} else {
			cb("Unknown OS", null);
		}
	};
	return exports;
};